from Main.models import Book, Users, PayPal_IPN
from django import forms

#-------------------------------------------------------------------------------
# 20:18:05 Jan 30, 2009 PST - PST timezone support is not included out of the box.
# PAYPAL_DATE_FORMAT = ("%H:%M:%S %b. %d, %Y PST", "%H:%M:%S %b %d, %Y PST",)
# PayPal dates have been spotted in the wild with these formats, beware!
PAYPAL_DATE_FORMAT = ("%H:%M:%S %b. %d, %Y PST",
                      "%H:%M:%S %b. %d, %Y PDT",
                      "%H:%M:%S %b %d, %Y PST",
                      "%H:%M:%S %b %d, %Y PDT",)
#-------------------------------------------------------------------------------

class Publish(forms.ModelForm):
    
    class Meta:
        model = Book
        fields = ('title', 'synopsis', 'rate_id', 'gender_id', 'language_id', 'book_file_epub', 'book_file_pdf', 'cover', 'user', 'sales', 'book')
        
class Signup(forms.ModelForm):
    
    class Meta:
        model = Users
        fields = ('full_name', 'user', 'passwd', 'email', 'is_author', 'sales', 'purchases', 'hashes')

        
class PayPal_IPN_Form(forms.ModelForm):
    """
    Form used to receive and record PayPal IPN notifications and store information about purchase operations.

    """
    cmd = forms.CharField(max_length=18, required=False)
    business = forms.CharField(max_length=127)
    invoice = forms.CharField(max_length=127)
    item_number_1 = forms.CharField(max_length=100, required=False)  # Watch out! This should always be 'non null', but since it is only used in backend it's left as non mandatory to avoid useless overhead (in 'paypal_ipn')
    item_number_2 = forms.CharField(max_length=100, required=False)
    item_number_3 = forms.CharField(max_length=100, required=False)
    item_number_4 = forms.CharField(max_length=100, required=False)
    item_number_5 = forms.CharField(max_length=100, required=False)
    item_number_6 = forms.CharField(max_length=100, required=False)
    item_number_7 = forms.CharField(max_length=100, required=False)
    item_number_8 = forms.CharField(max_length=100, required=False)
    item_number_9 = forms.CharField(max_length=100, required=False)
    item_number_10 = forms.CharField(max_length=100, required=False)
    receiver_email = forms.EmailField(max_length=127)
    residence_country = forms.CharField(required=False,max_length=2)
    test_ipn = forms.BooleanField(required=False)
    txn_id = forms.CharField(max_length=19)
    txn_type = forms.CharField(max_length=128)
    payer_email = forms.CharField(required=False, max_length=127)
    mc_currency = forms.CharField(max_length=32)
    mc_gross = forms.DecimalField(max_digits=64, decimal_places=2)
    num_cart_items = forms.IntegerField()
    payment_date = forms.DateTimeField(input_formats=PAYPAL_DATE_FORMAT)
    payment_status = forms.CharField(max_length=9)
    payment_type = forms.CharField(max_length=7)

    class Meta:
        model = PayPal_IPN
