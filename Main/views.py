# coding: utf-8

from django.shortcuts import render_to_response, redirect, HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from Main.models import *
from Main.forms import *
from Main.extra import video_transcode, img_resize
from Blog.models import *
import random, re, unicodedata, os.path, string, hashlib, smtplib
from random import choice
from os import remove, path, error
from subprocess import call
from urllib import urlencode
from urllib2 import urlopen, Request
from decimal import Decimal, InvalidOperation
from django.template.defaultfilters import register
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import ObjectDoesNotExist
from django.utils.datastructures import MultiValueDictKeyError
from django.core.mail import send_mail
from django.core.servers.basehttp import FileWrapper
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt


#-------------------------------------------------------------------------------
# Filter to allow accessing lists of dicts with dynamic indexes (like in 'book.html', line 158):
@register.filter(name='cut')  # Register to allow using this filter from templates.
def cut(value, arg):
    return value[arg]


# Validate input (title, synopsis, username, quote...):
def validate(string, max_length, mode):
    ok = 0
    if (mode == 0): regexp = u'[\w\s\.,:;áéíóúÁÉÍÓÚàèìòùÀÈÌÒÙâêîôûÂÊÎÔÛÑñü¿\?¡!\-\(\)\r]{1,%d}$' % max_length  # Title, synopsis, full_name...
    elif (mode == 1): regexp = u'^[_a-z0-9-\+]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$'  # E-mail address
    
    if re.match(regexp, string) and urlizer(string) != '': ok = 1
    
    return ok


# Function to turn usernames and book titles into 'URLable' names (No spaces, hyphens...)
def urlizer(string):
    remove = ['.', ',', ':', ';', '?', '!', ' ', '"']
    ascii_string = unicodedata.normalize('NFKD', ' '.join(string.split()).replace(' ', '-')).encode('ascii', 'ignore')
    for i in remove:
        if i in ascii_string:
            ascii_string = ascii_string.replace(i, '')
    
    if string == '': string = '-' # To prevent empty titles due to unexpected user input
    
    return ascii_string


# Function to log in users from everywhere on the website:
# It's not scalable to add the 'login stuff' on every view, it should be placed
# on a 'master' view only. The 'navbar' maybe? (TODO)
def login_user(request):
    username = request.POST['username'] # We'll user email (as entered by the user) as the username
    password = request.POST['password']
    
    try:
        user = Users.objects.get(email=username)
    except ObjectDoesNotExist:
        return 1
    
    # Username corresponding to provided email (Django API compares to 'username' and 'password'):
    username_email = user.user
    user_auth = authenticate(username=username_email, password=password)
    if user_auth is not None:
        if user.account_active: # Account is enabled
            login(request, user_auth)
            # Clear user pending signup/password requests (hashes):
            if (user.hashes):
                user.hashes = ''
                user.save()
            return 0
            # Login OK
        else:
            return 2 # Account is disabled
    else:
        return 1 # Invalid credentials
        
        
# Function to generate random passwords of length 'n':
def password_gen(n):

    return ''.join([choice(string.letters + string.digits) for i in range(n)])


def verify_paypal_ipn(POST):
    # Prepares provided dataset for PayPal validation:
    data = POST.copy()
    data['cmd'] = '_notify-validate'
	
    # Sends the data and request to PayPal
    req = Request(settings.PAYPAL_URL, urlencode(dict([k, v.encode('utf-8')] for k, v in data.items())))
    req.add_header("Content-type", "application/x-www-form-urlencoded")
    # Reads the response back from PayPal
    status = urlopen(req).read()
    # If not verified
    if status != "VERIFIED":
        return False

    # If not the correct receiver ID
    if data["receiver_email"] != settings.PAYPAL_RECEIVER_EMAIL:
        return False

    # If not the correct currency
    if data["mc_currency"] != "EUR":
        return False

    # otherwise...
    return True     


#-------------------------------------------------------------------------------

def main(request):
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in: 
        login_result = login_user(request)
        if login_result == 0:  # Login OK
            return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
        # login_result = 1 and 2 are handled in the 'navbar.html' template
        
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1
    
    
    # Select a random element for the page header:
    header = random.randrange(0, 4)
     
    # Carousel:
    # Get 6 random books:
    books_count = Book.objects.count()
    temp = []
    numbers = []
    item = {'book_active': 0}    
    if books_count >= 6:
        for i in range(0, 6): # 6 books, 2 per carousel.
            number = -1
            book_inactive = 1
            # This will loop infinitely if number os active books is less than 6 (by default, 6 books in carousel) WATCH OUT!
            while number == -1 or number in numbers or item['book_active'] != 1: # Repeated numbers not allowed
                number = random.randrange(0, books_count)
                item = Book.objects.values('title', 'book', 'user', 'synopsis', 'cover', 'book_active')[number]
            numbers.append(number)
            temp.append(item)        
    
        carousel_1 = [temp[0], temp[1]]
        carousel_2 = [temp[2], temp[3]]
        carousel_3 = [temp[4], temp[5]]
    
    
    # Get Best sellers (Top 3):
    best_sellers = Book.objects.values('title', 'book', 'cover', 'book_active', 'synopsis').filter(book_active=1).order_by('-sales', 'title')[:3]
       
    # Get Top 5 writers:
    top5_writers = Users.objects.values('full_name', 'user', 'quote', 'photo').filter(is_author=1).filter(photo__gt='').order_by('-sales', 'full_name')[:5]    
    
    # Get Top 5 readers:
    #top5_readers = Users.objects.values('full_name', 'user', 'quote', 'photo').filter(photo__gt='').order_by('-purchases', 'full_name')[:5] 
    
    # For 'og' meta tags:
    og_url = 'https://' + request.get_host() # 'link' used for 'og' meta tags in HTML.
    
    # Get latest blog article:
    blog_entry = Blog_Entry.objects.all().order_by('-date')[:1]

    return render_to_response('index.html',
                                 locals(), 
                                 context_instance=RequestContext(request),
                             )


def book(request, title):
    try:        
        # Login stuff:
        user_logged_in = request.user.is_authenticated()
        if request.method == 'POST' and not user_logged_in and request.POST.get('username', 0): 
            login_result = login_user(request)
            if login_result == 0:  # Login OK
                return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
            # login_result = 1 and 2 are handled in the 'navbar.html' template
    
        # Shopping cart in navbar:
        if (request.COOKIES.get('cart', 0) and request.COOKIES.get('cart') != '_'): 
            cart = 1
            cart_content = request.COOKIES.get('cart', '').split('_')[1:-1]
        else:
            cart = 0
            cart_content = []     
    
        
        book = Book.objects.get(book=title)
        # Check if book is already in shopping cart
        if cart:
            if book.book in cart_content:
                in_cart = 1
            else: in_cart = 0
        
        
        # Show shopping cart if the user has just clicked on 'Buy' but the cookie's 
        # still not updated:
        if request.POST.get('buy', 0): 
            cart = 1
            in_cart = 1

        
        # Formats:
        formats = []
        if book.book_file_epub: formats.append('EPUB')
        if book.book_file_pdf: formats.append('PDF')
        numformats = len(formats) # for pluralize in template...
        
        
        # Other publications by the the author:
        other_count = Book.objects.values('book').filter(user__exact=book.user).count() - 1  # Limit other books to 5 (not enough space) (TODO)
        
        if other_count:
            other_books = Book.objects.values('title', 'book', 'cover').filter(user__exact=book.user).exclude(book=book.book)
        
        # Is the user connecting to one of his books' page?
        if ((user_logged_in) and request.user.username == book.user.user): is_owner = 1
        else: is_owner = 0
        # Check for new comments before loading them:
        og_image_tag = 'https://' + request.get_host() + '/uploaded/' + book.cover.name
        link = 'https://' + request.get_host() + '/books/' + book.book # 'link' used for email notifications and 'og' meta tags in HTML.
        if request.POST.get('new_comment', 0):
            new = Book_Comment(user=Users.objects.get(user=request.user.username), book=Book.objects.get(book=title), comment=request.POST.get('new_comment'))
            new.save()
            
            # Notify author a comment has been published on his book's frontpage (not if it's his comment):
            if (new.user.user != book.user.user and book.user.wants_mail_notifications):
                send_mail(u'PeopleEbooks.com: Nuevo comentario', u'Hola.\n\n' + new.user.full_name + u' ha publicado un comentario en el muro de \"' + book.title + u'\" en PeopleEbooks.com:\n\n\"' + new.comment + u'\"\n\nHaz click en el siguiente enlace para ir a la página de tu libro y revisar los comentarios:\n\n' + link + u'\n\nUn saludo.\nEl equipo de PeopleEbooks.', 'website@peopleebooks.com', [book.user.email], fail_silently=True) 
                 
            
        # Comments?
        comments = Book_Comment.objects.filter(book=book.book).order_by('id')
        comment_count = comments.count()
        if comment_count:
            comment_text = []
            comment_user = []
            comment_img = []
            comment_link = []
            for i in reversed(range(0, comment_count)):
                comment_user.append(comments[i].user)
                comment_text.append(comments[i].comment)
                comment_img.append(comments[i].user.photo)
                comment_link.append(comments[i].user.user)
        
        
        # Get user's library to check if he's allowed to post comments:
        library = Purchase.objects.values('book').filter(user=request.user.username)
        # Check if user has already purchased the book and generate download hash if he has:
        for i in library:
            user_is_reader = 0
            if book.book == i['book']: 
                user_is_reader = 1
                break
         
        # Send file if download is requested:
        try:
            if request.POST.get('get_epub', 0):  # EPUB
                response = HttpResponse(FileWrapper(book.book_file_epub), content_type='application/epub+zip')
                response['Content-Disposition'] = 'attachment; filename="%s.epub"' % book.book
                return response
            if request.POST.get('get_pdf', 0): # PDF
                response = HttpResponse(FileWrapper(book.book_file_pdf), content_type='application/pdf')
                response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % book.book
                return response                           
        except IOError:
            HttpResponseRedirect('404')        
         
    except ObjectDoesNotExist:
        return redirect('/', permanent=True)
    
    response = render_to_response('book.html',
                                     locals(),
                                     context_instance=RequestContext(request),
                                 )
                                 
    if request.POST.get('buy', 0):  # User has clicked on 'Buy'
        if request.COOKIES.get('cart', 0):  # If cart exists
             if not book.book in request.COOKIES.get('cart'):  # If book not in shopping cart
                # Add to shopping cart:
                updated_cart = request.COOKIES.get('cart') + book.book + u'_'
                response.set_cookie('cart', updated_cart)
        else:  # Cart is empty
            # Add to shopping cart:
            response.set_cookie('cart', u'_' + book.book + u'_')
    
    return response
    

def user(request, username):
    try:
    
        # Login stuff:
        user_logged_in = request.user.is_authenticated()
        if request.method == 'POST' and not user_logged_in: 
            login_result = login_user(request)
            if login_result == 0:  # Login OK
                return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
            # login_result = 1 and 2 are handled in the 'navbar.html' template
    
        # Shopping cart in navbar:
        if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
        else: cart = 1


        # Get the user as provided in URL:
        user = Users.objects.get(user=username)
        
        # Is the user connecting to his own user page?. All logged in users, apart from the owner, can post on any user's page, so:
        if user_logged_in and request.user.username == username: is_owner = 1
        else: is_owner = 0
        
        # Check for the user's photo and video, if both are left, we'll display a message encouraging him for them:
        if (not user.purchases) and (not user.photo) and (not user.video): new_user = 1
        else: new_user = 0
        
        if user.is_author:
            books = Book.objects.values('title', 'book', 'cover').filter(user__exact=user.user)
        
        # Library:
        library = Purchase.objects.values('book').filter(user=user.user)
        library_items = []
        for i in library:
            library_items.append(Book.objects.get(book=i['book']))

        # Check for new comments before loading them:
        og_image_tag = 'https://' + request.get_host() + '/uploaded/' + user.photo.name
        link = 'https://' + request.get_host() + '/people/' + user.user # 'link' used for email notifications and 'og' meta tags in HTML.
        if request.POST.get('new_comment', 0):
            new = User_Comment(user_who=Users.objects.get(user=request.user.username), user_about=Users.objects.get(user=username), comment=request.POST.get('new_comment'))
            new.save()
            
            # Notify user a comment has been published on his frontpage (but not if it's his comment):
            if (new.user_who.user != user.user and user.wants_mail_notifications):
                send_mail(u'PeopleEbooks.com: Nuevo comentario', u'Hola.\n\n' + new.user_who.full_name + u' ha publicado un comentario en tu muro de PeopleEbooks.com:\n\n\"' + new.comment + u'\"\n\nHaz click en el siguiente enlace para ir a tu página y revisar los comentarios:\n\n' + link + u'\n\nUn saludo.\nEl equipo de PeopleEbooks.', 'website@peopleebooks.com', [user.email], fail_silently=True)                        
        
        # Comments?
        comments = User_Comment.objects.filter(user_about=user.user).order_by('id')
        comment_count = comments.count()
        if comment_count:
            comment_text = []
            comment_user = []
            comment_img = []
            comment_link = []
            for i in reversed(range(0, comment_count)):
                comment_user.append(comments[i].user_who)
                comment_text.append(comments[i].comment)
                comment_img.append(comments[i].user_who.photo)
                comment_link.append(comments[i].user_who.user)
    
    
    except ObjectDoesNotExist:        # If user does not exist, redirect to main:
        return redirect('/', permanent=True)
        
    return render_to_response('user.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )                             
                             
def publish(request):
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in: 
        login_result = login_user(request)
        if login_result == 0:  # Login OK
            return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
        # login_result = 1 and 2 are handled in the 'navbar.html' template


    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1

    #Get options for Rate, Gender and Language:
    genders = Gender.objects.values('gender_id', 'gender_es')
    rates = Rate.objects.values('rate_id', 'rate_es')
    languages = Language.objects.values('language_id', 'language_es')
    
    if request.method == 'POST' and request.POST.get('title', 0):  # Form has been submitted
        form = Publish()
        if validate(request.POST['title'], settings.BOOK_TITLE_MAX_LENGTH, 0) and validate(request.POST['synopsis'], 1400, 0):
            tempdata = request.POST.copy()
            tempdata['title'] = ' '.join(tempdata['title'].split())  # Remove all duplicate spaces.
            # Correct case if appropriate:
            if tempdata['title'].isupper() or tempdata['title'].islower():
                tempdata['title'] = tempdata['title'].capitalize()
            if tempdata['synopsis'].isupper() or tempdata['synopsis'].islower():
                tempdata['synopsis'] = tempdata['synopsis'].capitalize()
            
            tempdata['sales'] = 0
            tempdata['book'] = urlizer(tempdata['title'])
            # Get 'user' field from current session:
            tempdata['user'] = request.user.username
            
            form = Publish(tempdata, request.FILES)  # Bound form to the POST(modified) data and files
            # Optimize 'presaving checks' due to excessive dB operations (TODO):
            # Check to ensure there are no other books already in dB with the same title ('book', with case insensitive search!):
            if form.is_valid() and (Book.objects.filter(book__iexact=tempdata['book']).count() == 0):
                if form.cleaned_data['book_file_epub'] or form.cleaned_data['book_file_pdf']:  # Check the user entered at least one file (ePub or PDF)
                    form.save()
                    # Book cover resizing:
                    path = Book.objects.get(book=form.cleaned_data['book']).cover.name
                    fname, dot, extension = path.rpartition('.')
                    status = img_resize(settings.MEDIA_ROOT + path, 0)
                    # Once published, change user's 'is_author' to 1:
                    user = Users.objects.get(user=tempdata['user'])
                    user.is_author = 1
                    user.save()
                
                    return HttpResponseRedirect('/books/' + tempdata['book'])     #Change for a page confirming correct publication!
    else:
        form = Publish()   # Unbound form
    

    book_title_max_length = settings.BOOK_TITLE_MAX_LENGTH

    return render_to_response('publish.html',
                                     locals(),
                                     context_instance=RequestContext(request),
                             )



# 'form' is used to store user data in the database in 'users' table.
# Fields 'user', 'email' and 'password' are saved also with Django auth API
# and 'password' field is not saved in 'users' table for security (unencrypted).
# This is redundant and should be optimized to use only Django auth API (TODO)
#
def signup(request):
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1
    
    
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in: 
        try:
            login_result = login_user(request)
            if login_result == 0:  # Login OK
                return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
            # login_result = 1 and 2 are handled in the 'navbar.html' template
            
        except MultiValueDictKeyError:  # POST is not for login but for signup:
            form = Signup()
            if validate(request.POST['full_name'], settings.USERNAME_MAX_LENGTH, 0) and validate(request.POST['email'].lower(), 75, 1):
                tempdata = request.POST.copy()
                tempdata['full_name'] = ' '.join(tempdata['full_name'].split())  # Remove all duplicate spaces.
                # Correct case if appropriate:
                if tempdata['full_name'].isupper() or tempdata['full_name'].islower():
                    tempdata['full_name'] = tempdata['full_name'].title()    
                
                tempdata['email'] = tempdata['email'].lower()  # Lowercase for easier 'email already registered' detection.
                tempdata['sales'] = 0
                tempdata['purchases'] = 0
                tempdata['is_author'] = 0
                tempdata['user'] = urlizer(tempdata['full_name'])
                # Leave 'passwd' blank (6 spaces) in table 'users' for security (else unencrypted)
                tempdata['passwd'] = '      '
                idhash = hashlib.sha256(request.COOKIES.get('csrftoken') + password_gen(12)).hexdigest()
                tempdata['hashes'] = idhash
                form = Signup(tempdata) 
                if form.is_valid() and not Users.objects.filter(user__iexact=tempdata['user']).count():  # Form valid and user does not exist (already)
                    # 'user_auth' is the user instance for the Django auth API:
                    user_auth = User.objects.create_user(tempdata['user'], request.POST['email'], request.POST['passwd'])
                    form.save()
                    
                    try:
                        # Account activation email:
                        activation_link = 'https://' + request.get_host() + '/activation/' + idhash
                        send_mail(u'PeopleEbooks.com: Alta de usuario', u'¡Bienvenido/a a PeopleEbooks!\n\nHas recibido este correo porque recientemente te has dado de alta en el portal https://www.peopleebooks.com.\nHaz click en el siguiente enlace para activar tu cuenta y acceder a todos los contenidos del portal:\n\n' + activation_link + u'\nEl enlace sólo es válido para un único uso.\n\nUn saludo.\nEl equipo de PeopleEbooks.\n\n\nSi no has solicitado el alta en PeopleEbooks, te rogamos disculpes las molestias y omitas este correo. Si el problema persistiera, contáctanos en contact@peopleebooks.com\n\nIf you have not signed up to PeopleEbooks, please omit this email and accept our apologies. If you keep receiving this email, please contact us at contact@peopleebooks.com', 'website@peopleebooks.com', [form.cleaned_data['email']], fail_silently=False)
                
                        link_sent = 1
                    except smtplib.SMTPException:
                        link_sent = 0
                    
                    response = HttpResponseRedirect('/pending/'+ form.cleaned_data['user'])
                    response.set_cookie('link_sent', value=link_sent, httponly=True)
                    return response
                    #return HttpResponseRedirect('/pending/' + tempdata['hashes'])  # Tells the user to check for activation email
                else:
                    if Users.objects.filter(user__iexact=tempdata['user']).count(): # User already exists
                        return HttpResponseRedirect('signup-error/user-already-exists')
                    elif Users.objects.filter(email__iexact=request.POST.get('email')).count():
                        if form.errors['email'][0] == 'Ya existe Users con este Email.':
                            return HttpResponseRedirect('signup-error/email-already-registered')
                
    else:
        form = Signup()
    
    
    username_max_length = settings.USERNAME_MAX_LENGTH # For use in the template's form

    return render_to_response('signup.html',
                                     locals(),
                                     context_instance=RequestContext(request),
                             )
                             
    if request.method == 'POST' and request.POST.get('searchfor', 0):
        response.set_cookie('searchfor', request.POST['searchfor'])
        response.set_cookie('searchbox', request.POST['searchbox'])
        
    return response

def logout_view(request):
    if request.user.is_authenticated():
        logout(request)
    
    return HttpResponseRedirect(request.META['HTTP_REFERER'])
        

def bookstore(request):    
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in: 
        login_result = login_user(request)
        if login_result == 0:  # Login OK
            return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
        # login_result = 1 and 2 are handled in the 'navbar.html' template
    
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1
    
    
    catalog_page = Book.objects.values('book', 'title', 'cover', 'synopsis', 'book_active').filter(book_active=1).order_by('book')
    
    # Paging:
    paginator = Paginator(catalog_page, 10)
    page = request.GET.get('page')
    try:
        results_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an int, deliver first page:
        results_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver last page:
        results_page = paginator.page(paginator.num_pages)
    # Custom paging:
    if (paginator.num_pages - (results_page.number + 5) > 0): end = results_page.number + 5
    else: end = paginator.num_pages
    if (results_page.number > 5): begin = results_page.number - 5
    else: begin = 1
    pages_list = range(begin, end + 1)
    
    
    return render_to_response('bookstore.html',
                                     locals(),
                                     context_instance=RequestContext(request),
                             )


def search_results(request):        
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1
        
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST': 
        if (request.POST.get('username', 0) and not user_logged_in):
            login_result = login_user(request)
            if login_result == 0:  # Login OK
                return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
            # login_result = 1 and 2 are handled in the 'navbar.html' template
            
    
        if request.POST.get('searchfor', 0):
            # Searched for book
            search_for = request.POST['searchfor']  # 'search_for' required by the template
            if (search_for == 'book'):
                # We query for the user's input minus the last two chars, so we can span plurals and gender cases:
                if len(request.POST.get('searchbox', 0)) > 2:
                    results = Book.objects.values('book', 'title', 'cover', 'synopsis').filter(book_active=1).filter(book__icontains=urlizer(unicode(request.POST['searchbox'][0:len(request.POST['searchbox'])-2]))).order_by('book')
                else:
                    results = Book.objects.values('book', 'title', 'cover', 'synopsis').filter(book_active=1).order_by('book')
            # Searched for user:
            else:
                if len(request.POST['searchbox']) > 2:
                    results = Users.objects.values('user', 'full_name', 'photo', 'quote').filter(user__icontains=urlizer(unicode(request.POST['searchbox'][0:len(request.POST['searchbox'])-2]))).order_by('user')
                else:
                    results = Users.objects.values('user', 'full_name', 'photo', 'quote').order_by('user')
    if request.method == 'GET' or not request.POST.get('searchfor', 0):
        if (request.COOKIES.get('searchfor') == 'book'):
            if len(request.COOKIES.get('searchbox')) > 2:
                results = Book.objects.values('book', 'title', 'cover', 'synopsis').filter(book_active=1).filter(book__icontains=urlizer(unicode(request.COOKIES.get('searchbox')))).order_by('book')
            else:
                results = Book.objects.values('book', 'title', 'cover', 'synopsis').filter(book_active=1).order_by('book')
        else:
            if len(request.COOKIES.get('searchbox')) > 2:
                results = Users.objects.values('user', 'full_name', 'photo', 'quote').filter(user__icontains=urlizer(unicode(request.COOKIES.get('searchbox')))).order_by('user')
            else:
                results = Users.objects.values('user', 'full_name', 'photo', 'quote').order_by('user')
        search_for = request.COOKIES['searchfor']   # 'search_for' required by the template
    
    num_results = results.count()
    
    # Paging:  
    paginator = Paginator(results, 10)
    page = request.GET.get('page')
    try:
        results_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an int, deliver first page:
        results_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver last page:
        results_page = paginator.page(paginator.num_pages)
    # Custom paging:
    if (paginator.num_pages - (results_page.number + 5) > 0): end = results_page.number + 5
    else: end = paginator.num_pages
    if (results_page.number > 5): begin = results_page.number - 5
    else: begin = 1
    pages_list = range(begin, end + 1)
    
    response = render_to_response('search.html',
                                     locals(),
                                     context_instance=RequestContext(request),
                                 )
                                 
    if request.method == 'POST' and request.POST.get('searchfor', 0):
        response.set_cookie('searchfor', urlizer(request.POST['searchfor']))
        if len(request.POST['searchbox']): response.set_cookie('searchbox', urlizer(request.POST['searchbox']))
        else: response.set_cookie('searchbox', u'')
        
    return response
    

def inactive_account(request):
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST': 
        if (request.POST.get('username', 0) and not user_logged_in):
            login_result = login_user(request)
            if login_result == 0:  # Login OK
                return HttpResponseRedirect('/people/' + request.user.username)   # Page where the user logged in from
            # login_result = 1 and 2 are handled in the 'navbar.html' template
    
    return render_to_response('inactive_account.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )


def activate_account(request, hashes):
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST': 
        if (request.POST.get('username', 0) and not user_logged_in):
            login_result = login_user(request)
            if login_result == 0:  # Login OK
                return HttpResponseRedirect('/people/' + request.user.username)   # Page where the user logged in from
            # login_result = 1 and 2 are handled in the 'navbar.html' template
    
    activated_ok = nonexistent_user = 0
    try:
        account = Users.objects.get(hashes=hashes)
        if (not account.account_active):
            account.account_active = 1
            account.hashes = ''
            account.save()
            activated_ok = 1
    except ObjectDoesNotExist:
        nonexistent_user = 1
    
    return render_to_response('activation.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )
    
def pending(request, user):
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in: 
        login_result = login_user(request)
        if login_result == 0:  # Login OK
            return HttpResponseRedirect('/people/' + request.user.username)   # Page where the user logged in from
        # login_result = 1 and 2 are handled in the 'navbar.html' template

    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1
    
    nonexistent_account = 0
    try:
        user_email = Users.objects.values('email').filter(user__exact=user)[0]['email']
    except IndexError:
        nonexistent_account = 1
    
    link_sent = request.COOKIES.get('link_sent')
    
    return render_to_response('pending.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )


def password_change_request(request):
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in: 
        try:
            login_result = login_user(request)
            if login_result == 0:  # Login OK
                return HttpResponseRedirect('/')
            # login_result = 1 and 2 are handled in the 'navbar.html' template
            
        except MultiValueDictKeyError:  # POST is not for login but for password request:
            return HttpResponseRedirect('/password-pending')
    
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1
    
    
    return render_to_response('password_change_request.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )


def password_pending(request):
    link_sent = nonexistent_user = 0
    
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1
    
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in: 
        try:
            login_result = login_user(request)
            if login_result == 0:  # Login OK
                return HttpResponseRedirect('/')
            # login_result = 1 and 2 are handled in the 'navbar.html' template
            
        except MultiValueDictKeyError:  # POST is not for login but for password request:
            try: # User exists:
                email = Users.objects.values('email').filter(email__iexact=request.POST.get('email', 0))[0]['email']
                user = Users.objects.get(email=email)
                if user.hashes: # User already has a password recover request pending.
                    request_pending = 1
                else:
                    request_pending = 0
                    # 'idhash' is the value in the link (sessionid + random 12 chars long string)
                    idhash = hashlib.sha256(request.COOKIES.get('csrftoken') + password_gen(12)).hexdigest()
                    user.hashes = idhash
                    user.save()
                                           
                    try:
                        # Send password change link on an email:
                        activation_link = 'https://' + request.get_host() + '/password-change/' + idhash
                        send_mail(u'PeopleEbooks.com: Cambio de contraseña', u'Hola\n\nHas recibido este correo porque recientemente has solicitado recuperar tu contraseña de acceso al portal https://www.peopleebooks.com.\nHaz click en el siguiente enlace para cambiar tu contraseña y recuperar el acceso a tu cuenta:\n\n' + activation_link + u'\nEl enlace sólo es válido para un único uso.\n\nUn saludo.\nEl equipo de PeopleEbooks.', 'website@peopleebooks.com', [email], fail_silently=False)
                                    
                        link_sent = 1
                    except smtplib.SMTPException:
                        link_sent = 0
            except IndexError: # User does not exist
                nonexistent_user = 1
               
               
        return render_to_response('password_pending.html',
                                     locals(),
                                     context_instance=RequestContext(request),
                                 )


def password_change(request, hashes):
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1
    
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in: 
        login_result = login_user(request)
        if login_result == 0:  # Login OK
            return HttpResponseRedirect('/')   # Page where the user logged in from
        # login_result = 1 and 2 are handled in the 'navbar.html' template               
    
    else:
        # Verify hashes (so the user did request password change):
        try:
            user = Users.objects.get(hashes=hashes)
            user_ok = 1
            #try:    # User has already 'POSTed' his new password
            new_passwd = request.POST.get('passwd', 0)
                
        except ObjectDoesNotExist:
            user_ok = 0
               
        return render_to_response('password_change.html',
                                     locals(),
                                     context_instance=RequestContext(request),
                                 )


def password_change_confirmed(request, hashes):
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1
    
    
    # 'Hashes' should be stored in 'auth_user' table, not in 'users', this leads to redundant operations: (TODO)
    if request.POST.get('passwd', 0):
        try:
            # Find 'auth_user' user based on email from table 'users':
            user = Users.objects.get(hashes=hashes)
            user_auth = User.objects.get(email=user.email)
            # Update password:
            user_auth.set_password(request.POST.get('passwd'))
            user_auth.save()
            # Clear user 'hashes' in table 'users':
            user.hashes = ''
            user.save()
            user_ok = 1
        except ObjectDoesNotExist:
            user_ok = 0
    else:
        # Login stuff:
        user_logged_in = request.user.is_authenticated()
        if request.method == 'POST' and not user_logged_in: 
            login_result = login_user(request)
            if login_result == 0:  # Login OK
                return HttpResponseRedirect('/')   # Page where the user logged in from
            # login_result = 1 and 2 are handled in the 'navbar.html' template
        

    return render_to_response('password_change_confirmed.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )


def user_home(request, username):
    # User must be logged in to see this page:
    user_logged_in = request.user.is_authenticated()
    if user_logged_in and request.user.username == username:
        # Shopping cart in navbar:
        if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
        else: cart = 1
    
        # Login stuff:
        user_logged_in = request.user.is_authenticated()
        if request.method == 'POST' and not user_logged_in: 
            login_result = login_user(request)
            if login_result == 0:  # Login OK
                return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
            # login_result = 1 and 2 are handled in the 'navbar.html' template
    
        user = Users.objects.get(user=username)
        country = []
        country_query = Country.objects.values('country_es', 'country_id').order_by('country_es')
        for i in range(0, len(country_query)):
            country.append(country_query[i]['country_es'])
        
# ---- USER PROFILE PAGE: ------------------------------------------------------        
        if request.method == 'POST' or request.method == 'FILES':
            if request.FILES.get('image', 0):
                if user.photo: del_photo = unicode(user.photo) # If user already had a photo, delete the existing one (when user.save())
                else: del_photo = ''
                user.photo = request.FILES.get('image')
            else: del_photo = 0
                            
            
            # PERSONAL DATA FORM:               
            # Email change, if new email is not already registered:            
            if request.POST.get('email', 0) and not Users.objects.filter(email__iexact=request.POST.get('email')).count():       
                idhash = hashlib.sha256(request.COOKIES.get('csrftoken') + password_gen(12)).hexdigest() + '_' + request.POST.get('email')
                user.hashes = idhash
                try:
                    # Send password change link on an email:
                    activation_link = 'https://' + request.get_host() + '/email-change/' + idhash
                    send_mail(u'PeopleEbooks.com: Cambio de correo', u'Hola\n\nEste correo es para confirmar el cambio de email que has solicitado en PeopleEbooks.com.\nHaz click en el siguiente enlace para activar tu nueva dirección de correo:\n\n' + activation_link + u'\n\nUn saludo.\nEl equipo de PeopleEbooks.\n\n\nSi no has solicitado nada en PeopleEbooks, te rogamos disculpes las molestias y omitas este correo. Si el problema persistiera, contáctanos en contact@peopleebooks.com\n\nIf you have not requested anything at PeopleEbooks, please omit this email and accept our apologies. If you keep receiving this email, please contact us at contact@peopleebooks.com', 'website@peopleebooks.com', [request.POST.get('email')], fail_silently=False)            
                    email_changed = 1  # Used in template to display a "Check your email for activation link..." message.
                except smtplib.SMTPException:
                    email_changed = 0      # Show error message?? (TODO)
                
            # Country:
            if request.POST.get('country', 0): 
                user.country_id = int(Country.objects.values('country_id').filter(country_es__iexact=request.POST.get('country'))[0]['country_id'])
            
            # Password change:
            if request.POST.get('passwd_confirm', 0):
                auth_user = User.objects.get(username__exact=request.user.username)
                auth_user.set_password(request.POST.get('passwd_confirm'))
                auth_user.save()
                passwd_changed = True
            
            # Video:
            if request.FILES.get('video_file', 0):            
                if user.video: del_video = unicode(user.video) # If user already had a video, delete the existing one (when user.save())
                else: del_video = 0
                user.video = request.FILES.get('video_file')
            else:
                del_video = 0

                
            # SOCIAL AND WEBSITE FORM:
            if request.POST.get('twitter', 0):
                if len(request.POST.get('twitter')) < 5: user.twitter = ''
                else: # If link does not begin with 'http', add it (to prevent not using absolute path on web pages)
                    if request.POST.get('twitter').startswith('http'):
                        user.twitter = request.POST.get('twitter')
                    else: user.twitter = 'http://' + request.POST.get('twitter') 
            if request.POST.get('facebook', 0):
                if len(request.POST.get('facebook')) < 5: user.facebook = ''
                else: 
                    if request.POST.get('facebook').startswith('http'):
                        user.facebook = request.POST.get('facebook')
                    else: user.facebook = 'http://' + request.POST.get('facebook')
            if request.POST.get('website', 0):
                if len(request.POST.get('website')) < 5: user.website = ''
                else:
                    if request.POST.get('website').startswith('http'):
                        user.website = request.POST.get('website')
                    else: user.website = 'http://' + request.POST.get('website')
            
            # QUOTE AND PRESENTATION FORM:
            if request.POST.get('quote', 0) and validate(request.POST.get('quote'), 300, 0):
                if len(request.POST.get('quote')) < 5: user.quote = ''
                else: 
                    if request.POST.get('quote').isupper() or request.POST.get('quote').islower():
                        user.quote = request.POST.get('quote').capitalize()
                    else:
                        user.quote = request.POST.get('quote') 
                
            if request.POST.get('presentation', 0) and validate(request.POST.get('presentation'), 1400, 0):
                if len(request.POST.get('presentation')) < 5: user.presentation = ''
                else: 
                    if request.POST.get('presentation').isupper() or request.POST.get('presentation').islower():
                        user.presentation = request.POST.get('presentation').capitalize()
                    else:
                        user.presentation = request.POST.get('presentation') 
            

            if request.POST.get('wants_mail_notifications', 0):
                if request.POST.get('wants_mail_notifications') == u'on':
                    user.wants_mail_notifications = 1
                else:
                    user.wants_mail_notifications = 0

            try: 
                if del_photo:
                    os.remove(settings.MEDIA_ROOT + del_photo)
                if del_video:
                    os.remove(settings.MEDIA_ROOT + del_video)
            except os.error:
                return HttpResponseRedirect('/home/' + request.user.username + '/#library')
                
            user.save()
                    
            if request.FILES.get('image', 0) or request.FILES.get('video_file', 0):
                # User photo resizing:
                if request.FILES.get('image', 0):
                    fname, dot, extension = request.FILES.get('image').name.rpartition('.')
                    status = img_resize(settings.MEDIA_ROOT + user.photo.name, 1)
                # Video transcoding:
                if request.FILES.get('video_file', 0):
                    # Pass filepath as the user uploaded it (not neccessarily with 'webm' extension) so no dB query is required
                    fname, dot, extension = request.FILES.get('video_file').name.rpartition('.')
                    status = video_transcode(settings.MEDIA_ROOT + user.video.name)
                    if status != 0:
                        user = Users.objects.get(user=username)
                        # Remove video from filesystem:
                        try:
                            call(["rm", settings.MEDIA_ROOT + user.video.name[:-1]])
                        except os.error:
                            pass # Mail notification?
                        user.video.name = ''
                    
                user.save()

# ---- USER LIBRARY PAGE: ------------------------------------------------------
        user_library = []
        books = Purchase.objects.values('book').filter(user=request.user.username)
        for i in books:
            user_library.append(Book.objects.get(book=i['book']))
    
        # Paging:
        '''
        paginator = Paginator(user_library, 10)
        page = request.GET.get('page')
        try:
            results_page = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an int, deliver first page:
            results_page = paginator.page(1)
        except EmptyPage:
            # If page is out of range, deliver last page:
            results_page = paginator.page(paginator.num_pages)
        # Custom paging:
        if (paginator.num_pages - (results_page.number + 5) > 0): end = results_page.number + 5
        else: end = paginator.num_pages
        if (results_page.number > 5 and (results_page.number + 5) - paginator.num_pages >= 0): begin = results_page.number - 5
        else: begin = 1
        pages_list = range(begin, end + 1)
        '''
            
        # Generate download links:
        try:
            if request.POST.get('ebook_epub', 0):  # EPUB
                ebook = Book.objects.get(book=request.POST.get('ebook_epub'))
                response = HttpResponse(FileWrapper(ebook.book_file_epub), content_type='application/epub+zip')
                response['Content-Disposition'] = 'attachment; filename="%s.epub"' % ebook.book
         
                return response
            
            if request.POST.get('ebook_pdf', 0):  # PDF
                ebook = Book.objects.get(book=request.POST.get('ebook_pdf'))
                response = HttpResponse(FileWrapper(ebook.book_file_pdf), content_type='application/pdf')
                response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % ebook.book

                return response
        except IOError:
            HttpResponseRedirect('404')
            
            
# ---- USER'S PUBLICATIONS PAGE: ------------------------------------------------------
        if user.is_author:
            user_publications = []
            books = Book.objects.values('book', 'sales').filter(user=request.user.username)
            for i in books:
                user_publications.append(Book.objects.get(book=i['book']))

            # Paging:
            '''
            paginator = Paginator(user_library, 10)
            page = request.GET.get('page')
            try:
                results_page = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an int, deliver first page:
                results_page = paginator.page(1)
            except EmptyPage:
                # If page is out of range, deliver last page:
                results_page = paginator.page(paginator.num_pages)
            # Custom paging:
            if (paginator.num_pages - (results_page.number + 5) > 0): end = results_page.number + 5
            else: end = paginator.num_pages
            if (results_page.number > 5 and (results_page.number + 5) - paginator.num_pages >= 0): begin = results_page.number - 5
            else: begin = 1
            pages_list = range(begin, end + 1)
            '''

        
            # Generate download links:
            try:
                if request.POST.get('ebook_epub', 0):  # EPUB
                    ebook = Book.objects.get(book=request.POST.get('ebook_epub'))
                    response = HttpResponse(FileWrapper(ebook.book_file_epub), content_type='application/epub+zip')
                    response['Content-Disposition'] = 'attachment; filename="%s.epub"' % ebook.book
                    return response

                if request.POST.get('ebook_pdf', 0):  # PDF
                    ebook = Book.objects.get(book=request.POST.get('ebook_pdf'))
                    response = HttpResponse(FileWrapper(ebook.book_file_pdf), content_type='application/pdf')
                    response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % ebook.book
                    return response
            except IOError:
                HttpResponseRedirect('404')
        
# ---- USER'S SALES PAGE: -------------------------------------------------------------
            withdrawals = Withdrawal.objects.filter(user=username).order_by('-operation_id')
            pending = 0
            for i in withdrawals:
                if i.hashes:
                    pending = 1
                    break
            if request.method == 'POST' and request.POST.get('amount', 0):                
                # Check for pending withdrawal requests:
                if Decimal(request.POST.get('amount')) >= 10 and Decimal(request.POST.get('amount')) <= Decimal(user.current_balance):
                    if not pending:
                        operation = Withdrawal(user=user, amount=request.POST.get('amount'), payment_account=request.POST.get('payment_account'))
                        operation.hashes = hashlib.sha256(request.COOKIES.get('csrftoken') + password_gen(12)).hexdigest()
                        operation.save()
                    
                        confirmation_link = 'https://' + request.get_host() + '/withdrawal/' + operation.hashes
                        # Send email notifying us a user has requested a transfer:            
                        try:
                            send_mail(u'PeopleEbooks.com: Solicitud de transferencia', u'Hola.' + u'\n\nUn usuario de PeopleEbooks ha solicitado una retirada de saldo:\n\nUsuario: ' + user.full_name + '\nE-mail: ' + user.email + '\nImporte: ' + request.POST.get('amount') + u'€' + '\nCuenta PayPal: ' + request.POST.get('payment_account') + '\nObservaciones: ' + request.POST.get('comments') + u'\n\nPulsa el siguiente enlace para confirmar la ejecución de la retirada de saldo:\n\n' + confirmation_link + u'\n\nUn saludo.\n', 'website@peopleebooks.com', ['paypal@peopleebooks.com'], fail_silently=False)
                                                                    
                            return HttpResponseRedirect('/home/' + user.user + '/#sales')
                    
                            transfer_request_mail_error = 0
                               
                        except smtplib.SMTPException:
                            transfer_request_mail_error = 1
                    else:
                        HttpResponseRedirect('/home/' + request.user.username + '/#sales')
                           
        
        return render_to_response('user_home.html',
                                     locals(),
                                     context_instance=RequestContext(request),
                                 )
        
    else:
        return HttpResponseRedirect('/')



def sales_report(request, book_title):
    user_logged_in = 0
    book = Book.objects.get(book=book_title)
    if request.user.is_authenticated() and request.user.username == book.user.user:
        user_logged_in = 1 
        if request.method == 'POST' and not user_logged_in and request.POST.get('username', 0): 
            login_result = login_user(request)
            if login_result == 0:  # Login OK
                return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
            # login_result = 1 and 2 are handled in the 'navbar.html' template
    
        sales = Purchase.objects.filter(book=book_title).order_by('-operation_date')
        profits = Decimal('0')
        profit_list = []
        for i in sales:
            if i.value < settings.BOOK_COMMISSION_BEGIN:
                profits += Decimal('0.50')
                profit_list.append(Decimal('0.50'))
            else:
                profits += i.value * (Decimal('1') - settings.BOOK_PRICE_COMMISSION)
                profit_list.append(i.value * (Decimal('1') - settings.BOOK_PRICE_COMMISSION))
        
        return render_to_response('sales_report.html',
                                     locals(),
                                     context_instance=RequestContext(request),
                                 )
    
    else:
        return HttpResponseRedirect('/')


    



def withdrawal(request, hashes):    
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in: 
        login_result = login_user(request)
        if login_result == 0:  # Login OK
            return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
        # login_result = 1 and 2 are handled in the 'navbar.html' template
    
    
    try:
        operation = Withdrawal.objects.get(hashes=hashes)
        user = Users.objects.get(user=operation.user.user)
        exists = True

        user.current_balance = user.current_balance - operation.amount
        user.money_withdrawn = user.money_withdrawn + operation.amount
        operation.hashes = ''
        
        user.save()
        operation.save()
        
    except ObjectDoesNotExist:
        exists = False


    return render_to_response('withdrawal_completed.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )


def shopping_cart(request):
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1
    
    
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in and request.POST.get('username', 0): 
        login_result = login_user(request)
        if login_result == 0:  # Login OK
            return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
        # login_result = 1 and 2 are handled in the 'navbar.html' template
    
 
    # Remove books from cart (if requested) before redisplaying the cart:
    def remove_from_cart(book):
        if book == request.POST.get('remove_book', 0):
            return False
        else:
            return True
    
    try:
        cart = request.COOKIES.get('cart', '').split('_')[1:-1]
    except AttributeError:  # Raises if shopping cart is empty (COOKIE does not exist, its a None type)
        cart = ''
    
    if request.method == 'POST' and request.POST.get('remove_book', 0):
        cart = filter(remove_from_cart, cart)

    books = []
    for i in cart:
        books.append(Book.objects.get(book=i))
    
    num_books = len(cart)
    total_price = 0
    for i in books:
        total_price += i.price

    
    response = render_to_response('cart.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )
    # Remove from cart:                         
    if request.method == 'POST' and request.POST.get('remove_book', 0):
        updated_cart = request.COOKIES.get('cart').replace('_' + request.POST.get('remove_book'), '')
        response.set_cookie('cart', updated_cart)     

    return response
    


def checkout(request):
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1
    
    
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in and request.POST.get('username', 0): 
        login_result = login_user(request)
        if login_result == 0:  # Login OK
            return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
        # login_result = 1 and 2 are handled in the 'navbar.html' template
    
    try:
        cart = request.COOKIES.get('cart').split('_')[1:-1]
    except AttributeError:
        cart = ''
        return HttpResponseRedirect('/cart')

    # Collect user's library to check for 'duplicate purchases'
    if user_logged_in:
        user_library = Purchase.objects.values('book').filter(user=request.user.username)
    
    books = []
    if user_logged_in:
        for i in cart:
            if {'book': i} not in user_library[0:len(user_library)]: books.append(Book.objects.get(book=i)) 
    else:
        for i in cart:
            books.append(Book.objects.get(book=i))

    num_books = len(books)
    total_price = 0
    for i in books:
        total_price += i.price
    
    
    # Create a temp IPN object in dB to prevent duplicate invoice ids. This 
    # object will be updated with the full PayPal info in 'paypal_ipn'
    
    book_ids_10 = []
    for i in range(settings.CART_MAX_SIZE):
        if i < len(books): book_ids_10.append(books[i].book)
        else: book_ids_10.append('')
        
    
    payment = PayPal_IPN.objects.create(business=settings.PAYPAL_RECEIVER_EMAIL,
                        receiver_email = settings.PAYPAL_RECEIVER_EMAIL,
                        #receiver_email = 
                        txn_id = 0,
                        txn_type = 'temp',
                        item_number_1 = book_ids_10[0],
                        item_number_2 = book_ids_10[1],
                        item_number_3 = book_ids_10[2],
                        item_number_4 = book_ids_10[3],
                        item_number_5 = book_ids_10[4],
                        item_number_6 = book_ids_10[5],
                        item_number_7 = book_ids_10[6],
                        item_number_8 = book_ids_10[7],
                        item_number_9 = book_ids_10[8],
                        item_number_10 = book_ids_10[9],
                        mc_currency = 'EUR',
                        mc_gross = total_price,
                        payment_date = '2000-01-01 00:00:01',
                        payment_status = 'temp',
                        payment_type = 'temp')
    invoice = payment.invoice
    # Add books' 'book_id' to payment object for further (in 'checkout_complete') checking:
    
    
    
    # PayPal form constants:
    paypal_url = settings.PAYPAL_URL
    account = settings.PAYPAL_RECEIVER_EMAIL
    notify_url = settings.NOTIFY_URL
    return_url = settings.RETURN_URL
    cancel_url = settings.CANCEL_URL
    
    response = render_to_response('checkout.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )
    response.set_cookie('invoice', invoice)
    return response   

#-------------------------------------------------------------------------------
# PayPal IPN processing:
#-------------------------------------------------------------------------------
@csrf_exempt
def paypal_ipn(request):
    if request.method == 'POST':
        if verify_paypal_ipn(request.POST):  # Check if message came from PayPal
            form = PayPal_IPN_Form(request.POST)
            try:
                if form.is_valid():
                    # DEBUG MAIL:
                    send_mail(u'PeopleEbooks.com: DEBUG: Mensaje de PayPal', u'Hola.\n\n' + u'Se ha recibido el siguiente mensaje IPN de PayPal:\n\nTipo: ' + form.cleaned_data['txn_type'] + '\nID: ' + form.cleaned_data['txn_id'] + '\nFecha: ' + unicode(form.cleaned_data['payment_date']) + u'\nPaís: ' + form.cleaned_data['residence_country'] + u'\nE-mail: ' + form.cleaned_data['payer_email'] + u'\n\nVERIFY_OK\nUn saludo.\n', 'website@peopleebooks.com', ['admin@peopleebooks.com'], fail_silently=False)
                
                    if PayPal_IPN.objects.filter(txn_id=form.cleaned_data['txn_id']): # Duplicate IPN transaction
                        return HttpResponse("Error: Duplicate transaction")
                    else: # Not a duplicate IPN transaction
                        if form.cleaned_data['txn_type'] == 'cart': # Update corresponding IPN object in dB with full info
                            payment = PayPal_IPN.objects.get(invoice=form.cleaned_data['invoice'])
                            payment.business = form.cleaned_data['business']
                            payment.receiver_email = form.cleaned_data['receiver_email']
                            payment.txn_id = form.cleaned_data['txn_id']
                            payment.txn_type = form.cleaned_data['txn_type']
                            payment.mc_currency = form.cleaned_data['mc_currency']
                            payment.mc_gross = form.cleaned_data['mc_gross']
                            payment.payment_date = form.cleaned_data['payment_date']
                            payment.payment_status = form.cleaned_data['payment_status']
                            payment.payment_type = form.cleaned_data['payment_type']
                            payment.cmd = form.cleaned_data['cmd']
                            payment.residence_country = form.cleaned_data['residence_country']
                            payment.test_ipn = form.cleaned_data['test_ipn']
                            payment.payer_email = form.cleaned_data['payer_email']
                            payment.num_cart_items = form.cleaned_data['num_cart_items']
                        
                            payment.save()
                            return HttpResponse("Okay")
                        else: # Not a 'cart checkout' (purchase) operation, so further analysis is required.
                            send_mail(u'PeopleEbooks.com: Mensaje de PayPal', u'Hola.\n\n' + u'Se ha recibido el siguiente mensaje IPN de PayPal:\n\nTipo: ' + form.cleaned_data['txn_type'] + '\nID: ' + form.cleaned_data['txn_id'] + '\nFecha: ' + unicode(form.cleaned_data['payment_date']) + u'\nPaís: ' + form.cleaned_data['residence_country'] + u'\nE-mail: ' + form.cleaned_data['payer_email'] + u'\n\nUn saludo.\n', 'website@peopleebooks.com', ['admin@peopleebooks.com'], fail_silently=False)
                            return HttpResponse("Message could not be processed")
                else:
                    # DEBUG MAIL:
                    send_mail(u'PeopleEbooks.com: DEBUG: Mensaje de PayPal', u'Hola.\n\n' + u'Se ha recibido un mensaje IPN de PayPal:\n\n' + u'Error de validación del formulario IPN\n' + unicode(form.errors), 'website@peopleebooks.com', ['admin@peopleebooks.com'], fail_silently=False)
                    return HttpResponse("Error processing submitted data!\n %s" % form.errors)
            except Exception:
                send_mail(u'PeopleEbooks.com DEBUG: Mensaje de PayPal', u'Hola.\n\n' + u'Error en validación de mensaje IPN (views.py, línea 1300 aprox.)\n\nTipo: ' + request.POST.get('txn_type', '-') + '\nID: ' + request.POST.get('txn_id', '-') + '\nFecha: ' + unicode(request.POST.get('payment_date', '-')) + u'\nPaís: ' + request.POST.get('residence_country', '-') + u'\nE-mail: ' + request.POST.get('payer_email', '-') + u'\n\nUn saludo.\n', 'website@peopleebooks.com', ['admin@peopleebooks.com'], fail_silently=False)
                return HttpResponse('Error: IPN form validation error')
        else:
            send_mail(u'PeopleEbooks.com: Mensaje de PayPal', u'Hola.\n\n' + u'Se ha producido un error de verificación de un mensaje IPN de PayPal:\n\nTipo: ' + request.POST.get('txn_type', '-') + '\nID: ' + request.POST.get('txn_id', '-') + '\nFecha: ' + unicode(request.POST.get('payment_date', '-')) + u'\nPaís: ' + request.POST.get('residence_country', '-') + u'\nE-mail: ' + request.POST.get('payer_email', '-') + u'\n\nUn saludo.\n', 'website@peopleebooks.com', ['admin@peopleebooks.com'], fail_silently=False)
            return HttpResponse("Error: Invalid message")
            
    else: # Request != POST
        return HttpResponseRedirect('/')
        

#-------------------------------------------------------------------------------

def checkout_complete(request):
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1

    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in and request.POST.get('username', 0): 
        login_result = login_user(request)
        if login_result == 0:  # Login OK
            return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
        # login_result = 1 and 2 are handled in the 'navbar.html' template


    # Get the user's library:
    books = []
    library = Purchase.objects.values('book').filter(user=request.user.username) 
    for i in library:
        books.append(Book.objects.get(book=i['book']))        
    
    
    shopping_cart = PayPal_IPN.objects.filter(invoice=request.COOKIES.get('invoice')).values()
    if not shopping_cart: return HttpResponseRedirect('/cart')  # If shopping cart is empty redirect to '/cart'
    # Put 'item_number_Xs into a list for easier iteration:
    shopping_cart_list = []

    try:
        for i in range(1, settings.CART_MAX_SIZE+1): # item_number_1 to 10(max allowed per purchase)
            shopping_cart_list.append(shopping_cart[0]['item_number_%d' % i])
    except IndexError:  # Raises if shopping cart is empty.
        return HttpResponseRedirect('/cart')
    
    purchased_books = []   
    for i in shopping_cart_list:   
        if i: 
            book = Book.objects.get(book=i)
                
            if request.user.is_authenticated():
                try:
                    payment = PayPal_IPN.objects.get(invoice=request.COOKIES.get('invoice'))
                    if payment.payment_status == 'Completed':
                        purchased_books.append(book)                            
                        # To avoid duplicate 'Purchase' objects in dB:
                        if not Purchase.objects.filter(book=book.book).filter(invoice=payment).count(): # Object is not currently in dB
                            purchase = Purchase(book=book, user=Users.objects.get(user=request.user.username), value=book.price, invoice=payment)
                            purchase.save()
                            # Increase 'sales' on books and users (author and purchaser), by 1:
                            book.sales += 1
                            book.save()
                            author = Users.objects.get(user=book.user.user)
                            author.sales += 1
                            if book.price < settings.BOOK_COMMISSION_BEGIN:
                                author.total_profits += Decimal('0.50') # book.price - PeopleEbooks profit
                                author.current_balance += Decimal('0.50') # book.price - PeopleEbooks profit
                            else:
                                author.total_profits += book.price * (Decimal('1') - settings.BOOK_PRICE_COMMISSION)
                                author.current_balance += book.price * (Decimal('1') - settings.BOOK_PRICE_COMMISSION)                           
                            author.save()
                
                            reader = Users.objects.get(user=request.user.username)
                            reader.purchases += 1
                            reader.save()
                                
                    else:
                        return HttpResponseRedirect('/awaiting-payment')
                except ObjectDoesNotExist:
                    # Payment could not be validated.
                    return HttpResponseRedirect('/awaiting-payment')                   
            else:
                try:
                    payment = PayPal_IPN.objects.get(invoice=request.COOKIES.get('invoice'))
                    if payment.payment_status == 'Completed':
                        purchased_books.append(book)
                        # To avoid duplicate 'Purchase' objects in dB:
                        if not request.POST.get('ebook', 0) and not Purchase.objects.filter(book=book.book).filter(invoice=payment).count(): # Object is not currently in dB
                            purchase = Purchase(book=book, value=book.price, invoice=payment)
                            purchase.save()
                        
                            # Increase 'sales' on book sales and user sales, by 1:
                            book.sales += 1
                            book.save()
                            author = Users.objects.get(user=book.user.user)
                            author.sales += 1
                            if book.price < settings.BOOK_COMMISSION_BEGIN:
                                author.total_profits += Decimal('0.50') # book.price - PeopleEbooks profit
                                author.current_balance += Decimal('0.50') # book.price - PeopleEbooks profit
                            else:
                                author.total_profits += book.price * (Decimal('1') - settings.BOOK_PRICE_COMMISSION)
                                author.current_balance += book.price * (Decimal('1') - settings.BOOK_PRICE_COMMISSION)                          
                            author.save()
                            
                    else:
                        return HttpResponseRedirect('/awaiting-payment')
                except ObjectDoesNotExist:
                    # Payment could not be validated.
                    return HttpResponseRedirect('/awaiting-payment')
    # Generate download links:
    try:
        # EPUB
        if request.method == 'POST' and not user_logged_in and request.POST.get('ebook_epub', 0):
            ebook = Book.objects.get(book=request.POST.get('ebook_epub'))
            if ebook in purchased_books:
                response = HttpResponse(FileWrapper(ebook.book_file_epub), content_type='application/epub+zip')
                response['Content-Disposition'] = 'attachment; filename="%s.epub"' % ebook.book
                return response
        # PDF
        if request.method == 'POST' and not user_logged_in and request.POST.get('ebook_pdf', 0):
            ebook = Book.objects.get(book=request.POST.get('ebook_pdf'))
            if ebook in purchased_books:
                response = HttpResponse(FileWrapper(ebook.book_file_pdf), content_type='application/pdf')
                response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % ebook.book
                return response
    except IOError:
        HttpResponseRedirect('404')
      
    response = render_to_response('checkout_complete.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )
    
    # Empty shopping cart:                     
    if user_logged_in and not request.POST.get('ebook', 0):
        response.set_cookie('cart', '_')
    
    return response
                                            

def email_change(request, hashes):
    
    try:
        user = Users.objects.get(hashes=hashes)
        if user:
            email = hashes.split('_')[1]
            user.email = email
            user.hashes = ''
            user.save()
            
    except ObjectDoesNotExist:
        return HttpResponseRedirect('/')
        
    return render_to_response('email_change.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )


def edit_book(request, book):
    user_logged_in = request.user.is_authenticated()
    book_editing = Book.objects.get(book=book)
    
    if user_logged_in and request.user.username == book_editing.user.user:
        # Login stuff:
        if request.method == 'POST' and not user_logged_in and request.POST.get('username', 0): 
            login_result = login_user(request)
            if login_result == 0:  # Login OK
                return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
            # login_result = 1 and 2 are handled in the 'navbar.html' template
    
        #Get options for Rate, Gender and Language:
        genders = Gender.objects.values('gender_id', 'gender_es')
        rates = Rate.objects.values('rate_id', 'rate_es')
        languages = Language.objects.values('language_id', 'language_es')
    
        if request.method == 'POST' or request.method == 'FILES':
            if request.FILES.get('image', 0):
                if book_editing.cover: del_cover = unicode(book_editing.cover) # If book already had a cover (should), delete the existing one (when book.save())
                book_editing.cover = request.FILES.get('image')
            else: del_cover = 0
            # Video:
            if request.FILES.get('video_file', 0):            
                if book_editing.video: del_video = unicode(book_editing.video) # If user already had a video, delete the existing one (when user.save())
                else: del_video = 0
                book_editing.video = request.FILES.get('video_file')
            else: 
                del_video = 0
            
            if request.POST.get('book_info', 0):
                book_editing.rate_id = Rate.objects.get(rate_id=request.POST.get('rate_id'))
                book_editing.gender_id = Gender.objects.get(gender_id=request.POST.get('gender_id'))
                book_editing.language_id = Language.objects.get(language_id=request.POST.get('language_id'))
            
            if request.POST.get('website', 0):
                if len(request.POST.get('website')) < 5 and request.POST.get('website') != '': book_editing.website = ''
                else: # If link does not begin with 'http', add it (to prevent not using absolute path on web pages)
                    if request.POST.get('website').startswith('http'):
                        book_editing.website = request.POST.get('website')
                    else: book_editing.website = 'http://' + request.POST.get('website')
            if request.POST.get('price', 0) and request.POST.get('price') != '':
                try:
                    price = Decimal(request.POST.get('price').replace(',', '.'))
                    if price >= settings.BOOK_PRICE_LOWEST and price <= settings.BOOK_PRICE_HIGHEST:
                        book_editing.price = price
                except InvalidOperation:
                    HttpResponseRedirect(request.META['HTTP_REFERER'])

            if request.POST.get('synopsis_field', 0):
                if len(request.POST.get('synopsis_field')) < 5:
                    # Display error delete next line
                    book_editing.synopsis = ''
                elif validate(request.POST.get('synopsis_field'), 1400, 0): 
                    if request.POST.get('synopsis_field').isupper() or request.POST.get('synopsis_field').islower():
                        book_editing.synopsis = request.POST.get('synopsis_field').capitalize()
                    else:
                        book_editing.synopsis = request.POST.get('synopsis_field')
            try:
                if del_cover:
                    os.remove(settings.MEDIA_ROOT + del_cover)
                if del_video:
                    os.remove(settings.MEDIA_ROOT + del_video)
            except os.error:
                return HttpResponseRedirect('/home/' + request.user.username + '/#library')
            
            book_editing.save()
            

            if request.FILES.get('image', 0) or request.FILES.get('video_file', 0):
                # Book cover resizing:
                if request.FILES.get('image', 0):
                    fname, dot, extension = request.FILES.get('image').name.rpartition('.')
                    status = img_resize(settings.MEDIA_ROOT + book_editing.cover.name, 0)
                # Video transcoding:
                if request.FILES.get('video_file', 0):
                    # Pass filepath as the user uploaded it (not neccessarily with 'webm' extension) so no dB query is required
                    fname, dot, extension = request.FILES.get('video_file').name.rpartition('.')
                    status = video_transcode(settings.MEDIA_ROOT + book_editing.video.name)
                    if status != 0:
                        book_editing = Book.objects.get(book=book)
                        # Remove video from filesystem:
                        try:
                            call(["rm", settings.MEDIA_ROOT + book_editing.video.name[:-1]])
                        except os.error:
                            pass # Mail notification?
                        book_editing.video.name = ''
                    
                book_editing.save()

        return render_to_response('edit_book.html',
                                     locals(),
                                     context_instance=RequestContext(request),
                                 )
    
    else:
        return HttpResponseRedirect('/')
    
    
                             
def news(request):
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in: 
        login_result = login_user(request)
        if login_result == 0:  # Login OK
            return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
        # login_result = 1 and 2 are handled in the 'navbar.html' template
    
    
    return render_to_response('news.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )


def already_exists(request):
    if request.path == request.get_host() + '/signup_error/user-already-exists': mode = 0
    elif request.path == request.get_host() + '/signup_error/email-already-registered': mode = 1


    return render_to_response('already_exists.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )


#-------------------------------------------------------------------------------
# Static web pages:
#-------------------------------------------------------------------------------

def faq(request):
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1
    
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in: 
        login_result = login_user(request)
        if login_result == 0:  # Login OK
            return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
        # login_result = 1 and 2 are handled in the 'navbar.html' template

    return render_to_response('faq.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )


def ceo(request):
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1
    
    
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in: 
        login_result = login_user(request)
        if login_result == 0:  # Login OK
            return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
        # login_result = 1 and 2 are handled in the 'navbar.html' template
        
    return render_to_response('ceo.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )
                             
                             
def about(request):
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1
    
    
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in: 
        login_result = login_user(request)
        if login_result == 0:  # Login OK
            return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
        # login_result = 1 and 2 are handled in the 'navbar.html' template
        
    return render_to_response('about.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )
                             
                             
def legal_terms(request):
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1
    
    
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in: 
        login_result = login_user(request)
        if login_result == 0:  # Login OK
            return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
        # login_result = 1 and 2 are handled in the 'navbar.html' template
        
    return render_to_response('legal_terms.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )
                             
                             
def contact(request):
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1
    
    
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.method == 'POST' and not user_logged_in: 
        login_result = login_user(request)
        if login_result == 0:  # Login OK
            return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
        # login_result = 1 and 2 are handled in the 'navbar.html' template
        
    return render_to_response('contact.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )
                             


def signup_error(request, error):
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1
    
    if (request.META['HTTP_REFERER'] == 'https://' + request.get_host() + '/signup'): 
        if error == 'user-already-exists': error_code = 1
        elif error == 'email-already-registered': error_code = 2
    else:
        return HttpResponseRedirect('/')
    
    return render_to_response('signup_error.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )


def awaiting_payment(request):
    
    return render_to_response('awaiting_payment.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )


def publishing_services(request):


    return render_to_response('publishing_services.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )


def error500(request):

    return render_to_response('500.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )

