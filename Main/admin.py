from django.contrib import admin
from Main.models import (Language, Country, Gender, Users, Book, Purchase, Rate,
                         Book_Comment, User_Comment)

admin.site.register(Language)
admin.site.register(Country)
admin.site.register(Gender)
admin.site.register(Users)
admin.site.register(Book)
admin.site.register(Purchase)
admin.site.register(Rate)
admin.site.register(Book_Comment)
admin.site.register(User_Comment)
