from django.db.models import FileField
from Main.models import Users
from django.forms import forms
from django.template.defaultfilters import filesizeformat
from django.utils.translation import ugettext_lazy as _
from subprocess import call
from os import error
import Image


class ContentTypeRestrictedFileField(FileField):
    """
    Same as FileField, but you can specify:
        * content_types - list containing allowed content_types. Example: ['application/pdf', 'image/jpeg']
        * max_upload_size - a number indicating the maximum file size allowed for upload.
            2.5MB - 2621440
            5MB - 5242880
            10MB - 10485760
            20MB - 20971520
            50MB - 5242880
            100MB 104857600
            250MB - 214958080
            500MB - 429916160
    """
    def __init__(self, *args, **kwargs):
        self.content_types = kwargs.pop("content_types")
        self.max_upload_size = kwargs.pop("max_upload_size")

        super(ContentTypeRestrictedFileField, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):        
        data = super(ContentTypeRestrictedFileField, self).clean(*args, **kwargs)
        
        file = data.file
        try:
            content_type = file.content_type
            if content_type in self.content_types:
                if file._size > self.max_upload_size:
                    raise forms.ValidationError(_('Please keep filesize under %s. Current filesize %s') % (filesizeformat(self.max_upload_size), filesizeformat(file._size)))
            else:
                raise forms.ValidationError(_('Filetype not supported.'))
        except AttributeError:
            pass        
            
        return data


def video_transcode(filepath):
    # Rename input file, transcode to output '*.webm', as in dB, and removes temp input file:
    try:
        status = call(["mv", filepath, filepath[:-1]])
        if not status: status = call(["ffmpeg", "-i", filepath[:-1], "-acodec", "libvorbis", "-vcodec", "libvpx", filepath])
        if not status: status = call(["rm", filepath[:-1]])
    except os.error:
        return 1
        
    return status


# Image resize using PIL, for reducing bandwidth usage:
def img_resize(filepath, mode):
    if not mode: # Book cover
        width = 250
        height = 350
    else:  # User photo
        width = 300
        height = 300
     
    img = Image.open(filepath)
    resized_image = img.resize((width, height), Image.ANTIALIAS)
    
    try:
        # Move original image to 'original' subdirectory. Directory must exist prior to this!: 
        path, slash, fname = filepath.rpartition('/')
        status = call(["cp", filepath, path + '/original/'])
        if not status:
            try:
                filename, dot, extension = fname.rpartition('.')
                resized_image.save(path + '/' + filename + '.jpg', 'JPEG')
            except EnvironmentError:
                status = 1
    except error:
        pass
    
    return status
    
