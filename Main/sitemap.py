from django.contrib.sitemaps import Sitemap
from Main.models import Book, Users
from Blog.models import Blog_Entry

class BooksSitemap(Sitemap):
    #changefreq = 'monthly'
    priority = 0.8
    protocol = 'https'
    
    def items(self):
        return Book.objects.all()
        
    #def lastmod(self, obj):
    #    return obj.updated
        
    def location(self, obj):
        return '/books/' + obj.book + '/'
    

class UsersSitemap(Sitemap):
    #changefreq = 'monthly'
    priority = 0.7
    protocol = 'https'
    
    def items(self):
        return Users.objects.exclude(photo__lte='').exclude(quote='')
        
    #def lastmod(self, obj):
    #    return obj.updated
        
    def location(self, obj):
        return '/people/' + obj.user + '/'


class BlogSitemap(Sitemap):
    #changefreq = 'weekly'
    priority = 0.6
    protocol = 'https'
    
    def items(self):
        return Blog_Entry.objects.all()
        
    #def lastmod(self, obj):
    #    return obj.updated
        
    def location(self, obj):
        return '/blog/' + obj.blog_entry + '/'

