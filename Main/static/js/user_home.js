$('#title_error').hide();
$('#synopsis_error').hide();
$('#file_book_error').hide();

$('#publications').hide();
$('#profile').hide();
$('#library').hide();
$('#sales').hide();
    
$('#photo_error').hide();
$('#video_error').hide();
$('#user_video_transcoding').hide();
$('#user_photo_uploading').hide(); 
$('#email_bad').hide();
$('#passwd_confirm_mismatch').hide();
$('#passwd_error').hide();

$('#amount_error').hide(); 
$('#account_error').hide();

$('#quote_error').hide(); 
$('#presentation_error').hide();

// Load requested div based on URL:
$(document.URL.substr(document.URL.lastIndexOf("/")+1)).fadeIn();


// Loads 'from' element into 'div' (JQuery):
function load_content(content){
    $('#profile').hide();
    $('#publications').hide();
    $('#library').hide();
    $('#sales').hide();
    $(content).fadeIn();
}

// Keeps 'items' properties after the user clicks on them:
    var items = document.getElementById('header').getElementsByTagName('h4');
    var lastItem;

    // Asigns new class to clicked items:
    var doSelect = function() {
        // First, return previously clicked items to default class:
        if( lastItem ) {
            lastItem.className = '';
        }
        // Now asign class to the clicked item:
        this.className = 'selected';
        lastItem = this;
    };
   
    // Register 'click' event for each 'h4':
    for( var i = 0, l = items.length; i < l; i++ ) {
        items[i].onclick = doSelect;
    }


//Check file extensions:
function getExtension(filename) {
    var parts = filename.split('.');
    return parts[parts.length - 1];
}

function isEbook(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
        case 'epub':
        case 'pdf':
        
        return true;
    }
    return false;
}

function isImage(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        
        return true;
    }
    return false;
}

function isVideo(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
        case 'avi':
        case 'mp4':
        case 'mkv':
        case 'flv':
        case 'wmv':
        case 'webm':
        
        return true;
    }
    return false;
}


//------------------------------------------------------------------------------
// PROFILE VALIDATION:
//------------------------------------------------------------------------------
function validate_email(){
    var regexp = /^[_a-z0-9-\+]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
    var email = document.forms['personal_data']['email'];

    if(email.value.replace(/ /g, '') == ''){
        return true;
    }
    else{
        if(!regexp.test(email.value.toLowerCase())){
            return false;
        }
        else{
            return true;
        }
    }
}

function validate_passwd(){
    var regexp = /^.{6,30}$/; 
    var passwd = document.forms['personal_data']['passwd'];
    
    if(passwd.value.replace(/ /g, '') == ''){
        return true;
    }
    else{ 
        if(!regexp.test(passwd.value)){
            return false;
        }
        else{
            return true;
        }
    }
}

function validate_passwd_confirm(){
    var passwd_confirm = document.forms['personal_data']['passwd_confirm'];
    
    $('#passwd_confirm_mismatch').hide();    
        
    if(passwd_confirm.value.replace(/ /g, '') == ''){
        return true;
    }
    else{
        if(passwd_confirm.value != document.forms['personal_data']['passwd'].value){
            return false;
        }
        else{
            return true;
        }
    }
}

function submit_form_personal_data(){
    var passwd = document.forms['personal_data']['passwd'];
    var passwd_confirm = document.forms['personal_data']['passwd_confirm'];
    var email = document.forms['personal_data']['email'];
    var passwd_ok = false;
    var email_ok = false;
    $('#passwd_changed').hide(); // Hide passwd changed message before showing new messages.
    $('#email_changed').hide();
    
    if (!validate_email()){
        email.style.background = '#c90a06';
        email.style.color = '#ffffff'; 
        $('#email_bad').fadeIn();
    }
    else{
        email_ok = true;
        $('#email_bad').hide();
        email.style.background = '#ffffff';
        email.style.color = '#333333'; 
    }
    
    if (validate_passwd()){
        if (validate_passwd_confirm()){
            passwd_confirm.style.background = '#ffffff';
            passwd_confirm.style.color = '#333333';
            $('#passwd_confirm_mismatch').hide();
            passwd_ok = true;
        }
        else{
            passwd_confirm.style.background = '#c90a06';
            passwd_confirm.style.color = '#ffffff'; 
            $('#passwd_confirm_mismatch').fadeIn();
        }
        passwd.style.background = '#ffffff';
        passwd.style.color = '#333333'; 
        $('#passwd_error').hide();
    }
    else{
        passwd.style.background = '#c90a06';
        passwd.style.color = '#ffffff'; 
        $('#passwd_error').fadeIn();
    }
    
    if (passwd_ok && email_ok){
        document.forms['personal_data'].submit();
    }
}


function submit_form_photo(){
    var photo = document.forms['photo']['image'].value;
    
    if (!isImage(photo)){
        document.forms['photo']['image'].style.background = '#c90a06';
        document.forms['photo']['image'].style.color = '#ffffff';
        $('#photo_error').fadeIn();
    }
    else{
        $('#photo_error').hide();
        document.forms['photo']['image'].style.background = '#647164';
        document.forms['photo']['image'].style.color = '#ffffff';
        $('#user_photo_uploading').fadeIn();  
        document.forms['photo'].submit();
    }   
}


function submit_form_video(){
    var video = document.forms['video']['video_file'].value;
    
    if (!isVideo(video)){
        document.forms['video']['video_file'].style.background = '#c90a06';
        document.forms['video']['video_file'].style.color = '#ffffff';
        $('#video_error').fadeIn();
    }
    else{
        $('#video_error').hide();
        // Uploading and transcoding video        
        document.forms['video']['video_file'].style.background = '#647164';
        document.forms['video']['video_file'].style.color = '#ffffff';
        $('#user_video_transcoding').fadeIn(); 
        document.forms['video'].submit();
    }   
}


function validate_amount(amount){
    var regexp = /^[\d]{1,10}$/;

    if(!regexp.test(amount.value) || parseFloat(amount.value) > parseFloat(document.forms['withdrawal']['balance'].value) || parseFloat(amount.value) < 10){
        document.forms['withdrawal']['amount'].style.background = '#c90a06';
        document.forms['withdrawal']['amount'].style.color = '#ffffff';
        $('#amount_error').fadeIn();
        return 0;
    }
    else{
        document.forms['withdrawal']['amount'].style.background = '#ffffff';
        document.forms['withdrawal']['amount'].style.color = '#333333';
        $('#amount_error').hide();         
        return 1;
    }
}


function validate_withdrawal(){
    if (validate_amount(document.forms['withdrawal']['amount']) == 1){
        if (document.forms['withdrawal']['payment_account'].value != ''){
            $('#account_error').hide();
            $('#amount_error').hide();
            document.forms['withdrawal'].submit();
        }
        else{
            $('#account_error').fadeIn(); 
        }
    }
    else{
        $('#amount_error').fadeIn(); 
    }
}

// Function to validate user's quote and presentation:
function submit_quote_pres(){
    var regexp_presentation = /[\w\s\.,:;áéíóúÁÉÍÓÚàèìòùÀÈÌÒÙâêîôûÂÊÎÔÛÑñü¿\?¡!\-\(\)"\r]{1,1000}$/;
    var regexp_quote = /[\w\s\.,:;áéíóúÁÉÍÓÚàèìòùÀÈÌÒÙâêîôûÂÊÎÔÛÑñü¿\?¡!\-\(\)"\r]{1,200}$/;
    var presentation = document.forms['misc']['presentation'].value;
    var quote = document.forms['misc']['quote'].value;
    
    
    if (quote != '' || presentation != ''){
        if (quote == '' || regexp_quote.test(quote)){
            document.forms['misc']['quote'].style.background = '#ffffff';
            document.forms['misc']['quote'].style.color = '#333333';
            $('#quote_error').hide(); 
            if (presentation != '' && !regexp_presentation.test(presentation)){
                document.forms['misc']['presentation'].style.background = '#c90a06';
                document.forms['misc']['presentation'].style.color = '#ffffff'; 
                $('#presentation_error').fadeIn();
            }
            else{
                document.forms['misc']['presentation'].style.background = '#ffffff';
                document.forms['misc']['presentation'].style.color = '#333333';
                $('#presentation_error').hide();
                document.forms['misc'].submit();            
            }
        }
        else{
            document.forms['misc']['quote'].style.background = '#c90a06';
            document.forms['misc']['quote'].style.color = '#ffffff'; 
            $('#quote_error').fadeIn();
        }
    }
}


// Show placeholder as field value on quotes and presentations when focused:
function show_placeholder(element){
    $(element).val($(element).attr('placeholder'));
}

    
// Select header option according to URL:
$('#header_' + document.URL.substr(document.URL.lastIndexOf("/")+2)).click();

