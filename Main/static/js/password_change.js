document.forms['password_change'].reset();

function validate_passwd(){
    var regexp = /^.{6,30}$/;
    var passwd = document.forms['password_change']['passwd'];

    if(!regexp.test(passwd.value)){
        passwd.style.background = '#c90a06';
        passwd.style.color = '#ffffff'; 
        $('#passwd_error').fadeIn();
        return false;
    }
    else{
        passwd.style.background = '#ffffff';
        passwd.style.color = '#333333'; 
        $('#passwd_error').hide();
        return true;
    }
}

function validate_passwd_confirm(){
    var passwd_confirm = document.forms['password_change']['passwd_confirm'];
    
    passwd_confirm.style.background = '#ffffff';
    passwd_confirm.style.color = '#333333'; 
    $('#passwd_confirm_empty').hide();
    $('#passwd_confirm_mismatch').hide();    
        
    if(passwd_confirm.value.replace(/ /g, '') == ''){
        passwd_confirm.style.background = '#c90a06';
        passwd_confirm.style.color = '#ffffff'; 
        $('#passwd_confirm_empty').fadeIn();
        return false;
    }
    else{
        if(passwd_confirm.value != document.forms['password_change']['passwd'].value){
            passwd_confirm.style.background = '#c90a06';
            passwd_confirm.style.color = '#ffffff'; 
            $('#passwd_confirm_mismatch').fadeIn();
            return false;
        }
        else{
            return true;
        }
    }
}

function submit_form(){
    $('#fields_blank').hide();  
    
    if (validate_passwd() && validate_passwd_confirm()){
        document.forms['password_change'].submit();
    }
    else{
        $('#fields_blank').fadeIn();
    }
}

$('#fields_blank').hide();
$('#passwd_error').hide();
$('#passwd_confirm_empty').hide();
$('#passwd_confirm_mismatch').hide();

