// Loads 'from' element into 'div' (JQuery):
function load_content(content){
    $('#publish_info1').hide();
    $('#publish_info2').hide();
    $('#publish_info3').hide();
    $(content).fadeIn();
}

// Keeps 'items' properties after the user clicks on them:
    // Gets all 'h4' items in 'right-pane:
    var items_right = document.getElementById('right-pane').getElementsByTagName('h4');
    var items_left = document.getElementById('left-pane').getElementsByTagName('h4');
    var lastItem1, lastItem2;

    // Asigns new class to clicked items:
    var doSelect1 = function() {
        // First, return previously clicked items to default class:
        if( lastItem1 ) {
            lastItem1.className = '';
        }
        // Now asign class to the clicked item:
        this.className = 'selected';
        lastItem1 = this;
    };
    
    var doSelect2 = function() {
        if( lastItem2 ) {
            lastItem2.parentNode.className = 'step-container';
        }
        this.parentNode.className = 'step-container selected';
        lastItem2 = this;
    };    
   
    // Register 'click' event for each 'h4':
    for( var i = 0, l = items_right.length; i < l; i++ ) {
        items_right[i].onclick = doSelect1;
    }
    for( var i = 0, l = items_left.length; i < l; i++ ) {
        items_left[i].onclick = doSelect2;
    }


//Check file extensions:
function getExtension(filename) {
    var parts = filename.split('.');
    return parts[parts.length - 1];
}

function isEbook(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
        case 'epub':
        case 'pdf':
        
        return true;
    }
    return false;
}

function isImage(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
        case 'jpg':
        case 'png':
        
        return true;
    }
    return false;
}

function isVideo(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
        case 'avi':
        case 'mp4':
        
        return true;
    }
    return false;
}



// Publishing steps:
function next_step(next, menu_left){
    var steps = document.getElementById('options').getElementsByTagName('span');
    
    $(steps).hide();
    $(menu_left).click();
    $(next).fadeIn();
}

// Publishing form validation:
// Step 1:
function publish_step1_validate(){
    var title_regexp = /^[^\\/\^<>\*%\$_|&;\{\}\[\]\(\)~#]{1,100}$/;
    var synopsis_regexp = /^[^\\/\^<>\*%\$_|&;\{\}\[\]\(\)~#]{1,1000}$/;
        
    var title = document.forms['publish']['title'].value;
    if(!title_regexp.test(title)){
        document.forms['publish']['title'].style.background = '#c90a06';
        document.forms['publish']['title'].style.color = '#ffffff'; 
        $('#title_error').fadeIn();
    }
    else{
        document.forms['publish']['title'].style.background = '#ffffff';
        document.forms['publish']['title'].style.color = '#333333';
        $('#title_error').hide(); 
        
        var synopsis = document.forms['publish']['synopsis'].value;
        if (!synopsis_regexp.test(synopsis)){
            document.forms['publish']['synopsis'].style.background = '#c90a06';
            document.forms['publish']['synopsis'].style.color = '#ffffff'; 
            $('#synopsis_error').fadeIn();
        }
        else{
            document.forms['publish']['synopsis'].style.background = '#ffffff';
            document.forms['publish']['synopsis'].style.color = '#333333';
            $('#synopsis_error').hide(); 
        
            next_step('#publish_step2', '#step2_left');  // Gets to step2
        }
    }
}

// Step 2:
function publish_step2_validate(){
    var ebook = document.forms['publish']['book_file'].value;
    var cover = document.forms['publish']['cover'].value;
     
    if (!isEbook(ebook)){
        document.forms['publish']['book_file'].style.background = '#c90a06';
        document.forms['publish']['book_file'].style.color = '#ffffff';
        $('#file_book_error').fadeIn();
    }
    else{
        document.forms['publish']['book_file'].style.background = '#ffffff';
        document.forms['publish']['book_file'].style.color = '#333333';
        $('#file_book_error').hide();
        
        if (!isImage(cover)){
            document.forms['publish']['cover'].style.background = '#c90a06';
            document.forms['publish']['cover'].style.color = '#ffffff';
            $('#file_cover_error').fadeIn();
        }
        else{
            document.forms['publish']['cover'].style.background = '#ffffff';
            document.forms['publish']['cover'].style.color = '#333333';
            $('#file_cover_error').hide(); 
            
            next_step('#publish_step3', '#step3_left');  // Gets to step3
        }
    }
}

function publish_step3_validate(){
    var agree = document.forms['publish']['agree'].checked;
    
    if(!agree){
        $('#agree_error').fadeIn();
    }
    else{
        $('#agree_error').hide();
        document.forms['publish'].submit();
    }
}


$('#publish_step2').hide();
$('#publish_step3').hide();
$('#publish_info1').hide();
$('#publish_info2').hide();
$('#publish_info3').hide();
$('#title_error').hide();
$('#synopsis_error').hide();
$('#file_book_error').hide();
$('#file_cover_error').hide();
$('#agree_error').hide();
$('#info').click();
$('#step1_left').click();
