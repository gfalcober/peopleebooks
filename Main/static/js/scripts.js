$('#publish_step2').hide();
$('#publish_step3').hide();
$('#publish_info1').hide();
$('#publish_info2').hide();
$('#publish_info3').hide();
$('#publishing_book').hide();
$('#title_error').hide();
$('#synopsis_error').hide();
$('#file_book_epub_error').hide();
$('#file_book_pdf_error').hide();
$('#file_book_error').hide();
$('#file_cover_error').hide();
$('#agree_error').hide();
$('#info').click();
$('#step1_left').click();


// Loads 'from' element into 'div' (JQuery):
function load_content(content){
    $('#publish_info1').hide();
    $('#publish_info2').hide();
    $('#publish_info3').hide();
    $(content).fadeIn();
}

// Keeps 'items' properties after the user clicks on them:
    // Gets all 'h4' items in 'right-pane:
    var items_right = document.getElementById('right-pane').getElementsByTagName('h4');
    var items_left = document.getElementById('left-pane').getElementsByTagName('h4');
    var lastItem1, lastItem2;

    // Asigns new class to clicked items:
    var doSelect1 = function() {
        // First, return previously clicked items to default class:
        if( lastItem1 ) {
            lastItem1.className = '';
        }
        // Now asign class to the clicked item:
        this.className = 'selected';
        lastItem1 = this;
    };
    
    var doSelect2 = function() {
        if( lastItem2 ) {
            lastItem2.parentNode.className = 'step-container';
        }
        this.parentNode.className = 'step-container selected';
        lastItem2 = this;
    };    
   
    // Register 'click' event for each 'h4':
    for( var i = 0, l = items_right.length; i < l; i++ ) {
        items_right[i].onclick = doSelect1;
    }
    for( var i = 0, l = items_left.length; i < l; i++ ) {
        items_left[i].onclick = doSelect2;
    }


//Check file extensions:
function getExtension(filename) {
    var parts = filename.split('.');
    return parts[parts.length - 1];
}

function isPDF(filename) {
    var ext = getExtension(filename);
    if (ext.toLowerCase() == 'pdf') {
        return true;
    }
    return false;
}

function isEPUB(filename) {
    var ext = getExtension(filename);
    if (ext.toLowerCase() == 'epub') {
        return true;
    }
    return false;
}

function isImage(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        
        return true;
    }
    return false;
}


// Publishing steps:
function next_step(next, menu_left){
    var steps = document.getElementById('publishing-steps').getElementsByClassName('step');
    
    $(steps).hide();
    $(menu_left).click();
    $(next).fadeIn();
}

// Publishing form validation:
// Step 1:
function eval_title(){
    var title_regexp = /[\w\s\.,:;áéíóúÁÉÍÓÚàèìòùÀÈÌÒÙâêîôûÂÊÎÔÛÑñü¿\?¡!\-\(\)\r]{1,100}$/;
    var title = document.forms['publish']['title'].value;
    var status = 0;
    
    if(!title_regexp.test(title)){
        document.forms['publish']['title'].style.background = '#c90a06';
        document.forms['publish']['title'].style.color = '#ffffff'; 
        $('#title_error').fadeIn();
    }
    else{
        document.forms['publish']['title'].style.background = '#ffffff';
        document.forms['publish']['title'].style.color = '#333333';
        $('#title_error').hide(); 
        status = 1;
    }
    return status;
}

function eval_synopsis(){
    var synopsis_regexp = /[\w\s\.,:;áéíóúÁÉÍÓÚàèìòùÀÈÌÒÙâêîôûÂÊÎÔÛÑñü¿\?¡!\-\(\)"\r]{1,1000}$/;
    var synopsis = document.forms['publish']['synopsis'].value;
    var status = 0;

    if (!synopsis_regexp.test(synopsis)){
        document.forms['publish']['synopsis'].style.background = '#c90a06';
        document.forms['publish']['synopsis'].style.color = '#ffffff'; 
        $('#synopsis_error').fadeIn();
    }
    else{
        document.forms['publish']['synopsis'].style.background = '#ffffff';
        document.forms['publish']['synopsis'].style.color = '#333333';
        $('#synopsis_error').hide(); 
        status = 1;
    }
    return status;
}


function eval_step1(){
    var status = 0;
    
    if (eval_title() && eval_synopsis()){
        next_step('#publish_step2', '#step2_left');  // Gets to step2
        status = 1;
    }
    return status; 
}


// Step 2:

function eval_epub(){
    var epub = document.forms['publish']['book_file_epub'].value;
    var status = 0;
    
    if (epub != '' && !isEPUB(epub)){
        document.forms['publish']['book_file_epub'].style.background = '#c90a06';
        document.forms['publish']['book_file_epub'].style.color = '#ffffff';
        $('#file_book_epub_error').fadeIn();
    }
    else{
        document.forms['publish']['book_file_epub'].style.background = '#ffffff';
        document.forms['publish']['book_file_epub'].style.color = '#333333';
        $('#file_book_epub_error').hide();
        status = 1;
    }
    return status;    
}

function eval_pdf(){
    var pdf = document.forms['publish']['book_file_pdf'].value;
    var status = 0;
    
    if (pdf != ''){
        if (!isPDF(pdf)){
            document.forms['publish']['book_file_pdf'].style.background = '#c90a06';
            document.forms['publish']['book_file_pdf'].style.color = '#ffffff';
            $('#file_book_pdf_error').fadeIn();
        }
        else{
            document.forms['publish']['book_file_pdf'].style.background = '#ffffff';
            document.forms['publish']['book_file_pdf'].style.color = '#333333';
            $('#file_book_pdf_error').hide();
            status = 1;
        }
    }
    return status;
}


function eval_cover(){
    var cover = document.forms['publish']['cover'].value;
    var status = 0;
    
    if (!isImage(cover)){
        document.forms['publish']['cover'].style.background = '#c90a06';
        document.forms['publish']['cover'].style.color = '#ffffff';
        $('#file_cover_error').fadeIn();
    }
    else{
        document.forms['publish']['cover'].style.background = '#ffffff';
        document.forms['publish']['cover'].style.color = '#333333';
        $('#file_cover_error').hide(); 
        status = 1;
    }
    return status;
}

function eval_step2(){
    var epub = document.forms['publish']['book_file_epub'].value;
    var pdf = document.forms['publish']['book_file_pdf'].value;
    var status = 0;
    
    if (epub == '' && pdf == ''){
        $('#file_book_error').fadeIn();
    }
    else{
        $('#file_book_error').hide();
        if ((eval_epub() || eval_pdf()) && eval_cover()){
            next_step('#publish_step3', '#step3_left');  // Gets to step3
            status = 1;
        }
    }
    return status; 
}

// Step 3:
// Also check previous ones:

function eval_step3(){
    var agree = document.forms['publish']['agree'].checked;
    
    if(!agree){
        $('#agree_error').fadeIn();
    }
    else{
        $('#agree_error').hide();
        if (eval_step1()){
            if (eval_step2()){
                $('#publishing_book').fadeIn();
                document.forms['publish'].submit();
            }
            else{ // Form validation failed in step 2
                next_step('#publish_step2', '#step2_left');  // Gets to step2
            }
        }
        else{ // Form validation failed in step 1
            next_step('#publish_step1', '#step1_left');  // Gets to step1
        }
    }
}

// Publishing form reset button. Resets PDF and ePub fields:
if (document.getElementById('reset')){
    document.getElementById('reset').onclick= function() {
        var epub = document.getElementById('pdf');
        var pdf = document.getElementById('epub');
        pdf.value = pdf.defaultValue;
        epub.value = epub.defaultValue;
    };
}

