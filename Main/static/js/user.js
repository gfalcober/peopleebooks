// Manage user's library / publications toggle:

// Keeps 'items' properties after the user clicks on them:
    // Gets all 'input' items in 'toggle' ('Publications' and 'Library' buttons):
var items = document.getElementById('toggle').getElementsByTagName('input');
var lastItem;

// Asigns new class to clicked items:
var doSelect = function() {
    // First, return previously clicked items to default class:
    if( lastItem ) {
        lastItem.className = 'tabs';
    }
    // Now asign class to the clicked item:
    this.className = 'tabs selected';
    lastItem = this;
    $('#first_option').hide();
    $('#second_option').hide();
    if($(this).attr('Id') == 'publications'){
        $('#first_option').fadeIn();
    }
    else{
        $('#second_option').fadeIn();
    };   
};
    
// Register 'click' event for each 'h4':
for( var i = 0, l = items.length; i < l; i++ ) {
    items[i].onclick = doSelect;
}


if(document.getElementById('publications')){
    $('#second_option').hide();
    $('#publications').click();
}
else{
    $('#first_option').hide();
    if(document.getElementById('library')){
        $('#library').click();
    }
};

