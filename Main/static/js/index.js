// Loads 'from' element into 'div' (JQuery):
var carousel = new Array();
carousel[0] = '#carousel_1';
carousel[1] = '#carousel_2';
carousel[2] = '#carousel_3';
// Loads random book from best-sellers top 3:
$(document.getElementById(Math.floor(Math.random() * 3) + 1)).fadeIn();

$(document).ready(slider());

var i = 0;
function slider(){    
    $(carousel[i]).fadeOut("slow", "swing", function(){
        if (i == 2) i = 0;
        else i++;
        $(carousel[i]).fadeIn("slow", "swing");
    });
    setTimeout(slider, 7000);
}


function show_best_seller(element){
    $('#1').hide();
    $('#2').hide();
    $('#3').hide();
    $(document.getElementById(element.id.substring(4))).fadeIn();
}

