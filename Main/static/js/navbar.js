// Login Form
$(document).ready(function(){
	$('#login-trigger').click(function(){
		$(this).next('#login-content').slideToggle(300);
		$(this).toggleClass('active');					
		
		if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
        else $(this).find('span').html('&#x25BC;')
    })
	// If lost focus, toggle back panel:
	$('body').click(function(){
	    if ($('#login-trigger').hasClass('active')){
	        $('#login-trigger').click();
	    }
	    if ($('#search-trigger').hasClass('active')){
	        $('#search-trigger').click();
	    }
	    
	});
	$('#navbar').click(function(event){  // Prevent outside click event to lauch click event on 'login-trigger', as its part of 'html'
        event.stopPropagation();
    });
	
		
    // Searchbox:
    $('#search-trigger').click(function(){
        $(this).next('#search_content').slideToggle(300);
        $(this).toggleClass('active');
        
        if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
        else $(this).find('span').html('&#x25BC;')
    })
});

window.onload = function(){
    var error = document.getElementById('error');
    if (error != undefined) $('#login-trigger').click();
};


