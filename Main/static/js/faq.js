// Loads 'from' element into 'div' (JQuery):
function load_content(content){
    $('#readers').hide();
    $('#authors').hide();
    $('#misc').hide();
    $(content).fadeIn();
    $('#header_' + content.substr(content.lastIndexOf("#")+1)).click();
}


// Keeps 'items' properties after the user clicks on them:
    var items = document.getElementById('header').getElementsByTagName('h4');
    var lastItem;

    // Asigns new class to clicked items:
    var doSelect = function() {
        // First, return previously clicked items to default class:
        if( lastItem ) {
            lastItem.className = '';
        }
        // Now asign class to the clicked item:
        this.className = 'selected';
        lastItem = this;
    };
   
    // Register 'click' event for each 'h4':
    for( var i = 0, l = items.length; i < l; i++ ) {
        items[i].onclick = doSelect;
    }

window.onload(load_content('#readers'));

jQuery(document).ready(function($) { 
	$(".scroll").click(function(event){		
		event.preventDefault();
		$('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
	});
});

