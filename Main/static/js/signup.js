var full_name_ok = false;
var email_ok = false;
var passwd_ok = false;
var passwd_conf_ok = false;

document.forms['signup_form'].reset();

function validate_full_name(full_name){
    var regexp = /[\w\s\.áéíóúÁÉÍÓÚàèìòùÀÈÌÒÙâêîôûÂÊÎÔÛÑñüçÇ\-"\r]{4,40}$/;
    
    if(!regexp.test(full_name.value)){
        document.forms['signup_form']['full_name'].style.background = '#c90a06';
        document.forms['signup_form']['full_name'].style.color = '#ffffff'; 
        $('#full_name_error').fadeIn();
        full_name_ok = false;
    }
    else{
        document.forms['signup_form']['full_name'].style.background = '#ffffff';
        document.forms['signup_form']['full_name'].style.color = '#333333';
        $('#full_name_error').hide();
        full_name_ok = true;
    }
}

function validate_email(email){
    var regexp = /^[_a-z0-9-\+]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;

    $('#email_empty').hide();
    $('#email_bad').hide();
    document.forms['signup_form']['email'].style.background = '#ffffff';
    document.forms['signup_form']['email'].style.color = '#333333'; 

    if(email.value.replace(/ /g, '') == ''){
        document.forms['signup_form']['email'].style.background = '#c90a06';
        document.forms['signup_form']['email'].style.color = '#ffffff'; 
        $('#email_empty').fadeIn();
        email_ok = false;
    }
    else{ 
        if(!regexp.test(email.value.toLowerCase())){
            document.forms['signup_form']['email'].style.background = '#c90a06';
            document.forms['signup_form']['email'].style.color = '#ffffff'; 
            $('#email_bad').fadeIn();
            email_ok = false;
        }
        else{
            email_ok = true;
        }
    }
}

function validate_passwd(passwd){
    regexp = /^.{6,30}$/;

    if(!regexp.test(passwd.value)){
        document.forms['signup_form']['passwd'].style.background = '#c90a06';
        document.forms['signup_form']['passwd'].style.color = '#ffffff'; 
        $('#passwd_error').fadeIn();
        passwd_ok = false;
    }
    else{
        document.forms['signup_form']['passwd'].style.background = '#ffffff';
        document.forms['signup_form']['passwd'].style.color = '#333333'; 
        $('#passwd_error').hide();
        passwd_ok = true;
    }
}

function validate_passwd_confirm(confirm){
    document.forms['signup_form']['passwd_confirm'].style.background = '#ffffff';
    document.forms['signup_form']['passwd_confirm'].style.color = '#333333'; 
    $('#passwd_confirm_empty').hide();
    $('#passwd_confirm_mismatch').hide();    
        
    if(confirm.value.replace(/ /g, '') == ''){
        document.forms['signup_form']['passwd_confirm'].style.background = '#c90a06';
        document.forms['signup_form']['passwd_confirm'].style.color = '#ffffff'; 
        $('#passwd_confirm_empty').fadeIn();
        passwd_conf_ok = false;
    }
    else{
        if(confirm.value != document.forms['signup_form']['passwd'].value){
            document.forms['signup_form']['passwd_confirm'].style.background = '#c90a06';
            document.forms['signup_form']['passwd_confirm'].style.color = '#ffffff'; 
            $('#passwd_confirm_mismatch').fadeIn();
            passwd_conf_ok = false;
        }
        else{
            passwd_conf_ok = true;
        }
    }
}

function submit_form(){
    $('#fields_blank').hide();
    var agree = document.forms['signup_form']['agree'];    
    
    if(!agree.checked){
        $('#agree_error').fadeIn();
    }
    else{
        $('#agree_error').hide();
        if (full_name_ok && email_ok && passwd_ok && passwd_conf_ok){
            document.forms['signup_form'].submit();
        }
        else{
            $('#fields_blank').fadeIn();
        }
    }
}


$('#full_name_error').hide();
$('#email_empty').hide();
$('#email_bad').hide();
$('#passwd_error').hide();
$('#passwd_confirm_empty').hide();
$('#passwd_confirm_mismatch').hide();
$('#agree_error').hide();
$('#fields_blank').hide();

