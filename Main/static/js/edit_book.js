$('#synopsis_error').hide();
$('#photo_error').hide();
$('#video_error').hide();
$('#price_error').hide();
$('#video_transcoding').hide();
$('#cover_uploading').hide();  


//Check file extensions:
function getExtension(filename) {
    var parts = filename.split('.');
    return parts[parts.length - 1];
}

function isImage(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
        case 'jpg':
        case 'png':
        
        return true;
    }
    return false;
}

function isVideo(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
        case 'avi':
        case 'mp4':
        case 'mkv':
        case 'flv':
        case 'wmv':
        case 'webm':
        
        return true;
    }
    return false;
}


function submit_form_cover(){
    var cover = document.forms['cover']['image'].value;
    
    if (!isImage(cover)){
        document.forms['cover']['image'].style.background = '#c90a06';
        document.forms['cover']['image'].style.color = '#ffffff';
        $('#photo_error').fadeIn();
    }
    else{
        $('#photo_error').hide();
        document.forms['cover']['image'].style.background = '#647164';
        document.forms['cover']['image'].style.color = '#ffffff';
        $('#cover_uploading').fadeIn();  
        document.forms['cover'].submit();
    }   
}



function submit_form_video(){
    var video = document.forms['video']['video_file'].value;
    
    if (!isVideo(video)){
        document.forms['video']['video_file'].style.background = '#c90a06';
        document.forms['video']['video_file'].style.color = '#ffffff';
        $('#video_error').fadeIn();
    }
    else{
        $('#video_error').hide();
        // Uploading and transcoding video        
        document.forms['video']['video_file'].style.background = '#647164';
        document.forms['video']['video_file'].style.color = '#ffffff';
        $('#video_transcoding').fadeIn();  
        document.forms['video'].submit();
    }   
}

function submit_form_synopsis(){
    var synopsis_regexp = /[\w\s\.,:;áéíóúÁÉÍÓÚàèìòùÀÈÌÒÙâêîôûÂÊÎÔÛÑñü¿\?¡!\-\(\)"\r]{1,1000}$/;
    var synopsis = document.forms['synopsis']['synopsis_field'].value;

    if (!synopsis_regexp.test(synopsis)){
        document.forms['synopsis']['synopsis_field'].style.background = '#c90a06';
        document.forms['synopsis']['synopsis_field'].style.color = '#ffffff'; 
        $('#synopsis_error').fadeIn();
    }
    else{
        document.forms['synopsis']['synopsis_field'].style.background = '#ffffff';
        document.forms['synopsis']['synopsis_field'].style.color = '#333333';
        $('#synopsis_error').hide(); 
        document.forms['synopsis'].submit();
    }
}

function check_price(price){
    var regexp = /^\d+([\.,]\d{1,2})?$/;
    
    if (regexp.test(price.value)){
        if (price.value < 0.99 || price.value > 100.00){
            document.forms['misc']['price'].style.background = '#c90a06';
            document.forms['misc']['price'].style.color = '#ffffff'; 
            $('#price_error').fadeIn();
            return 0;
        }
        else{
            document.forms['misc']['price'].style.background = '#ffffff';
            document.forms['misc']['price'].style.color = '#333333';
            $('#price_error').hide();
            return 1;
        }
    }
    else{
        document.forms['misc']['price'].style.background = '#c90a06';
        document.forms['misc']['price'].style.color = '#ffffff'; 
        $('#price_error').fadeIn();
        return 0;
    }
}


function submit_misc(){
    var price = document.forms['misc']['price'];

    if (check_price(price)){
        document.forms['misc'].submit();
    }
}


// Show placeholder as field value on quotes and presentations when focused:
function show_placeholder(element){
    $(element).val($(element).attr('placeholder'));
}


