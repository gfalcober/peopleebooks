from django.db import models
from decimal import Decimal 
from string import lower
from django.template.defaultfilters import slugify
from django.contrib.contenttypes.models import ContentType
#from Main.extra import ContentTypeRestrictedFileField
                
#-------------------------------------------------------------------------------

def get_path(instance, filename):
    # Supported formats:
    vid_formats = ['mp4', 'avi', 'flv', 'wmv', 'mkv', 'webm']
    content_type = ContentType.objects.get_for_model(instance)

    fname, dot, extension = filename.rpartition('.')
    extension = extension.lower()
    
    if content_type.model == 'book':
        slug = slugify(instance.title)
        
        
        path = ''
        if (extension == 'jpg') or (extension == 'jpeg') or (extension == 'png'):
            path = u'img/books/'
            file_name = unicode('%s.%s' % (slug, extension))
        elif extension in vid_formats:
            path = u'vid/books/'
            file_name = unicode('%s.webm' % slug)
        elif (extension == 'pdf'):
            path = u'books/'
            file_name = unicode('%s.pdf' % slug)
        elif (extension == 'epub'):
            path = u'books/'
            file_name = unicode('%s.epub' % slug)
    elif (content_type.model == 'users'):
        slug = slugify(instance.full_name)
        
        
        if (extension == 'jpg') or (extension == 'jpeg') or (extension == 'png'):
            path = u'img/users/'
            file_name = unicode('%s.%s' % (slug, extension))
        elif extension in vid_formats:
            path = u'vid/users/'
            file_name = unicode('%s.webm' % slug)
         
    if path == '': return '/dev/null'
    else: return path + file_name            


#-------------------------------------------------------------------------------
# Values with 'editable=True' must be 'False' in production

class Language(models.Model):
    language_id = models.AutoField(primary_key=True)
    language_en = models.CharField(max_length=20, editable=True)
    language_es = models.CharField(max_length=20, editable=True)
    
    def __unicode__(self):
        return self.language_es
    
    class Meta:
        db_table = 'languages'

class Country(models.Model):
    iso = models.CharField(max_length=2, unique=True, editable=True)
    country_id = models.AutoField(primary_key=True)
    country_es = models.CharField(max_length=40, editable=True)
    country_en = models.CharField(max_length=40, editable=True)
    #language_id = models.ForeignKey(Language, editable=True)
    
    def __unicode__(self):
        return self.country
    
    class Meta:
        db_table = 'countries'

class Gender(models.Model):
    gender_id = models.AutoField(primary_key=True)
    gender_en = models.CharField(max_length=20, unique=True, editable=True)
    gender_es = models.CharField(max_length=20, unique=True, editable=True)
    
    def __unicode__(self):
        return self.gender_es
    
    class Meta:
        db_table = 'genders'

class Rate(models.Model):
    rate_id = models.AutoField(primary_key=True)
    rate_en = models.CharField(max_length=20, editable=True)
    rate_es = models.CharField(max_length=20, editable=True)
    
    def __unicode__(self):
        return self.rate_es
    
    class Meta:
        db_table = 'rates'

class Users(models.Model):
    user = models.CharField(primary_key=True, max_length=55) #For use in URLs. Remove special chars and turn spaces into '-' (10 chars more)
    full_name = models.CharField(unique=True, max_length=55) # +15 due to special chars in unicode may be stored as more than 1 char. Further analysis required.
    email = models.EmailField(unique=True)
    passwd = models.CharField(max_length=30)                  # Encryption?
    country = models.ForeignKey(Country, blank=True, null=True)
    signup_date = models.DateField(auto_now_add=True, editable=False)
    is_author = models.BooleanField(default=0, editable=True) # MySQL stores this as either 1 or 0, not 'True' or 'False'. Watch out!
    sales = models.PositiveIntegerField(default=0, editable=True)  # Increment on every sale. User must have 'is_author=1'
    purchases = models.PositiveIntegerField(editable=True)
    account_active = models.BooleanField(editable=False, default=0) # MySQL: True = 1 and False = 0!
    hashes = models.CharField(max_length=320)
    wants_mail_notifications = models.BooleanField(default=1, editable=True)
    current_balance = models.DecimalField(max_digits=64, decimal_places=2, default=0)
    total_profits = models.DecimalField(max_digits=64, decimal_places=2, default=0)
    money_withdrawn = models.DecimalField(max_digits=64, decimal_places=2, default=0)
    # Optional values:
    website = models.URLField(blank=True)
    facebook = models.URLField(blank=True)
    twitter = models.URLField(blank=True)
    quote = models.CharField(max_length=300, blank=True) # +100 due to special chars in unicode may be stored as more than 1 char. Further analysis required.
    presentation = models.TextField(max_length=1400, blank=True) # +400 due to special chars in unicode may be stored as more than 1 char. Further analysis required.
    photo = models.ImageField(upload_to=get_path, blank=True)
    video = models.FileField(upload_to=get_path, blank=True) 
        
    
    def __unicode__(self):
        return self.full_name
    
    class Meta:
        db_table = 'users'

class Book(models.Model):
    book = models.CharField(primary_key=True, max_length=150) #For use in URLs.    
    title = models.CharField(max_length=150) # +50 due to special chars in unicode may be stored as more than 1 char. Further analysis required.
    synopsis = models.TextField(max_length=1400) # +400 due to special chars in unicode may be stored as more than 1 char. Further analysis required.
    user = models.ForeignKey(Users)
    language_id = models.ForeignKey(Language)
    book_file_epub = models.FileField(upload_to=get_path, blank=True, max_length=150)
    book_file_pdf = models.FileField(upload_to=get_path, blank=True, max_length=150)
    cover = models.ImageField(upload_to=get_path, max_length=200)
    publication_date = models.DateField(auto_now_add=True, editable=False)
    gender_id = models.ForeignKey(Gender)
    sales = models.PositiveIntegerField(editable=True)
    rate_id = models.ForeignKey(Rate)
    price = models.DecimalField(max_digits=64, decimal_places=2, default=Decimal('0.99'))
    book_active = models.BooleanField(editable=False, default=True)
    
    # Optional values:
    website = models.URLField(blank=True)
    video = models.FileField(upload_to=get_path, blank=True, max_length=200) 
       
    def __unicode__(self):
        return self.title
    
    class Meta:
        db_table = 'books'
        
class Book_Comment(models.Model):
    book = models.ForeignKey(Book)
    user = models.ForeignKey(Users)
    comment = models.TextField(max_length=600)
    
    def __unicode__(self):
        return unicode(self.book_id)
        
    class Meta:
        db_table = 'book_comments'
        
class User_Comment(models.Model):
    user_about = models.ForeignKey(Users, related_name='+')  # User the comment is written about
    user_who = models.ForeignKey(Users)                  # User who writes the comment
    comment = models.TextField(max_length=600)
    
    def __unicode__(self):
        return unicode(self.uid)
        
    class Meta:
        db_table = 'user_comments'


#-------------------------------------------------------------------------------
# PayPal IPN
#-------------------------------------------------------------------------------
class PayPal_IPN(models.Model):
    business = models.CharField(max_length=127)
    receiver_email = models.EmailField(max_length=127)
    invoice = models.AutoField(primary_key=True)
    cmd = models.CharField(max_length=18, blank=True)
    item_number_1 = models.CharField(max_length=100, blank=True)  # Watch out! This should always be 'non null', but since it is only used in backend it's left as non mandatory to avoid useless overhead (in 'paypal_ipn')
    item_number_2 = models.CharField(max_length=100, blank=True)
    item_number_3 = models.CharField(max_length=100, blank=True)
    item_number_4 = models.CharField(max_length=100, blank=True)
    item_number_5 = models.CharField(max_length=100, blank=True)
    item_number_6 = models.CharField(max_length=100, blank=True)
    item_number_7 = models.CharField(max_length=100, blank=True)
    item_number_8 = models.CharField(max_length=100, blank=True)
    item_number_9 = models.CharField(max_length=100, blank=True)
    item_number_10 = models.CharField(max_length=100, blank=True)
    residence_country = models.CharField(max_length=2, blank=True)
    test_ipn = models.BooleanField(default=False, blank=True)
    txn_id = models.CharField(max_length=19)
    txn_type = models.CharField(max_length=128)
    payer_email = models.CharField(max_length=127, blank=True)
    mc_currency = models.CharField(max_length=32, default="EUR")
    mc_gross = models.DecimalField(max_digits=64, decimal_places=2, default=0.99)
    num_cart_items = models.IntegerField(default=0, null=True)
    payment_date = models.DateTimeField()
    payment_status = models.CharField(max_length=9)
    payment_type = models.CharField(max_length=7)
    
    def __unicode__(self):
        return unicode(self.txn_id)
    
    class Meta:
        db_table = 'paypal_ipn'


class Purchase(models.Model):
    operation_id = models.AutoField(primary_key=True)
    book = models.ForeignKey(Book)
    user = models.ForeignKey(Users, blank=True, null=True)          # User who purchased the book, 'null' if non registered.
    value = models.DecimalField(max_digits=64, decimal_places=2, default=0.99)   # Total price of purchase
    operation_date = models.DateField(auto_now_add=True, editable=False)
    invoice = models.ForeignKey(PayPal_IPN, to_field='invoice')
        
    def __unicode__(self):
        return unicode(self.operation_id)
    
    class Meta:
        db_table = 'purchases'

class Withdrawal(models.Model):
    operation_id = models.AutoField(primary_key=True)
    operation_date = models.DateField(auto_now_add=True, editable=False)
    user = models.ForeignKey(Users)
    amount = models.DecimalField(max_digits=64, decimal_places=2, default=0.99)
    payment_account = models.CharField(max_length=75)
    hashes = models.CharField(max_length=320)
    
    def __unicode__(self):
        return unicode(self.operation_id)
    
    class Meta:
        db_table = 'withdrawals'


