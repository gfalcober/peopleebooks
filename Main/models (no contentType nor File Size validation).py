from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.contenttypes.models import ContentType


#-------------------------------------------------------------------------------
#Previous one, now replaced by 'get_path':
#def filename(instance, filename):
#    fname, dot, extension = filename.rpartition('.')
#    slug = slugify(instance.title)
#    return '%s.%s' % (slug, extension) 

def get_path(instance, filename):
    content_type = ContentType.objects.get_for_model(instance)

    fname, dot, extension = filename.rpartition('.')
    slug = slugify(instance.title)
    file_name = unicode('%s.%s' % (slug, extension))
    
    if content_type.model == 'book':
        if (extension == 'jpg') or (extension == 'png'):
            path = u'img/books/'
        elif (extension == 'mp4') or (extension == 'avi'):
            path = u'vid/books/'
        elif (extension == 'pdf') or (extension == 'epub'):
            path = u'books/'
    elif (content_type.model == 'user'):
        if (extension == 'jpg') or (extension == 'jpeg') or (extension == 'png'):
            path = u'img/users/'
        elif (extension == 'mp4') or (extension == 'avi'):
            path = u'vid/users/'
                
    return path + file_name            
    
#-------------------------------------------------------------------------------

# Values with 'editable=True' must be 'False' in production

class Language(models.Model):
    language_id = models.AutoField(primary_key=True)
    language_en = models.CharField(max_length=20, editable=True)
    language_es = models.CharField(max_length=20, editable=True)
    
    def __unicode__(self):
        return self.language_es
    
    class Meta:
        db_table = 'languages'

class Country(models.Model):
    iso = models.CharField(max_length=2, unique=True, editable=True)
    country_id = models.AutoField(primary_key=True)
    country = models.CharField(max_length=40, editable=True)
    language_id = models.ForeignKey(Language, editable=True)
    
    def __unicode__(self):
        return self.country
    
    class Meta:
        db_table = 'countries'

class Gender(models.Model):
    gender_id = models.AutoField(primary_key=True)
    gender_en = models.CharField(max_length=20, unique=True, editable=True)
    gender_es = models.CharField(max_length=20, unique=True, editable=True)
    
    def __unicode__(self):
        return self.gender_es
    
    class Meta:
        db_table = 'genders'

class Rate(models.Model):
    rate_id = models.AutoField(primary_key=True)
    rate_en = models.CharField(max_length=20, editable=True)
    rate_es = models.CharField(max_length=20, editable=True)
    
    def __unicode__(self):
        return self.rate_es
    
    class Meta:
        db_table = 'rates'

class Users(models.Model):
    user = models.CharField(primary_key=True, max_length=50) #For use in URLs. Just remove special chars (accents, '-') and ' '. In forms.
    full_name = models.CharField(unique=True, max_length=50)
    email = models.EmailField(unique=True)
    passwd = models.CharField(max_length=30)                  # Encryption?
    country = models.ForeignKey(Country, blank=True)
    signup_date = models.DateField(auto_now_add=True, editable=False)
    is_author = models.BooleanField(default=0, editable=True) # MySQL stores this as either 1 or 0, not 'True' or 'False'. Watch out!
    sales = models.PositiveIntegerField(default=0, editable=True)  # Increment on every sale. User must have 'is_author=1'
    purchases = models.PositiveIntegerField(editable=True)
    account_active = models.BooleanField(editable=False, default=0) # MySQL: True = 1 and False = 0!
    hashes = models.CharField(max_length=320)
    wants_mail_notifications = models.BooleanField(default=1, editable=True)
    # Optional values:
    address = models.CharField(max_length=50, blank=True)
    city = models.CharField(max_length=40, blank=True)
    website = models.URLField(blank=True)
    facebook = models.URLField(blank=True)
    twitter = models.URLField(blank=True)
    quote = models.CharField(max_length=200, blank=True)
    presentation = models.TextField(max_length=600, blank=True)
    photo = models.ImageField(upload_to='Main/static/img/users', blank=True)         # Review!
    video = models.FileField(upload_to='Main/static/vid/users', blank=True)          # Review!
    
    
    def __unicode__(self):
        return self.full_name
    
    class Meta:
        db_table = 'users'

class Book(models.Model):
    book = models.CharField(primary_key=True, max_length=100) #For use in URLs.    
    title = models.CharField(max_length=100)
    synopsis = models.TextField(max_length=1000)
    user = models.ForeignKey(Users)
    language_id = models.ForeignKey(Language)
    book_file = models.FileField(upload_to=get_path)
    cover = models.ImageField(max_length=10000, upload_to=get_path)  # Is that a top of 10KB? Review! blank=False when in production! Filename must be user_id
    publication_date = models.DateField(auto_now_add=True, editable=False)
    gender_id = models.ForeignKey(Gender)
    sales = models.PositiveIntegerField(editable=True)
    rate_id = models.ForeignKey(Rate)
    price = models.CharField(max_length=6, default='0.99')
    # Optional values:
    video = models.FileField(upload_to=get_path, blank=True)        # Review!
       
    def __unicode__(self):
        return self.title
    
    class Meta:
        db_table = 'books'
        
class Book_Comment(models.Model):
    book = models.ForeignKey(Book)
    user = models.ForeignKey(Users)
    comment = models.TextField(max_length=600)
    
    def __unicode__(self):
        return unicode(self.book_id)
        
    class Meta:
        db_table = 'book_comments'
        
class User_Comment(models.Model):
    user_about = models.ForeignKey(Users, related_name='+')  # User the comment is written about
    user_who = models.ForeignKey(Users)                  # User who writes the comment
    comment = models.TextField(max_length=600)
    
    def __unicode__(self):
        return unicode(self.uid)
        
    class Meta:
        db_table = 'user_comments'

class Purchase(models.Model):
    operation_id = models.AutoField(primary_key=True)
    book = models.ForeignKey(Book)
    user = models.ForeignKey(Users, blank=True)          # User who purchased the book
    value = models.CharField(max_length=6)   # Total price of purchase
    operation_date = models.DateField(auto_now_add=True, editable=False)
        
    def __unicode__(self):
        return unicode(self.operation_id)
    
    class Meta:
        db_table = 'purchases'

