# coding: utf-8

from models import *
from forms import *
from Main.views import urlizer, login_user
from extra import img_resize
from django.conf import settings
from django.shortcuts import render_to_response, redirect, HttpResponseRedirect
from django.template import RequestContext
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
import base64


def blog(request, article=None): # article is optional, since we pass it with a default value (None)
    # Login stuff:
    user_logged_in = request.user.is_authenticated()
    if request.POST.get('username', 0) and not user_logged_in: 
        login_result = login_user(request)
        if login_result == 0:  # Login OK
            return HttpResponseRedirect(request.META['HTTP_REFERER'])   # Page where the user logged in from
        # login_result = 1 and 2 are handled in the 'navbar.html' template
        
    # Shopping cart in navbar:
    if (not request.COOKIES.get('cart', 0) or request.COOKIES.get('cart') == '_'): cart = 0
    else: cart = 1

    # Template metadata:
    og_url = 'https://' + request.META['HTTP_HOST'] + request.META['PATH_INFO']
    og_host = 'https://' + request.META['HTTP_HOST']

    if request.method == 'GET':
        search = 0
        if article: # A specific article was requested
            try:
                blog_entry = Blog_Entry.objects.get(blog_entry__iexact=article)
            except ObjectDoesNotExist:
                raise Http404

            if blog_entry.index == Blog_Entry.objects.all().order_by('-index')[:1][0].index: next = None # This is the latest article.
            else: # Not the latest, get the next one:
                next = Blog_Entry.objects.get(index=blog_entry.index+1)
            
            if blog_entry.index == 1: previous = None # This is the first article
            else: # Not the first, get the previous one:
                previous = Blog_Entry.objects.get(index=blog_entry.index-1)
            
        else:
            try:
                blog_entry = Blog_Entry.objects.values('blog_entry').order_by('-date')[:1]
                return redirect('/blog/' + blog_entry[0]['blog_entry'])
            except IndexError:
                raise Http404
    else: # Article search
        search = 1
        if len(request.POST.get('article_search')):
            blog_entry = Blog_Entry.objects.filter(tags__icontains=urlizer(unicode(request.POST['article_search']))).order_by('-date')
        else:
            blog_entry = Blog_Entry.objects.all().order_by('-date')

    return render_to_response('blog.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )


def blogger(request):
    if request.user.is_authenticated() and request.user.is_staff:
        if request.method == 'POST':
            tempdata = request.POST.copy()
            tempdata['blog_entry'] = urlizer(tempdata['title_en'])

            form = Publish_article(tempdata, request.FILES)  # Bound form to the POST(modified) data and files
            
            if form.is_valid():
                form.save()
                # Image resizing:
                path = Blog_Entry.objects.get(blog_entry=form.cleaned_data['blog_entry']).image.name
                fname, dot, extension = path.rpartition('.')
                status = img_resize(settings.MEDIA_ROOT + path, 0)

            return redirect('/blog')
        else:
            return render_to_response('blogger.html',
                                     locals(),
                                     context_instance=RequestContext(request),
                                 )
    else:
        return redirect('/', permanent='True')


def blog_preview(request):
    if request.user.is_authenticated() and request.user.is_staff and request.method == 'POST':
        title_es = request.POST.get('title_es')
        article_body_es = request.POST.get('article_body_es')
        image = base64.b64encode(request.FILES['image'].read())

        return render_to_response('blog_preview.html',
                                 locals(),
                                 context_instance=RequestContext(request),
                             )
    else:
        return redirect('/', permanent='True')

