from django.db.models import FileField
from django.forms import forms
#from django.template.defaultfilters import filesizeformat
#from django.utils.translation import ugettext_lazy as _
from subprocess import call
from os import error
import Image


# Image resize using PIL, for reducing bandwidth usage:
def img_resize(filepath, mode):
    width = 250
    height = 250
     
    img = Image.open(filepath)
    resized_image = img.resize((width, height), Image.ANTIALIAS)
    
    try:
        # Move original image to 'original' subdirectory. Directory must exist prior to this!: 
        path, slash, fname = filepath.rpartition('/')
        status = call(["cp", filepath, path + '/original/'])
        if not status:
            try:
                filename, dot, extension = fname.rpartition('.')
                resized_image.save(path + '/' + filename + '.jpg', 'JPEG')
            except EnvironmentError:
                status = 1
    except error:
        pass
    
    return status
    
