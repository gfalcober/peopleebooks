from Blog.models import Blog_Entry
from django import forms


class Publish_article(forms.ModelForm):
    
    class Meta:
        model = Blog_Entry
        fields = ('blog_entry', 'title_es', 'title_en', 'article_body_es', 'article_body_en', 'image', 'tags')

