from django.db import models
from string import lower
from django.template.defaultfilters import slugify

def file_save(instance, filename):
    # Supported formats:
    vid_formats = ['mp4', 'avi', 'flv', 'wmv', 'mkv', 'webm']
    fname, dot, extension = filename.rpartition('.')
    extension = extension.lower()
    
    slug = slugify(instance.title_en)                
    path = ''
    if (extension == 'jpg') or (extension == 'jpeg') or (extension == 'png'):
        path = u'blog/img/'
        file_name = unicode('%s.%s' % (slug, extension))
    elif extension in vid_formats:
        path = u'blog/vid/'
        file_name = unicode('%s.webm' % slug)        
         
    if path == '': return '/dev/null'
    else: return path + file_name
  

class Blog_Entry(models.Model):
    date = models.DateField(auto_now_add=True, editable=False)
    blog_entry = models.CharField(unique=True, max_length=50) #For use in URLs.
    title_es = models.CharField(max_length=50)
    title_en = models.CharField(max_length=50)
    article_body_es = models.TextField(max_length=10000)
    article_body_en = models.TextField(max_length=10000)
    image = models.ImageField(upload_to=file_save, max_length=200, blank=True)
    tags = models.CharField(max_length=40)
    index = models.AutoField(primary_key=True, editable=False)

    def __unicode__(self):
        return self.blog_entry
    
    class Meta:
        db_table = 'blog'


