# coding: utf-8

import unicodedata, string, Image, glob
from os import error
from subprocess import call


# Turn usernames into 'urlizable' strings (for usernames)
def urlizer(string):
    remove = ['.', ',', ':', ';', '?', '!', ' ', '"']
    ascii_string = unicodedata.normalize('NFKD', string.replace(' ', '-')).encode('ascii', 'ignore')
    for i in remove:
        if i in ascii_string:
            ascii_string = ascii_string.replace(i, '')
    return ascii_string


def img_resize(filepath, mode):
    if not mode: # Book cover
        width = 250
        height = 350
    else:  # User photo
        width = 300
        height = 300
     
    img = Image.open(filepath)
    resized_image = img.resize((width, height), Image.ANTIALIAS)
    
    try:
        # Move original image to 'original' subdirectory. Directory must exist prior to this!: 
        path, slash, fname = filepath.rpartition('/')
        status = call(["cp", filepath, path + 'original/' + fname])
        if not status:
            try:
                resized_image.save(path + fname[:-3].title() + 'jpg', 'JPEG')
                call(["rm", filepath])
            except EnvironmentError:
                status = 1
    except error:
        pass
    
    return status


written = 0
errors = []
files = glob.glob('*.png')
for i in files:
    try:
        img_resize('%s' % i, 0)
        written += 1
    except IOError:
        errors.append(i)


#-------------------------------------------------------------------------------
# Output info:
#-------------------------------------------------------------------------------
print('\n' + unicode(len(files)) + ' registros de entrada')
print(unicode(written) + ' registros de salida\n')
if len(errors): print 'REGISTROS OMITIDOS:'
for item in errors:
    print item, '\n'
print '\n'  

