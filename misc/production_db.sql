-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.66-0+squeeze1


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema db
--

CREATE DATABASE IF NOT EXISTS db;
USE db;

--
-- Definition of table `db`.`auth_group`
--

DROP TABLE IF EXISTS `db`.`auth_group`;
CREATE TABLE  `db`.`auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`auth_group`
--

/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
LOCK TABLES `auth_group` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;


--
-- Definition of table `db`.`auth_group_permissions`
--

DROP TABLE IF EXISTS `db`.`auth_group_permissions`;
CREATE TABLE  `db`.`auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_bda51c3c` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`auth_group_permissions`
--

/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
LOCK TABLES `auth_group_permissions` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;


--
-- Definition of table `db`.`auth_permission`
--

DROP TABLE IF EXISTS `db`.`auth_permission`;
CREATE TABLE  `db`.`auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_e4470c6e` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`auth_permission`
--

/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
LOCK TABLES `auth_permission` WRITE;
INSERT INTO `db`.`auth_permission` VALUES  (1,'Can add permission',1,'add_permission'),
 (2,'Can change permission',1,'change_permission'),
 (3,'Can delete permission',1,'delete_permission'),
 (4,'Can add group',2,'add_group'),
 (5,'Can change group',2,'change_group'),
 (6,'Can delete group',2,'delete_group'),
 (7,'Can add user',3,'add_user'),
 (8,'Can change user',3,'change_user'),
 (9,'Can delete user',3,'delete_user'),
 (10,'Can add content type',4,'add_contenttype'),
 (11,'Can change content type',4,'change_contenttype'),
 (12,'Can delete content type',4,'delete_contenttype'),
 (13,'Can add session',5,'add_session'),
 (14,'Can change session',5,'change_session'),
 (15,'Can delete session',5,'delete_session'),
 (16,'Can add site',6,'add_site'),
 (17,'Can change site',6,'change_site'),
 (18,'Can delete site',6,'delete_site'),
 (19,'Can add language',7,'add_language'),
 (20,'Can change language',7,'change_language'),
 (21,'Can delete language',7,'delete_language'),
 (22,'Can add country',8,'add_country'),
 (23,'Can change country',8,'change_country'),
 (24,'Can delete country',8,'delete_country'),
 (25,'Can add gender',9,'add_gender'),
 (26,'Can change gender',9,'change_gender'),
 (27,'Can delete gender',9,'delete_gender'),
 (28,'Can add rate',10,'add_rate'),
 (29,'Can change rate',10,'change_rate'),
 (30,'Can delete rate',10,'delete_rate'),
 (31,'Can add users',11,'add_users'),
 (32,'Can change users',11,'change_users'),
 (33,'Can delete users',11,'delete_users'),
 (34,'Can add book',12,'add_book'),
 (35,'Can change book',12,'change_book'),
 (36,'Can delete book',12,'delete_book'),
 (37,'Can add book_ comment',13,'add_book_comment'),
 (38,'Can change book_ comment',13,'change_book_comment'),
 (39,'Can delete book_ comment',13,'delete_book_comment'),
 (40,'Can add user_ comment',14,'add_user_comment'),
 (41,'Can change user_ comment',14,'change_user_comment'),
 (42,'Can delete user_ comment',14,'delete_user_comment'),
 (43,'Can add pay pal_ipn',15,'add_paypal_ipn'),
 (44,'Can change pay pal_ipn',15,'change_paypal_ipn'),
 (45,'Can delete pay pal_ipn',15,'delete_paypal_ipn'),
 (46,'Can add purchase',16,'add_purchase'),
 (47,'Can change purchase',16,'change_purchase'),
 (48,'Can delete purchase',16,'delete_purchase'),
 (49,'Can add withdrawal',17,'add_withdrawal'),
 (50,'Can change withdrawal',17,'change_withdrawal'),
 (51,'Can delete withdrawal',17,'delete_withdrawal'),
 (52,'Can add log entry',18,'add_logentry'),
 (53,'Can change log entry',18,'change_logentry'),
 (54,'Can delete log entry',18,'delete_logentry');
UNLOCK TABLES;
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;


--
-- Definition of table `db`.`auth_user`
--

DROP TABLE IF EXISTS `db`.`auth_user`;
CREATE TABLE  `db`.`auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=523 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`auth_user`
--

/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
LOCK TABLES `auth_user` WRITE;
INSERT INTO `db`.`auth_user` VALUES  (1,'gabriel.faet','','','gabriel.faet@451mood.com','pbkdf2_sha256$10000$eH8PXkYF1W7r$8m0izvuYZ7at+brj0dR3XMjyXXfv5Ipo9vupSbUuPZQ=',1,1,1,'2013-02-01 01:41:17','2013-01-31 12:28:20'),
 (2,'Munarriz','','','cecilucho@gmail.com','70cd6b93fd5520aa2a3cdcae12cd138f',0,1,0,'0001-01-01 00:00:00','2012-11-03 00:00:00'),
 (3,'--Pe--','','','angelfaet.professional@gmail.com','cee847a409bf85de48c6da9d5211abf0',0,1,0,'0001-01-01 00:00:00','2013-01-31 00:00:00'),
 (4,'Virgilio','','','angel.alvarez@kzgunea.net','f43f862b9bba44c31bfee5bed74bdc37',0,1,0,'0001-01-01 00:00:00','2012-12-13 00:00:00'),
 (5,'7Maresazules','','','7maresazules@gmail.com','71c94c598120b136777dedc82bdb4f9d',0,1,0,'0001-01-01 00:00:00','2012-11-21 00:00:00'),
 (6,'7Udp','','','eduard7_udp@hotmail.com','26269f9335264b744e92f5d1752e7ce6',0,1,0,'0001-01-01 00:00:00','2012-11-07 00:00:00'),
 (7,'Aaaaaaaaaaaaaaaaaa','','','aaaaaaaaaaaaaaaaaa@yopmail.com','e807f1fcf82d132f9bb018ca6738a19f',0,1,0,'0001-01-01 00:00:00','2012-07-31 00:00:00'),
 (8,'Abel-Nedi','','','abelnedi@hotmail.com','3563c254c77191775eb33b7762358fdd',0,1,0,'0001-01-01 00:00:00','2012-12-29 00:00:00'),
 (9,'Academia-Tucuman','','','jlcruz@telmex.net.co','b987fa7dfecd964bd90658c1be5d81dd',0,1,0,'0001-01-01 00:00:00','2012-11-10 00:00:00'),
 (10,'Adan-De-Mariass','','','adandemariass@outlook.com','dedf3fef451136e227a1b17c31933832',0,1,0,'0001-01-01 00:00:00','2012-11-01 00:00:00'),
 (11,'Adrian-Mendieta','','','ammcat_19@hotmail.com','79d8677aea3be01e53f34abe4b2b6835',0,1,0,'0001-01-01 00:00:00','2012-09-19 00:00:00'),
 (12,'Adrian-TM','','','adri951_8@hotmail.com','2d7d1ee657cb9954bbf360a7936c32ce',0,1,0,'0001-01-01 00:00:00','2012-08-14 00:00:00'),
 (13,'Adrian-Zabala','','','zabalamaderas@yahoo.com','958679e5a0e05d23d2137e22e6ae6aaa',0,1,0,'0001-01-01 00:00:00','2012-12-21 00:00:00'),
 (14,'Adriana-Paire','','','adrianapaire@hotmail.com','7f1c1f3b1afe362095971388b93767f1',0,1,0,'0001-01-01 00:00:00','2012-07-30 00:00:00'),
 (15,'Afonso','','','wiper@terra.com.br','a673a5696a2987c63f262b64bbc552c4',0,1,0,'0001-01-01 00:00:00','2012-08-18 00:00:00'),
 (16,'Agustina-Aguirre','','','agustinaaguirre_18@hotmail.com','c87de261f183ba681d2227051a514fc4',0,1,0,'0001-01-01 00:00:00','2012-09-06 00:00:00'),
 (17,'Aidatrujillo','','','aidatrujilloricart@gmail.com','0b7c62782b34f2883acf0ded79d4dd19',0,1,0,'0001-01-01 00:00:00','2013-01-04 00:00:00'),
 (18,'Akaro2009','','','akaro2009@gmail.com','50459ac585049673858229a78a038ce6',0,1,0,'0001-01-01 00:00:00','2012-11-05 00:00:00'),
 (19,'Albaarenas','','','biruxideal@gmail.com','7af384c41dfcd8a8a794b5e54c1ba88a',0,1,0,'0001-01-01 00:00:00','2012-10-28 00:00:00'),
 (20,'Albademoses','','','albademoses@gmail.com','7dfafb4dd7bf3d8623a35afc3fb0f9d6',0,1,0,'0001-01-01 00:00:00','2012-10-06 00:00:00'),
 (21,'Albenis-Andrade','','','albenisandrade@hotmail.com','945e8bb0491c591950dc663a4094c097',0,1,0,'0001-01-01 00:00:00','2012-11-04 00:00:00'),
 (22,'Alberto','','','albertoviana@adinet.com.uy','bb3edaf1f87e734fce2a1f587acc93bc',0,1,0,'0001-01-01 00:00:00','2012-10-02 00:00:00'),
 (23,'Alberto-Jimenez-Ure','','','urescritor@hotmail.com','2632fd8507966268317ac7e2972ea700',0,1,0,'0001-01-01 00:00:00','2012-11-05 00:00:00'),
 (24,'Alberto-Viana-Reyes','','','albertoviana42@hotmail.com','630880acd4a1f5f5a5da1abf576a835c',0,1,0,'0001-01-01 00:00:00','2012-11-13 00:00:00'),
 (25,'Albertoesparza','','','albertoesparza1@hotmail.com','88c889f70d4c91a43ff717db93ba1797',0,1,0,'0001-01-01 00:00:00','2012-10-30 00:00:00'),
 (26,'Aleida-Leyva-Garcia','','','aleidaleyva24@ymail.com','827ccb0eea8a706c4c34a16891f84e7b',0,1,0,'0001-01-01 00:00:00','2012-11-27 00:00:00'),
 (27,'Alejandro-Fuentes','','','afuentesmartinez.5@gmail.com','a7e43281c1e4e52ddae09d10d3244c56',0,1,0,'0001-01-01 00:00:00','2013-01-18 00:00:00'),
 (28,'Alejandro-Rosen','','','uterpandragon@hotmail.com','acacb1b5aebbe6c49e52fa5304a7c151',0,1,0,'0001-01-01 00:00:00','2012-08-02 00:00:00'),
 (29,'Alejandro-Sotodosos','','','sotowoods@gmail.com','827ccb0eea8a706c4c34a16891f84e7b',0,1,0,'0001-01-01 00:00:00','2012-07-30 00:00:00'),
 (30,'Alejo-Chavez','','','alejoc.209@gmail.com','d6b22962f13d03b1d32b5ffde94baa1a',0,1,0,'0001-01-01 00:00:00','2012-11-26 00:00:00'),
 (31,'Alex-Molina','','','alexis.molinaleyva@gmail.com','22ff0a5648f6a260649731e29ad868f8',0,1,0,'0001-01-01 00:00:00','2012-11-21 00:00:00'),
 (32,'Alexander-Ogma-Fenix','','','alexjota.89@hotmail.com','9790156c7ce0bc12a4cb69ffee3b671c',0,1,0,'0001-01-01 00:00:00','2012-08-01 00:00:00'),
 (33,'Alexandra-Pathz','','','paingenito@yahoo.com.ar','529b2929ee28ed3fb136551c02c4637c',0,1,0,'0001-01-01 00:00:00','2012-12-14 00:00:00'),
 (34,'Alexis-Gomez','','','gomezgeraldino@hotmail.com','ede38fa55713a98701f0a993e08722a8',0,1,0,'0001-01-01 00:00:00','2012-12-24 00:00:00'),
 (35,'Alfonso-Barreto','','','escriauto@yahoo.es','bf0cf5a99e3544796adb2a60d587bc58',0,1,0,'0001-01-01 00:00:00','2012-11-10 00:00:00'),
 (36,'Alfred1992','','','cristianalfredol78@gmail.com','12fa78eed30e8972ec4ab9816ebefe05',0,1,0,'0001-01-01 00:00:00','2012-12-17 00:00:00'),
 (37,'Alfredo-Ascanio','','','alfredo.ascanio@gmail.com','31a3d1a3248123cf13f94813e9ecf762',0,1,0,'0001-01-01 00:00:00','2012-11-18 00:00:00'),
 (38,'Alices1111','','','trader_onlines@hotmail.com','96e79218965eb72c92a549dd5a330112',0,1,0,'0001-01-01 00:00:00','2012-11-20 00:00:00'),
 (39,'Alicia-Elena','','','carola.uribec@gmail.com','65d41e6fc56294389ccd06a094c03802',0,1,0,'0001-01-01 00:00:00','2012-10-17 00:00:00'),
 (40,'Alinebusch','','','alinebusch12@gmail.com','f7279e850b4c3d7621d05d004f787b31',0,1,0,'0001-01-01 00:00:00','2012-11-29 00:00:00'),
 (41,'Almotasim','','','oscardgp@gmail.com','0aa0b6b3207f0b3839381db1962574a2',0,1,0,'0001-01-01 00:00:00','2012-09-24 00:00:00'),
 (42,'Aluna','','','antluna@gmail.com','435e2393ee9da1e87b797b8d45ab5c4e',0,1,0,'0001-01-01 00:00:00','2013-01-01 00:00:00'),
 (43,'Alvarohart','','','alv_hp@hotmail.com','231c1819a60f501bcbb6d799106dca8a',0,1,0,'0001-01-01 00:00:00','2012-11-19 00:00:00'),
 (44,'Aly-L-Silva','','','eals_99@hotmail.com','f5b8d9834f321ee2ca1904548a4a7f04',0,1,0,'0001-01-01 00:00:00','2012-10-26 00:00:00'),
 (45,'Amalia','','','amalia_lasilver@hotmail.com','7d6ecacbbd84bb6e0d32a4577881196e',0,1,0,'0001-01-01 00:00:00','2012-10-16 00:00:00'),
 (46,'Amor-Aouini','','','clubsastre@hotmail.com','c14435078d1337c367f102c0d6e24371',0,1,0,'0001-01-01 00:00:00','2012-11-14 00:00:00'),
 (47,'Anafaet','','','anafaet@hotmail.com','00859217940bfa505506daee576dca54',0,1,0,'0001-01-01 00:00:00','2012-07-31 00:00:00'),
 (48,'Anais-Hidalgo','','','anais_barbie_estrella@hotmail.com','4924479f4ef3a0de1c2f275bae7737d8',0,1,0,'0001-01-01 00:00:00','2012-12-21 00:00:00'),
 (49,'Anderson-Arevalo','','','produccionesaa100@hotmail.com','041b75c41352621c17b187e2a0d4631e',0,1,0,'0001-01-01 00:00:00','2012-09-29 00:00:00'),
 (50,'Andreina','','','laprinsecitabella2010@hotmail.com','703042aefd627a8c86c4de140cc80c6e',0,1,0,'0001-01-01 00:00:00','2012-10-17 00:00:00'),
 (51,'Angel-Borges','','','angelborges1986@gmail.com','4a0a1abca09e892299ae8ef662ba38e2',0,1,0,'0001-01-01 00:00:00','2012-08-01 00:00:00'),
 (52,'Angel-Faet','','','unhdlt@gmail.com','pbkdf2_sha256$10000$0KvtGMsei436$q/XKuTV9oTIGjIVRgGUKtF8/iplIBXXOKFa6Tb7opdE=',0,1,0,'2013-02-05 22:53:40','2012-07-29 00:00:00'),
 (53,'Angelvida1','','','hernandezexx@hotmail.com','b64932ae3171e88a89b8760805b05de8',0,1,0,'0001-01-01 00:00:00','2012-12-10 00:00:00'),
 (54,'Angie-Zelada','','','marianzelguz@hotmail.com','9c87400128d408cdcda0e4b3ff0e66fa',0,1,0,'0001-01-01 00:00:00','2012-11-11 00:00:00'),
 (55,'Anny','','','aebag@hotmail.com','936d4b43209e2e5ae021e2ba8bc87389',0,1,0,'0001-01-01 00:00:00','2012-10-05 00:00:00'),
 (56,'Anny-P','','','anny.palavecino@hotmail.com','4475e835165a1f524432526ef4875274',0,1,0,'0001-01-01 00:00:00','2012-12-04 00:00:00'),
 (57,'Antillonf','','','capellof@yahoo.com','304e4ca57a6848aaccfa49c03ca17334',0,1,0,'0001-01-01 00:00:00','2012-08-04 00:00:00'),
 (58,'Antoine-Nolla','','','hola@antoine-nolla.es','827ccb0eea8a706c4c34a16891f84e7b',0,1,0,'0001-01-01 00:00:00','2012-07-30 00:00:00'),
 (59,'Antonio-De-Chencha','','','antonio-gonzalez-@hotmail.com','b7f7fad57c36b290125b4b5285f2c3a3',0,1,0,'0001-01-01 00:00:00','2012-10-31 00:00:00'),
 (60,'Antonio-Guerra-Colon','','','tonyvidadeseamas@yahoo.com','468117f7e7b57b7a857a728bcb3ac385',0,1,0,'0001-01-01 00:00:00','2012-11-03 00:00:00'),
 (61,'Antonio-R-Mota-G','','','antoniomota3@hotmail.com','6225b2279d852724b0290b4c24c5d204',0,1,0,'0001-01-01 00:00:00','2012-11-29 00:00:00'),
 (62,'Antonio-Toro','','','pcstuffandtools@gmail.com','cda5565b6bfd12e535846c71186bbcb6',0,1,0,'0001-01-01 00:00:00','2012-11-29 00:00:00'),
 (63,'Aosara','','','saritacosita_17@hotmail.com','e0dfe2c1c5938a83a12eb0eeacf2725c',0,1,0,'0001-01-01 00:00:00','2012-11-14 00:00:00'),
 (64,'Aqueos','','','aqueos@yahoo.es','8b038e3dc591f02f9bd83686f33c9264',0,1,0,'0001-01-01 00:00:00','2012-11-07 00:00:00'),
 (65,'Arami','','','aramiarza@hotmail.com','50d3d056bd5bd0d559ae5755121ab35d',0,1,0,'0001-01-01 00:00:00','2012-12-05 00:00:00'),
 (66,'Argelia','','','zihuacuani@hotmail.com','9b802a4b8e9ea8e897deaf2d2fb6f744',0,1,0,'0001-01-01 00:00:00','2013-01-03 00:00:00'),
 (67,'Armando-Nar-Alsina','','','naralsina_2@hotmail.com','ee8406b40b0d49cdffe3e9f6e6634fe9',0,1,0,'0001-01-01 00:00:00','2012-11-02 00:00:00'),
 (68,'Asdf22','','','angelafernandezbrito@gmail.com','36388794be2cf5f298978498ff3c64a2',0,1,0,'0001-01-01 00:00:00','2012-08-21 00:00:00'),
 (69,'Asdre123','','','andreasmassip@live.fr','e10adc3949ba59abbe56e057f20f883e',0,1,0,'0001-01-01 00:00:00','2012-12-11 00:00:00'),
 (70,'Avelino-Melendez','','','linomelvin@hotmail.com','7a75def1792c37ecd126c6d28c9d0312',0,1,0,'0001-01-01 00:00:00','2012-08-06 00:00:00'),
 (71,'Babo','','','alangc2000@hotmail.com','e61df301d164c28aad9ba59a0439d787',0,1,0,'0001-01-01 00:00:00','2012-11-17 00:00:00'),
 (72,'Beatriz_Perez','','','beatriz_perezmoya@hotmail.com','7a89e835d5ffb0c193d9286d3c7580c4',0,1,0,'0001-01-01 00:00:00','2012-08-02 00:00:00'),
 (73,'Beida-R-Vega-R','','','beidavega@hotmail.com','4315384bd72691c4e81389e0ce5b5f13',0,1,0,'0001-01-01 00:00:00','2012-11-30 00:00:00'),
 (74,'BerlinaAzn','','','alvarozambranonovoa@gmail.com','09287bf92f456a2b7ad60bdab5865d64',0,1,0,'0001-01-01 00:00:00','2012-10-30 00:00:00'),
 (75,'Bernapuerta','','','castrillonpuertabernardo08@gmail.com','d9552aca953c1afe506d3639c9a489d2',0,1,0,'0001-01-01 00:00:00','2013-01-03 00:00:00'),
 (76,'Bernardo-Pereira','','','b.pereira.garcia@euskaltel.net','7df667804ad2d8af2a3c861dbd3d49fa',0,1,0,'0001-01-01 00:00:00','2012-09-03 00:00:00'),
 (77,'Berthier','','','berthiercorinne256@yahoo.fr','b29af47e38864c60fa81158291fe40a1',0,1,0,'0001-01-01 00:00:00','2012-12-20 00:00:00'),
 (78,'Betina1993','','','isabelladiazbermeo@hotmail.com','440ea58142019f42b85c7ba2136f491b',0,1,0,'0001-01-01 00:00:00','2012-12-06 00:00:00'),
 (79,'Bichorevoltoso','','','tenfesanz@gmail.com','a16df7a8c165fbfb3dc794085a0a9022',0,1,0,'0001-01-01 00:00:00','2012-08-06 00:00:00'),
 (80,'Blanquita','','','chispi231@hotmail.com','0a18fffa2242974b2a842f068cddfbb6',0,1,0,'0001-01-01 00:00:00','2012-08-03 00:00:00'),
 (81,'Blueyes','','','ptms07@gmail.com','bc369b0c87ab5a3f47dd4d2152a71064',0,1,0,'0001-01-01 00:00:00','2012-11-26 00:00:00'),
 (82,'Bradiely-Aguilare','','','bradiely.aguilare@gmail.com','fad30812a2e7c15748fd3de06b616e0c',0,1,0,'0001-01-01 00:00:00','2012-11-30 00:00:00'),
 (83,'Breno','','','sigmaster@ig.com.br','d87d12237bf06f3c19673752d0d3a9be',0,1,0,'0001-01-01 00:00:00','2012-09-14 00:00:00'),
 (84,'Buy123','','','buynowstoreltd@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99',0,1,0,'0001-01-01 00:00:00','2012-11-16 00:00:00'),
 (85,'Camila-Cordova','','','camu_yani@hotmail.com','d2099d7b4ea2c294b929c74f52f44d2d',0,1,0,'0001-01-01 00:00:00','2012-12-21 00:00:00'),
 (86,'Canetmart','','','canetmart@hotmail.com','e10adc3949ba59abbe56e057f20f883e',0,1,0,'0001-01-01 00:00:00','2012-09-03 00:00:00'),
 (87,'Carboledam','','','carlos.arboleda5@gmail.com','315f0956846c0bab4331cff92577b1b0',0,1,0,'0001-01-01 00:00:00','2012-12-26 00:00:00'),
 (88,'CarloFormisano','','','carlo.formisano@gmail.com','3c4da879a6c7fa0d2c22c698e14e43f4',0,1,0,'0001-01-01 00:00:00','2012-11-07 00:00:00'),
 (89,'Carlos--Jandres','','','carlosjandresbejarano@gmail.com','8621ffdbc5698829397d97767ac13db3',0,1,0,'0001-01-01 00:00:00','2012-12-09 00:00:00'),
 (90,'Carlos-De-Hernan','','','carlos_dehernan01@hotmail.com','500af16c688ec9ba097bb7a936d594f0',0,1,0,'0001-01-01 00:00:00','2012-08-02 00:00:00'),
 (91,'Carlos-Gamissans','','','carlos-albertogl@hotmail.com','858afe942825fce1a401d755139fb1a7',0,1,0,'0001-01-01 00:00:00','2012-07-30 00:00:00'),
 (92,'Carlos-Manuel','','','carlosmanuelgm@hotmail.com','e4f01e7c679b2cd2692ef2bacc0d5a31',0,1,0,'0001-01-01 00:00:00','2012-12-22 00:00:00'),
 (93,'Carlos-Mingorance','','','nirnel@hotmail.com','827ccb0eea8a706c4c34a16891f84e7b',0,1,0,'0001-01-01 00:00:00','2012-07-29 00:00:00'),
 (94,'Carlos-Pareja','','','carlosparejagonzalez@yahoo.es','2e5f1d377950934227a72485dbbe893d',0,1,0,'0001-01-01 00:00:00','2012-10-29 00:00:00'),
 (95,'Carmita-Ontaneda','','','caontaneda@hotmail.com','b98a5a57d055dbabf959dcd6f36509ef',0,1,0,'0001-01-01 00:00:00','2012-11-07 00:00:00'),
 (96,'Carolina-Gonzalez-S','','','cgstraduccioneschile@gmail.com','506d6300046745e301114857db5ba194',0,1,0,'0001-01-01 00:00:00','2012-12-10 00:00:00'),
 (97,'Cataloiu','','','cata_loiu@yahoo.com','db02cda1496afded3fe782a4e412525f',0,1,0,'0001-01-01 00:00:00','2012-11-22 00:00:00'),
 (98,'Cavalli','','','fcavalli@gmail.com','880e748c90fc57496bb9a1f5eab25735',0,1,0,'0001-01-01 00:00:00','2012-08-19 00:00:00'),
 (99,'Cece','','','carito.cg@hotmail.com','a9482718031761f3bdbc1f0fea60a95d',0,1,0,'0001-01-01 00:00:00','2012-09-27 00:00:00'),
 (100,'Cesar','','','k_a_zter@hotmail.com','6f597c1ddab467f7bf5498aad1b41899',0,1,0,'0001-01-01 00:00:00','2012-11-08 00:00:00'),
 (101,'Cesarpereda','','','gunepereda@gmail.com','12ad1214553426ff7537d40ba12b8849',0,1,0,'0001-01-01 00:00:00','2012-12-06 00:00:00'),
 (102,'Cezar','','','art.lit.cul@gmail.com','3f1dfe346b7c97d09681498e8629a014',0,1,0,'0001-01-01 00:00:00','2012-11-18 00:00:00'),
 (103,'Chabi-Angulo','','','javier.angulo.hernandez@gmail.com','490a70277e54ed304a46ba01ea4e1b7b',0,1,0,'0001-01-01 00:00:00','2012-09-09 00:00:00'),
 (104,'Chano-Castano','','','andres.castano123@gmail.com','ec32f2dc567b4a0090a2df5f824d7ae4',0,1,0,'0001-01-01 00:00:00','2012-09-18 00:00:00'),
 (105,'Chica-De-Cristal','','','martad_88@hotmail.es','fb435be3744390b470a0f1d4a5653a84',0,1,0,'0001-01-01 00:00:00','2012-09-17 00:00:00'),
 (106,'Christian-Calandra','','','christiancalandra@gmail.com','3931eb41f83ae5d177081de2f95fd659',0,1,0,'0001-01-01 00:00:00','2012-11-05 00:00:00'),
 (107,'Christian-Tiako','','','pitou2001fr@yahoo.fr','1e3b71bd0f402a7303dae4c109a69fcc',0,1,0,'0001-01-01 00:00:00','2013-01-24 00:00:00'),
 (108,'Churrisarah','','','churrisara@hotmail.com','3d53c68907dce655fb81a42e5627d427',0,1,0,'0001-01-01 00:00:00','2012-08-04 00:00:00'),
 (109,'Chusing','','','celtalibros.ec@gmail.com','42c3bc7d1c655a5d31878fafca8fc914',0,1,0,'0001-01-01 00:00:00','2012-08-25 00:00:00'),
 (110,'Ciatax11','','','princess_venazir@yahoo.com','e10adc3949ba59abbe56e057f20f883e',0,1,0,'0001-01-01 00:00:00','2012-12-06 00:00:00'),
 (111,'Cjleon','','','aunpasodelinfinito@gmail.com','d99655c8c5acdc83311fc0f27fa109d2',0,1,0,'0001-01-01 00:00:00','2012-09-25 00:00:00'),
 (112,'Clasicos','','','unhdlt.@gmail.com','9492f196cd1ebf564a789e45f0c8807a',0,1,0,'0001-01-01 00:00:00','2012-09-12 00:00:00'),
 (113,'Clin0813','','','mrtiti13@hotmail.com','760b8c9241ca3b6100a4ffe85117b1ed',0,1,0,'0001-01-01 00:00:00','2013-01-04 00:00:00'),
 (114,'Cmarbelis','','','geromarbe27@hotmail.es','eed79358f3fc3d19a36cc9404c1af603',0,1,0,'0001-01-01 00:00:00','2012-10-26 00:00:00'),
 (115,'Concienciadelcamino','','','concienciadelcamino@yahoo.com.ar','5d24859f3f9e85ea4b08de40f67b4111',0,1,0,'0001-01-01 00:00:00','2012-12-10 00:00:00'),
 (116,'Coronel','','','losluisesdecadiz@hotmail.com','dcb528af00b223b13db19a3b12fb743a',0,1,0,'0001-01-01 00:00:00','2012-08-26 00:00:00'),
 (117,'Criescace','','','restaurantelaspiletas@hotmail.es','3237aa07f8c31e89801d5f3de2bfb6e8',0,1,0,'0001-01-01 00:00:00','2012-08-09 00:00:00'),
 (118,'Cristian-Rada','','','cristiandavidradaher@hotmail.com','b8dcb8f172b6e1363d80fab84b883982',0,1,0,'0001-01-01 00:00:00','2012-09-29 00:00:00'),
 (119,'Cristina-Leiva','','','cristina.leiva2012@yahoo.es','b413d53dfbc5dbf5fe28105be5ed1568',0,1,0,'0001-01-01 00:00:00','2012-12-01 00:00:00'),
 (120,'Cristina-Zeta','','','cristina-zeta@hotmail.com','e03510d9eb76f580e070d52bf4ba2f8b',0,1,0,'0001-01-01 00:00:00','2012-10-29 00:00:00'),
 (121,'Croqueta','','','helenatejera@gmail.com','6c148d2d690dc0ec914ed0dcad9a918d',0,1,0,'0001-01-01 00:00:00','2012-11-02 00:00:00'),
 (122,'Cvredits','','','lineghilslaine9@yahoo.fr','21929bcae11fad459ad54d8bea3666ef',0,1,0,'0001-01-01 00:00:00','2012-12-12 00:00:00'),
 (123,'Cybernapya','','','cybernapya@gmail.com','716487a9245c90f82e96321c715f16e2',0,1,0,'0001-01-01 00:00:00','2012-08-14 00:00:00'),
 (124,'DamianAlegria','','','damian.alegria@gmail.com','04cf4fab50dc5510e812cb77d6c7c34f',0,1,0,'0001-01-01 00:00:00','2012-11-05 00:00:00'),
 (125,'Daniel-Hernandez','','','hdaniel121@hotmail.com','f1ff02c62c898fad57d1914385b736f0',0,1,0,'0001-01-01 00:00:00','2012-12-10 00:00:00'),
 (126,'Daniela-Luna','','','daniela_vega96@hotmail.com','ba8a48b0e34226a2992d871c65600a7c',0,1,0,'0001-01-01 00:00:00','2012-08-08 00:00:00'),
 (127,'Daniela_Micaela','','','solopersonal@hotmail.com.ar','2a14a1283a660e54faca1d13fe904863',0,1,0,'0001-01-01 00:00:00','2012-11-13 00:00:00'),
 (128,'Danielle-Cuppone','','','danni_poeta@hotmail.com','b70b0bcb678da8b844c7408bb2001449',0,1,0,'0001-01-01 00:00:00','2012-08-19 00:00:00'),
 (129,'Danilo-Umana-Sacasa','','','dusacasa@gmail.com','c322e42c391cb2560e481e9e1e33068b',0,1,0,'0001-01-01 00:00:00','2012-12-05 00:00:00'),
 (130,'David-Sanchez','','','davidsanz68@gmail.com','a8c1df51a8a989c3e4c3b9e248cd7b96',0,1,0,'0001-01-01 00:00:00','2012-11-15 00:00:00'),
 (131,'Denissecienfuegos','','','denisseprobable@gmail.com','8b484298260dc7365455b827490bd792',0,1,0,'0001-01-01 00:00:00','2012-08-31 00:00:00'),
 (132,'Dffigueroa','','','diegoffigueroa@gmail.com','ee64f2b3cfcfdb3cfa97f069afbb9884',0,1,0,'0001-01-01 00:00:00','2013-01-03 00:00:00'),
 (133,'Dhioyi','','','dhivi@hotmail.es','976f5b164ea58533a0cb1adbeb58d941',0,1,0,'0001-01-01 00:00:00','2012-11-15 00:00:00'),
 (134,'Dicc-Agro-Ganadero','','','hernia88@hotmail.com','913d231bed39b683f8b60621fee8d376',0,1,0,'0001-01-01 00:00:00','2012-12-10 00:00:00'),
 (135,'Divaldy','','','divaldy@msn.com','cb685e9c09abe4c253f9cf8d79d5097a',0,1,0,'0001-01-01 00:00:00','2012-12-11 00:00:00'),
 (136,'Djovenes','','','papirico26@hotmail.com','9728a4c86cfea37bba7085d655c2d4dc',0,1,0,'0001-01-01 00:00:00','2012-10-15 00:00:00'),
 (137,'Dominic','','','guadafues@hotmail.com','2d2df593391e0963f99737db7ee01190',0,1,0,'0001-01-01 00:00:00','2012-11-08 00:00:00'),
 (138,'Domusaurea','','','santiago.maspoch@yahoo.es','293d1d232fe899411b8e8ea45f0d8bbc',0,1,0,'0001-01-01 00:00:00','2012-08-03 00:00:00'),
 (139,'Dorian-Villa-S','','','dorianvilla1@gmail.com','30a0f82133dcce4f75c254e08dcaaade',0,1,0,'0001-01-01 00:00:00','2012-12-29 00:00:00'),
 (140,'Douglas','','','dk3chat@gmail.com','3b16dc694c38d04f7d7451cc37d3c654',0,1,0,'0001-01-01 00:00:00','2012-09-15 00:00:00'),
 (141,'Dowglasz','','','dowglasz@gmail.com','1819662bc51e6240a39ff54b533296a9',0,1,0,'0001-01-01 00:00:00','2012-09-04 00:00:00'),
 (142,'Dracox555','','','farben-5@hotmail.com','ba82a86d435eb71dbaf901a4158e59e4',0,1,0,'0001-01-01 00:00:00','2012-10-12 00:00:00'),
 (143,'Dragon_Literario','','','dragonne71@yahoo.com','35ad695f77004113affa202918783bfb',0,1,0,'0001-01-01 00:00:00','2012-12-14 00:00:00'),
 (144,'E065701','','','che_emily@hotmail.com','346aa94685fbce505a5cabbd5f27e375',0,1,0,'0001-01-01 00:00:00','2012-12-09 00:00:00'),
 (145,'Ecaf','','','edcastillof@hotmail.com','4da31245551b6efb3fe1146030e0fb08',0,1,0,'0001-01-01 00:00:00','2012-12-01 00:00:00'),
 (146,'Eddy-Leon','','','eddyleonb@gmail.com','7d6c7189189562a2680c0811d78fe8b2',0,1,0,'0001-01-01 00:00:00','2012-08-01 00:00:00'),
 (147,'Ediciones-Casas','','','edicionescasas@yahoo.es','827ccb0eea8a706c4c34a16891f84e7b',0,1,0,'0001-01-01 00:00:00','2012-10-29 00:00:00'),
 (148,'Edmirandao','','','edmirandao@gmail.com','7a639086fea61375fef56f4f9cea511d',0,1,0,'0001-01-01 00:00:00','2012-08-04 00:00:00'),
 (149,'Eduardo-Denunzio','','','eduardo_denunzio@hotmail.com.ar','a44aec14c6bc9570594695ec3664293d',0,1,0,'0001-01-01 00:00:00','2012-10-18 00:00:00'),
 (150,'Eduardogzava','','','carloseduardo5_11@hotmail.com','827ccb0eea8a706c4c34a16891f84e7b',0,1,0,'0001-01-01 00:00:00','2012-11-02 00:00:00'),
 (151,'Elena-Saldana-Ruiz','','','mgatogonz@gmail.com','7563687f72c1ae5c14a1e730d13fc1dc',0,1,0,'0001-01-01 00:00:00','2012-12-12 00:00:00'),
 (152,'Elenabr','','','elenabuilrobres@hotmail.es','3fb50837a8d8c2539ec0f42fafef153a',0,1,0,'0001-01-01 00:00:00','2012-10-24 00:00:00'),
 (153,'Elmer-Rojas','','','ehra1@yahoo.es','96e41a850cb70617c55d493ee47f1aa9',0,1,0,'0001-01-01 00:00:00','2012-11-03 00:00:00'),
 (154,'Emanuel','','','emanuel_delgado@hotmail.es','24313544194234b7be64b2fbbb654f90',0,1,0,'0001-01-01 00:00:00','2012-11-28 00:00:00'),
 (155,'Emilio-Tejera','','','emilio.tejera@gmail.com','b25f96e64d94d9ea27ec9a21f34a3cf5',0,1,0,'0001-01-01 00:00:00','2012-07-30 00:00:00'),
 (156,'Emma-Campbell','','','emmacalderin1975@hotmail.com','eec4bac1b710e01a5d026b0f08347876',0,1,0,'0001-01-01 00:00:00','2012-09-19 00:00:00'),
 (157,'Eric_Kirby','','','eric_kirby@hotmail.es','b08f953879f69b37f5d3a28dd3d09101',0,1,0,'0001-01-01 00:00:00','2012-08-12 00:00:00'),
 (158,'Erick','','','erick_seifert@hotmail.com','2366e7fa869fb576abf02fdd451e2a66',0,1,0,'0001-01-01 00:00:00','2012-08-10 00:00:00'),
 (159,'Ernesto-Ramirez','','','ernestrv@yahoo.es','827ccb0eea8a706c4c34a16891f84e7b',0,1,0,'0001-01-01 00:00:00','2012-07-30 00:00:00'),
 (160,'Esaud','','','abravo0005@gmail.com','bb82e080de9553fd1be4504f94f3e216',0,1,0,'0001-01-01 00:00:00','2012-11-04 00:00:00'),
 (161,'Escribidor-Humberto','','','vivebienysefeliz@hotmail.es','8445073bfd49470ab877751bc975ad25',0,1,0,'0001-01-01 00:00:00','2012-11-07 00:00:00'),
 (162,'Espepe','','','espepe@hotmail.com','65b50ac5ae825bb55032cbbe2087079b',0,1,0,'0001-01-01 00:00:00','2012-09-23 00:00:00'),
 (163,'Evaristo-A-Ujueta-A','','','evaristoujueta@hotmail.com','09f26c1d1aec02f6c00a3055542cd51d',0,1,0,'0001-01-01 00:00:00','2012-11-16 00:00:00'),
 (164,'Evodio-Perez-Castro','','','evodioperezcastro@hotmail.com','3302dd73fee6b7809575a5b74dfb710d',0,1,0,'0001-01-01 00:00:00','2012-10-27 00:00:00'),
 (165,'FGMota','','','francisco.g.mota@gmail.com','d34e1c1f8ebf8907d9b1320ccde15321',0,1,0,'0001-01-01 00:00:00','2013-01-03 00:00:00'),
 (166,'Facu-Russi','','','larba103@gmail.com','2da01da6fbb298153eaea232a6ea5a77',0,1,0,'0001-01-01 00:00:00','2012-11-10 00:00:00'),
 (167,'Fajardo','','','leoterraneo@gmail.com','4f5256a4f39ed06bb9840500f7a9bcf9',0,1,0,'0001-01-01 00:00:00','2012-11-30 00:00:00'),
 (168,'Fdragone','','','fdragonne@yahoo.com','fc0f7328d32494587bf5da727bb72f56',0,1,0,'0001-01-01 00:00:00','2013-01-02 00:00:00'),
 (169,'Felicidad','','','antonieta_mm@yahoo.com','1ce674a7b68083f065bb542429d77c7b',0,1,0,'0001-01-01 00:00:00','2012-11-29 00:00:00'),
 (170,'Fernad123','','','creditrapidos@gmail.com','ab4f63f9ac65152575886860dde480a1',0,1,0,'0001-01-01 00:00:00','2012-12-14 00:00:00'),
 (171,'Fernando','','','ferferpal@hotmail.com','937b5532eaf7ff12a6573901b6f16693',0,1,0,'0001-01-01 00:00:00','2012-11-09 00:00:00'),
 (172,'Fernando-Marttell-C','','','poescrit@live.cl','a750c2a93dcc44170f1a485ade2be871',0,1,0,'0001-01-01 00:00:00','2012-12-15 00:00:00'),
 (173,'Fernando-Romero-S','','','carlos.15401@hotmail.com','7c1e28ba674454b3bc3c4c266bace987',0,1,0,'0001-01-01 00:00:00','2013-01-08 00:00:00'),
 (174,'Fernando-Roque','','','arcanus.viritis@gmail.com','13e3b8be25be272e044d51162e6725c7',0,1,0,'0001-01-01 00:00:00','2012-08-26 00:00:00'),
 (175,'Fernando-Salvador','','','vueloaltoazul@gmail.com','295f99ba452446ed9ef38dc6f5d64461',0,1,0,'0001-01-01 00:00:00','2012-12-03 00:00:00'),
 (176,'Feysalud','','','feysalud@gmail.com','707d069a5d20de7153e7fbfef785a2fb',0,1,0,'0001-01-01 00:00:00','2013-01-07 00:00:00'),
 (177,'FinanciaPrestito','','','financia.prestito28@gmail.com','566b41e1291696f6871e6a56e3446d60',0,1,0,'0001-01-01 00:00:00','2012-12-11 00:00:00'),
 (178,'Fjpago','','','fjpago@hotmail.com','d2bfc2d233186184bfd5c194da4ad59a',0,1,0,'0001-01-01 00:00:00','2012-12-10 00:00:00'),
 (179,'Foreverjamas','','','forever.jamas@outlook.com','c3d6aa12143e827d8963553eedfa90ab',0,1,0,'0001-01-01 00:00:00','2012-09-06 00:00:00'),
 (180,'Francis','','','fsolis@sima88.com.pa','e10adc3949ba59abbe56e057f20f883e',0,1,0,'0001-01-01 00:00:00','2012-12-12 00:00:00'),
 (181,'Francisco-Benitez','','','salem8250@hotmail.com','128afe59be4e3725816d2f5040e00cea',0,1,0,'0001-01-01 00:00:00','2012-12-30 00:00:00'),
 (182,'Frank-Losa-Aguila','','','editor1@spicm.cfg.sld.cu','3ff22e67f3b17ebd878b8712e949aaa8',0,1,0,'0001-01-01 00:00:00','2012-07-31 00:00:00'),
 (183,'Franklinlizano','','','franklinlizano7@hotmail.com','7b28bbf7ea72df010a32658c5d1f7af6',0,1,0,'0001-01-01 00:00:00','2012-12-06 00:00:00'),
 (184,'Fray-J-Granado-M','','','fray.granado@gmail.com','80c94c09453dfe07681fde78e769353f',0,1,0,'0001-01-01 00:00:00','2012-12-06 00:00:00'),
 (185,'Freddy-Marcial','','','freddymarcial@gmail.com','990da244c22daa921e6fcbe58f0ac904',0,1,0,'0001-01-01 00:00:00','2013-01-08 00:00:00'),
 (186,'Fredymido','','','fredymido@gmail.com','49d5860180efadf44163e68e97709be5',0,1,0,'0001-01-01 00:00:00','2012-12-13 00:00:00'),
 (187,'Fvacasg','','','fvacas_gonzales@hotmail.com','6a5631c94912bfd498b09701e3f12b3a',0,1,0,'0001-01-01 00:00:00','2012-11-19 00:00:00'),
 (188,'Gabriel','','','gabriel_gallardo_lozano@yahoo.es','acb40a3afefd8681a6d10abf80be24d4',0,1,0,'0001-01-01 00:00:00','2012-08-20 00:00:00'),
 (189,'Gabriel-Faet','','','gfalcober@gmail.com','pbkdf2_sha256$10000$DuQuZcRphdJG$vdRvm+JTO3OFJEhfv0PAs6x3o5p2IjBOWhIb4SIvJik=',0,1,0,'2013-02-05 22:56:24','2012-07-31 00:00:00'),
 (190,'Gabrieldavalos','','','gabriel.td@hotmail.com','4419fa317021b410a098435661e37f54',0,1,0,'0001-01-01 00:00:00','2012-08-06 00:00:00'),
 (191,'Galicamilo','','','evildark1939@gmail.com','e807f1fcf82d132f9bb018ca6738a19f',0,1,0,'0001-01-01 00:00:00','2012-08-13 00:00:00'),
 (192,'Gammaleuka','','','gammaleuka@gmail.com','ff84a9f3c0d335d25413dd8dfc9b2e8f',0,1,0,'0001-01-01 00:00:00','2012-08-19 00:00:00'),
 (193,'Gaviota-Viajera','','','anitaperu21@gmail.com','75fe5d5a2b243b5f9a1c391eb7c22ffa',0,1,0,'0001-01-01 00:00:00','2012-11-05 00:00:00'),
 (194,'Gelito','','','bonikun@gmail.com','90ffa905303431b866920ec43a913615',0,1,0,'0001-01-01 00:00:00','2012-07-31 00:00:00'),
 (195,'Gema-Heras','','','sylviacr@hotmail.es','10c160b28ccae5704ac4d98f84b809aa',0,1,0,'0001-01-01 00:00:00','2012-08-02 00:00:00'),
 (196,'Gemam38','','','gemam38@hotmail.com','db714f6dd81d131eb8ddef1ebdf5190f',0,1,0,'0001-01-01 00:00:00','2012-08-03 00:00:00'),
 (197,'German-Albo-Lastra','','','german.lastra@hotmail.com','1904c05a8cb4a663953bc2b5c99d10e2',0,1,0,'0001-01-01 00:00:00','2012-11-24 00:00:00'),
 (198,'German-Sanchez','','','ballack_dr.house@hotmail.com','6aff05df2ea9928889bc938b3f82587e',0,1,0,'0001-01-01 00:00:00','2012-10-30 00:00:00'),
 (199,'Germanfueyo','','','german@fueyo.com','498accfff3027f4b8619d66609e7cd40',0,1,0,'0001-01-01 00:00:00','2012-08-03 00:00:00'),
 (200,'Giselacarolina','','','parapsicologacarol@hotmail.com','fcb255c321b18d58d3526f4d1845d22b',0,1,0,'0001-01-01 00:00:00','2012-12-12 00:00:00'),
 (201,'Giselacarolinatv','','','tarotgiselatv@gmail.com','cf9a317736cca710d45ba1d938b1be96',0,1,0,'0001-01-01 00:00:00','2012-12-23 00:00:00'),
 (202,'Giselita','','','gisselita_nyuu.19@hotmail.com','2e5089bd638eeb7bd2426b0cc702521b',0,1,0,'0001-01-01 00:00:00','2012-12-22 00:00:00'),
 (203,'Gmedrano','','','medrano.digital@gmail.com','52b985145ae9fbd61c101003af8a1a81',0,1,0,'0001-01-01 00:00:00','2012-10-27 00:00:00'),
 (204,'Grimorio-Cronequiur','','','business.pereyra@gmail.com','fedf6b7c0245d0ae02a074b355fd5794',0,1,0,'0001-01-01 00:00:00','2012-10-13 00:00:00'),
 (205,'Guereyda','','','guereyda@hotmail.com','b0bb915d1ce7861cb6d9f4b8c7719401',0,1,0,'0001-01-01 00:00:00','2012-08-04 00:00:00'),
 (206,'Gustavo-Cano','','','rolandogustavocano@gmail.com','dafa5728dfc851bf10d2a5cae762f990',0,1,0,'0001-01-01 00:00:00','2012-12-18 00:00:00'),
 (207,'Gustavo-Piquero','','','gustavopiquero@hotmail.com','dd343981ff36fdcf49736d6a310e5b5b',0,1,0,'0001-01-01 00:00:00','2012-12-07 00:00:00'),
 (208,'Hector-Awazacko','','','hectorawazackoreyes@hotmail.com','958a74a4695ec722416c949165fd7c50',0,1,0,'0001-01-01 00:00:00','2012-08-02 00:00:00'),
 (209,'Helena','','','haillons@gmail.com','3b41d857845a240c556c06b529de6546',0,1,0,'0001-01-01 00:00:00','2012-11-19 00:00:00'),
 (210,'Heriberto','','','heribertotellez430@hotmail.com','a5769e7ef5e9ea00a29ff7f6ee6fa0b6',0,1,0,'0001-01-01 00:00:00','2012-11-26 00:00:00'),
 (211,'Hojadeamor','','','hurtado@hojadeamor.com','e55c661177ba3ef011183d792581c20a',0,1,0,'0001-01-01 00:00:00','2012-08-14 00:00:00'),
 (212,'Hospa','','','hostiliano24@hotmail.com','7048507440ee0c34530cb50d7be6523b',0,1,0,'0001-01-01 00:00:00','2012-10-15 00:00:00'),
 (213,'Hrgeesemount','','','hugo.roig@yahoo.com','08ec4c20eb9781c66d867dd2d47a7fd7',0,1,0,'0001-01-01 00:00:00','2012-09-21 00:00:00'),
 (214,'Igor-Rodtem','','','rodtem@gmail.com','f8642434a64e81384761d6839dfb396a',0,1,0,'0001-01-01 00:00:00','2012-08-15 00:00:00'),
 (215,'Ihelped12','','','globalcapitals1@aol.com','fdc5105e8c36c9aa4b4e04884db2d7d8',0,1,0,'0001-01-01 00:00:00','2012-11-20 00:00:00'),
 (216,'Ilusion4','','','una_ilusion@hotmail.es','8f91bdb4de0142710ac1718345b96308',0,1,0,'0001-01-01 00:00:00','2012-10-02 00:00:00'),
 (217,'Imagina-Yo','','','amidamaru_13@hotmail.com','cf9ee5bcb36b4936dd7064ee9b2f139e',0,1,0,'0001-01-01 00:00:00','2012-09-24 00:00:00'),
 (218,'Imawolfe','','','amatxu@hotmail.es','d827244b1f132402fdfabfc8ac19852f',0,1,0,'0001-01-01 00:00:00','2012-11-04 00:00:00'),
 (219,'Inma-Perez','','','inmaperezortola@ymail.com','cebdd715d4ecaafee8f147c2e85e0754',0,1,0,'0001-01-01 00:00:00','2012-12-10 00:00:00'),
 (220,'Ionzaga','','','ionzaga@yahoo.es','a56ac99d521c97ffcd84bcd3c2fb2565',0,1,0,'0001-01-01 00:00:00','2012-08-16 00:00:00'),
 (221,'Isabo','','','idelaflor06@gmail.com','7c1ae7f13882ef4979f7ca4f4763fa02',0,1,0,'0001-01-01 00:00:00','2012-08-09 00:00:00'),
 (222,'Iseth','','','isethbiel@gmail.com','03fb28a9f6880ec2305da7b63285e7a5',0,1,0,'0001-01-01 00:00:00','2012-09-23 00:00:00'),
 (223,'Iseth1','','','vicortalv@msn.com','03fb28a9f6880ec2305da7b63285e7a5',0,1,0,'0001-01-01 00:00:00','2012-12-23 00:00:00'),
 (224,'Ishtar89','','','sarag1989@gmail.com','94069795da9215c462352301f84bec8b',0,1,0,'0001-01-01 00:00:00','2012-11-05 00:00:00'),
 (225,'Isidora','','','isianriquez_22@hotmail.com','d33705bf0b4c62c7de21dab2df639d4e',0,1,0,'0001-01-01 00:00:00','2012-09-28 00:00:00'),
 (226,'Jahirob','','','jahirob@gmail.com','c79f453eace6b31ecf11abc8ea2118ee',0,1,0,'0001-01-01 00:00:00','2012-12-01 00:00:00'),
 (227,'Jaime-E-Villa-Mejia','','','jvillamejia94@gmail.com','0d54fc7703c7f84700334db1b2972a84',0,1,0,'0001-01-01 00:00:00','2012-11-05 00:00:00'),
 (228,'Janmarco','','','ingeniero__@live.com','f6e014747ba5b09b6eb76aafed3f0977',0,1,0,'0001-01-01 00:00:00','2012-10-28 00:00:00'),
 (229,'Jantog','','','aries14_93@hotmail.com','997bea19976fdee238700a3dd61ab40a',0,1,0,'0001-01-01 00:00:00','2012-09-14 00:00:00'),
 (230,'Jason','','','bjason128@gmail.com','173f0fad30559288f67eb63c30866390',0,1,0,'0001-01-01 00:00:00','2012-10-06 00:00:00'),
 (231,'Javier-Beirute','','','javier.beirute@gmail.com','616b0a1bf7d74183e0304a27b6bb5be2',0,1,0,'0001-01-01 00:00:00','2012-08-07 00:00:00'),
 (232,'Javier-Garcia-Wk','','','javier.garcia.wk@gmail.com','2e005b6972af553c6289c6e4a9510c67',0,1,0,'0001-01-01 00:00:00','2012-08-06 00:00:00'),
 (233,'Javiernavarro','','','j_r_s_212216@hotmail.com','2e90c1256e62303e1e77a27f3fed05ea',0,1,0,'0001-01-01 00:00:00','2012-12-03 00:00:00'),
 (234,'Jbarriga','','','josebarriga@entelnet.bo','f3976a4e6ece7ff933fce94091a44f99',0,1,0,'0001-01-01 00:00:00','2012-10-29 00:00:00'),
 (235,'Jcmaldonado','','','jcmr72@yahoo.es','d74363eee3dae51d77f7bbd721096d2d',0,1,0,'0001-01-01 00:00:00','2012-08-02 00:00:00'),
 (236,'Jeav10','','','jorarismendi@gmail.com','fae1c4bcf1070891cff980d96c1b0c92',0,1,0,'0001-01-01 00:00:00','2012-08-04 00:00:00'),
 (237,'Jefferson-Moreno','','','boris_pelotero.100@hotmail.com','110c86505c43d0a95313b2931ef6b520',0,1,0,'0001-01-01 00:00:00','2012-12-01 00:00:00'),
 (238,'Jessy_Arenas','','','jessyrock_30@hotmail.com','64d2ae6e32d2de0bb11ec93ce809868b',0,1,0,'0001-01-01 00:00:00','2012-11-24 00:00:00'),
 (239,'Jesus-De-Castro','','','sthendal-1966@hotmail.com','84c4207ef45932386bb79fdbd21fb126',0,1,0,'0001-01-01 00:00:00','2012-09-13 00:00:00'),
 (240,'Jesus-Fernandez','','','jesusfernandezayas@gmail.com','d7e204f45c8399cd05d471a2217bda97',0,1,0,'0001-01-01 00:00:00','2012-07-30 00:00:00'),
 (241,'Jesusmanuel','','','jm251995@hotmail.com','29ebf2f279da44f69a35206885cd2dbc',0,1,0,'0001-01-01 00:00:00','2012-11-09 00:00:00'),
 (242,'Jesusmrr','','','jesusmrr@hotmail.com','7d2fb69fd2077ccf30f199f69a8c0d15',0,1,0,'0001-01-01 00:00:00','2012-12-21 00:00:00'),
 (243,'Jesusvr','','','jesus_villanueva_ramirez@hotmail.com','fc0de868765f30f0e4e5d9a02acc3ce7',0,1,0,'0001-01-01 00:00:00','2012-11-14 00:00:00'),
 (244,'Jleiva','','','juanleivamarquez@gmail.com','0dcde190ddef2fa213dae22ec61df82c',0,1,0,'0001-01-01 00:00:00','2012-07-31 00:00:00'),
 (245,'Jmdeben','','','josemanuel.deben@gmail.com','b0cb5bd69447b65a609598c5a801044a',0,1,0,'0001-01-01 00:00:00','2012-08-03 00:00:00'),
 (246,'Jmelcue','','','jmelcue56@hotmail.com','2d56586b5f7f9f27a95b811c056dccd3',0,1,0,'0001-01-01 00:00:00','2012-11-07 00:00:00'),
 (247,'Joan-Abello','','','joan.abello@gmail.com','ac0ac45665158df324cae01dd2a14025',0,1,0,'0001-01-01 00:00:00','2012-09-09 00:00:00'),
 (248,'Joangelitoxicomano','','','joangelitoxicomano@hotmail.es','0bb46c938090ba203eb4b6573328876d',0,1,0,'0001-01-01 00:00:00','2012-11-27 00:00:00'),
 (249,'Joao','','','tayson-xsiempre@hotmail.com','e45bba48e1a1bfa964839e478cbe0034',0,1,0,'0001-01-01 00:00:00','2012-12-25 00:00:00'),
 (250,'Joaquin-Dholdan','','','joadoldan@gmail.com','24db39f44c643beb3c88e7ab7983b00a',0,1,0,'0001-01-01 00:00:00','2012-10-25 00:00:00'),
 (251,'Joaquin_Pereira','','','tallerdejoaquinpereira@gmail.com','51943125dc3430a39945a1055b9ccdb9',0,1,0,'0001-01-01 00:00:00','2012-08-21 00:00:00'),
 (252,'Joaquincaniete','','','joaquincaniete@gmail.com','06036321a2c2f161efafa86984f44168',0,1,0,'0001-01-01 00:00:00','2012-12-17 00:00:00'),
 (253,'Joelfortunato','','','joelfortunato@yahoo.com.mx','dbb7177ffd8790e187c9f968ffec6f57',0,1,0,'0001-01-01 00:00:00','2012-12-12 00:00:00'),
 (254,'Johandiaz95','','','johandiaz625@gmail.com','14df5154cf15623bfd0c2e1954c2ad04',0,1,0,'0001-01-01 00:00:00','2012-09-29 00:00:00'),
 (255,'Johnny-Rengifo','','','elultimoagujero@gmail.com','14c3ddcd4563ffecfa341de113e8ab34',0,1,0,'0001-01-01 00:00:00','2012-11-04 00:00:00'),
 (256,'JokiRomero','','','joki.romero@gmail.com','842dd1e0c632835ba4605fd17a94b6ec',0,1,0,'0001-01-01 00:00:00','2012-10-01 00:00:00'),
 (257,'Jonathan','','','jonathanestebansanchez@gmail.com','a70f6367370541a657fd1870625c5ffc',0,1,0,'0001-01-01 00:00:00','2012-08-24 00:00:00'),
 (258,'Jorge-Bericat','','','jorgebericat@hotmail.com','96c02c13268b738c87e08f7fce628f02',0,1,0,'0001-01-01 00:00:00','2012-08-07 00:00:00'),
 (259,'Jorge-Garibaldi','','','jorgegaribaldig@hotmail.com','5086399e9bc7a90e6e8b57f9d29e2f68',0,1,0,'0001-01-01 00:00:00','2012-08-30 00:00:00'),
 (260,'Jorge-Sotto-Mayor','','','jorgesottomayor36@hotmail.com','6080d71c0cfe431dac3b2d5bb52f07f5',0,1,0,'0001-01-01 00:00:00','2012-11-29 00:00:00'),
 (261,'Jorgescritor','','','jorgekiss.inventarios@gmail.com','4715c2aea197a327865851d07af458da',0,1,0,'0001-01-01 00:00:00','2012-08-20 00:00:00'),
 (262,'Jose','','','miguel2103_5@hotmail.com','90ce1667fcedcab5adc40aa901439787',0,1,0,'0001-01-01 00:00:00','2012-12-07 00:00:00'),
 (263,'Jose-Antonio','','','profetasyapostoles@hotmail.com','5479fb52b6de6600cc54d562619a4061',0,1,0,'0001-01-01 00:00:00','2012-12-18 00:00:00'),
 (264,'Jose-Carrera','','','joerace@gmail.com','e10adc3949ba59abbe56e057f20f883e',0,1,0,'0001-01-01 00:00:00','2012-09-12 00:00:00'),
 (265,'Jose-Fuenmayor','','','jocefuenmayor@hotmail.com','6b4903a572e81f5ffeeb92a5b3005819',0,1,0,'0001-01-01 00:00:00','2012-10-25 00:00:00'),
 (266,'Jose-J-Bejaranis','','','jotabejaqu@yahoo.es','827ccb0eea8a706c4c34a16891f84e7b',0,1,0,'0001-01-01 00:00:00','2012-07-30 00:00:00'),
 (267,'Jose-Luis-Urbano','','','j.luis.urbano@gmail.com','f9be311e65d81a9ad8150a60844bb94c',0,1,0,'0001-01-01 00:00:00','2012-12-25 00:00:00'),
 (268,'Josemaria','','','josemaria@morales-vazquez.com','9789252e95b0692b1ccf2f078ac59fff',0,1,0,'0001-01-01 00:00:00','2012-08-19 00:00:00'),
 (269,'Joshua-Mdq','','','laion277@hotmail.com','5fb81ef7386c839540081b6ab590d10e',0,1,0,'0001-01-01 00:00:00','2012-10-27 00:00:00'),
 (270,'Jr-Marklin','','','jrmarklin@gmail.com','c52278ca063fe3beb5ada38ae7628d25',0,1,0,'0001-01-01 00:00:00','2013-01-07 00:00:00'),
 (271,'Juan','','','juannorte123@gmail.com','b63e82aa3ee378b8fbf07095b05c8a6f',0,1,0,'0001-01-01 00:00:00','2012-12-07 00:00:00'),
 (272,'Juan-C-Munoz-E','','','jjccmunoz@gmail.com','305e87b29b947e1ea66776ed90b1bd5f',0,1,0,'0001-01-01 00:00:00','2012-11-09 00:00:00'),
 (273,'Juan-Deschamps','','','juandediosdeschamps@hotmail.com','debb3a4d984f845bfe9ad57c1dd3bc1f',0,1,0,'0001-01-01 00:00:00','2012-09-10 00:00:00'),
 (274,'Juan-Freiz','','','jlagos@colegioingles.org','cefb8d1e98d6323ff6d52348b9a3693c',0,1,0,'0001-01-01 00:00:00','2012-10-02 00:00:00'),
 (275,'Juan-Munoz','','','judas652000@yahoo.com','305e87b29b947e1ea66776ed90b1bd5f',0,1,0,'0001-01-01 00:00:00','2013-01-17 00:00:00'),
 (276,'Juan_Gonzalez','','','juan2005gg@hotmail.com','f92d9de19107eefe335a7596ef761049',0,1,0,'0001-01-01 00:00:00','2012-11-30 00:00:00'),
 (277,'Juana-Rivera-Bolados','','','jrivera.bolados@gmail.com','3f60eb7939d4fdcb9244c0999b175f09',0,1,0,'0001-01-01 00:00:00','2012-11-05 00:00:00'),
 (278,'Juanadm','','','juanadm@yahoo.es','d2f0c294711426f440af6c188232e774',0,1,0,'0001-01-01 00:00:00','2012-08-08 00:00:00'),
 (279,'Juanan-Zea','','','juan.zea.herranz@gmail.com','e590854851bc15451c85e8240f6d1ded',0,1,0,'0001-01-01 00:00:00','2012-08-01 00:00:00'),
 (280,'Juankarloscf','','','juankarloscf@hotmail.com','25f9e794323b453885f5181f1b624d0b',0,1,0,'0001-01-01 00:00:00','2012-08-19 00:00:00'),
 (281,'Juantricell','','','umbrellacorp_marty@hotmail.com','9e5024f0e0e1895f8b02ef14fdf1963a',0,1,0,'0001-01-01 00:00:00','2012-08-01 00:00:00'),
 (282,'Jukka','','','jukkapalacios@hotmail.com','289e594ab24f1d4e7a3e5b5690a67f24',0,1,0,'0001-01-01 00:00:00','2012-12-12 00:00:00'),
 (283,'Juli-Moreira','','','julimoreira_7@hotmail.com','906073ddc3b47540ab064deb4b963fa2',0,1,0,'0001-01-01 00:00:00','2012-09-24 00:00:00'),
 (284,'Juliana001','','','julianakolb@aol.fr','71b3b26aaa319e0cdf6fdb8429c112b0',0,1,0,'0001-01-01 00:00:00','2012-12-16 00:00:00'),
 (285,'Juliette','','','julias02@hotmail.com','d8e12bb275d7928f08cc79c08a85d60c',0,1,0,'0001-01-01 00:00:00','2012-11-05 00:00:00'),
 (286,'Junior','','','junior238335@gmail.com','421382b1c21507130ed82e9a6e0a94de',0,1,0,'0001-01-01 00:00:00','2012-11-30 00:00:00'),
 (287,'Junji','','','macolloca@gmail.com','e8e446bb57fc6e1a89a42f8d77cc7ef0',0,1,0,'0001-01-01 00:00:00','2012-08-19 00:00:00'),
 (288,'Kami','','','kamichurasan@hotmail.com','99da3be859264c7829e64e60174565f5',0,1,0,'0001-01-01 00:00:00','2012-12-09 00:00:00'),
 (289,'Karina-Quinones','','','kquinonescsesur@gmail.com','309176d4b3257238c91470c515e7b5e8',0,1,0,'0001-01-01 00:00:00','2012-11-08 00:00:00'),
 (290,'Kelevra','','','keyo_007@hotmail.com','9c8b8c6384022c57c1862b333ef831ee',0,1,0,'0001-01-01 00:00:00','2012-12-11 00:00:00'),
 (291,'Kenna','','','c.quesadamorales@gmail.com','55c8e58f560d80e0996c086e5f600096',0,1,0,'0001-01-01 00:00:00','2012-08-03 00:00:00'),
 (292,'Kenni-Bolanos','','','clubdelacultura@hotmail.com','827ccb0eea8a706c4c34a16891f84e7b',0,1,0,'0001-01-01 00:00:00','2012-07-30 00:00:00'),
 (293,'Kevin','','','krivas-1405@hotmail.com','1e31cb5e172cb48537a713b150323acb',0,1,0,'0001-01-01 00:00:00','2012-11-04 00:00:00'),
 (294,'Kevin60082829','','','kevin_fuentes98@hotmail.com','37f299007792a4e9dec1481bca604016',0,1,0,'0001-01-01 00:00:00','2012-12-03 00:00:00'),
 (295,'Kevinsanlukas','','','kag-2014@hotmail.com','28d966aa541b02d722498002b5c2a284',0,1,0,'0001-01-01 00:00:00','2013-01-03 00:00:00'),
 (296,'Ksanchez','','','karinasancz@gmail.com','a4261000fc15b9f2e419821533eb9627',0,1,0,'0001-01-01 00:00:00','2012-10-31 00:00:00'),
 (297,'La-Clave-Espiritual','','','contacto@dioslaclaveespiritual.com','b64f1a77b1b317d347f5cb79332c86d2',0,1,0,'0001-01-01 00:00:00','2012-08-03 00:00:00'),
 (298,'Laramaceda','','','lara_maceda@hotmail.com','f7a543c78932345150bcbec63df0dd5d',0,1,0,'0001-01-01 00:00:00','2012-08-19 00:00:00'),
 (299,'Larxs','','','5tornado@myway.com','da6629d9487d49888331d36368b4e534',0,1,0,'0001-01-01 00:00:00','2012-10-28 00:00:00'),
 (300,'Laura-Peralta','','','laskulisueltas_@hotmail.com','5ba16d57d49fee4f7b2c62bd4f34c426',0,1,0,'0001-01-01 00:00:00','2012-08-22 00:00:00'),
 (301,'Laureanosecundo','','','laureanosecundo@ibest.com.br','761363e3a893edc6b19593f91be139ed',0,1,0,'0001-01-01 00:00:00','2012-10-19 00:00:00'),
 (302,'Leo-Antonelli','','','daxlife@hotmail.com','e87199638bde78cfcebe52574e5bed93',0,1,0,'0001-01-01 00:00:00','2012-11-05 00:00:00'),
 (303,'Leo-De-Quevedo','','','prometoe007@hotmail.com','f6963eaaef30bda9f310461df33133be',0,1,0,'0001-01-01 00:00:00','2012-08-12 00:00:00'),
 (304,'Leonardo-Avello','','','recuerdosyalgomas@gmail.com','f6322d30dcbe3181ed5379ac571bb980',0,1,0,'0001-01-01 00:00:00','2012-09-26 00:00:00'),
 (305,'Leonor-Sampelayoruiz','','','sampelayoruiz@hotmail.com','b5326634ee9352d1dda55dfc6049d592',0,1,0,'0001-01-01 00:00:00','2012-12-26 00:00:00'),
 (306,'Ley-Del-Artista','','','leydelartista@bolivia.com','965a78f2ade051c1b241974cd2c9f53b',0,1,0,'0001-01-01 00:00:00','2012-12-19 00:00:00'),
 (307,'Lilithazure','','','apocalipsys8_1@hotmail.com','9b7d3814da6fea663f989ee009d9d33b',0,1,0,'0001-01-01 00:00:00','2012-11-14 00:00:00'),
 (308,'Literalyaritza','','','irreal_1992@hotmail.com','6f93e3bd3c06df4b50be4effd555ed3d',0,1,0,'0001-01-01 00:00:00','2012-12-26 00:00:00'),
 (309,'Lluis-Carrera','','','lluiskr@hotmail.com','827ccb0eea8a706c4c34a16891f84e7b',0,1,0,'0001-01-01 00:00:00','2012-12-04 00:00:00'),
 (310,'Lmedinac','','','oriana1979@hotmail.com','6eff0bd1b01d2e26f63892ccbe7ce468',0,1,0,'0001-01-01 00:00:00','2012-10-16 00:00:00'),
 (311,'Locueloman','','','locueloman@hotmail.com','71b64f189f658665a5b8d01741ad38f0',0,1,0,'0001-01-01 00:00:00','2012-08-01 00:00:00'),
 (312,'Loli','','','lolasantamaria8@yahoo.com.ar','7ddf05aa9110c22e99e106f4c94815d1',0,1,0,'0001-01-01 00:00:00','2012-09-11 00:00:00'),
 (313,'Lolisan','','','lolimarsan@gmail.com','d4c16bc9c80367aeafcfaef89df60ba8',0,1,0,'0001-01-01 00:00:00','2012-11-30 00:00:00'),
 (314,'Lolitasi','','','lolasantamaria08@yahoo.com.ar','7ddf05aa9110c22e99e106f4c94815d1',0,1,0,'0001-01-01 00:00:00','2012-09-11 00:00:00'),
 (315,'Lorenochka','','','lomepa@gmail.com','86740fe221ab07d38aa3a9b094ab0ff9',0,1,0,'0001-01-01 00:00:00','2012-08-06 00:00:00'),
 (316,'Lsoto1953','','','lsoto1953@yahoo.com','3ea8127de3e2cfcd0de2ff3b009d201f',0,1,0,'0001-01-01 00:00:00','2012-12-20 00:00:00'),
 (317,'Ltm-Decoraciones','','','ltm_servicios@hotmail.com','dbdd472d8a2f3af021d6615fc6542d29',0,1,0,'0001-01-01 00:00:00','2012-11-15 00:00:00'),
 (318,'Lualewis','','','luanidia@hotmail.com','2135a2ccca4ef6d03d0e9f3caab9fbe7',0,1,0,'0001-01-01 00:00:00','2012-09-30 00:00:00'),
 (319,'Lucas-Julian','','','davidmallyguerra@gmail.com','805399b68322d118137624684c1d8503',0,1,0,'0001-01-01 00:00:00','2012-11-14 00:00:00'),
 (320,'Lucia','','','rataflaca9@hotmail.com','b381a6498b64c85b9a697a6b5ada4143',0,1,0,'0001-01-01 00:00:00','2012-08-01 00:00:00'),
 (321,'Lucilia-Ribeiro','','','luciliabrribeiro@gmail.com','7c786dd0cfa53696557601225f39cebf',0,1,0,'0001-01-01 00:00:00','2012-08-18 00:00:00'),
 (322,'Luis-Arroyo-Perez','','','luisatilioarroyo@yahoo.com.ar','7bfc7530d049a9ace6e6ab9c2ddd7d95',0,1,0,'0001-01-01 00:00:00','2012-11-18 00:00:00'),
 (323,'Luis-Benavides','','','luismkt@latinmail.com','353a3f847836d20d967d753afe88ccb7',0,1,0,'0001-01-01 00:00:00','2012-11-24 00:00:00'),
 (324,'LuisRoman','','','eyder_xipe_totec@hotmail.com','70a2cb678d70fd5a2d17a29434654ac9',0,1,0,'0001-01-01 00:00:00','2012-12-20 00:00:00'),
 (325,'Luis_Sahe','','','luis_sahe@hotmail.com','f21fc43259c175660d9e5f8fed722557',0,1,0,'0001-01-01 00:00:00','2012-08-03 00:00:00'),
 (326,'Luiscabezos','','','luis_cabezos@hotmail.com','36d4b27a25ac59872aa811e68f9d0d45',0,1,0,'0001-01-01 00:00:00','2012-08-05 00:00:00'),
 (327,'Luisricardo','','','arevalo777@gmail.com','c9740fe6f49fac2233d34ef3a9b259cd',0,1,0,'0001-01-01 00:00:00','2012-09-22 00:00:00'),
 (328,'Luli','','','espinos@adinet.com.uy','b1630f9af8a8607ac4bd421c48a99e7c',0,1,0,'0001-01-01 00:00:00','2012-09-04 00:00:00'),
 (329,'Luzdalila1974','','','luz_entusojos@hotmail.com','e8d30f584399c7d856ca756ea9f14c51',0,1,0,'0001-01-01 00:00:00','2012-11-10 00:00:00'),
 (330,'Luzma','','','luznaveda@gmail.com','22a984c2bfab7471e62fbc27da5adff7',0,1,0,'0001-01-01 00:00:00','2012-12-05 00:00:00'),
 (331,'Mafe-Sabatini','','','mafesabatini@hotmail.com','ace0e77f996e5afc57f8a9b335f1b8b8',0,1,0,'0001-01-01 00:00:00','2012-12-07 00:00:00'),
 (332,'Maheluz','','','maheluz@hotmail.com','e9de5fd51a98f7dc3eec957eab749214',0,1,0,'0001-01-01 00:00:00','2012-10-12 00:00:00'),
 (333,'Malu','','','mispoemasehistorias@gmail.com','4ee0888cf5c24f55292df932e945f6d0',0,1,0,'0001-01-01 00:00:00','2012-09-08 00:00:00'),
 (334,'Manuel-Gomez','','','manuel.goberman@gmail.com','d6c3007ec30c01e2f306902ce93381be',0,1,0,'0001-01-01 00:00:00','2012-10-25 00:00:00'),
 (335,'Manuel-Ibarra','','','manibarra@hotmail.com','37c5e2ed36050fcce3047ff0551b00d4',0,1,0,'0001-01-01 00:00:00','2012-12-09 00:00:00'),
 (336,'Manuel-Montenegro-R','','','periodistamontenegro@gmail.com','94666f83f81fbbef2898a218295bc39e',0,1,0,'0001-01-01 00:00:00','2012-11-16 00:00:00'),
 (337,'Manuel-Silva','','','alegresilva@sapo.pt','9b75fd61ac3d128601b0978cc3ef23d0',0,1,0,'0001-01-01 00:00:00','2012-09-09 00:00:00'),
 (338,'Manuel3','','','dasilvamanuel3@hotmail.fr','af4d7d87f0013b49126b5eb327f0e88b',0,1,0,'0001-01-01 00:00:00','2012-12-06 00:00:00'),
 (339,'Manuelalbaprieto','','','manuel.alba.prieto@hotmail.com','f2b2c32820f660b524983fabc7cfeed5',0,1,0,'0001-01-01 00:00:00','2012-11-09 00:00:00'),
 (340,'Manuelrebollo','','','manuelrebollo@yahoo.es','8fd9e6b4243d058d998f3e74d7a2b001',0,1,0,'0001-01-01 00:00:00','2012-09-18 00:00:00'),
 (341,'Manuzon','','','manuzon_666_3@hotmail.com','0e59caaab167e26a1cca4efd89fc2489',0,1,0,'0001-01-01 00:00:00','2012-11-21 00:00:00'),
 (342,'Marco-Ramos','','','marco_ramos1@hotmail.com','b83decfb4daf6c227eefd58408b767f2',0,1,0,'0001-01-01 00:00:00','2012-11-23 00:00:00'),
 (343,'Marcos-Urbina','','','marcosescri@hotmail.com','f74514abc3270bf0dac5607737c1ee61',0,1,0,'0001-01-01 00:00:00','2012-11-06 00:00:00'),
 (344,'Maria-Fiore','','','dcvmariadelrosariofiore@gmail.com','5fee685aff20a9b7a86e13ee7cc9f1cf',0,1,0,'0001-01-01 00:00:00','2012-11-12 00:00:00'),
 (345,'Maria-Jesus','','','alcobermj@hotmail.com','28487e313d53f00f9289a17bba79721f',0,1,0,'0001-01-01 00:00:00','2012-07-31 00:00:00'),
 (346,'Maria-Jose-Morai','','','luzvioleta999@hotmail.com','a6f569b7ee37f716ef5763c55e437cf5',0,1,0,'0001-01-01 00:00:00','2012-09-07 00:00:00'),
 (347,'Mariano','','','marianocofre@hotmail.cl','feba56e44e1a67719d82f6858c648fdc',0,1,0,'0001-01-01 00:00:00','2012-10-21 00:00:00'),
 (348,'Mariela-Andrea','','','marielaandreagonzalez@hotmail.com','dd22141acb5ea065acd5ed773729c98f',0,1,0,'0001-01-01 00:00:00','2012-08-20 00:00:00'),
 (349,'Mariluz-Lozano-Gago','','','marateneapalas@yahoo.es','200dc7ebe2381a9bf570298b86bd0a9d',0,1,0,'0001-01-01 00:00:00','2012-08-16 00:00:00'),
 (350,'Mario-Valencia','','','mvalehe@yahoo.com.mx','827ccb0eea8a706c4c34a16891f84e7b',0,1,0,'0001-01-01 00:00:00','2012-07-30 00:00:00'),
 (351,'Marisol-Escribano','','','marisol_princesa@hotmail.com','827ccb0eea8a706c4c34a16891f84e7b',0,1,0,'0001-01-01 00:00:00','2012-07-30 00:00:00'),
 (352,'Martillo','','','martrix24@hotmail.com','3747cbba98b07d0f30454cbf11ed7e63',0,1,0,'0001-01-01 00:00:00','2012-08-06 00:00:00'),
 (353,'Martin','','','martinsoriapintor@gmail.com','ff1b5f0641f12de4f074c6a4c90a4a63',0,1,0,'0001-01-01 00:00:00','2012-09-21 00:00:00'),
 (354,'Martin-Rae','','','nvgmlla@yahoo.es','98e7f6750d542701c5386e536e7ba69a',0,1,0,'0001-01-01 00:00:00','2012-10-20 00:00:00'),
 (355,'Mary-Carmen','','','arfasay@gmail.com','795481e199a2a94a1db1d04f68ba5ac5',0,1,0,'0001-01-01 00:00:00','2012-11-04 00:00:00'),
 (356,'Matilde-Pelaez','','','fundacrister@hotmail.com','427682857540156fe878cd72a8d6b54b',0,1,0,'0001-01-01 00:00:00','2012-10-03 00:00:00'),
 (357,'Max-Power','','','marmen1971@hotmail.com','a5299a4acb5d84a41edce14139d466aa',0,1,0,'0001-01-01 00:00:00','2012-11-13 00:00:00'),
 (358,'Maximiliano-Martin','','','maximartin2011@hotmail.com.ar','7fd2ab5559bd4c2b38dd2a0448627fd3',0,1,0,'0001-01-01 00:00:00','2012-08-25 00:00:00'),
 (359,'Maxus800','','','phamtomganon@hotmail.com','e299904bd1b5069a33a1fba4fdd8c181',0,1,0,'0001-01-01 00:00:00','2012-12-16 00:00:00'),
 (360,'Mdorao','','','martadorao@hotmail.com','9773ac31298699a64c34de789576b65d',0,1,0,'0001-01-01 00:00:00','2012-11-15 00:00:00'),
 (361,'Melanie','','','estergutierrez591@gmail.com','413cf50ab38245461c41381f4d78626a',0,1,0,'0001-01-01 00:00:00','2012-11-16 00:00:00'),
 (362,'Mendezz','','','mendezzcarlos@gmail.com','6bb63a19722519c6e3a0a6e5280ce76d',0,1,0,'0001-01-01 00:00:00','2012-08-13 00:00:00'),
 (363,'Mensnudum','','','mensnudum@gmail.com','c7be6d2d427a6417c176d00ba8119a1e',0,1,0,'0001-01-01 00:00:00','2012-08-09 00:00:00'),
 (364,'Mich77','','','danitojessita@hotmail.com','987167be64378c1582b31355e0828a43',0,1,0,'0001-01-01 00:00:00','2012-11-23 00:00:00'),
 (365,'Miguel-Alvarez','','','ajedrecistanovato@gmail.com','4b84d557a612089a8e44c7e17b6a663a',0,1,0,'0001-01-01 00:00:00','2012-08-03 00:00:00'),
 (366,'Miguel-Castro','','','miguel.castro@avina.net','194dd862e46c34be6cb82aa1f87ec40a',0,1,0,'0001-01-01 00:00:00','2012-10-28 00:00:00'),
 (367,'Miguel-Gutierrez','','','nocter@live.com','7fdfe8a7219bc284df2f6ecb111a996c',0,1,0,'0001-01-01 00:00:00','2012-08-14 00:00:00'),
 (368,'Miguel-Marroquin','','','miguel.marroquin.mm@gmail.com','41e2f1c65ba6b9aad9f568592e4a7f57',0,1,0,'0001-01-01 00:00:00','2012-10-08 00:00:00'),
 (369,'Miguel-Pinto','','','miguelpintosalvatierra@hotmail.com','1ee1877c6655ecc71dfead311c771bd0',0,1,0,'0001-01-01 00:00:00','2012-12-31 00:00:00'),
 (370,'Miguel-Torres-E','','','lauromigueltorresencalada@gmail.com','299b4f18157c4f1190195e396ab6410f',0,1,0,'0001-01-01 00:00:00','2012-08-04 00:00:00'),
 (371,'Miguell-E-Valdivia','','','micky0018@hotmail.com','342c676216e96a1fa26c7819b8a29612',0,1,0,'0001-01-01 00:00:00','2012-07-30 00:00:00'),
 (372,'Miguelramirez','','','comunicaciones@worldlegalcorp.com','3201e530a18aec98b255659e40f644d6',0,1,0,'0001-01-01 00:00:00','2012-12-02 00:00:00'),
 (373,'Mikel','','','mikelestefania@yahoo.es','c6cc56ae4ef272c95ca2014247e6d1f7',0,1,0,'0001-01-01 00:00:00','2013-01-17 00:00:00'),
 (374,'Milad-Jilo','','','milad.jilo@hotmail.com','f797c779698d5f55e63d5ebe8aeefc03',0,1,0,'0001-01-01 00:00:00','2012-12-19 00:00:00'),
 (375,'Millylennon','','','milly.yeey@yahoo.com','24d592216e98be81151d8617a55f62ac',0,1,0,'0001-01-01 00:00:00','2012-10-02 00:00:00'),
 (376,'Mio-Bresni','','','berolouniletras@aveviajera.org','45eb8fd773f077b1b347c011648fc131',0,1,0,'0001-01-01 00:00:00','2012-11-12 00:00:00'),
 (377,'Miriamse','','','doll.dllmrdr@gmail.com','dd214399ba4b80ec14d9a0318512fa1b',0,1,0,'0001-01-01 00:00:00','2013-01-30 00:00:00'),
 (378,'Moka','','','unhdl.t@gmail.com','a042b6e5d9fee7e6671baf6480e8c91d',0,1,0,'0001-01-01 00:00:00','2012-12-08 00:00:00'),
 (379,'Mometolo-Alejandro','','','almotre-20@hotmail.com','25273e46fa5248544fdbbd69a4e3c51a',0,1,0,'0001-01-01 00:00:00','2012-11-30 00:00:00'),
 (380,'Monicgordon','','','monicga@gmail.com','1e58c53e268affc4d61acefc6b6c6cdd',0,1,0,'0001-01-01 00:00:00','2012-09-24 00:00:00'),
 (381,'Morislandro','','','morislandro@gmail.com','800a0e21225906fe82d141def1a9202d',0,1,0,'0001-01-01 00:00:00','2012-12-14 00:00:00'),
 (382,'Mprin12','','','marin.pierre12@yahoo.com','ab4f63f9ac65152575886860dde480a1',0,1,0,'0001-01-01 00:00:00','2012-11-13 00:00:00'),
 (383,'MtcaInfo','','','mtca.info@gmail.com','a67bb5b499b0b5a577df1069dada69eb',0,1,0,'0001-01-01 00:00:00','2012-12-21 00:00:00'),
 (384,'Mvcrendija','','','mvasquez.carmona@gmail.com','1d83d3006558da667a5b4068aa7b5fa8',0,1,0,'0001-01-01 00:00:00','2012-10-31 00:00:00'),
 (385,'Natalia','','','dorisbel@ryaguajay.icrt.cu','0e63a18f915c0c819498ef4c6bcada1f',0,1,0,'0001-01-01 00:00:00','2012-11-13 00:00:00'),
 (386,'Nefi-Reyes','','','nefireyes96@hotmail.com','19f2cf1eb3969c101914a87ddad28f51',0,1,0,'0001-01-01 00:00:00','2012-12-07 00:00:00'),
 (387,'Nelcolosho','','','renderos.nelson@outlook.com','0c241dcc6acb8200be67a66f01234443',0,1,0,'0001-01-01 00:00:00','2012-08-02 00:00:00'),
 (388,'Neo3504','','','neo3504@yahoo.com','ff2c58bc7e0a45a5f41b8b2db22e4c8d',0,1,0,'0001-01-01 00:00:00','2012-12-01 00:00:00'),
 (389,'Neptuno','','','otzi777@hotmail.com','563dbc1de7cf0b8605e1397770ce36a8',0,1,0,'0001-01-01 00:00:00','2012-11-25 00:00:00'),
 (390,'Neriant-Ramos','','','dexter416@gmail.com','518d46b22729b91037c8a401c1903a3a',0,1,0,'0001-01-01 00:00:00','2012-12-22 00:00:00'),
 (391,'Nestor-R-Ruiz-M','','','ruizmatiauda@gmail.com','27f89ed684c8caae85542ba326acd705',0,1,0,'0001-01-01 00:00:00','2012-11-15 00:00:00'),
 (392,'Ninoska-Davila','','','ndavila14@gmail.com','c913303f392ffc643f7240b180602652',0,1,0,'0001-01-01 00:00:00','2012-11-30 00:00:00'),
 (393,'Noctis-Caelum','','','nocturno_cielo@hotmail.com','596d43ca379ef3bfebcddbcad28abb05',0,1,0,'0001-01-01 00:00:00','2013-01-08 00:00:00'),
 (394,'Noe-Puigdengolas','','','noe.puig3@gmail.com','0bd00b6643215e5bf804ac2c1fcaecc9',0,1,0,'0001-01-01 00:00:00','2013-01-03 00:00:00'),
 (395,'Noelp','','','noelp@ccb.uclv.edu.cu','d43174e8ac6ba9edc1e7461a590d26c9',0,1,0,'0001-01-01 00:00:00','2012-12-01 00:00:00'),
 (396,'Nono-Espinar','','','nonoech@gmail.com','ddeebd8ab02b72f76af640fdf68cbeeb',0,1,0,'0001-01-01 00:00:00','2012-07-31 00:00:00'),
 (397,'Ny1109','','','hf@antevasin.com','7e39cfce74d155294619613f42484f18',0,1,0,'0001-01-01 00:00:00','2012-09-10 00:00:00'),
 (398,'Olga157','','','kitsia2000-@hotmail.com','8bb28b39d19dd3a4c1b9d6b5ed8da480',0,1,0,'0001-01-01 00:00:00','2012-11-25 00:00:00'),
 (399,'Omar-Gomez','','','gomezomar13@hotmail.com','de0c799cf3c1ebe535bb296bd527350d',0,1,0,'0001-01-01 00:00:00','2012-08-20 00:00:00'),
 (400,'Omar-Omara','','','lex145@hotmail.com','a4fd6d157ad9bbb149c3e3ff3ff965df',0,1,0,'0001-01-01 00:00:00','2012-09-21 00:00:00'),
 (401,'Omar1980','','','scornedang@hotmail.com','03fd027d8e71de4cea6fa2e4b051ff50',0,1,0,'0001-01-01 00:00:00','2012-08-05 00:00:00'),
 (402,'Omargom13','','','gomezomar13@operamail.com','187399decc0eaf99f83119958c774a16',0,1,0,'0001-01-01 00:00:00','2012-08-07 00:00:00'),
 (403,'Onel-Jose-Gonzalez','','','onelcaballerog@gmail.com','5ec097478bb7b73a98bb568e03376c09',0,1,0,'0001-01-01 00:00:00','2012-12-18 00:00:00'),
 (404,'Osavan','','','osavan39@hotmail.com','eed71a84b77ebe093326df8dfe49b7f0',0,1,0,'0001-01-01 00:00:00','2012-11-13 00:00:00'),
 (405,'Oscar','','','teorios09@gmail.com','f529756fc80e2bd311c3987eb3a4a7d9',0,1,0,'0001-01-01 00:00:00','2012-10-26 00:00:00'),
 (406,'Oscar-Juarez','','','oscar_hjh@hotmail.com','8e4290d797cd3b5e84f5e15ad530a789',0,1,0,'0001-01-01 00:00:00','2012-12-29 00:00:00'),
 (407,'Oscar-Peraza','','','oscarperaza@hotmail.com','95c01815ef7cce64fcb70e91cd3978b9',0,1,0,'0001-01-01 00:00:00','2012-10-05 00:00:00'),
 (408,'Oscargentino','','','o-alfredo@hotmail.com','e807f1fcf82d132f9bb018ca6738a19f',0,1,0,'0001-01-01 00:00:00','2012-12-16 00:00:00'),
 (409,'Osvaldo','','','osvaldowsp@live.cl','822a456dbd65ffd231ca043f7a22b49d',0,1,0,'0001-01-01 00:00:00','2012-11-30 00:00:00'),
 (410,'Otaku-Juan','','','juan93pablodm@hotmail.com','1b6e6bcf4231e1795147ca03462a6d0e',0,1,0,'0001-01-01 00:00:00','2012-11-02 00:00:00'),
 (411,'Otto-Ortega','','','ottomvs@hotmail.com','d31d1592d76ba9b08512d82a0c643d47',0,1,0,'0001-01-01 00:00:00','2012-08-01 00:00:00'),
 (412,'Pablodelai','','','poreldespertar@gmail.com','f521c9d23f82e30be29328886970179b',0,1,0,'0001-01-01 00:00:00','2012-08-21 00:00:00'),
 (413,'Pakinen','','','muuu_85@hotmail.com','dc485f4fbafdd3dd074bfb519d377353',0,1,0,'0001-01-01 00:00:00','2012-08-02 00:00:00'),
 (414,'Panzertruppen','','','okh1939@gmail.com','246c28d4dc1e83bedd734f61ea48f914',0,1,0,'0001-01-01 00:00:00','2012-11-17 00:00:00'),
 (415,'Paolacardenasmercado','','','paolacardenasmercado@gmail.com','9fd8301ac24fb88e65d9d7cd1dd1b1ec',0,1,0,'0001-01-01 00:00:00','2012-08-04 00:00:00'),
 (416,'Paolomaquerni','','','nico.vs@hotmail.com.ar','31603dee8f7d0e4676e025d6f4525e56',0,1,0,'0001-01-01 00:00:00','2012-09-29 00:00:00'),
 (417,'Patricia-Escarbais','','','pattescarbais@hotmail.com','fbf5d9fc9ea43ec18d8965aa6704f6fd',0,1,0,'0001-01-01 00:00:00','2012-10-26 00:00:00'),
 (418,'Patricio63Rojas','','','patricio63rojas@gmail.com','1be5df7a1ac3f14fc77553ed0ff97422',0,1,0,'0001-01-01 00:00:00','2012-12-20 00:00:00'),
 (419,'Pattychiba','','','silvita1986@hotmail.com','a1e5d172f3c7a70d432382f375472d5a',0,1,0,'0001-01-01 00:00:00','2012-10-30 00:00:00'),
 (420,'Paumaneiro','','','paumaneiro@gmail.com','f53dffc3c7517ab7c16e24ac2c62fea9',0,1,0,'0001-01-01 00:00:00','2012-10-08 00:00:00'),
 (421,'Pcarballosa','','','pedrocarballosam@gmail.com','0b5de470bdace90bd6cfb2541eb79f99',0,1,0,'0001-01-01 00:00:00','2012-12-04 00:00:00'),
 (422,'Pepecomunidades','','','pepemanager@gmail.com','002b547fb50055645b1736082e82807b',0,1,0,'0001-01-01 00:00:00','2012-09-16 00:00:00'),
 (423,'Perezj','','','perezj@lipesa.com','e10adc3949ba59abbe56e057f20f883e',0,1,0,'0001-01-01 00:00:00','2012-11-30 00:00:00'),
 (424,'Poetayanes','','','poetayanes@hotmail.com','3bbd8db3e20abbf6c640adc7c39fbae8',0,1,0,'0001-01-01 00:00:00','2012-11-22 00:00:00'),
 (425,'Polloman','','','criescace@hotmail.com','e6f64be0f560bcc795d0f92e1169e2bf',0,1,0,'0001-01-01 00:00:00','2012-08-09 00:00:00'),
 (426,'Pomelapocha','','','pomelapocha1@gmail.com','addc879fc34dcbeae7c2918176073288',0,1,0,'0001-01-01 00:00:00','2012-11-14 00:00:00'),
 (427,'Pookie','','','johanna_dominguez@hotmail.es','a101f5517d910319bf5d7f9493e6cca7',0,1,0,'0001-01-01 00:00:00','2012-11-20 00:00:00'),
 (428,'Profe_Creador','','','deleyade.creador@gmail.com','6f95bdaf41a2763a8789e26a76741745',0,1,0,'0001-01-01 00:00:00','2012-12-07 00:00:00'),
 (429,'Radhames-Ramos-Pena','','','radhamesramos23@hotmail.com','293e121ea613f326cd70fca19ee7bdb2',0,1,0,'0001-01-01 00:00:00','2012-11-27 00:00:00'),
 (430,'Rafael','','','rgb@cubarte.cult.cu','571657d7cc50b37a0affb982677b036c',0,1,0,'0001-01-01 00:00:00','2012-12-27 00:00:00'),
 (431,'Rafael-Casanova','','','rafael.casanova.fuertes@gmail.com','953f334ba4e5b5a85686c2cb9228ff1e',0,1,0,'0001-01-01 00:00:00','2012-12-06 00:00:00'),
 (432,'Rafael-Josue-Baez','','','rafaeljosue_basilisco@hotmail.com','e543fe3a816d17d6937e1a386de060b5',0,1,0,'0001-01-01 00:00:00','2012-11-25 00:00:00'),
 (433,'Rafael-Lindem','','','lindem75@hotmail.com','7d0710824ff191f6a0086a7e3891641e',0,1,0,'0001-01-01 00:00:00','2012-08-03 00:00:00'),
 (434,'Rafael-Mendez','','','erbealles@gmail.com','0c7d72d41dbdcf26668d0c9a6cae6f8e',0,1,0,'0001-01-01 00:00:00','2012-11-19 00:00:00'),
 (435,'Ramon-Chavez','','','ramon.chavez@prodigy.net.mx','f8f01ea0ac9fc0f45cdcb7cb6e730769',0,1,0,'0001-01-01 00:00:00','2012-09-16 00:00:00'),
 (436,'Ransett','','','ransett.tv@hotmail.com','4aec381049020b69cf1b0286d82dae20',0,1,0,'0001-01-01 00:00:00','2012-10-01 00:00:00'),
 (437,'Raul','','','raulbonachia@hotmail.com','02439b17d662ae26b2ed2b59ed975e3d',0,1,0,'0001-01-01 00:00:00','2012-11-07 00:00:00'),
 (438,'Raul-Nurena','','','raul_ndc@hotmail.com','bb5a652893b7ee62c679f5e7cb7d6a19',0,1,0,'0001-01-01 00:00:00','2012-11-03 00:00:00'),
 (439,'Raulcardillo','','','raul.cardillo@gmail.com','284539cb1e57b167a26ea0c6eaaf1689',0,1,0,'0001-01-01 00:00:00','2012-09-02 00:00:00'),
 (440,'Rendan','','','fernandousal@hotmail.com','a7162d621db825ab519faa27b2c3d3ac',0,1,0,'0001-01-01 00:00:00','2012-12-31 00:00:00'),
 (441,'Ricardo','','','ricardo.valdiviezo@hotmail.com','ba848824eb20bfd5a7fa5346fd834819',0,1,0,'0001-01-01 00:00:00','2012-12-17 00:00:00'),
 (442,'Ricardo-SA','','','ricardosalgadoarias@yahoo.es','33768d865bb4f0578dc9a6549b507824',0,1,0,'0001-01-01 00:00:00','2012-10-14 00:00:00'),
 (443,'Rickycorti','','','clubpuertochic@gmail.com','a1ff413e756121e9c4731c2d6f3d0cb6',0,1,0,'0001-01-01 00:00:00','2012-09-05 00:00:00'),
 (444,'Rigo-Mozart','','','mozartrigo@gmail.com','5efdfce1a94128d550ed9c6549866dbc',0,1,0,'0001-01-01 00:00:00','2012-11-15 00:00:00'),
 (445,'Rigores','','','rigores1507@hotmail.com','21daa466170028afae98b3d3f8ddb9a1',0,1,0,'0001-01-01 00:00:00','2012-11-22 00:00:00'),
 (446,'Rigores1507','','','santaellar@lipesa.com','e1892b056f477976f9f43436908b94dc',0,1,0,'0001-01-01 00:00:00','2012-11-22 00:00:00'),
 (447,'Rmilanes','','','milanesbalsalobre@hotmail.es','3a729f5ff174d568090c91a612cb2cc3',0,1,0,'0001-01-01 00:00:00','2012-08-11 00:00:00'),
 (448,'Robe-Ferrer','','','robe_spv@wanadoo.es','ea89805f556ca8cb3aa763f91d3e5663',0,1,0,'0001-01-01 00:00:00','2012-08-17 00:00:00'),
 (449,'Roberto-Rios-Reyes','','','rhrr08@hotmail.com','610bc7ee460662c9dc497122702ea38a',0,1,0,'0001-01-01 00:00:00','2012-12-15 00:00:00'),
 (450,'Roberto01','','','robertodubois002@gmail.com','e10adc3949ba59abbe56e057f20f883e',0,1,0,'0001-01-01 00:00:00','2012-11-03 00:00:00'),
 (451,'Robertorafael','','','rramirezm@estudiantes.uci.cu','ee54da89d4bd87302908b4a11a1a5eb6',0,1,0,'0001-01-01 00:00:00','2012-10-08 00:00:00'),
 (452,'RodolfoBlanquillo','','','r.blanquillo@hotmail.com','780ad80df47b23436462d9b33a4400be',0,1,0,'0001-01-01 00:00:00','2012-12-06 00:00:00'),
 (453,'Rodrigo222','','','rodrigo_lujan99@hotmail.com','e3788efbb5d04c345a37b5ece4f02879',0,1,0,'0001-01-01 00:00:00','2012-12-24 00:00:00'),
 (454,'Roger','','','r9lio99@gmail.com','ea8453bbb8d507a7899fefe88638c0d6',0,1,0,'0001-01-01 00:00:00','2013-01-02 00:00:00'),
 (455,'Ronald-Harrison-Qp','','','ronaldh.qp@gmail.com','f97e358e0dec8cb23d47b09b5f58b724',0,1,0,'0001-01-01 00:00:00','2013-01-08 00:00:00'),
 (456,'Ronald-Urquizo','','','raurquizo@gmail.com','69bd09c1f8bae452ecd9e91276c82c41',0,1,0,'0001-01-01 00:00:00','2012-08-19 00:00:00'),
 (457,'Roy-Lima','','','rlcrodrigo0@hotmail.com','49267bacf99a80daf610965202f2914d',0,1,0,'0001-01-01 00:00:00','2012-10-05 00:00:00'),
 (458,'Rpalacio','','','ruben_p@hotmail.com','6b503b2f2c97a63e99aea5bc694f6b5d',0,1,0,'0001-01-01 00:00:00','2012-08-19 00:00:00'),
 (459,'Rrobertdux','','','roberttauro@latinmail.com','38d324ab48874a3307b5f10214bfd15b',0,1,0,'0001-01-01 00:00:00','2012-11-10 00:00:00'),
 (460,'Rrrr','','','rrrodriguez05@yahoo.com.mx','082f0c91e23f357bbedbffdc51ef5d7d',0,1,0,'0001-01-01 00:00:00','2012-10-27 00:00:00');
INSERT INTO `db`.`auth_user` VALUES  (461,'Rubenoide','','','rubenalistevidal@gmail.com','1bd424edb668f48d22cd2f1cc20dbff7',0,1,0,'0001-01-01 00:00:00','2012-09-11 00:00:00'),
 (462,'Rubizul','','','el_rincon_de_eva@hotmail.com','084f61befecb3cc2ee227b0739bcf0b4',0,1,0,'0001-01-01 00:00:00','2012-08-04 00:00:00'),
 (463,'S0Met','','','carlos.somet@yahoo.com','ddf4dc44eaace28fd8d818c3a92b7ea6',0,1,0,'0001-01-01 00:00:00','2012-11-01 00:00:00'),
 (464,'Saloduenas','','','salo.duenas@hotmail.com','5eae511d337b0e3cea18ae514d8a1718',0,1,0,'0001-01-01 00:00:00','2012-08-23 00:00:00'),
 (465,'Sammy01','','','incltd4@yahoo.com','e10adc3949ba59abbe56e057f20f883e',0,1,0,'0001-01-01 00:00:00','2012-11-14 00:00:00'),
 (466,'Samrivas','','','samrivas14@gmail.com','8bb889419b68139beca50ba9bff49b24',0,1,0,'0001-01-01 00:00:00','2012-11-22 00:00:00'),
 (467,'Samuel-David','','','samif64@hotmail.com','29a3eab0fa053e29a4aac8428b0c8d56',0,1,0,'0001-01-01 00:00:00','2012-10-30 00:00:00'),
 (468,'Samuel-Elias-B','','','tu.friend-forever@hotmail.com','e7d94fe05464bac1a2c8954212cdb1a5',0,1,0,'0001-01-01 00:00:00','2012-09-22 00:00:00'),
 (469,'Sandra','','','sandrawandemberg@hotmail.com','87698924e638aaa7c88cef12353010b9',0,1,0,'0001-01-01 00:00:00','2012-11-03 00:00:00'),
 (470,'Sandra-Rodriguez','','','srrnlove@hotmail.com','de316140349a2daa6c00008a1cad7188',0,1,0,'0001-01-01 00:00:00','2012-12-09 00:00:00'),
 (471,'Sandra74','','','sdrasmne@yahoo.com.ar','fba4102ea19593583a472d97d5e83dec',0,1,0,'0001-01-01 00:00:00','2012-12-01 00:00:00'),
 (472,'Santaella','','','rigobertos1@cantv.net','e1892b056f477976f9f43436908b94dc',0,1,0,'0001-01-01 00:00:00','2012-11-26 00:00:00'),
 (473,'Santiago-E-Torres','','','santiago_tr_sucks@hotmail.com','afc656e03ad145fb719f3266bd1926ef',0,1,0,'0001-01-01 00:00:00','2012-10-29 00:00:00'),
 (474,'Sara-Duval','','','spadilla2908@hotmail.com','5124b28ea83446cc5495d0b87a946243',0,1,0,'0001-01-01 00:00:00','2012-12-12 00:00:00'),
 (475,'Sarai','','','miris_yo@hotmail.com','a0998df7841cf77fa3c7d20ac1daaff7',0,1,0,'0001-01-01 00:00:00','2012-12-21 00:00:00'),
 (476,'Scarlett','','','criz.nwh_9208@hotmail.com','d06d4e5b40ceaacc69454cf769114a34',0,1,0,'0001-01-01 00:00:00','2012-08-23 00:00:00'),
 (477,'Seba_Anuch5','','','seba_anuch5@hotmail.com','f59446240fcd619c6e831dbc8dddcd39',0,1,0,'0001-01-01 00:00:00','2012-08-19 00:00:00'),
 (478,'Seka21','','','am21@podmod.eu','7264fb5b59b43ec93f90ce61d41ca315',0,1,0,'0001-01-01 00:00:00','2012-12-03 00:00:00'),
 (479,'Serafin-Gimeno','','','serafi_gimeno@yahoo.es','827ccb0eea8a706c4c34a16891f84e7b',0,1,0,'0001-01-01 00:00:00','2012-07-29 00:00:00'),
 (480,'Sergio-Dasilva','','','sergiodasilva263@yahoo.es','ca28878916b9b81cda33a215608afb87',0,1,0,'0001-01-01 00:00:00','2012-11-12 00:00:00'),
 (481,'Silescribe','','','silje07@gmail.com','785f1dba5edaa3a4573299c530863d9b',0,1,0,'0001-01-01 00:00:00','2012-11-04 00:00:00'),
 (482,'Sofia-Del-Peru','','','corinarimondi@hotmail.com','c9c540982adec14f9b878c041bf51b47',0,1,0,'0001-01-01 00:00:00','2012-11-02 00:00:00'),
 (483,'Sofisilan','','','sofisilan@gmail.com','80bc852545794d408b4163011e9b0860',0,1,0,'0001-01-01 00:00:00','2012-08-13 00:00:00'),
 (484,'Solmarialuz','','','mergongross1@yahoo.com','1c49eb289edf02906d40e3dfe7aa91cf',0,1,0,'0001-01-01 00:00:00','2012-11-04 00:00:00'),
 (485,'Solselene','','','selene2sol@hotmail.com','32ab755a8de099df71400ce7e5b0d103',0,1,0,'0001-01-01 00:00:00','2012-12-04 00:00:00'),
 (486,'Sonia28','','','taki_sonia@hotmail.com','25f9e794323b453885f5181f1b624d0b',0,1,0,'0001-01-01 00:00:00','2012-11-09 00:00:00'),
 (487,'Stormtrooper71126','','','stormtrooper71126@hotmail.com','26c08e0ee3a0ed1e7d9a70d10735d92f',0,1,0,'0001-01-01 00:00:00','2012-11-26 00:00:00'),
 (488,'Tamil','','','tamilmorgan@hotmail.com','4cea55729c9bc51f3c631ba5102e4113',0,1,0,'0001-01-01 00:00:00','2012-12-15 00:00:00'),
 (489,'Tania-Puertas','','','universopresente@gmail.com','c0cbc0bd20dabae8cc946f893d6e7b41',0,1,0,'0001-01-01 00:00:00','2012-08-26 00:00:00'),
 (490,'Tanitaek','','','tanitaek@hotmail.com','9c6b1174a992ca5c900bcb6de7ce3258',0,1,0,'0001-01-01 00:00:00','2012-10-29 00:00:00'),
 (491,'Tayiel','','','jchristian318@gmail.com','2d75aa5be3ad54f58eb9a80b3cb40103',0,1,0,'0001-01-01 00:00:00','2012-11-29 00:00:00'),
 (492,'Templar-Puerto-Rico','','','cwodjs@gmail.com','8ecc1166ba1af390d96e284ccf61b02a',0,1,0,'0001-01-01 00:00:00','2012-08-18 00:00:00'),
 (493,'Tentets-Alex','','','tentetsalex1993@yahoo.es','81dc9bdb52d04dc20036dbd8313ed055',0,1,0,'0001-01-01 00:00:00','2012-11-08 00:00:00'),
 (494,'Tonnydau','','','tonnydau@yahoo.com','47ca45270122196613dfad2437d5174a',0,1,0,'0001-01-01 00:00:00','2012-10-26 00:00:00'),
 (495,'Tremel','','','tremel.marjorie@gmail.com','7fa6c26474ce9d03c70c8c16619b3083',0,1,0,'0001-01-01 00:00:00','2012-12-23 00:00:00'),
 (496,'Ueliton-RPereira','','','tom.pereira@hotmail.com','d0102ed58572e4f3f3181f17e3238651',0,1,0,'0001-01-01 00:00:00','2012-11-28 00:00:00'),
 (497,'Umar-Faruk-Al-Sadik','','','archivodelsol@yahoo.es','c8ff17f4f110795d639f67403ac8d1dc',0,1,0,'0001-01-01 00:00:00','2012-10-24 00:00:00'),
 (498,'Valerymr','','','vale_mar_r@hotmail.es','ec66a085a443216c060444ba56951d0d',0,1,0,'0001-01-01 00:00:00','2012-11-07 00:00:00'),
 (499,'Vanessa','','','vanessalumo@hotmail.com','323b7a32eb6e03fb2435a8bb442b86d5',0,1,0,'0001-01-01 00:00:00','2012-08-03 00:00:00'),
 (500,'Vanessajoses','','','vanessajoses@yahoo.com','6f4c0d0d32592fc86f44d9ee3f8b7184',0,1,0,'0001-01-01 00:00:00','2012-11-03 00:00:00'),
 (501,'Verano-Brisas','','','veranobrisas@gmail.com','188a8d84f10cd6c50f0335d252f8bcce',0,1,0,'0001-01-01 00:00:00','2012-12-01 00:00:00'),
 (502,'Veronica','','','verito.quezada.h@gmail.com','fd317045cbdcd26054de08112d6031b4',0,1,0,'0001-01-01 00:00:00','2012-12-04 00:00:00'),
 (503,'Viajeschina','','','vacacionchina@hotmail.com','e10adc3949ba59abbe56e057f20f883e',0,1,0,'0001-01-01 00:00:00','2012-12-23 00:00:00'),
 (504,'Viasoluciones','','','viasoluciones@outlook.com','7125fb326dcea16b0a4078b307be5c8a',0,1,0,'0001-01-01 00:00:00','2012-10-31 00:00:00'),
 (505,'Victor-De-Domingo','','','leonitas_2505@hotmail.com','11358d60bb5e993236e51f01febd7aa9',0,1,0,'0001-01-01 00:00:00','2012-08-27 00:00:00'),
 (506,'Vroc','','','veraemlille@hotmail.com','5dc3d054877155f1a1159ad3bb05266b',0,1,0,'0001-01-01 00:00:00','2013-01-04 00:00:00'),
 (507,'Wachter002','','','wachterbruno6@gmail.com','71b3b26aaa319e0cdf6fdb8429c112b0',0,1,0,'0001-01-01 00:00:00','2012-12-05 00:00:00'),
 (508,'Wallp','','','walterpascual@gmail.com','da6fa909f1c0188c539feb08d4496eb7',0,1,0,'0001-01-01 00:00:00','2012-08-23 00:00:00'),
 (509,'Walter-Delgado','','','instrenacer@yahoo.com','8251fa4cfdf5e33d8c040b87bd8c4bdb',0,1,0,'0001-01-01 00:00:00','2012-11-14 00:00:00'),
 (510,'Walter-Quispe-Castro','','','walqcas@yahoo.es','594d95233357032635f46943d05ec2a4',0,1,0,'0001-01-01 00:00:00','2012-10-04 00:00:00'),
 (511,'Workimedes','','','geral@workimedes.pt','97dc545cfdf228aadbdb2c39f8c0cc51',0,1,0,'0001-01-01 00:00:00','2012-08-05 00:00:00'),
 (512,'Xabitxo','','','romera@euskaltel.net','c83c67d9846bb4d3cbc7e861904bf772',0,1,0,'0001-01-01 00:00:00','2012-09-09 00:00:00'),
 (513,'Xavie','','','beaumonxavier@gmail.com','b01f7e3779c689d3942199e4ac39f7ee',0,1,0,'0001-01-01 00:00:00','2012-12-22 00:00:00'),
 (514,'Xlvx','','','alkbus16@hotmail.com','9019cbe4458150159d9cc2f1cd473cf1',0,1,0,'0001-01-01 00:00:00','2012-10-31 00:00:00'),
 (515,'Yader','','','rayj_2@hotmail.com','3b7c7ac1973f37a06f19c6c4b65091bc',0,1,0,'0001-01-01 00:00:00','2012-11-05 00:00:00'),
 (516,'Yanmarcosjbc','','','eltriunfador05@hotmail.com','e9d7ecaf3c083afc0b52df358596905a',0,1,0,'0001-01-01 00:00:00','2012-09-25 00:00:00'),
 (517,'Yazbek-Heredia-Ojeda','','','yazbek.14@hotmail.com','07e455655926b625501cfc90c784ff47',0,1,0,'0001-01-01 00:00:00','2012-11-23 00:00:00'),
 (518,'Yolanda-Palomares','','','ypmarcos@gmail.com','0028afda3993c0b53ace6d01ad70e31b',0,1,0,'0001-01-01 00:00:00','2012-07-31 00:00:00'),
 (519,'Yulicame','','','yuleidyscaraballo@hotmail.com','4aadd0789ef2d04daab6d3c52f71685c',0,1,0,'0001-01-01 00:00:00','2012-11-02 00:00:00'),
 (520,'Zarquis','','','zarquisb22@gmail.com','4e5e038025fc95fc75128c172c7149a7',0,1,0,'0001-01-01 00:00:00','2012-11-05 00:00:00'),
 (521,'Zenonita','','','lujanenroma@gmail.com','b93a17c9c2d8314c1b00474c7ec12695',0,1,0,'0001-01-01 00:00:00','2012-11-16 00:00:00'),
 (522,'Zeronightmares','','','zeronightmares@gmail.com','7b8353b278b217d39a47a83a85d66b7a',0,1,0,'0001-01-01 00:00:00','2012-07-29 00:00:00');
UNLOCK TABLES;
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;


--
-- Definition of table `db`.`auth_user_groups`
--

DROP TABLE IF EXISTS `db`.`auth_user_groups`;
CREATE TABLE  `db`.`auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_fbfc09f1` (`user_id`),
  KEY `auth_user_groups_bda51c3c` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`auth_user_groups`
--

/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
LOCK TABLES `auth_user_groups` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;


--
-- Definition of table `db`.`auth_user_user_permissions`
--

DROP TABLE IF EXISTS `db`.`auth_user_user_permissions`;
CREATE TABLE  `db`.`auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_fbfc09f1` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`auth_user_user_permissions`
--

/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
LOCK TABLES `auth_user_user_permissions` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;


--
-- Definition of table `db`.`book_comments`
--

DROP TABLE IF EXISTS `db`.`book_comments`;
CREATE TABLE  `db`.`book_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` varchar(150) NOT NULL,
  `user_id` varchar(55) NOT NULL,
  `comment` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `book_comments_752eb95b` (`book_id`),
  KEY `book_comments_fbfc09f1` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`book_comments`
--

/*!40000 ALTER TABLE `book_comments` DISABLE KEYS */;
LOCK TABLES `book_comments` WRITE;
INSERT INTO `db`.`book_comments` VALUES  (1,'Sistema','Gelito','A ver qué tal está el libro, parece que promete...'),
 (2,'Sistema','Angel-Faet','Hola Gelito, eres la primera persona que lo lee. Ya nos dirás que te parece. Un saludo!'),
 (3,'Sistema','Gelito','Muy interesantes las ideas del sistema que propones, a mí parecer te ha faltado detalle explicándolo y te ha sobrado explicando el sistema actual. Te hace pensar.'),
 (4,'Sistema','Angel-Faet','Pensar implica dedicación activa, energía dedicada. Celebro ese camino y agradezco un comentario que tomo por invitación. De recibir otros sin duda una segunda parte será publicada, pero no bajo la forma de ensayo. Como comento en la introducción “Sin duda el lector se sentirá invitado a exigirme el detalle sólido del fondo que le planteo, entre tanto sépase en paz de atacar sin reparos las ideas que ahora le llegan: en ello mi deseo.” Un saludo.'),
 (5,'Sistema','Gabriel-Faet','Interesantes tus críticas y propuestas, aunque también les veo sus debilidades. Defiendes posturas que pueden chocar a más de uno, aunque se trate de sentido común. Divulgativo, interesante y fácil de leer. Enhorabuena.'),
 (6,'Juicio-A-Un-Escritor','Gabriel-Faet','Al fin lo he leído. Hay varias historias que, en mi opinión, pueden dar para más que un relato corto, y te dejan un poco con la miel en los labios. Como \"El inventor mental\", \"El alma de los caídos\" y alguna otra. Un saludo!'),
 (7,'El-Troll','Gabriel-Faet','Tiene buen ritmo y la idea de una mano invisible que maneja más de lo que parece me ha gustado. Sí que creo que estaría bien ahondar más en el personaje del troll. Estupenda novela!'),
 (8,'Equilibrio-En-La-Sombra','Gabriel-Faet','Suspense con el Hollywood de primera mitad del siglo XX de fondo. Personajes muy elaborados. Una novela muy entretenida. Un saludo!'),
 (9,'Leyendas-Templarias','Gabriel-Faet','Muy curioso. Me ha hecho reparar en que, efectivamente, los templarios y su legado están más cerca de lo que creemos ( al menos los profanos...).'),
 (10,'El-Troll','Emilio-Tejera','Muchas gracias por tu aportación, Gabriel. Cualquier clase de opinión es bienvenida, y más si constructiva y/o favorable. Nos seguimos viendo por aquí. Un saludo'),
 (11,'Cthulhu-En-El-Pais-Vasco','Bernardo-Pereira','Queridos fans de Lovecraft: siempre quise escribir un cuento sobre los Mitos de Cthulhu. Hace poco empezaron unos extraños ruidos subterráneos en mi casa del País Vasco. El momento había llegado.'),
 (12,'Especiales-Todos','Luzdalila1974','Desde ahora la vida cambia, gracias a a los ángeles que me sonríen cada día, el cambio esta en mirar con los ojos del corazón...'),
 (13,'Las-Posadas-Turisticas','Alfredo-Ascanio','Esta es la publicación de A. Ascanio. LAS POSADAS TURISTICAS'),
 (18,'Dosis-De-Bien','Jesusmanuel','Hay un pequeño error ortográfico en mi apellido en la portada. Soy Sastrè no Satrè. ok? Cualquier cosa a mi correo: jm251995@hotmail.com'),
 (16,'Dosis-De-Bien','Jesusmanuel','Leanlo, está padre.'),
 (17,'Dosis-De-Bien','Jesusmanuel','Se los recomiendo.'),
 (19,'Dosis-De-Bien','Jesusmanuel','Ya corregí la portada. :)'),
 (20,'El-Troll','Emilio-Tejera','Tenemos un pequeño concurso para los lectores de \"El troll\". Podéis informaros acerca de él en la columna de la derecha del blog, emilio-tejera.blogspot.com. ¡Buena suerte a todos!'),
 (21,'El-Troll','Emilio-Tejera','A.P.F. dijo: \"Para el Concurso EL TROLL: Mi personaje favorito es Encarnita porque es la única buena persona\". Tú también puedes participar. '),
 (22,'Poemas-De-Amor-Para-Tiempos-De-Odio','Alejandro-Fuentes','Podéis ver el tráiler del libro en YouTube, espero que os guste: http://www.youtube.com/watch?v=1wISlXsgmVg'),
 (23,'Especiales-Todos','Luzdalila1974','Cada paso que doy, pienso quienes salen dañados, y cuanto puedo salir dañada, miro al rededor y sonrio....');
UNLOCK TABLES;
/*!40000 ALTER TABLE `book_comments` ENABLE KEYS */;


--
-- Definition of table `db`.`books`
--

DROP TABLE IF EXISTS `db`.`books`;
CREATE TABLE  `db`.`books` (
  `book` varchar(150) NOT NULL,
  `title` varchar(150) NOT NULL,
  `synopsis` longtext NOT NULL,
  `user_id` varchar(55) NOT NULL,
  `language_id_id` int(11) NOT NULL,
  `book_file_epub` varchar(150) NOT NULL,
  `book_file_pdf` varchar(150) NOT NULL,
  `cover` varchar(200) NOT NULL,
  `publication_date` date NOT NULL,
  `gender_id_id` int(11) NOT NULL,
  `sales` int(10) unsigned NOT NULL,
  `rate_id_id` int(11) NOT NULL,
  `price` decimal(64,2) NOT NULL,
  `book_active` tinyint(1) NOT NULL,
  `website` varchar(200) NOT NULL,
  `video` varchar(200) NOT NULL,
  PRIMARY KEY (`book`),
  KEY `books_fbfc09f1` (`user_id`),
  KEY `books_449741c3` (`language_id_id`),
  KEY `books_5ace06a8` (`gender_id_id`),
  KEY `books_71d01327` (`rate_id_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`books`
--

/*!40000 ALTER TABLE `books` DISABLE KEYS */;
LOCK TABLES `books` WRITE;
INSERT INTO `db`.`books` VALUES  ('Vertedero-De-Munecas','Vertedero de Muñecas','Novela coral de amores turbulentos, de relaciones-sutura incapaces de contener la derrota y el naufragio que acaban por engullir a sus protagonistas. Mireia, poseída por una melancolía mórbida que hunde sus raíces en una violación sufrida durante la infancia. Pau, su pareja, personaje vitalista incapaz de contener el dolor de ella. Dani y Mary, el primero vive confundido y atemorizado por el sentimiento desbordado de una mujer, Mary, que trata de exprimir la vida como si tras el presente no pudiera vislumbrarse o intuirse siquiera la existencia de un solo día, hora o minuto con antelación; amordazada por una impulsividad cimentada en un cuadro familiar repetitivo. Dani, débil y cobarde, intentará sobreponerse a sus limitaciones de la mano de Pau, cosa que conseguirá con éxito tras un cataclismo final. No deja de ser ésta una historia provista de villanos, Mandrágora, una psicóloga sin escrúpulos que intenta construir una secta que le permita vivir a expensas de sus adeptos; contará con la ayuda de Manel, un lumpen de comportamientos limítrofes con la psicopatía. Como tampoco deja de estar surtida de héroes, Marín Gonzalo, un policía bajo sospecha que intentará esclarecer las misteriosas apariciones de cadáveres que tienen lugar en un lago; Julián, un mendigo lastrado por el pasado a partir de cuyos percances intentarán rodar un corto un par de cineastas noveles.\r\n\r\nLa voz, la argamasa que une a todos estos personajes febriles, vitales y enamorados, es un antiguo profesor que nos cuenta la historia de todos ellos, como alumnos suyos que eran. La narración del docente tiene lugar mientras éste permanece sumergido en las mismas aguas del lago en el cual aparecen y desaparecen toda una colección de misteriosos ahogados.','Serafin-Gimeno',1,'books/Vertedero-De-Munecas.epub','','img/books/Vertedero-De-Munecas.jpg','2012-07-30',1,1,1,'0.99',1,'',''),
 ('Viaje-Al-Centro-Del-Universo','Viaje al centro del universo','Enol, Sergio y Nicolas son tres niños asturianos de siete años que nos invitan a realizar un viaje inolvidable. En busca de una respuesta: ¿cuál es el misterio de la vida? Estos tres amigos nos conducen a un mundo interior lleno de misterios donde el lector inevitablemente se replantea los pilares de su propia existencia. \r\nEsta parábola divertida y fresca está pensada para hacer reír al lector, entretenerlo y enriquecerlo. Una aventura para los niños y todo un aprendizaje para los adultos.\r\n\r\n<<Los cuentos no están creados para entretener a los niños sino para despertar a los adultos>> Isabel Menéndez\r\n<<Viaje al centro del Universo es toda una lección de vida que debería de estar en la biblioteca de todos los hogares>> Julita Casielles','Tania-Puertas',1,'books/Viaje-Al-Centro-Del-Universo.epub','','img/books/Viaje-Al-Centro-Del-Universo.jpg','2012-08-27',5,1,1,'0.99',1,'',''),
 ('Un-Camino-Para-Iniciar-Cambios-Positivos','Un camino para iniciar cambios positivos','Un camino para iniciar cambios positivos en la vida.','Aleida-Leyva-Garcia',1,'books/Un-Camino-Para-Iniciar-Cambios-Positivos.epub','','img/books/Un-Camino-Para-Iniciar-Cambios-Positivos.jpg','2012-11-27',5,1,1,'0.99',1,'',''),
 ('Versos-De-Luz-Un-Verso-Una-Luz','Versos de luz. un verso una luz.','Versos de luz son un conjunto de versos principalmente sobre la triple temática de la paz, la libertad y el amor.\r\n\r\nEs un libro con dos partes.\r\n\r\nLos primeros versos son de índole preciosista, formando un conjunto mayoritariamente de sonetos, versos en clave de tres y versos de métrica libre.\r\n\r\nLa segunda parte se denomina: VERSOS VERDADES.\r\n\r\nTodos los VERSOS VERDADES tienen como denominador común  el comenzar con la expresión de una verdad en un pareado.\r\n A este pareado le sigue una frase que es un complemento cultural de la verdad expresada o también un complemento estético que la embellece.\r\n\r\nEl conjunto forma así un terceto que además de su interés intrínseco, puede servir de molde para nuevas expresiones artístico/culturales de otros poetas. \r\n ','Fernando-Salvador',1,'books/Versos-De-Luz-Un-Verso-Una-Luz.epub','books/Versos-De-Luz-Un-Verso-Una-Luz.pdf','img/books/Versos-De-Luz-Un-Verso-Una-Luz.jpg','2012-12-04',3,0,1,'0.99',1,'',''),
 ('Tratado-De-Quimica-Mundana','Tratado de química mundana','Prepárese a realizar un viaje por dos caminos en teoría divergentes, el de la ciencia y el de la literatura, paseándose por cuentos, reflexiones y poemas muy breves pero llenos de romanticismo, meditación y sobre todo una simplicidad tal, que encanta al lector desde el principio. Con su libro el autor pretende a través de un enfoque mundano, transmutar sin fórmulas matemáticas ni químicas, el riguroso mundo científico...en uno accesible y mágico.','Rigores',1,'books/Tratado-De-Quimica-Mundana.epub','','img/books/Tratado-De-Quimica-Mundana.jpg','2012-11-25',4,2,1,'0.99',1,'',''),
 ('Suenos-De-Poeta','Sueños de poeta','La poesía es esencia sublime del corazón, motor fundamental del amor, amor que lucha a pesar de las guerras que a diario se viven, principalmente las internas. La poesía de Edwin Yanes, es un aliciente para el mundo que vive en constante guerra, son dardos directos al corazón del lector que ávidamente busca perderse en el universo de los sentimientos, sentimientos que no están muertos, sólo yertos por falta de cariño y solidaridad humana.\r\n\r\nVen y juntos naveguemos el mar de la imaginación, en el camino fascinante del amor y el desamor hechos poesía, que nace del corazón.\r\n\r\nLIBERTAD\r\n¿Dime dónde estás?\r\n¿Te quedaste en la imaginación?\r\n¿Eres tan sólo una ilusión?\r\nO quizás un disfraz\r\n\r\nMucho se habla de ti,\r\nLos filósofos te cuestionan,\r\nLos juristas te traicionan\r\nY yo quiero creer en ti.\r\n\r\n¿Qué eres en verdad?\r\nDe pronto una fantasía,\r\nAlgo más que utopía\r\nO simple vanidad.\r\n\r\nHacer uso de ti quiero,\r\nQue importa la cordura,\r\nSi el alma no tiene cura,\r\nEn un ser prisionero.\r\n\r\nA quienes han bebido de tu fuente,\r\nLa sociedad ha querido postergar,\r\nCondenando todo de pecado,\r\nSin embargo han sabido triunfar.','Poetayanes',1,'books/Suenos-De-Poeta.epub','','img/books/Suenos-De-Poeta.jpg','2012-11-22',3,0,1,'0.99',1,'',''),
 ('Sistema','Sistema','¿Y si todos pudiéramos perseguir nuestros sueños sin miedo a que otros tomen lo que es nuestro? ¿Y si no fuera necesario endeudar países, rescatar bancos o engañar ciudadanos? ¿Y si pudiéramos elegir en todo momento el participar del sistema financiero... o el no hacerlo?\r\n\r\nLadrones y estados, corporaciones e indignados, especuladores y jubilados... valores e intereses en apariencia y fondo enfrentados. Somos diferentes, y lo seguiremos siendo. Convivimos en un mismo mundo, y lo seguiremos haciendo. Para que los estados se liberen de los bancos, los bancos se liberen de los estados, y los ciudadanos y las empresas de las ataduras de ambos.\r\n\r\nFinanzas presentes, Finanzas futuras. Un nuevo SISTEMA.','Angel-Faet',1,'books/Sistema.epub','books/Sistema.pdf','img/books/Sistema.jpg','2012-07-29',2,6,1,'2.99',1,'','vid/books/Sistema.webm'),
 ('Sueno-Inalcanzable','Sueño Inalcanzable','Una pequeña novela de amor, con un poco de erotismo\r\n\r\nBasada  en una historia de una chica, que se enamora de un doctor, ambos pasan por muchos obstáculos, la diferencia de edad, la familia y un grave accidente seran siempre los encargados de hacer qué su amor se desvanezca\r\n\r\n¿Salvarán todos los obstáculos?','Rubizul',1,'books/Sueno-Inalcanzable.epub','','img/books/Sueno-Inalcanzable.jpg','2012-08-04',1,0,4,'0.99',1,'',''),
 ('Sidi-El-Nino-Del-Desierto','Sidi, el niño del desierto','Totó y Mía son hermanos, viven con sus padres en un bonito pueblo de la provincia de Valladolid. Ellos no lo saben pero estas vacaciones tendrán un invitado muy especial. Sidi, un niño saharaui pasará sus primeras vacaciones en España.\r\nAcompaña a los niños en esta entrañable aventura y disfruta de un verano único, donde la amistad, la generosidad y la familia, serán los mayores tesoros que guardaremos en nuestro corazones después de conocer esta historia.\r\n','Adriana-Paire',1,'books/Sidi-El-Nino-Del-Desierto.epub','','img/books/Sidi-El-Nino-Del-Desierto.jpg','2012-07-31',1,1,1,'0.99',1,'',''),
 ('Saideth','Saideth','Saideth, una chica particular y atractiva conoce a Danielle un chico simpático.El al primer momento se enamora de ella,pero  sin saber que hay detrás de toda su atracción. La mentira y la verdad se unen, el no sabrá que hacer. Hasta que descubre la verdad en sus narices sin imaginárselo.','Samrivas',1,'books/Saideth.epub','','img/books/Saideth.jpg','2012-11-22',9,0,1,'0.99',1,'',''),
 ('Recetas-De-Mary','Recetas de mary','Espero que este pequeño y sencillo recetario te sirva para elaborar unos muy sabrosos platos. \r\n\r\n','Mary-Carmen',1,'books/Recetas-De-Mary.epub','','img/books/Recetas-De-Mary.jpg','2012-11-21',7,0,1,'0.99',1,'',''),
 ('Saberes','Saberes','Saberes es una obra poco convencional. Es quizás la historia mas constructivista que se haya escrito. Es tu mente la encargada de darle el sentido y la interpretación a esta obra. Tú formas el contexto, el aspecto, el sentimiento y la razón del \"Saber\". Es un viaje profundo en la locura de simpáticos personajes, es un manantial de conversaciones que mezclados crean uno de los textos mas particulares de nuestra literatura moderna.','German-Sanchez',1,'books/Saberes.epub','','img/books/Saberes.jpg','2012-11-14',1,0,1,'0.99',1,'',''),
 ('Nose-Si-Ya-Te-Paso','Nose si ya te paso','breve relato de una chica de 19 años que sufrió una brisa fugaz de algo parecido al amor ','Angie-Zelada',1,'','books/Nose-Si-Ya-Te-Paso.pdf','img/books/Nose-Si-Ya-Te-Paso.jpg','2012-11-14',9,0,1,'0.99',1,'','vid/books/Nose-Si-Ya-Te-Paso.webm'),
 ('Pacifico-Sur','Pacífico Sur','El cuento Pacifico Sur trata de un hombre que vive en 2 dimensiones, conectado en ellas bajo una fiebre, dandose cuenta de que ambas son reales, y que quiere vivir en la que le agrada mas, convive con su realidad, y esta dispuesto a aceptar su extraña situación...hasta que cosas inesperadas suceden...en las costas del Pacifico Sur...','Jose-J-Bejaranis',1,'books/Pacifico-Sur.epub','','img/books/Pacifico-Sur.jpg','2012-07-30',1,0,1,'0.99',1,'',''),
 ('Poemas-De-Amor-Para-Tiempos-De-Odio','Poemas de Amor para Tiempos de Odio','En este poemario el autor alicantino nos presenta 32 poemas inéditos. Además, el libro incluye el prólogo escrito por el autor lorquino de novelas de misterio Fran J. Marber y un epílogo del propio autor. Poemas con rima clara, sin resultar simple, y cargados de sentimiento. Esta nueva obra (que actualmente acaba de publicar su 4ª edición en papel) no dejará indiferente a nadie.','Alejandro-Fuentes',1,'','books/Poemas-De-Amor-Para-Tiempos-De-Odio.pdf','img/books/Poemas-De-Amor-Para-Tiempos-De-Odio.jpg','2013-01-18',3,0,1,'0.99',1,'http://alejandrofuentes.neositios.com/',''),
 ('Mi-Primer-Ovni','Mi Primer OVNI ','El periodista Joaquín Pereira desentraña el misterio de la caída de un extraño objeto en un pueblo del interior de Venezuela. Lo que encontró cambió su vida y lo muestra en su libreta de notas. Usted también se conmoverá con la lectura de este sorprendente libro.','Joaquin_Pereira',1,'','books/Mi-Primer-Ovni.pdf','img/books/Mi-Primer-Ovni.jpg','2012-08-21',2,0,1,'0.99',1,'',''),
 ('Miracle-A-LInfern','Miracle a l´Infern','Siglo XXII, diversas potencias envían sus misiones al planeta Milagro, en el sistema de Alpha Centauri. Los tripulantes, algunos de los cuales arrastran un pasado de guerras terribles en la Tierra, encuentran una flora y una fauna similares a las que conocen de su propio mundo. Mientras los nuevos colonos se instalan en su nuevo hogar, tribus autóctonas aún por descubrir por los terrestres se enfrentan entre ellas?','Lluis-Carrera',9,'books/Miracle-A-LInfern.epub','','img/books/Miracle-A-LInfern.jpg','2012-12-04',1,0,1,'0.99',1,'',''),
 ('Menino-Que-Se-Chamava-Ninguem','Menino que se chamava Ninguém','Era uma vez um menino chamado Ninguém vivia na terra dos perdidos e o seu maior sonho era ser alguém, mas para isso terá que viver muitas aventuras .','Marco-Ramos',5,'','books/Menino-Que-Se-Chamava-Ninguem.pdf','img/books/Menino-Que-Se-Chamava-Ninguem.jpg','2012-11-23',10,0,2,'0.99',1,'',''),
 ('Mi-Compromiso-Con-El-Amor','Mi compromiso con el amor','Una novela de amor con personajes reales en hechos reales, donde se describe cómo hacemos del amor una ficción.\r\n\r\nEs un contenido analítico y reflexivo sobre cómo influye este sentimiento en nuestro comportamiento, retratando escenas cotidianas, y no por ello menos aleccionadoras. A pesar de lo complejo del tema (el amor ni más ni menos), es una lectura digerible.\r\n\r\nAbel, un poeta de Zacatecas, México, evalúa de un modo crítico, en verso y en prosa, la metamorfosis de la que somos objeto cuando lo experimentamos, abordándolo desde su primer amor hasta el compromiso final con la mujer que ama. Incrusta también historias alternas de su entorno social, en las cuales, junto con la suya, podemos advertir las fallas en que incurrimos cuando nos enamoramos, y cuando enfrentamos el desamor...\r\n\r\nParte de este libro es parte de nuestra historia. Para el amor cada cabeza es un mundo; para el desamor somos una colectividad...\r\n','Abel-Nedi',1,'','books/Mi-Compromiso-Con-El-Amor.pdf','img/books/Mi-Compromiso-Con-El-Amor.jpg','2012-12-29',1,0,1,'0.99',1,'',''),
 ('Los-Templarios-En-El-Camino-De-Santiago','Los Templarios en el Camino de Santiago','El Camino de las ocas salvajes, la vía de las estrellas, el Camino de Santiago como ruta espiritual para la mayoría de los peregrinos y la ruta sagrada de los templarios. En este libro encontramos el Camino del Temple donde las señales y los mensajes de los templarios permanecen.','Antoine-Nolla',1,'books/Los-Templarios-En-El-Camino-De-Santiago.epub','','img/books/Los-Templarios-En-El-Camino-De-Santiago.jpg','2012-07-30',2,11,1,'0.99',1,'',''),
 ('Luztragaluz','Luztragaluz','El mundo cibernético se ha expandido tanto por Gea Terra Gaia y todos los planetas colonizados, las Terrae, que la humanidad ve peligrar su hegemonía frente a los ekstrim, robots con apariencia y psicoimplantes humanos. Para erradicarlos, cuenta con los focos de luz, ekstrim reprogramados para la caza, anulación y conversión de otros ekstrim.\r\n\r\nAdeldran, un foco de luz muy especial, se irá cuestionando la razón de esta situación y nos irá descubriendo los vericuetos ocultos del Sistema.','Jesus-Fernandez',1,'books/Luztragaluz.epub','books/Luztragaluz.pdf','img/books/Luztragaluz.jpg','2012-07-30',1,2,1,'0.99',1,'',''),
 ('Las-Posadas-Turisticas','Las Posadas turísticas','Las Posadas turísticas: una alternativa de alojamiento artesanal y sustentable.','Alfredo-Ascanio',1,'books/Las-Posadas-Turisticas.epub','books/Las-Posadas-Turisticas.pdf','img/books/Las-Posadas-Turisticas.jpg','2012-11-18',4,0,1,'0.99',1,'',''),
 ('Leyendas-Templarias','Leyendas Templarias','Antoine Nolla es un conocido investigador de temas relacionados con la Orden del Temple. En esta obra ha recopilado una serie de leyendas relacionadas con los templarios que son las que con mayor rigor han perdurado en el tiempo. Estas leyendas son las que han conservado la esencia del espíritu de los templarios y nos harán darnos cuenta que están más cerca de lo que pensamos.','Antoine-Nolla',1,'books/Leyendas-Templarias.epub','','img/books/Leyendas-Templarias.jpg','2012-07-30',2,6,1,'0.99',1,'',''),
 ('Los-Templarios-En-El-Bierzo','Los Templarios en el Bierzo','El autor pone a nuestro alcance los resultados de su trabajo en tierras del Bierzo después de dos años de estancia. Da a conocer las huellas del Conocimiento y la Sabiduría dejada por los Pobres Caballeros de Cristo en lugares donde la historia oficial ha ignorado, hasta ahora su existencia.','Antoine-Nolla',1,'books/Los-Templarios-En-El-Bierzo.epub','','img/books/Los-Templarios-En-El-Bierzo.jpg','2012-07-30',2,3,1,'0.99',1,'',''),
 ('La-Felicidad-Esta-En-El-Corazon','La felicidad está en el corazón','Después de la muerte de Saray, mi perro guardián, decidí escribir una historia sobre estos perros de la Fundación ONCE del Perro Guía, su educación y su convivencia con nosotros','Mary-Carmen',1,'books/La-Felicidad-Esta-En-El-Corazon.epub','','img/books/La-Felicidad-Esta-En-El-Corazon.jpg','2012-11-04',5,1,1,'0.99',1,'',''),
 ('La-Luz-En-Dostoievski','La luz en dostoievski','Manuel Díaz Márquez nos muestra de una manera magistral en este ensayo filosófico cómo es el verdadero Dostoievski, muy lejos de ese hombre oscuro que nos habían retratado en el pasado. Un libro ideal para las personas con más curiosidad intelectual que deseen conocer a fondo la extraordinaria y original manera de pensar del escritor y filósofo ruso.','Ediciones-Casas',1,'books/La-Luz-En-Dostoievski.epub','','img/books/La-Luz-En-Dostoievski.jpg','2012-10-29',2,0,1,'0.99',1,'',''),
 ('La-Mascara-Del-Pigmeo','La Máscara del Pigmeo','En el fondo del golfo de guinea, sobrevive una civilización aborigen amenazada de erradicación completa del mapa terrestre. Inmerso en la exuberancia de lo real maravilloso, Kandongo no es solamente el universo ensoñador y fantástico en que brotan ostensiblemente los retales de la idiosincrasia africana, sino también el foco de las codicias, malicias y tragedias por una parte, de las ambiciones, devociones, y sensaciones por otra.','Christian-Tiako',1,'books/La-Mascara-Del-Pigmeo.epub','','img/books/La-Mascara-Del-Pigmeo.jpg','2013-01-25',10,0,1,'0.99',0,'',''),
 ('La-Piramide-De-Luz','La Pirámide de Luz','En el Egipto del año 1908 antes de nuestra era, una joven sacerdotisa tendrá que enfrentarse al poder de la oscuridad.\r\n\r\nAlguien intenta destronar al faraón Sesostris I y sumir a Egipto en las tinieblas de Set. Sólo el poder del círculo de Ra podrá combatir a las fuerzas del mal.\r\n\r\n¿Estará Ahotep preparada para la difícil prueba que cambiará su sencilla existencia para siempre?\r\n\r\n¿Podrá el faraón salir victorioso de este complot y seguir gobernando con mano firme el país de las Dos Tierras?','Gema-Heras',1,'books/La-Piramide-De-Luz.epub','books/La-Piramide-De-Luz.pdf','img/books/La-Piramide-De-Luz.jpg','2012-08-03',1,2,1,'0.99',1,'',''),
 ('Escantula','Escántula','Categoría: Teatro\r\n\r\nEscántula: una mansión en algún lugar remoto de España, en algún momento del siglo XX anterior a la aparición de los móviles e Internet.\r\n\r\nMarta no esperaba visitar la casa de Escántula hasta que tuvo noticias de la muerte de su hermana, que trabajaba allí de criada. Tras acudir apresuradamente, conocerá a sus habitantes, sobre todo a Ángel, el carismático terrateniente que parece capaz de leer el fondo de su corazón sin mostrar a cambio emociones propias.\r\n\r\nEl viaje de Marta les hará profundizar en el sentido de la belleza y el dolor, los misterios de la casa, y la incontenible curiosidad de Ángel, y hará que la vida en esta casa cambie para siempre.','Carlos-Mingorance',1,'books/Escantula.epub','','img/books/Escantula.jpg','2012-07-29',6,2,1,'0.99',1,'',''),
 ('Especiales-Todos','Especiales Todos','Esta obra esta dirigida a todo publico que lea con el corazón y que diga no a la discriminación, la mayoría son historias sacadas de la realidad, cartas reales y vivencias propias. espero lo disfrutes..','Luzdalila1974',1,'books/Especiales-Todos.epub','','img/books/Especiales-Todos.jpg','2012-11-11',5,0,1,'0.99',0,'','vid/books/Especiales-Todos.webm'),
 ('Guia-Para-Tramitar-Extradiciones-En-Colombia','Guía para tramitar extradiciones en Colombia','Cuando una persona es capturada en Colombia por pedido de extradición a una corte de otro país, son muchos los interrogantes que se plantea el capturado y su familia.\r\n      \r\nEn World Legal Corporation pretendemos hacer un acercamiento del tema a quien sufre y padece esta situación especial. Nuestros consejos tienen por objeto mostrar en detalle los aspectos más relevantes del proceso de extradición en Colombia, desde que el solicitado es capturado con fines de extradición hasta que el proceso finaliza\r\n','Miguelramirez',1,'','books/Guia-Para-Tramitar-Extradiciones-En-Colombia.pdf','img/books/Guia-Para-Tramitar-Extradiciones-En-Colombia.jpg','2012-12-02',4,0,1,'0.99',1,'',''),
 ('Jamas-Y-Siempre-A-La-Vez','Jamás y siempre a la vez','Hacía tiempo que el agua era un bien escaso en el planeta Tierra. El descubrir nuevos mundos y colonizar el espacio no contribuyó a que ese tesoro tan preciado dejara de ser la clave para comenzar una guerra, cualquier guerra.\r\n\r\nEl planeta Tierra entró a formar parte de la Confederación Cósmica de Mundos Habitados en el año 2525. El agua era su bien exclusivo pero, al sumarse a esta unión de mundos, tuvo que compartir todas las bondades de la misma. A medida que crecía el nivel de exportación, también lo hacía el riesgo de deshidratación irreversible del planeta. La solución se halló en la creación de productores estables en cada uno de los planetas, planetoides y satélites naturales destinados para tal fin en cada uno de los 58 sectores espaciales existentes.\r\n\r\nPero los poderes fácticos de los mundos no terráqueos no se podían permitir el depender de la patente exclusiva que detentaba el Planeta Azul. Las rebeliones locales, las catástrofes naturales y la ambición empezaron a desestabilizar la perfecta armonía de la Confederación.\r\n\r\nLa guerra sedujo a los pacíficos, el poder a los humildes y la Humanidad fue testigo de cómo el origen de la vida podría ser la causa de destrucción de la misma.\r\n\r\nUna más de las paradojas del Universo.','Jesus-Fernandez',1,'books/Jamas-Y-Siempre-A-La-Vez.epub','books/Jamas-Y-Siempre-A-La-Vez.pdf','img/books/Jamas-Y-Siempre-A-La-Vez.jpg','2012-07-30',1,1,1,'0.99',1,'',''),
 ('La-Copula-De-La-Mantis','La cópula de la mantis','Esther huye de su madre, busca respuestas y acude al encuentro de un antiguo pretendiente de su progenitora: Ismael. Entre ambos irán destejiendo la tempestuosa vida de Beatriz, la madre de Esther. A partir del encuentro entre Esther e Ismael se establece una narración a dos voces: Esther habla dirigiéndose a su madre, Ismael a la niña ya crecida que un día tuvo en su casa. A medida que ambas voces se entrecruzan hasta casi solaparse, descubrimos a una mujer, madre y amante, torturada por percepciones vitales que invitan a la zozobra, al naufragio emocional. Beatriz es incapaz de una gestión adecuada de sus emociones, vive atrapada en una hostilidad hacia el mundo y hacia si misma que la devora sin remisión. Afectada por un trastorno límite de personalidad, su búsqueda de un romanticismo continuamente renovado la obliga a cambios de pareja, cuantiosos y precipitados.\r\n\r\n\"La cópula de la mantis\" refleja la penuria emocional en que vive un 2% de la población. El trastorno límite es una dolencia psíquica de la que cada vez se dispone de mayor información y que despierta el interés de un creciente número de personas, ya sea por lo que respecta a las personas afectadas de lleno por el trastorno como por la influencia del mismo en familiares y amigos. En este sentido, la novela llena un hueco narrativo muy importante.\r\n\r\nEs ésta una novela de traiciones, de despechos, de incomprensiones, de amores inalcanzables, de estupor y dolor? de renuncias y cobardías; pero también es una novela tierna que arroja una mirada de comprensión a esa herida, siempre sangrante del alma humana. Una novela que contribuye a materializar, con un trazo preciso, el gran fresco de esa España agitada en la que vivimos, enmarcándola en el concepto más amplio de la sociedad occidental.','Serafin-Gimeno',1,'books/La-Copula-De-La-Mantis.epub','','img/books/La-Copula-De-La-Mantis.jpg','2012-07-29',1,1,1,'0.99',1,'',''),
 ('Lagrimas-De-Madera','Lágrimas de madera','Dos mundos opuestos, dos vidas separadas por un abismo insuperable? Una misma calle y un elemento común: la nicotina. Certezas, incertidumbres. Porque el amor, cuando no muere, mata. Porque amores que matan nunca mueren.\r\n\r\nLas leyes, los reyes, las clases sociales, las aberraciones cotidianas? todo eso se puede detener, cambiar, modificar e incluso anular. Sin embargo, hay dos cosas que nada ni nadie pueden detener: el tiempo y el amor. Así que comienza una historia de amor en la que el tiempo pasa marcha atrás.\r\n\r\nUna constelación de personajes tan vivos y reales que casi se pueden tocar con las manos. Y en el centro, ellos dos: Mario y Lucía. Eterno juego del equilibrio. Y sin red.\r\n\r\nQuédate en silencio. Escucha el tic-tac del reloj que tienes sobre la mesa del comedor. O el que tienes en la mesilla de noche. Se escapa y nunca vuelve. Las oportunidades también. No pierdas la tuya y sumérgete en una historia de amor que te dejará con la boca abierta.','Alejandro-Sotodosos',1,'books/Lagrimas-De-Madera.epub','','img/books/Lagrimas-De-Madera.jpg','2012-07-30',1,0,1,'0.99',1,'',''),
 ('Juicio-A-Un-Escritor','Juicio a un escritor','\"Juicio a un escritor\" es una recopilación de 16 relatos fruto de ciertas inquietudes, de sueños interrumpidos y de ideas disparatadas. Su principal característica es la heterogeneidad. Hay cuentos fantásticos y otros más realistas, pero lo onírico está presente en todos ellos.\r\n\r\nSus protagonistas van desde un escritor que se enfrenta a un juicio por culpa de una excavadora hasta un inventor que se niega a compartir sus descubrimientos, pasando por un hombre incapaz de levantar una vieja fotografía de su padre.\r\n\r\nResultó ganadora del Primer Premio en el Concurso de Literatura Joven del Instituto Aragonés de la Juventud (año 2011).  ','Carlos-Gamissans',1,'books/Juicio-A-Un-Escritor.epub','books/Juicio-A-Un-Escritor.pdf','img/books/Juicio-A-Un-Escritor.jpg','2012-07-30',9,8,1,'0.99',1,'http://cgamissans.blogspot.com/',''),
 ('En-La-Sombra-Del-Miedo','En la Sombra del Miedo','Una historia donde nuestro protagonista, debido a la enfermedad de su esposa, deberá viajar a un pequeño pueblo misterioso, donde sufrirá una gran pesadilla, lleno por el remordimiento del pasado de esta pareja.','Carlos-De-Hernan',1,'books/En-La-Sombra-Del-Miedo.epub','','img/books/En-La-Sombra-Del-Miedo.jpg','2012-08-07',1,0,4,'0.99',1,'http://carlosdehernan.blogspot.mx/2012/08/en-la-sombra-del-miedo.html',''),
 ('Equilibrio-En-La-Sombra','Equilibrio en la sombra','\"Equilibrio en la sombra\" se sumerge en el mundo cinematográfico hollywoodiense de la primera mitad del siglo XX a partir de las experiencias de un productor de origen irlandés en un período marcado por las dos guerras mundiales y otros acontecimientos como la persecución instigada por el senador conservador McCarthy en los años 50, conocida como la Caza de brujas. Es una reflexión sobre el séptimo arte con dosis de suspense e intensos diálogos.','Ernesto-Ramirez',1,'books/Equilibrio-En-La-Sombra.epub','books/Equilibrio-En-La-Sombra.pdf','img/books/Equilibrio-En-La-Sombra.jpg','2012-07-30',1,2,1,'0.99',1,'',''),
 ('El-Tunel-Y-El-Espejismo','El túnel y el espejismo','En enero del 2006 se descubrió en la franja fronteriza de Tijuana y San Diego un túnel con todos los adelantos técnicos. Pasadizo obra de la mafia con el propósito de pasar drogas y personas a los Estados Unidos, fue el túnel número 39 encontrado a lo largo de la frontera norte, la mayoría en Baja California y Sonora. Este hecho noticioso es el motivo inicial para la elaboración de esta novela.\r\n\r\nUno de los aspectos más sobresalientes de la vida fronteriza es el constante flujo migratorio. Personas del centro y sur del país que intentan a toda costa traspasar la línea. En las décadas de los ochentas y con mayor énfasis en los noventas, este intento por encontrar trabajo en Estados Unidos es acompañado por la emigración centroamericana, donde la salvadoreña se ha destacado.\r\n\r\nEl narcotráfico y el paso de indocumentados conforman los elementos centrales de dos historias que se entrelazan en esta obra. El denominador común es la trama policíaca. La lucha contra las mafias; la dedicada al narcotráfico, así como al secuestro y el contrabando de personas. Dos hilos conductores dan lugar a una sola historia: la construcción del túnel y la travesía de un niño salvadoreño desde su tierra hasta Tijuana con la intención de llegar a Los Ángeles, Ca.\r\n\r\nLa novela transcurre, entre agosto del 2005 y julio del 2006. En este período se suceden aspectos de gran relevancia que son el fondo en donde las historias se desenvuelven: el proceso electoral mexicano que culmina en julio del 2006; las movilizaciones de organizaciones latinas en contra de la construcción de la malla fronteriza y el debate contra la Sensebrenner 447 para impedir el paso de indocumentados; la pelea entre pandillas salvadoreñas (mara salvatrucha) en Los Ángeles y las fiestas patrias mexicanas.','Mario-Valencia',1,'books/El-Tunel-Y-El-Espejismo.epub','','img/books/El-Tunel-Y-El-Espejismo.jpg','2012-07-30',1,0,4,'0.99',1,'',''),
 ('El-Heredero-De-Merlin','El heredero de Merlín','Esta novela cuenta la historia de Toni, un chico próximo a cumplir los 15 años de edad. Hijo adoptivo, le es revelado el secreto de su nacimiento profetizado por Nostradamus. Junto a su amiga Mariseth, viajará a una isla desconocida en el Triángulo de Las Bermudas, donde gárgolas, vampiros y otros seres fantásticos habitan desafiando toda imaginación.','Frank-Losa-Aguila',1,'books/El-Heredero-De-Merlin.epub','','img/books/El-Heredero-De-Merlin.jpg','2012-07-31',1,1,1,'0.99',1,'',''),
 ('El-Maestrillo-Y-Sus-Discipulos','El maestrillo y sus discípulos','EL MAESTRILLO Y SUS DISCÍPULOS, constituye una serie de cuentos breves, microcuentos o minificciones, que muestran un personaje muy singular: EL MAESTRILLO; un sabio cotidiano que mediante la ironía, la lúdica y la reflexión, enseña valores y realidades para enriquecer la vida. De manera que en este texto encontrará 30 minutos excelentes para enriquecer la vida y reflexionar la realidad social circundante. Se asume este texto como una primera etapa, pero pronto vendrán nuevos cuentitos para alegrar y mejorar la vida, la personalidad y las actitudes. No se arrepentirá de estas lecturas.','Alfonso-Barreto',1,'books/El-Maestrillo-Y-Sus-Discipulos.epub','','img/books/El-Maestrillo-Y-Sus-Discipulos.jpg','2012-11-10',9,0,1,'0.99',1,'',''),
 ('El-Troll','El troll','En la sección de local de un pequeño periódico, todo parece relativamente anodino: fotógrafos, redactores, pequeñas miserias, y las habituales rutinas cotidianas. Y sin embargo, hay una persona que no debería estar allí. Hay alguien cuyo objetivo no es informar. Y si tú te encuentras cerca de su órbita, es bastante probable que le estés ayudando, aún sin saberlo. Y si esto es así, ten por seguro que alguien saldrá herido: y que varias personas (quizás tú mismo) pueden acabar terriblemente mal.\r\nDel autor de ?Cartago. El imperio de los dioses? llega una novela corta que se adentra en los resquicios más oscuros del corazón de las personas. Una historia que recorre los más bajos instintos; un relato sobre la incertidumbre, la maldad, la desconfianza, la esperanza, la redención, el miedo. Un recorrido sin retorno por la naturaleza de la humanidad.\r\nPrepárate para sumergirte en un mundo situado en los pasadizos subterráneos del tuyo propio. Encuéntrate con el lado más oscuro del hombre. Descubre un nuevo tipo de sociedad.\r\nPorque para él, sólo eres un peón (o un obstáculo) para llevar a cabo sus planes?\r\n','Emilio-Tejera',1,'books/El-Troll.epub','','img/books/El-Troll.jpg','2012-07-30',1,6,1,'0.99',1,'','vid/books/El-Troll.webm'),
 ('El-Aguila-Y-El-Gorrion','El águila y el gorrión','Esta obra está basada en la historia real de un chico adolescente (el gorrión) que pierde a su madre en un trágico accidente quedándose solo en el mundo; y aunque el creía que era una persona fuerte para afrontar tan duro golpe, al poco tiempo se ve inmerso en una profunda tristeza que le hace perder toda ilusión y esperanza de volver a ser el mismo de siempre: un chico alegre, divertido, sociable y aventurero con gran cantidad de recursos que le hacían ponerse cada día \"el mundo por montera\". Su apatía y falta de lucha le llevan a perder el interés incluso para alimentarse apoderándose de él una gran debilidad.\r\n\r\nDe repente aparece en su vida una Enfermera ( el águila ) que con infinita paciencia y cariño y con un constante y disciplinado trabajo, cada día le va poniendo pequeñas metas hasta que consigue devolverle la alegría que siempre le caracterizó.\r\n\r\nDe la constancia y el tesón de esta gran profesional de la Enfermería, junto a su gran corazón nació una gran amistad entre ambos que ni el tiempo ni la distancia han podido borrar y en la actualidad siguen siendo grandes amigos.','Marisol-Escribano',1,'books/El-Aguila-Y-El-Gorrion.epub','','img/books/El-Aguila-Y-El-Gorrion.jpg','2012-07-30',9,0,2,'0.99',1,'',''),
 ('El-Devenir-De-La-Senora-White','El Devenir de la Señora White','Cuatro cuentos atravesados por la tecnología, el amor, la reflexión y la verdad oculta...','Maria-Fiore',1,'books/El-Devenir-De-La-Senora-White.epub','','img/books/El-Devenir-De-La-Senora-White.jpg','2012-11-12',1,0,4,'0.99',1,'',''),
 ('El-Evangelio-De-Andrea','El Evangelio de Andrea','Es la historia de a una pareja de jóvenes; uno agnóstico y la otra cristiana evangélica, que se conocen a raíz un accidente.\r\n\r\nProducto de este suceso, a él le surge una especie de sinestesia cerebral,  la que  le permite percibir los espíritus de las personas sólo cuando éstas dejan el cuerpo al morir o  al nacer [encarnar].\r\n\r\nLa trama se matiza con una sutil dosis de encandilado erotismo, complementada con vívidas y emocionantes experiencias de regresión y reencarnación. Las mismas que tras un inesperado final, los une para siempre como pareja y en un mismo pensar espiritual sobre lo que seria para ellos el verdadero amor de Dios.','Miguell-E-Valdivia',1,'books/El-Evangelio-De-Andrea.epub','books/El-Evangelio-De-Andrea.pdf','img/books/El-Evangelio-De-Andrea.jpg','2012-07-30',1,0,1,'0.99',1,'http://www.migueleduardovc.com/','vid/books/El-Evangelio-De-Andrea.webm'),
 ('El-Grupo-Del-Fin-Del-Mundo','El grupo del fin del mundo','Simón y Natalia son una pareja de novios muy enamorada. Durante la celebración de año nuevo para recibir el 2012, Simón brinda en broma por el último año del mundo. Entre quienes los acompañan en la mesa esta Rafael, un convencido de que el final de los tiempos esta en curso.\r\nRafael relata sobre un grupo que se reúne cada cierto tiempo para intercambiar opiniones sobre, lo que ellos consideran, las señales del apocalipsis. ¿Cómo se relacionan las vidas de estos jóvenes con la del líder del grupo, Octavio?, ¿qué puede hacer Natalia para convencer a su novio de que el 21 de diciembre no pasará nada? \r\nNadie sabe qué pasa por la mente de Octavio al acercarse el día, pero para entonces todos estarán involucrados.','Dragon_Literario',1,'books/El-Grupo-Del-Fin-Del-Mundo.epub','','img/books/El-Grupo-Del-Fin-Del-Mundo.jpg','2012-12-19',1,0,1,'0.99',1,'',''),
 ('Dosis-De-Bien','Dosis de bien','Texto literario de sastre que te ayudará a hacer filantropía aplicando las normas para ser retribuido en abundancia. Cómpralo ya.','Jesusmanuel',1,'books/Dosis-De-Bien.epub','','img/books/Dosis-De-Bien.jpg','2012-12-04',5,0,1,'0.99',1,'',''),
 ('Ehsariell','Ehsariell','¿Qué harías si un día descubres que aquella voz en el interior de tu cabeza no es la de tu conciencia? ¿Estarías dispuesto a dejarlo todo? ¿Tu familia, tu vida, tu mundo? \r\n¿Serías capaz de soportar las más intensas pruebas de valor, fuerza, lealtad y astucia con el fin ayudar a una mujer desconocida y a todo un mundo? ¿Llegarías incluso a arriesgar tu vida con tal cumplir una promesa?\r\n\r\nEsta es la historia de un niño convertido en hombre y la más grande aventura jamás contada. Narra el viaje de Ike a través de las tierras en guerra de un mundo paralelo al nuestro, en donde habitan seres que para nosotros son sólo mitos. En su camino, Ike, conocerá humanos con poderes casi comparables a los de los dioses: Los Guardianes y Vicarios; encargados de proteger los elementos más importantes del mundo y que tienen un papel de vital importancia que le permitirá al muchacho poder cumplir la misión que le fue encomendada trece años atrás, cuando tenía sólo siete años de edad: Liberar a Ehsariell.\r\n\r\nSaludos cordiales, ','Alvarohart',1,'books/Ehsariell.epub','','img/books/Ehsariell.jpg','2012-11-19',1,0,1,'0.99',1,'',''),
 ('Cuentario-De-Guri','Cuentario de Guri','En diciembre del año 1999 ocurrió una de las peores desgracias en Venezuela. Los deslaves de Vargas han sido una de las peores catástrofes que dejó el siglo pasado, producto de las interminables lluvias. Miles de damnificados de Vargas fueron trasladados a distintas regiones y estados del país. Una parte la trasladaron al sur, al pueblo Guri, asiento de una de las más importantes represas hidroeléctricas del mundo. Estos... ¿diarios?, ¿cuentos? intentan una aproximación a ese pedacito de varguenses y sus historias. Al final, uno no sabe si éstos son diarios que sueñan ser cuentos o cuentos que pretenden ser diarios.','Mvcrendija',1,'books/Cuentario-De-Guri.epub','','img/books/Cuentario-De-Guri.jpg','2012-10-31',2,0,1,'0.99',1,'',''),
 ('Y-Si-Fueras-Ella','¿y si fueras ella?','Esta novela es el idilio de la ideas entre dos amantes que han de separarse de manera dolorosa. Carlos tendrá que buscar de nuevo siempre con la duda: ¿y si fueras ella?\r\nEn una relación amorosa siempre hay cosas que aunque se digan no se saben.\r\ndos mujeres, un solo amante, una relación mas fuerte de lo que cabe suponer','Omar-Gomez',1,'books/Y-Si-Fueras-Ella.epub','','img/books/Y-Si-Fueras-Ella.jpg','2012-08-25',1,0,1,'0.99',1,'',''),
 ('1-Ss-Division-Panzer-Leibstandarte-Adolf-Hitler','1° SS Division Panzer Leibstandarte Adolf Hitler','Desde los principios de la constitución del Partido Nazi NSDAP, antes de tomarse el poder, en sus años de formación, la élite del Partido conformo unidades especiales cuyo único propósito   es   la   guarda   y protección de sus líderes. La primera de ellas se constituyo cuando   los   hombres   de   la Compañía 19 de Morteros, bajo el mando de Ernst Rohm, actúa como un cuerpo de guardia en las primeras reuniones de Adolf Hitler y otros miembros del Partido.\r\nEste primer grupo de hombres se constituyo como las SA, las cuales pronto empezaron a crecer. Desde sus inicios Adolf Hitler las vio mas como un problema, que como una herramienta útil. A medida que fueron creciendo Hitler ordeno la formación de una unidad especial de seguidores leales destinada a proteger a los líderes de la NSDAP. Esta unidad fue constituida bajo el mando de Julius Schreck y Joseph Berchtold.\r\nSin embargo a pesar de estar separada en sus funciones, seguía bajo el control de las SA. Para esta época se determino utilizar como insignia de esta unidad la Calavera o Totenkopf, sobre la gorra de los hombres, además de otra serie de elementos exclusivos de esta organización. El 9 de Noviembre de 1923 esta unidad, además de las SA y otras de la NSDAP tomo parte en el fracasado Putsch de Munich contra la República de Weimar.Las SA continuaron existiendo bajo el nombre de Frontbahn, bajo el lide-razgo de Ernst Rohm. Bajo el mando de Rohm, las SA empezaron a crecer sustancialmente, desde 2.000 en Noviembre de 1923 a 30.000 en muy corto tiempo.\r\n','Panzertruppen',1,'books/1-Ss-Division-Panzer-Leibstandarte-Adolf-Hitler.epub','','img/books/1-Ss-Division-Panzer-Leibstandarte-Adolf-Hitler.jpg','2012-11-17',8,0,1,'0.99',1,'',''),
 ('Dos-Meses-Sin-Alba','Dos meses sin Alba','Marta, dieciocho años, se ha dado cuenta de que cada día quiere más a su hermana: más que el sol a los planetas, más que los puntos finales a las frases que buscan su fin. Alba tiene siete años, y ha desaparecido. Nadie sabe dónde está. Y Marta sólo puede consolarse enviándole cartas diariamente, para que así, cuando la vuelva a ver, no se haya perdido ni un solo detalle de su vida sin ella.\r\n\r\nComo las vías del tren, sus existencias siguen una misma trayectoria sin tocarse jamás. Pero si alzamos la vista, hay un lugar, donde las retinas ya no abarcan, en que las rectas se unen: el infinito. Y Marta, aunque tenga que cruzar todos los mares del mundo, llegará a tocar el alba.\r\n\r\nEn este su primer libro publicado, Alejandro Sotodosos ahonda sin complejos en el difícil mundo de los sentimientos. Un libro intimista, desgarrado a veces, esperanzado otras, que aborda temas tabú con la frente bien alta.\r\n\r\nUna historia estremecedora, que cautiva desde la primera línea y nos lleva directos a ese punto más allá de la vista donde se unen las líneas paralelas: el infinito, que impregna nuestras vidas desde el primer instante de la existencia.\r\n\r\nSólo tú puedes descubrir si la terminará encontrando. ¿Vas a quedarte con la intriga?','Alejandro-Sotodosos',1,'books/Dos-Meses-Sin-Alba.epub','','img/books/Dos-Meses-Sin-Alba.jpg','2012-07-30',1,2,1,'0.99',1,'',''),
 ('Dialogos-Con-Jaque','Diálogos con Jaque','Diálogos con Jaque es una antología poética que pretende comprender lo que no se puede comprender pero que existe en la naturaleza y en las letras: la poética de los sentimientos, la melancolía, el narcisismo, la nostalgia, el deseo, la filosofía, los sueños?y sobre todo el amor como motor de todo lo lírico.','Ernesto-Ramirez',1,'books/Dialogos-Con-Jaque.epub','books/Dialogos-Con-Jaque.pdf','img/books/Dialogos-Con-Jaque.jpg','2012-07-30',3,1,1,'0.99',1,'',''),
 ('Destino-Sangriento','Destino Sangriento','Noa es una vampiresa de 600 años que hace ya mucho que dejó de creer en la humanidad.\r\nEn su eterno deambular por la tierra encuentra a Erick que se ve arrastrado a un mundo que desconoce y no comprende.\r\nAmbos, tendrán que unir sus fuerzas para poder escapar de aquellos que desean someterlos y usarlos para sus propios fines.\r\n¿Serán capaces de escapar de las trabas que el destino les pondrá delante?¿Llegarán a descubrir qué les hace especiales? Un nuevo mundo se extiende ante ellos y, juntos, tendrán que encontrar sus propias respuestas.','Imawolfe',1,'books/Destino-Sangriento.epub','','img/books/Destino-Sangriento.jpg','2012-11-04',1,0,4,'0.99',1,'',''),
 ('Cthulhu-En-El-Pais-Vasco','Cthulhu en el País Vasco','Un descendiente de los marinos del capitán Marsh hereda una propiedad en el País Vasco, donde no tardan a manifestarse extraños fenómenos. Todos los tópicos que el buen fan de Cthulhu desea.  Los mitos de Lovecraft en un cuento perfecto para leer cuando te acuestes.','Bernardo-Pereira',1,'','books/Cthulhu-En-El-Pais-Vasco.pdf','img/books/Cthulhu-En-El-Pais-Vasco.jpg','2012-11-05',1,1,1,'0.99',1,'',''),
 ('Cronicas-De-Un-Nino-Que-Volaba','Crónicas de un niño que volaba','Estas líneas parten de mi historia de niño, pero es una historia latente que muchos viven, sin que alguien haga algo. Más allá de las aventuras y desventuras que aquí se cuentan con esmero, estas líneas son un grito sobre eso que todo mundo habla: la realidad de los niños y las niñas en nuestros países, sobre la que nos inundan con discursos, lemas, promesas políticas, campañas, proselitismo; el día del niño, la navidad? pero la realidad es y sigue siendo la misma, adversa en extremo, como silencioso huracán contra seres tan frágiles que no son sólo son esos mugres duendes a los que ya nos hemos acostumbrado a utilizar y llamar ?Niños de la calle?, sino a vergonzosas cantidades de ellos en los territorios marginales, urbanos y rurales, donde, como bien lo dicen tantos ?consultores? y burócratas que viven de las cifras y estadísticas, producto de la pobreza extrema, sufren todas las plagas que asolan sus calles, sus habitaciones o covachas, su mundo de subdesarrollo. En forma de historia o drama personal, es un esfuerzo por develar la miseria que contra ellos se da en todos los sentidos, directos y colaterales, marcándoles la vida o truncándoselas; a veces destruyéndoles o convirtiéndoles en monstruos que luego nos preguntamos de donde salieron o en qué fallamos. Muchos, desde su comodidad, se niegan a reconocer que existe la muerte infantil por hambre, ignorando que ésta y la enraizada desnutrición producen demasiada vulnerabilidad, propensión e inmunodeficiencias, tanto físicas, sicológicas, emocionales y espirituales, como la que se origina en esos seres que, según los noticieros, murieron por trastornos digestivos, o infectocontagiosos, ocultando deliberadamente que se envenenaron con las bacterias, hongos o amibas contenidas en la basura que se vieron obligados a comer; eso se llama muerte por hambre, y la han sufrido y siguen sufriendo miles de niños y niñas, producto de nuestra desidia e indiferencia. Y así mueren de súbito, progresivamente o superviven llenos de cicatrices internas, producto de la violencia intrafamiliar, por abandono, por su entorno comunitario, mediático. Es sólo cuestión de rating noticioso, lucro, y 30 segundos de compasión televidente, ante la emisión de imágenes de niños encadenados, calcinados al interior de sus jaulas de cartón, cinc y pedazos de madera; ángeles mutilados o asesinados. ?Crónicas de un niño que volaba? habla de un niño, y en él de cada niño, con nombre, cuerpo y alma, y el derecho a no ser invisible en su ?drama?. Hubiese deseado prologar estas líneas con un poema sobre lo bello que suele ser esa estación de la vida, la infancia inmortal, sus virtudes y sueños; los ideales sobre el deber ser de algo tan determinante para todo ser humano y sus frutos. Decidí romper con la sutileza y la emotividad retorica para contar lo terrible y lo intenso; prescindí de las mentirillas para contar una verdad.\r\n\r\nPero también se escribe aquí la ternura, el valor, la luz que tienen los niños para curarse y para crear puertas de salida, sueños.\r\n\r\nSin pretensiones o monotonías, hablo de que ?Yo puedo ser tú, tú puedes ser yo, y que no es preciso ser un famoso o célebre para ser alguien, tener historia, para, por medio de tu vida, ayudar a otros a sobreponerse del dolor y, a pesar de los mil pesares, aprender a amar y cuidar la vida, a los demás, que es lo único que al final tenemos. Sin adoptar poses de modelo o maestro, por medio de esta historia sugiero que si tan sólo nos interesáramos en los demás, en los más frágiles o vulnerables, en el vecino, en los niños? su sentir y pensar, su historia, les entenderíamos, y los conflictos serían menos duros, y sabríamos de la verdadera armonía, esa palabra en la que ya muchos no quieren creer, el amor, único camino a la paz.\r\n\r\nCada hoja es una motivación a liberarse de los días oscuros, recuperar para siempre la sonrisa. Bien dicen que tu vida puede ser útil para la humanidad si tu espíritu es contar la fatalidad y la experiencia por amor, no por egos o re-sentimientos, sino con un propósito, rescatar nuestros días de ayer para vislumbrar un ahora y un m','Kenni-Bolanos',1,'books/Cronicas-De-Un-Nino-Que-Volaba.epub','','img/books/Cronicas-De-Un-Nino-Que-Volaba.jpg','2012-07-30',1,1,1,'0.99',1,'',''),
 ('Conmigo-Pero-Solo','Conmigo Pero Solo','La soledad es el remedio perfecto de los cobardes, la cura a los males que no poseemos y que nunca serán nuestros. La soledad es la enfermedad del que, a ojos del mundo, es totalmente sano. La soledad hace daño, tortura, hace cosquillas y besa. La soledad soy yo? ¿Quieren acompañarme?','Jeav10',1,'books/Conmigo-Pero-Solo.epub','','img/books/Conmigo-Pero-Solo.jpg','2012-11-09',2,0,1,'0.99',1,'http://climaxlit.blogspot.com/','');
UNLOCK TABLES;
/*!40000 ALTER TABLE `books` ENABLE KEYS */;


--
-- Definition of table `db`.`countries`
--

DROP TABLE IF EXISTS `db`.`countries`;
CREATE TABLE  `db`.`countries` (
  `iso` varchar(2) NOT NULL,
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_es` varchar(40) NOT NULL,
  `country_en` varchar(40) NOT NULL,
  PRIMARY KEY (`country_id`),
  UNIQUE KEY `iso` (`iso`)
) ENGINE=MyISAM AUTO_INCREMENT=238 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`countries`
--

/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
LOCK TABLES `countries` WRITE;
INSERT INTO `db`.`countries` VALUES  ('',1,'Selecciona tu país','Choose your country'),
 ('AF',2,'Afganistán','Afghanistan'),
 ('AL',3,'Albania','Albania'),
 ('DE',4,'Alemania','Germany'),
 ('AD',5,'Andorra','Andorra'),
 ('AO',6,'Angola','Angola'),
 ('AI',7,'Anguilla','Anguilla'),
 ('AQ',8,'Antártida','Antarctica'),
 ('AG',9,'Antigua y Barbuda','Antigua and Barbuda'),
 ('SA',10,'Arabia Saudí','Saudi Arabia'),
 ('DZ',11,'Argelia','Algeria'),
 ('AR',12,'Argentina','Argentina'),
 ('AM',13,'Armenia','Armenia'),
 ('AW',14,'Aruba','Aruba'),
 ('AU',15,'Australia','Australia'),
 ('AT',16,'Austria','Austria'),
 ('AZ',17,'Azerbaiyán','Azerbaijan'),
 ('BS',18,'Bahamas','Bahamas'),
 ('BH',19,'Bahréin','Bahrein'),
 ('BD',20,'Bangladesh','Bangladesh'),
 ('BB',21,'Barbados','Barbados'),
 ('BY',22,'Bielorrusia','Belorussia'),
 ('BE',23,'Bélgica','Belgium'),
 ('BZ',24,'Belice','Belize'),
 ('BJ',25,'Benin','Benin'),
 ('BM',26,'Bermudas','Bermudas'),
 ('BT',27,'Bhután','Bhutan'),
 ('BO',28,'Bolivia','Bolivia'),
 ('BA',29,'Bosnia Herzegovina','Bosnia and Herzegovina'),
 ('BW',30,'Botsuana','Botswana'),
 ('BV',31,'Isla Bouvet','Bouvet Island'),
 ('BR',32,'Brasil','Brazil'),
 ('BN',33,'Brunéi','Brunei Darussalam'),
 ('BG',34,'Bulgaria','Bulgaria'),
 ('BF',35,'Burkina Faso','Burkina Faso'),
 ('BI',36,'Burundi','Burundi'),
 ('CV',37,'Cabo Verde','Cape Verde'),
 ('KY',38,'Islas Caimán','Cayman Islands'),
 ('KH',39,'Camboya','Cambodia'),
 ('CM',40,'Camerún','Cameroon'),
 ('CA',41,'Canadá','Canada'),
 ('CF',42,'República Centroafricana','Central African Republic'),
 ('TD',43,'Chad','Tchad'),
 ('CZ',44,'República Checa','Czech Republic'),
 ('CL',45,'Chile','Chile'),
 ('CN',46,'China','China'),
 ('CY',47,'Chipre','Cyprus'),
 ('CX',48,'Isla de Navidad','Christmas Island'),
 ('VA',49,'Ciudad del Vaticano','Vatican City'),
 ('CC',50,'Islas Cocos','Cocos (Keeling) Islands'),
 ('CO',51,'Colombia','Colombia'),
 ('KM',52,'Comoras','Comoro Islands'),
 ('CD',53,'República Democrática del Congo','The Democratic Republic of the Congo'),
 ('CG',54,'Congo','Congo'),
 ('CK',55,'Islas Cook','Cook Islands'),
 ('KP',56,'Corea del Norte','North Korea'),
 ('KR',57,'Corea del Sur','South Korea'),
 ('CI',58,'Costa de Marfil','Ivory Coast'),
 ('CR',59,'Costa Rica','Costa Rica'),
 ('HR',60,'Croacia','Croatia'),
 ('CU',61,'Cuba','Cuba'),
 ('DK',62,'Dinamarca','Denmark'),
 ('DM',63,'Dominica','Dominica'),
 ('DO',64,'República Dominicana','Dominican Republic'),
 ('EC',65,'Ecuador','Ecuador'),
 ('EG',66,'Egipto','Egypt'),
 ('SV',67,'El Salvador','El Salvador'),
 ('AE',68,'Emiratos Árabes Unidos','United Arab Emirates'),
 ('ER',69,'Eritrea','Eritrea'),
 ('SK',70,'Eslovaquia','Slovakia'),
 ('SI',71,'Eslovenia','Slovenia'),
 ('ES',72,'España','Spain'),
 ('UM',73,'Islas ultramarinas de Estados Unidos','United States Minor Outlying Islands'),
 ('US',74,'Estados Unidos','United States of America'),
 ('EE',75,'Estonia','Estonia'),
 ('ET',76,'Etiopía','Ethiopia'),
 ('FO',77,'Islas Feroe','Faroe Islands'),
 ('PH',78,'Filipinas','Philippines'),
 ('FI',79,'Finlandia','Finland'),
 ('FJ',80,'Fiyi','Fiji'),
 ('FR',81,'Francia','France'),
 ('GA',82,'Gabón','Gabon'),
 ('GM',83,'Gambia','Gambia'),
 ('GE',84,'Georgia','Georgia'),
 ('GH',85,'Ghana','Ghana'),
 ('GI',86,'Gibraltar','Gibraltar'),
 ('GD',87,'Granada','Grenada'),
 ('GR',88,'Grecia','Greece'),
 ('GL',89,'Groenlandia','Greenland'),
 ('GP',90,'Guadalupe','Guadeloupe'),
 ('GU',91,'Guam','Guam'),
 ('GT',92,'Guatemala','Guatemala'),
 ('GF',93,'Guayana Francesa','French Guiana'),
 ('GN',94,'Guinea','Guinea'),
 ('GQ',95,'Guinea Ecuatorial','Equatorial Guinea'),
 ('GW',96,'Guinea-Bissau','Guinea-Bissau'),
 ('GY',97,'Guyana','Guyana'),
 ('HT',98,'Haití','Haiti'),
 ('HM',99,'Islas Heard y McDonald','Heard Island and McDonald Islands'),
 ('HN',100,'Honduras','Honduras'),
 ('HK',101,'Hong Kong','Hong Kong'),
 ('HU',102,'Hungría','Hungary'),
 ('IN',103,'India','India'),
 ('ID',104,'Indonesia','Indonesia'),
 ('IR',105,'Irán','Iran'),
 ('IQ',106,'Iraq','Iraq'),
 ('IE',107,'Irlanda','Ireland'),
 ('IS',108,'Islandia','Iceland'),
 ('IL',109,'Israel','Israel'),
 ('IT',110,'Italia','Italy'),
 ('JM',111,'Jamaica','Jamaica'),
 ('JP',112,'Japón','Japan'),
 ('JO',113,'Jordania','Jordan'),
 ('KZ',114,'Kazajstán','Kazakhstan'),
 ('KE',115,'Kenia','Kenya'),
 ('KG',116,'Kirguistán','Kyrgyzstan'),
 ('KI',117,'Kiribati','Kiribati'),
 ('KW',118,'Kuwait','Kuwait'),
 ('LA',119,'Laos','Laos'),
 ('LS',120,'Lesotho','Lesotho'),
 ('LV',121,'Letonia','Latvia'),
 ('LB',122,'Líbano','Lebanon'),
 ('LR',123,'Liberia','Liberia'),
 ('LY',124,'Libia','Libya'),
 ('LI',125,'Liechtenstein','Liechtenstein'),
 ('LT',126,'Lituania','Lithuania'),
 ('LU',127,'Luxemburgo','Luxembourg'),
 ('MO',128,'Macao','Macao'),
 ('MK',129,'Macedonia','Macedonia'),
 ('MG',130,'Madagascar','Madagascar'),
 ('MY',131,'Malasia','Malaysia'),
 ('MW',132,'Malawi','Malawi'),
 ('MV',133,'Maldivas','Maldives'),
 ('ML',134,'Mali','Mali'),
 ('MT',135,'Malta','Malta'),
 ('FK',136,'Islas Malvinas','Falkland Islands (Malvinas)'),
 ('MP',137,'Islas Marianas del Norte','Northern Mariana Islands'),
 ('MA',138,'Marruecos','Morocco'),
 ('MH',139,'Islas Marshall','Marshall Islands'),
 ('MQ',140,'Martinica','Martinique'),
 ('MU',141,'Mauricio','Mauritius'),
 ('MR',142,'Mauritania','Mauritania'),
 ('YT',143,'Mayotte','Mayotte'),
 ('MX',144,'México','Mexico'),
 ('FM',145,'Micronesia','Micronesia'),
 ('MD',146,'Moldavia','Moldova'),
 ('MC',147,'Mónaco','Monaco'),
 ('MN',148,'Mongolia','Mongolia'),
 ('MS',149,'Montserrat','Montserrat'),
 ('MZ',150,'Mozambique','Mozambique'),
 ('MM',151,'Myanmar','Myanmar'),
 ('NA',152,'Namibia','Namibia'),
 ('NR',153,'Nauru','Nauru'),
 ('NP',154,'Nepal','Nepal'),
 ('NI',155,'Nicaragua','Nicaragua'),
 ('NE',156,'Níger','Niger'),
 ('NG',157,'Nigeria','Nigeria'),
 ('NU',158,'Niue','Niue'),
 ('NF',159,'Isla Norfolk','Norfolk Island'),
 ('NO',160,'Noruega','Norway'),
 ('NC',161,'Nueva Caledonia','New Caledonia'),
 ('NZ',162,'Nueva Zelanda','New Zealand'),
 ('OM',163,'Omán','Oman'),
 ('NL',164,'Países Bajos','Netherlands'),
 ('PK',165,'Pakistán','Pakistan'),
 ('PW',166,'Palau','Palau'),
 ('PA',167,'Panamá','Panama'),
 ('PG',168,'Papúa Nueva Guinea','Papua New Guinea'),
 ('PY',169,'Paraguay','Paraguay'),
 ('PE',170,'Perú','Peru'),
 ('PN',171,'Islas Pitcairn','Pitcairn'),
 ('PF',172,'Polinesia Francesa','French Polynesia'),
 ('PL',173,'Polonia','Poland'),
 ('PT',174,'Portugal','Portugal'),
 ('PR',175,'Puerto Rico','Puerto Rico'),
 ('QA',176,'Qatar','Qatar'),
 ('GB',177,'Reino Unido','United Kingdom'),
 ('RE',178,'Reunión','Reunion'),
 ('RW',179,'Ruanda','Rwanda'),
 ('RO',180,'Rumania','Romania'),
 ('RU',181,'Rusia','Russia'),
 ('EH',182,'Sahara Occidental','Western Sahara'),
 ('SB',183,'Islas Salomón','Solomon Islands'),
 ('WS',184,'Samoa','Samoa'),
 ('AS',185,'Samoa Americana','American Samoa'),
 ('KN',186,'San Cristóbal y Nevis','Saint Kitts and Nevis'),
 ('SM',187,'San Marino','San Marino'),
 ('PM',188,'San Pedro y Miquelón','Saint Pierre and Miquelon'),
 ('VC',189,'San Vicente y las Granadinas','Saint Vincent and the Grenadines'),
 ('SH',190,'Santa Helena','Saint Helena'),
 ('LC',191,'Santa Lucía','Saint Lucia'),
 ('ST',192,'Santo Tomé y Príncipe','Sao Tome and Principe'),
 ('SN',193,'Senegal','Senegal'),
 ('CS',194,'Serbia y Montenegro','Serbia and Montenegro'),
 ('SC',195,'Seychelles','Seychelles'),
 ('SL',196,'Sierra Leona','Sierra Leone'),
 ('SG',197,'Singapur','Singapore'),
 ('SY',198,'Siria','Syria'),
 ('SO',199,'Somalia','Somalia'),
 ('LK',200,'Sri Lanka','Sri Lanka'),
 ('SZ',201,'Suazilandia','Swaziland'),
 ('ZA',202,'Sudáfrica','South Africa'),
 ('SD',203,'Sudán','Sudan'),
 ('SE',204,'Suecia','Sweden'),
 ('CH',205,'Suiza','Switzerland'),
 ('SR',206,'Surinam','Suriname'),
 ('SJ',207,'Svalbard y Jan Mayen','Svalbard and Jan Mayen'),
 ('TH',208,'Tailandia','Thailand'),
 ('TW',209,'Taiwán','Taiwan'),
 ('TZ',210,'Tanzania','Tanzania'),
 ('TJ',211,'Tayikistán','Tajikistan'),
 ('IO',212,'Territorio Británico del Océano Índico','British Indian Ocean Territory'),
 ('TF',213,'Territorios Australes Franceses','French Southern Territories'),
 ('TL',214,'Timor Oriental','Timor-Leste'),
 ('TG',215,'Togo','Togo'),
 ('TK',216,'Tokelau','Tokelau'),
 ('TO',217,'Tonga','Tonga'),
 ('TT',218,'Trinidad y Tobago','Trinidad and Tobago'),
 ('TN',219,'Túnez','Tunisia'),
 ('TC',220,'Islas Turcas y Caicos','Turks and Caicos Islands'),
 ('TM',221,'Turkmenistán','Turkmenistan'),
 ('TR',222,'Turquía','Turkey'),
 ('TV',223,'Tuvalu','Tuvalu'),
 ('UA',224,'Ucrania','Ukraine'),
 ('UG',225,'Uganda','Uganda'),
 ('UY',226,'Uruguay','Uruguay'),
 ('UZ',227,'Uzbekistán','Uzbekistan'),
 ('VU',228,'Vanuatu','Vanuatu'),
 ('VE',229,'Venezuela','Venezuela'),
 ('VN',230,'Vietnam','Vietnam'),
 ('VG',231,'Islas Vírgenes Británicas','Virgin Islands, British'),
 ('VI',232,'Islas Vírgenes de los Estados Unidos','Virgin Islands, US'),
 ('WF',233,'Wallis y Futuna','Wallis and Futuna'),
 ('YE',234,'Yemen','Yemen'),
 ('DJ',235,'Yibuti','Djibouti'),
 ('ZM',236,'Zambia','Zambia'),
 ('ZW',237,'Zimbabue','Zimbabwe');
UNLOCK TABLES;
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;


--
-- Definition of table `db`.`django_admin_log`
--

DROP TABLE IF EXISTS `db`.`django_admin_log`;
CREATE TABLE  `db`.`django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_fbfc09f1` (`user_id`),
  KEY `django_admin_log_e4470c6e` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`django_admin_log`
--

/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
LOCK TABLES `django_admin_log` WRITE;
INSERT INTO `db`.`django_admin_log` VALUES  (1,'2013-01-31 21:46:15',1,12,'Cthulhu-En-El-Pais-Vasco','Cthulhu en el País Vasco',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (2,'2013-01-31 21:51:43',1,12,'Cthulhu-En-El-Pais-Vasco','Cthulhu en el País Vasco',2,'No ha cambiado ningún campo.'),
 (3,'2013-01-31 21:52:35',1,12,'La-Felicidad-Esta-En-El-Corazon','La felicidad está en el corazón',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (4,'2013-01-31 21:54:47',1,12,'Juicio-A-Un-Escritor','Juicio a un escritor',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (5,'2013-01-31 21:55:18',1,12,'Tratado-De-Quimica-Mundana','Tratado de química mundana',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (6,'2013-01-31 21:55:58',1,12,'Dos-Meses-Sin-Alba','Dos meses sin Alba',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (7,'2013-01-31 21:56:14',1,12,'Sistema','Sistema',2,'Modificado/a synopsis, language_id, gender_id y rate_id.'),
 (8,'2013-01-31 21:56:44',1,12,'Los-Templarios-En-El-Camino-De-Santiago','Los Templarios en el Camino de Santiago',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (9,'2013-01-31 21:57:07',1,12,'El-Troll','El troll',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (10,'2013-01-31 21:57:30',1,12,'El-Heredero-De-Merlin','El heredero de Merlín',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (11,'2013-01-31 21:57:58',1,12,'Leyendas-Templarias','Leyendas Templarias',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (12,'2013-01-31 21:58:27',1,12,'Los-Templarios-En-El-Bierzo','Los Templarios en el Bierzo',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (13,'2013-01-31 21:58:52',1,12,'Equilibrio-En-La-Sombra','Equilibrio en la sombra',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (14,'2013-01-31 21:59:12',1,12,'Viaje-Al-Centro-Del-Universo','Viaje al centro del universo',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (15,'2013-01-31 21:59:37',1,12,'Dialogos-Con-Jaque','Diálogos con Jaque',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (16,'2013-01-31 22:00:09',1,12,'Cronicas-De-Un-Nino-Que-Volaba','Crónicas de un niño que volaba',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (17,'2013-01-31 22:00:39',1,12,'Sidi-El-Nino-Del-Desierto','Sidi, el niño del desierto',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (18,'2013-01-31 22:00:58',1,12,'La-Piramide-De-Luz','La Pirámide de Luz',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (19,'2013-01-31 22:01:12',1,12,'Vertedero-De-Munecas','Vertedero de Muñecas',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (20,'2013-01-31 22:01:35',1,12,'Escantula','Escántula',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (21,'2013-01-31 22:01:56',1,12,'Luztragaluz','Luztragaluz',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (22,'2013-01-31 22:02:12',1,12,'Jamas-Y-Siempre-A-La-Vez','Jamás y siempre a la vez',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (23,'2013-01-31 22:02:30',1,12,'La-Copula-De-La-Mantis','La cópula de la mantis',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (24,'2013-01-31 22:02:50',1,12,'Un-Camino-Para-Iniciar-Cambios-Positivos','Un camino para iniciar cambios positivos',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (25,'2013-01-31 22:07:48',1,12,'1-Ss-Division-Panzer-Leibstandarte-Adolf-Hitler','1° SS Division Panzer Leibstandarte Adolf Hitler',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (26,'2013-01-31 22:13:34',1,12,'Cuentario-De-Guri','Cuentario de Guri',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (27,'2013-01-31 22:14:54',1,12,'Destino-Sangriento','Destino Sangriento',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (28,'2013-01-31 22:17:59',1,12,'Dosis-De-Bien','Dosis de bien',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (29,'2013-01-31 22:18:31',1,12,'Ehsariell','Ehsariell',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (30,'2013-01-31 22:19:06',1,12,'El-Aguila-Y-El-Gorrion','El águila y el gorrión',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (31,'2013-01-31 22:20:04',1,12,'El-Devenir-De-La-Senora-White','El Devenir de la Señora White',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (32,'2013-01-31 22:20:43',1,12,'El-Evangelio-De-Andrea','El Evangelio de Andrea',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (33,'2013-01-31 22:23:46',1,12,'El-Grupo-Del-Fin-Del-Mundo','El grupo del fin del mundo',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (34,'2013-01-31 22:24:40',1,12,'El-Maestrillo-Y-Sus-Discipulos','El maestrillo y sus discípulos',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (35,'2013-01-31 22:26:35',1,12,'El-Tunel-Y-El-Espejismo','El túnel y el espejismo',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (36,'2013-01-31 22:27:00',1,12,'En-La-Sombra-Del-Miedo','En la Sombra del Miedo',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (37,'2013-01-31 22:35:51',1,12,'Especiales-Todos','Especiales Todos',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (38,'2013-01-31 22:37:21',1,12,'Guia-Para-Tramitar-Extradiciones-En-Colombia','Guía para tramitar extradiciones en Colombia',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (39,'2013-01-31 22:38:00',1,12,'La-Luz-En-Dostoievski','La luz en dostoievski',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (40,'2013-01-31 22:39:44',1,12,'La-Mascara-Del-Pigmeo','La Máscara del Pigmeo',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (41,'2013-01-31 22:40:06',1,12,'Lagrimas-De-Madera','Lágrimas de madera',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (42,'2013-01-31 22:41:02',1,12,'Las-Posadas-Turisticas','Las Posadas turísticas',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (43,'2013-01-31 22:42:15',1,12,'Menino-Que-Se-Chamava-Ninguem','Menino que se chamava Ninguém',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (44,'2013-01-31 22:42:49',1,12,'Mi-Compromiso-Con-El-Amor','Mi compromiso con el amor',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (45,'2013-01-31 22:44:18',1,12,'Mi-Primer-Ovni','Mi Primer OVNI ',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (46,'2013-01-31 22:45:08',1,12,'Miracle-A-LInfern','Miracle a l´Infern',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (47,'2013-01-31 22:46:30',1,12,'Nose-Si-Ya-Te-Paso','Nose si ya te paso',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (48,'2013-01-31 22:46:57',1,12,'Pacifico-Sur','Pacífico Sur',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (49,'2013-01-31 22:48:38',1,12,'Poemas-De-Amor-Para-Tiempos-De-Odio','Poemas de Amor para Tiempos de Odio',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (50,'2013-01-31 22:49:56',1,12,'Recetas-De-Mary','Recetas de mary',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (51,'2013-01-31 22:50:28',1,12,'Saberes','Saberes',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (52,'2013-01-31 22:52:06',1,12,'Saideth','Saideth',2,'Modificado/a user, language_id, gender_id y rate_id.'),
 (53,'2013-01-31 22:54:17',1,12,'Sueno-Inalcanzable','Sueño Inalcanzable',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (54,'2013-01-31 22:56:50',1,12,'Suenos-De-Poeta','Sueños de poeta',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (55,'2013-01-31 22:59:54',1,12,'Versos-De-Luz-Un-Verso-Una-Luz','Versos de luz. un verso una luz.',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (56,'2013-01-31 23:00:33',1,12,'Y-Si-Fueras-Ella','¿y si fueras ella?',2,'Modificado/a synopsis, user, language_id, gender_id y rate_id.'),
 (57,'2013-02-01 01:43:59',1,12,'Conmigo-Pero-Solo','Conmigo Pero Solo',2,'Modificado/a user, language_id, gender_id, rate_id y website.');
UNLOCK TABLES;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;


--
-- Definition of table `db`.`django_content_type`
--

DROP TABLE IF EXISTS `db`.`django_content_type`;
CREATE TABLE  `db`.`django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`django_content_type`
--

/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
LOCK TABLES `django_content_type` WRITE;
INSERT INTO `db`.`django_content_type` VALUES  (1,'permission','auth','permission'),
 (2,'group','auth','group'),
 (3,'user','auth','user'),
 (4,'content type','contenttypes','contenttype'),
 (5,'session','sessions','session'),
 (6,'site','sites','site'),
 (7,'language','Main','language'),
 (8,'country','Main','country'),
 (9,'gender','Main','gender'),
 (10,'rate','Main','rate'),
 (11,'users','Main','users'),
 (12,'book','Main','book'),
 (13,'book_ comment','Main','book_comment'),
 (14,'user_ comment','Main','user_comment'),
 (15,'pay pal_ipn','Main','paypal_ipn'),
 (16,'purchase','Main','purchase'),
 (17,'withdrawal','Main','withdrawal'),
 (18,'log entry','admin','logentry');
UNLOCK TABLES;
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;


--
-- Definition of table `db`.`django_session`
--

DROP TABLE IF EXISTS `db`.`django_session`;
CREATE TABLE  `db`.`django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_c25c2c28` (`expire_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`django_session`
--

/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
LOCK TABLES `django_session` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;


--
-- Definition of table `db`.`django_site`
--

DROP TABLE IF EXISTS `db`.`django_site`;
CREATE TABLE  `db`.`django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`django_site`
--

/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
LOCK TABLES `django_site` WRITE;
INSERT INTO `db`.`django_site` VALUES  (1,'example.com','example.com');
UNLOCK TABLES;
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;


--
-- Definition of table `db`.`genders`
--

DROP TABLE IF EXISTS `db`.`genders`;
CREATE TABLE  `db`.`genders` (
  `gender_id` int(11) NOT NULL AUTO_INCREMENT,
  `gender_en` varchar(20) NOT NULL,
  `gender_es` varchar(20) NOT NULL,
  PRIMARY KEY (`gender_id`),
  UNIQUE KEY `gender_en` (`gender_en`),
  UNIQUE KEY `gender_es` (`gender_es`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`genders`
--

/*!40000 ALTER TABLE `genders` DISABLE KEYS */;
LOCK TABLES `genders` WRITE;
INSERT INTO `db`.`genders` VALUES  (1,'Novel','Novela'),
 (2,'Essay','Ensayo'),
 (3,'Poetry','Poesía'),
 (4,'Technical','Técnico'),
 (5,'Personal development','Desarrollo personal'),
 (6,'Drama','Teatro'),
 (7,'Spare time','Ocio'),
 (8,'History','Historia'),
 (9,'Short story','Relato corto'),
 (10,'Other','Otros');
UNLOCK TABLES;
/*!40000 ALTER TABLE `genders` ENABLE KEYS */;


--
-- Definition of table `db`.`languages`
--

DROP TABLE IF EXISTS `db`.`languages`;
CREATE TABLE  `db`.`languages` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_en` varchar(20) NOT NULL,
  `language_es` varchar(20) NOT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`languages`
--

/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
LOCK TABLES `languages` WRITE;
INSERT INTO `db`.`languages` VALUES  (1,'Spanish','Español'),
 (2,'English','Inglés'),
 (3,'French','Francés'),
 (4,'German','Alemán'),
 (5,'Portuguese','Portugués'),
 (6,'Italian','Italiano'),
 (7,'Chinese','Chino'),
 (8,'Russian','Ruso'),
 (9,'Other','Otros'),
 (10,'','');
UNLOCK TABLES;
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;


--
-- Definition of table `db`.`paypal_ipn`
--

DROP TABLE IF EXISTS `db`.`paypal_ipn`;
CREATE TABLE  `db`.`paypal_ipn` (
  `business` varchar(127) NOT NULL,
  `receiver_email` varchar(127) NOT NULL,
  `invoice` int(11) NOT NULL AUTO_INCREMENT,
  `cmd` varchar(18) NOT NULL,
  `item_number_1` varchar(100) NOT NULL,
  `item_number_2` varchar(100) NOT NULL,
  `item_number_3` varchar(100) NOT NULL,
  `item_number_4` varchar(100) NOT NULL,
  `item_number_5` varchar(100) NOT NULL,
  `item_number_6` varchar(100) NOT NULL,
  `item_number_7` varchar(100) NOT NULL,
  `item_number_8` varchar(100) NOT NULL,
  `item_number_9` varchar(100) NOT NULL,
  `item_number_10` varchar(100) NOT NULL,
  `residence_country` varchar(2) NOT NULL,
  `test_ipn` tinyint(1) NOT NULL,
  `txn_id` varchar(19) NOT NULL,
  `txn_type` varchar(128) NOT NULL,
  `payer_email` varchar(127) NOT NULL,
  `mc_currency` varchar(32) NOT NULL,
  `mc_gross` decimal(64,2) NOT NULL,
  `num_cart_items` int(11) DEFAULT NULL,
  `payment_date` datetime NOT NULL,
  `payment_status` varchar(9) NOT NULL,
  `payment_type` varchar(7) NOT NULL,
  PRIMARY KEY (`invoice`)
) ENGINE=MyISAM AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`paypal_ipn`
--

/*!40000 ALTER TABLE `paypal_ipn` DISABLE KEYS */;
LOCK TABLES `paypal_ipn` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `paypal_ipn` ENABLE KEYS */;


--
-- Definition of table `db`.`purchases`
--

DROP TABLE IF EXISTS `db`.`purchases`;
CREATE TABLE  `db`.`purchases` (
  `operation_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` varchar(150) NOT NULL,
  `user_id` varchar(55) DEFAULT NULL,
  `value` decimal(64,2) NOT NULL,
  `operation_date` date NOT NULL,
  `invoice_id` int(11) NOT NULL,
  PRIMARY KEY (`operation_id`),
  KEY `purchases_752eb95b` (`book_id`),
  KEY `purchases_fbfc09f1` (`user_id`),
  KEY `purchases_59f72b12` (`invoice_id`)
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`purchases`
--

/*!40000 ALTER TABLE `purchases` DISABLE KEYS */;
LOCK TABLES `purchases` WRITE;
INSERT INTO `db`.`purchases` VALUES  (1,'La-Copula-De-La-Mantis','Angel-Faet','0.99','2013-02-01',3),
 (2,'Escantula','Angel-Faet','0.99','2013-02-01',4),
 (3,'Dos-Meses-Sin-Alba','Angel-Faet','0.99','2013-02-01',5),
 (4,'Juicio-A-Un-Escritor','Angel-Faet','0.99','2013-02-01',6),
 (5,'Luztragaluz','Angel-Faet','0.99','2013-02-01',7),
 (6,'El-Troll','Angel-Faet','0.99','2013-02-01',8),
 (7,'Sistema','Gabriel-Faet','0.99','2013-02-01',10),
 (8,'Juicio-A-Un-Escritor','Gabriel-Faet','0.99','2013-02-01',11),
 (9,'Equilibrio-En-La-Sombra','Gabriel-Faet','0.99','2013-02-01',12),
 (10,'Leyendas-Templarias','Gabriel-Faet','0.99','2013-02-01',13),
 (11,'El-Troll','Gabriel-Faet','0.99','2013-02-01',14),
 (12,'Viaje-Al-Centro-Del-Universo','Gabriel-Faet','0.99','2013-02-01',15),
 (13,'Leyendas-Templarias','Afonso','0.99','2013-02-01',0),
 (14,'Los-Templarios-En-El-Bierzo','Afonso','0.99','2013-02-01',1),
 (15,'Los-Templarios-En-El-Camino-De-Santiago','Afonso','0.99','2013-02-01',2),
 (16,'Dos-Meses-Sin-Alba','Emilio-Tejera','0.99','2013-02-01',9),
 (17,'Los-Templarios-En-El-Bierzo','Yolanda-Palomares','0.99','2013-02-01',16),
 (18,'Sistema','Gelito','0.99','2013-02-01',17),
 (19,'Sistema','Munarriz','0.99','2013-02-01',18),
 (20,'Los-Templarios-En-El-Camino-De-Santiago','Munarriz','0.99','2013-02-01',19),
 (21,'Leyendas-Templarias','Xabitxo','0.99','2013-02-01',20),
 (22,'Los-Templarios-En-El-Camino-De-Santiago','Xabitxo','0.99','2013-02-01',21),
 (23,'Leyendas-Templarias','Ny1109','0.99','2013-02-01',22),
 (24,'Los-Templarios-En-El-Camino-De-Santiago','Ny1109','0.99','2013-02-01',23),
 (25,'Un-Camino-Para-Iniciar-Cambios-Positivos','Neo3504','0.99','2013-02-01',24),
 (26,'El-Troll','Monicgordon','0.99','2013-02-01',25),
 (27,'Cthulhu-En-El-Pais-Vasco','Mikel','0.99','2013-02-01',26),
 (28,'El-Troll','Martillo','0.99','2013-02-01',27),
 (29,'Sistema','Maria-Jesus','0.99','2013-02-01',28),
 (30,'Los-Templarios-En-El-Camino-De-Santiago','Maria-Jesus','0.99','2013-02-01',29),
 (31,'Leyendas-Templarias','Maheluz','0.99','2013-02-01',30),
 (32,'Los-Templarios-En-El-Camino-De-Santiago','Maheluz','0.99','2013-02-01',31),
 (33,'Luztragaluz','Locueloman','0.99','2013-02-01',32),
 (34,'Jamas-Y-Siempre-A-La-Vez','Locueloman','0.99','2013-02-01',33),
 (35,'Cronicas-De-Un-Nino-Que-Volaba','Juanadm','0.99','2013-02-01',34),
 (36,'Sidi-El-Nino-Del-Desierto','Juanadm','0.99','2013-02-01',35),
 (37,'Juicio-A-Un-Escritor','Gemam38','0.99','2013-02-01',36),
 (38,'Vertedero-De-Munecas','Gemam38','0.99','2013-02-01',37),
 (39,'Dialogos-Con-Jaque','Gabriel','0.99','2013-02-01',38),
 (40,'Equilibrio-En-La-Sombra','Gabriel','0.99','2013-02-01',39),
 (41,'Los-Templarios-En-El-Camino-De-Santiago','Breno','0.99','2013-02-01',40),
 (42,'Leyendas-Templarias','Breno','0.99','2013-02-01',41),
 (43,'Los-Templarios-En-El-Bierzo','Breno','0.99','2013-02-01',42),
 (44,'Juicio-A-Un-Escritor','Ionzaga','0.99','2013-02-01',43),
 (45,'Juicio-A-Un-Escritor','Fjpago','0.99','2013-02-01',44),
 (46,'Los-Templarios-En-El-Camino-De-Santiago','Joaquin_Pereira','0.99','2013-02-01',45),
 (47,'Los-Templarios-En-El-Camino-De-Santiago','Coronel','0.99','2013-02-01',46),
 (48,'Los-Templarios-En-El-Camino-De-Santiago','Chusing','0.99','2013-02-01',47),
 (49,'Los-Templarios-En-El-Camino-De-Santiago','Cavalli','0.99','2013-02-01',48),
 (50,'Sistema','Javier-Beirute','0.99','2013-02-01',49),
 (51,'Sistema','Criescace','0.99','2013-02-01',50),
 (52,'El-Troll','Gammaleuka','0.99','2013-02-01',51),
 (53,'El-Troll','Croqueta','0.99','2013-02-01',52),
 (54,'El-Heredero-De-Merlin','Denissecienfuegos','0.99','2013-02-01',53),
 (55,'La-Piramide-De-Luz','Churrisarah','0.99','2013-02-01',54),
 (56,'Escantula','Beatriz_Perez','0.99','2013-02-01',55),
 (57,'La-Felicidad-Esta-En-El-Corazon','Aluna','0.99','2013-02-01',56),
 (58,'Juicio-A-Un-Escritor','Wallp','0.99','2013-02-01',57),
 (59,'La-Piramide-De-Luz','Vanessa','0.99','2013-02-01',58),
 (60,'Tratado-De-Quimica-Mundana','Santaella','0.99','2013-02-01',59),
 (61,'Tratado-De-Quimica-Mundana','Perezj','0.99','2013-02-01',60),
 (62,'Juicio-A-Un-Escritor','JokiRomero','0.99','2013-02-01',61),
 (63,'Juicio-A-Un-Escritor','Elenabr','0.99','2013-02-01',62);
UNLOCK TABLES;
/*!40000 ALTER TABLE `purchases` ENABLE KEYS */;


--
-- Definition of table `db`.`rates`
--

DROP TABLE IF EXISTS `db`.`rates`;
CREATE TABLE  `db`.`rates` (
  `rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `rate_en` varchar(20) NOT NULL,
  `rate_es` varchar(20) NOT NULL,
  PRIMARY KEY (`rate_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`rates`
--

/*!40000 ALTER TABLE `rates` DISABLE KEYS */;
LOCK TABLES `rates` WRITE;
INSERT INTO `db`.`rates` VALUES  (1,'','Todas las edades'),
 (2,'','Infantil'),
 (3,'','Juvenil'),
 (4,'','Adultos');
UNLOCK TABLES;
/*!40000 ALTER TABLE `rates` ENABLE KEYS */;


--
-- Definition of table `db`.`user_comments`
--

DROP TABLE IF EXISTS `db`.`user_comments`;
CREATE TABLE  `db`.`user_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_about_id` varchar(55) NOT NULL,
  `user_who_id` varchar(55) NOT NULL,
  `comment` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_comments_c5b8a7db` (`user_about_id`),
  KEY `user_comments_fbf6f738` (`user_who_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`user_comments`
--

/*!40000 ALTER TABLE `user_comments` DISABLE KEYS */;
LOCK TABLES `user_comments` WRITE;
INSERT INTO `db`.`user_comments` VALUES  (1,'Angel-Faet','Angel-Faet','Un saludo y la mejor bienvenida a todos los autores y lectores de PeopleEbooks.');
UNLOCK TABLES;
/*!40000 ALTER TABLE `user_comments` ENABLE KEYS */;


--
-- Definition of table `db`.`users`
--

DROP TABLE IF EXISTS `db`.`users`;
CREATE TABLE  `db`.`users` (
  `user` varchar(55) NOT NULL,
  `full_name` varchar(55) NOT NULL,
  `email` varchar(75) NOT NULL,
  `passwd` varchar(30) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `signup_date` date NOT NULL,
  `is_author` tinyint(1) NOT NULL,
  `sales` int(10) unsigned NOT NULL,
  `purchases` int(10) unsigned NOT NULL,
  `account_active` tinyint(1) NOT NULL,
  `hashes` varchar(320) NOT NULL,
  `wants_mail_notifications` tinyint(1) NOT NULL,
  `current_balance` decimal(64,2) NOT NULL,
  `total_profits` decimal(64,2) NOT NULL,
  `money_withdrawn` decimal(64,2) NOT NULL,
  `website` varchar(200) NOT NULL,
  `facebook` varchar(200) NOT NULL,
  `twitter` varchar(200) NOT NULL,
  `quote` varchar(300) NOT NULL,
  `presentation` longtext NOT NULL,
  `photo` varchar(100) NOT NULL,
  `video` varchar(100) NOT NULL,
  PRIMARY KEY (`user`),
  UNIQUE KEY `full_name` (`full_name`),
  UNIQUE KEY `email` (`email`),
  KEY `users_534dd89` (`country_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`users`
--

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
LOCK TABLES `users` WRITE;
INSERT INTO `db`.`users` VALUES  ('Munarriz','Munarriz','cecilucho@gmail.com','',NULL,'2012-11-03',0,0,2,1,'',1,'0.00','0.00','0.00','','','','La historia es la realidad del hombre que no se hereda y debemos buscarla, porque estaría muy incompleto el hombre que no conozca su realidad.','Nacido en Pamplona, Navarra, España, con cero kilómetros de recorrido al nacer, sin uso,  eso fue el día 27/03/1939. Desde que tuve uso de razón me interesé en: mujer; vino; historia; lectura; escritura; chismes electrónicos; el mar; investigar; instigar; la cocina y la buena alimentación; trabajar mucho pero sin obligación alguna, puesto que las tuve y me liberé de ellas; aunque, siempre, supe que no hacer nada y después parar a reposar es una delicia.\nCuando converso, recibo, a veces, cada opinión, tan incongruente para mí entendimiento, que terminan dejándome patitieso, máxime cuando recuerdo haber leído que muchos sabios coinciden en decir que todo el mundo tiene la razón, por ello, no me atrevo a replicar lo que escucho aunque crea que son barbaridades lo que me bombardean. Me gusta escribir que es como instigar y como están los tiempos para no callarse, aunado a que no puedo instigar en casa, no me queda otro recurso que publicar mis instigaciones o mis escritos, claro que aspiro a que no me replique feo porque me podría irritar y eso no es recomendable para mi salud, así que piensen bien lo que hacen.\n','img/users/Munarriz.jpg',''),
 ('Virgilio','\"Virgilio\"','angel.alvarez@kzgunea.net','',NULL,'2012-12-13',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('7Maresazules','7Maresazules','7maresazules@gmail.com','',NULL,'2012-11-21',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('7Udp','7Udp','eduard7_udp@hotmail.com','',NULL,'2012-11-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','\"Envido a los que envidian la envidia que les tengo\". por: 7udp.','','img/users/7Udp.jpg',''),
 ('Abel-Nedi','Abel Nedi','abelnedi@hotmail.com','',NULL,'2012-12-29',1,0,0,1,'',1,'0.00','0.00','0.00','','','','Somos Cometas','...','',''),
 ('Academia-Tucuman','Academia Tucuman','jlcruz@telmex.net.co','',NULL,'2012-11-10',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Adan-De-Mariass','Adán De Maríass','adandemariass@outlook.com','',NULL,'2012-11-01',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Adrian-Mendieta','Adrian Mendieta','ammcat_19@hotmail.com','',NULL,'2012-09-19',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Adrian-TM','Adrián T.M.','adri951_8@hotmail.com','',NULL,'2012-08-14',0,0,0,1,'',1,'0.00','0.00','0.00','http://hirakisinister.jimdo.com','http://facebook.com/hiraki951','','La verdadera pobreza sólo se halla en la mente de quién se siente pobre','','img/users/Adrian-TM.jpg',''),
 ('Adrian-Zabala','Adrian Zabala','zabalamaderas@yahoo.com','',NULL,'2012-12-21',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Adriana-Paire','Adriana Paire','adrianapaire@hotmail.com','',NULL,'2012-07-30',1,1,0,1,'',1,'0.50','0.50','0.00','','','','','','img/users/Adriana-Paire.jpg',''),
 ('Afonso','Afonso','wiper@terra.com.br','',NULL,'2012-08-18',0,0,3,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Agustina-Aguirre','Agustina Aguirre','agustinaaguirre_18@hotmail.com','',NULL,'2012-09-06',0,0,0,1,'',1,'0.00','0.00','0.00','','','','stay strong','Hola soy agustina me encanta escribir y componer canciones de eso y la abogacia quiero vivir','img/users/Agustina-Aguirre.jpg',''),
 ('Aidatrujillo','Aidatrujillo','aidatrujilloricart@gmail.com','',NULL,'2013-01-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Akaro2009','Akaro2009','akaro2009@gmail.com','',NULL,'2012-11-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Albaarenas','Albaarenas','biruxideal@gmail.com','',NULL,'2012-10-28',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Albademoses','Albademoses','albademoses@gmail.com','',NULL,'2012-10-06',0,0,0,1,'',1,'0.00','0.00','0.00','','','','...\"Para ser imprescindible debes ser siempre diferente\"','...Escribir, aprender, re - aprender es la vida... Soy Yo.','img/users/Albademoses.jpg',''),
 ('Albenis-Andrade','Albenis Andrade','albenisandrade@hotmail.com','',NULL,'2012-11-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Alberto','Alberto','albertoviana@adinet.com.uy','',NULL,'2012-10-02',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Alberto-Jimenez-Ure','Alberto Jiménez Ure','urescritor@hotmail.com','',NULL,'2012-11-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Alberto-Viana-Reyes','Alberto Viana Reyes','albertoviana42@hotmail.com','',NULL,'2012-11-13',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Albertoesparza','Albertoesparza','albertoesparza1@hotmail.com','',NULL,'2012-10-30',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Aleida-Leyva-Garcia','Aleida Leyva García','aleidaleyva24@ymail.com','',NULL,'2012-11-27',1,1,0,1,'',1,'0.50','0.50','0.00','','','','?Sea intrépido y fuerzas poderosas vendrán en su ayuda?  Basil King...','\nHola! Me presento ante Uds. con este libro que es una guía práctica para iniciar cambios positivos en la vida, si tenemos la disposición para realizarlos. Yo misma lo he usado y lo uso a diario para lograr los objetivos que me he propuesto y deseo alcanzar. Este libro y su publicación es uno de los muchos logros que he obtenido. Los invito a comprarlo y que comprueben por Uds. mismos los beneficios que les aportará. Gracias.\n...','',''),
 ('Alejandro-Fuentes','Alejandro Fuentes','afuentesmartinez.5@gmail.com','',NULL,'2013-01-18',1,0,0,1,'',1,'0.00','0.00','0.00','http://alejandrofuentes.neositios.com','http://www.facebook.com/afuentes51','','\"Los árboles sin raíces nunca tocarán el cielo. Los hombres sin principios, acabarán llorando en el suelo\"','Columnista en las revista literarias \"VIM Magazine\", donde redacto la sección de recomendaciones, y \"Granite & Rainbow\", donde colaboro en la sección \"Breves\".\n\nActualmente estoy trabajando en mi siguiente poemario titulado \"La gota que colma el verso\", que saldrá a la venta en septiembre, y en mi primera novela; \"Siempre solo dura un rato\"','img/users/Alejandro-Fuentes.jpg',''),
 ('Alejandro-Rosen','Alejandro Rosen','uterpandragon@hotmail.com','',NULL,'2012-08-02',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Alejandro-Sotodosos','Alejandro Sotodosos','sotowoods@gmail.com','',NULL,'2012-07-30',1,2,0,1,'',1,'1.00','1.00','0.00','http://www.alejandrosotodosos.com','','','Si no te recuerdan, no importa lo bueno que seas.','No creo que deba hablar de mi vida personal ni de mis aficiones. Un escritor no tiene nada que ver con la persona en la que vive, sino que es una extensión que utiliza para transmitir a los lectores.\n\nAsí que les dejo que me conozcan \"por dentro\". Quizá algún día también conozcan a la persona que lo \"encierra\". Tiempo al tiempo.','img/users/Alejandro-Sotodosos.jpg',''),
 ('Alejo-Chavez','Alejo Chávez','alejoc.209@gmail.com','',NULL,'2012-11-26',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Alex-Molina','Alex Molina','alexis.molinaleyva@gmail.com','',NULL,'2012-11-21',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Alexander-Ogma-Fenix','Alexander Ogma Fénix','alexjota.89@hotmail.com','',NULL,'2012-08-01',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Alexandra-Pathz','Alexandra Pathz','paingenito@yahoo.com.ar','',NULL,'2012-12-14',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Alexis-Gomez','Alexis Gomez','gomezgeraldino@hotmail.com','',NULL,'2012-12-24',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Alfonso-Barreto','Alfonso Barreto','escriauto@yahoo.es','',NULL,'2012-11-10',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Alfonso-Barreto.jpg',''),
 ('Alfred1992','Alfred1992','cristianalfredol78@gmail.com','',NULL,'2012-12-17',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Alfredo-Ascanio','Alfredo Ascanio','alfredo.ascanio@gmail.com','',NULL,'2012-11-18',1,0,0,1,'',1,'0.00','0.00','0.00','http://www.askain.blogspot.com.es/','','','Lo primero es lo primero','Soy economista con un doctorado en ciencia politica.','img/users/Alfredo-Ascanio.jpg',''),
 ('Alices1111','Alices1111','trader_onlines@hotmail.com','',NULL,'2012-11-20',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Alicia-Elena','Alicia Elena','carola.uribec@gmail.com','',NULL,'2012-10-17',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Alinebusch','Alinebusch','alinebusch12@gmail.com','',NULL,'2012-11-29',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Almotasim','Almotasim','oscardgp@gmail.com','',NULL,'2012-09-24',0,0,0,1,'',1,'0.00','0.00','0.00','','','','Nadie es inocente','Médico, escritor. Finalista del Certamen Nacional de Novela año 2006 de la Biblioteca Nacional de Buenos Aires por mi Novela \"La Red y el Trapecio\" ','img/users/Almotasim.jpg',''),
 ('Aluna','Aluna','antluna@gmail.com','',NULL,'2013-01-01',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Alvarohart','Alvarohart','alv_hp@hotmail.com','',NULL,'2012-11-19',1,0,0,1,'',1,'0.00','0.00','0.00','http://cronicasdeunempleado.blogspot.com','','https://twitter.com/AlvaroHart','El esmero en el trabajo es la única convicción moral del escritor. ','Disfruto de un buen libro al igual que de una buena tormenta. Me fascina ver la lluvia golpear en la ventana y el olor de la tierra mojada. Aparte de ello, me gusta caminar sin pisar las líneas. ','img/users/Alvarohart.jpg',''),
 ('Aly-L-Silva','Aly L. Silva','eals_99@hotmail.com','',NULL,'2012-10-26',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Amalia','Amalia','amalia_lasilver@hotmail.com','',NULL,'2012-10-16',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Amor-Aouini','Amor Aouini','clubsastre@hotmail.com','',NULL,'2012-11-14',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Anafaet','Anafaet','anafaet@hotmail.com','',NULL,'2012-07-31',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Anais-Hidalgo','Anais Hidalgo','anais_barbie_estrella@hotmail.com','',NULL,'2012-12-21',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Anais-Hidalgo.jpg',''),
 ('Anderson-Arevalo','Anderson Arévalo','produccionesaa100@hotmail.com','',NULL,'2012-09-29',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Andreina','Andreina','laprinsecitabella2010@hotmail.com','',NULL,'2012-10-17',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Angel-Borges','Angel Borges','angelborges1986@gmail.com','',NULL,'2012-08-01',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Angel-Faet','Ángel Faet','unhdlt@gmail.com','',72,'2012-07-29',1,6,6,1,'',1,'3.00','3.00','0.00','','http://www.facebook.com/AngelFaet','https://twitter.com/AngelFaet','Si encontráramos el coraje de andar nuestros sueños, la Tierra iluminaría el Sol.','Camino la vida gobernando el velero de mis actos convencido de un motor resucito en tempestades que lejos de evitar, anhelo. Me quito a cada paso caretas que antaño engañaron salvarme de mis miedos. Uso la razón para fundir el corazón en mi pecho y así la voluntad de conocer reales, sueños.  Artífice de mis errores, culpable único de mis decisiones y pasos.','img/users/Angel-Faet.jpg',''),
 ('Angelvida1','Angelvida1','hernandezexx@hotmail.com','',NULL,'2012-12-10',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Angie-Zelada','Angie Zelada','marianzelguz@hotmail.com','',NULL,'2012-11-11',1,0,0,1,'',1,'0.00','0.00','0.00','','https://www.facebook.com/angie.zeladaguzman','','palabras sueltas que llegan a mi mente y solo quieren escapar al espacio real para escribir una versión personal de mi mundo virtual.','Es el relato de una chica que cree haber vivido algo parecido a una momentánea caída a causa de un incierto amor. Suspenso y Drama','img/users/Angie-Zelada.jpg',''),
 ('Anny','Anny','aebag@hotmail.com','',NULL,'2012-10-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Anny-P','Anny P.','anny.palavecino@hotmail.com','',NULL,'2012-12-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Antillonf','Antillonf','capellof@yahoo.com','',NULL,'2012-08-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Antoine-Nolla','Antoine Nolla','hola@antoine-nolla.es','',NULL,'2012-07-30',1,20,0,1,'',1,'10.00','10.00','0.00','http://www.antoine-nolla.es','','','\"Hace años que he dejado de creer en la casualidad\"','','img/users/Antoine-Nolla.jpg',''),
 ('Antonio-De-Chencha','Antonio De Chencha','antonio-gonzalez-@hotmail.com','',NULL,'2012-10-31',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Antonio-Guerra-Colon','Antonio Guerra Colon','tonyvidadeseamas@yahoo.com','',NULL,'2012-11-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Antonio-R-Mota-G','Antonio R. Mota G.','antoniomota3@hotmail.com','',NULL,'2012-11-29',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Antonio-Toro','Antonio Toro','pcstuffandtools@gmail.com','',NULL,'2012-11-29',0,0,0,1,'',1,'0.00','0.00','0.00','','','','I won´t go to heaven and I won´t go to hell. Salvations does not depend on my decision solely, but on God´s decision to save me.      A.Toro','You may indulge in having intercourse with almost any? But you cannot live with an acquaintance -if there´s no trust, the commitment of love and the fellowship of ideas and the same faith.    A. Toro\n\nCristiano mundano y humano. \n\nRomántico, quizá demasiado; pero no hallé mejor cosa que intentar amar, con estas diferencias de todos...\n\n¡Presentarme no quiero!\n\nQue hable lo que escribo (si las letras o ideas me quieren).\n','img/users/Antonio-Toro.jpg',''),
 ('Aosara','Aosara','saritacosita_17@hotmail.com','',NULL,'2012-11-14',0,0,0,1,'',1,'0.00','0.00','0.00','http://silence-walker.blogspot.com/','http://www.facebook.com/17AOSARA','','','','img/users/Aosara.jpg',''),
 ('Aqueos','Aqueos','aqueos@yahoo.es','',NULL,'2012-11-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Arami','Arami','aramiarza@hotmail.com','',NULL,'2012-12-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Argelia','Argelia','zihuacuani@hotmail.com','',NULL,'2013-01-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Armando-Nar-Alsina','Armando Nar Alsina','naralsina_2@hotmail.com','',NULL,'2012-11-02',0,0,0,1,'',1,'0.00','0.00','0.00','','https://www.facebook.com/armando.naralsina','','','','',''),
 ('Asdf22','Asdf22','angelafernandezbrito@gmail.com','',NULL,'2012-08-21',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Asdre123','Asdre123','andreasmassip@live.fr','',NULL,'2012-12-11',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Avelino-Melendez','Avelino Melendez','linomelvin@hotmail.com','',NULL,'2012-08-06',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Babo','Babo','alangc2000@hotmail.com','',NULL,'2012-11-17',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Babo.jpg',''),
 ('Beatriz_Perez','Beatriz_Perez','beatriz_perezmoya@hotmail.com','',NULL,'2012-08-02',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Beida-R-Vega-R','Beida R. Vega R.','beidavega@hotmail.com','',NULL,'2012-11-30',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('BerlinaAzn','Berlina.Azn','alvarozambranonovoa@gmail.com','',NULL,'2012-10-30',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Bernapuerta','Bernapuerta','castrillonpuertabernardo08@gmail.com','',NULL,'2013-01-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Bernardo-Pereira','Bernardo Pereira','b.pereira.garcia@euskaltel.net','',NULL,'2012-09-03',1,1,0,1,'',1,'0.50','0.50','0.00','','','','','','img/users/Bernardo-Pereira.jpg',''),
 ('Berthier','Berthier','berthiercorinne256@yahoo.fr','',NULL,'2012-12-20',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Betina1993','Betina1993','isabelladiazbermeo@hotmail.com','',NULL,'2012-12-06',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Bichorevoltoso','Bichorevoltoso','tenfesanz@gmail.com','',NULL,'2012-08-06',0,0,0,1,'',1,'0.00','0.00','0.00','http://tulectoradecabecera.blogspot.com.es/','','','La vida es un don precioso. Es lo único que no debes robar jamás.','','img/users/Bichorevoltoso.jpg',''),
 ('Blanquita','Blanquita','chispi231@hotmail.com','',NULL,'2012-08-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Blueyes','Blueyes','ptms07@gmail.com','',NULL,'2012-11-26',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Bradiely-Aguilare','Bradiely Aguilare','bradiely.aguilare@gmail.com','',NULL,'2012-11-30',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Breno','Breno','sigmaster@ig.com.br','',NULL,'2012-09-14',0,0,3,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Buy123','Buy123','buynowstoreltd@gmail.com','',NULL,'2012-11-16',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Camila-Cordova','Camila Cordova','camu_yani@hotmail.com','',NULL,'2012-12-21',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Camila-Cordova.jpg',''),
 ('Canetmart','Canetmart','canetmart@hotmail.com','',NULL,'2012-09-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Carboledam','Carboledam','carlos.arboleda5@gmail.com','',NULL,'2012-12-26',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('CarloFormisano','Carlo.Formisano','carlo.formisano@gmail.com','',NULL,'2012-11-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Carlos--Jandres','Carlos  Jandres','carlosjandresbejarano@gmail.com','',NULL,'2012-12-09',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Carlos-De-Hernan','Carlos De Hernán','carlos_dehernan01@hotmail.com','',NULL,'2012-08-02',1,0,0,1,'',1,'0.00','0.00','0.00','http://carlosdehernan.blogspot.mx/','','https://twitter.com/CarlosdeHernan','Lo que somos ahora es un reflejo de nuestro pasado y una ventana a nuestro futuro','Un poco extrovertido y también excéntrico, tal vez aun muy joven pero tengo muchos sueños por delante, me gusta escribir desde historias cortas, poemas y novelas. Soy admirador del trabajo de Edgar Allan Poe al cual también considero mi maestro y espero un día escribir también como él. No puedo decir que he escrito mucho porque eso sería mentira, pero si puedo decir que escribo desde hace algunos años. ','img/users/Carlos-De-Hernan.jpg',''),
 ('Carlos-Gamissans','Carlos Gamissans','carlos-albertogl@hotmail.com','',NULL,'2012-07-30',1,8,0,1,'',1,'4.00','4.00','0.00','http://cgamissans.blogspot.com.es/','https://www.facebook.com/pages/Carlos-Gamissans','https://twitter.com/Gamissans','El mundo real es mucho más pequeño que el mundo de la imaginación. (Nietzsche)','Mi nombre es Carlos Alberto Gamissans López. Nací en Calatayud hace 23 años y  estoy graduado en Periodismo, pero mi vocación es la Literatura. He ganado el XXX Concurso Literario para Jóvenes de Albacete en la modalidad de relato y el Concurso de Literatura Joven del Instituto Aragonés de la Juventud (año 2011). He publicado un libro de cuentos y espero que el siguiente sea mi novela. También puedes leerme en Twitter y en Facebook, donde comparto algunas reflexiones, noticias sobre libros y en general todo lo que me llama la atención:  https://twitter.com/Gamissans  \nhttps://www.facebook.com/pages/Carlos-Alberto-Gamissans-L%C3%B3pez/204043799671935?ref=hl\n\n\n','img/users/Carlos-Gamissans.jpg',''),
 ('Carlos-Manuel','Carlos Manuel','carlosmanuelgm@hotmail.com','',NULL,'2012-12-22',0,0,0,1,'',1,'0.00','0.00','0.00','https://sites.google/site/revenueyieldmanagement/','','','Carpe diem, \"la vida no es una utopia, es lo mas real que tendremos\" (Carlos Manuel Gonzalez Mazon)','Enamorado de la vida, y de todas las expresiones que la manifiestan, la risa, la alegria, la poesia, la musica como discernimiento de lo incomprensible, el amor, la convivencia, la ironia, las metaforas, el sarcasmo idealizado....tanto que decir y tan poco espacio para decirlo que me dan ganas de empezar un libro pero para nunca terminarlo...\n\nCarlos Manuel Gonzalez Mazon','',''),
 ('Carlos-Mingorance','Carlos Mingorance','nirnel@hotmail.com','',NULL,'2012-07-29',1,2,0,1,'',1,'1.00','1.00','0.00','','','','','','img/users/Carlos-Mingorance.jpg',''),
 ('Carlos-Pareja','Carlos Pareja','carlosparejagonzalez@yahoo.es','',NULL,'2012-10-29',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Carmita-Ontaneda','Carmita Ontaneda','caontaneda@hotmail.com','',NULL,'2012-11-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Carolina-Gonzalez-S','Carolina González S.','cgstraduccioneschile@gmail.com','',NULL,'2012-12-10',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Cataloiu','Cataloiu','cata_loiu@yahoo.com','',NULL,'2012-11-22',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Cavalli','Cavalli','fcavalli@gmail.com','',NULL,'2012-08-19',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Cece','Cece','carito.cg@hotmail.com','',NULL,'2012-09-27',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Cesar','Cesar','k_a_zter@hotmail.com','',NULL,'2012-11-08',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Cesarpereda','Cesarpereda','gunepereda@gmail.com','',NULL,'2012-12-06',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Cezar','Cezar','art.lit.cul@gmail.com','',NULL,'2012-11-18',0,0,0,1,'',1,'0.00','0.00','0.00','http://artaliteratura.ucoz.ro','','','','','img/users/Cezar.jpg',''),
 ('Chabi-Angulo','Chabi Angulo','javier.angulo.hernandez@gmail.com','',NULL,'2012-09-09',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Chabi-Angulo.jpg',''),
 ('Chano-Castano','Chano Castaño','andres.castano123@gmail.com','',NULL,'2012-09-18',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Chica-De-Cristal','Chica De Cristal','martad_88@hotmail.es','',NULL,'2012-09-17',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Christian-Calandra','Christian Calandra','christiancalandra@gmail.com','',NULL,'2012-11-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Christian-Tiako','Christian Tiako','pitou2001fr@yahoo.fr','',NULL,'2013-01-24',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Christian-Tiako.jpg',''),
 ('Churrisarah','Churrisarah','churrisara@hotmail.com','',NULL,'2012-08-04',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Chusing','Chusing','celtalibros.ec@gmail.com','',NULL,'2012-08-25',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Ciatax11','Ciatax11','princess_venazir@yahoo.com','',NULL,'2012-12-06',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Cjleon','Cjleon','aunpasodelinfinito@gmail.com','',NULL,'2012-09-25',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Clasicos','Clásicos','unhdlt.@gmail.com','',NULL,'2012-09-12',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Clin0813','Clin0813','mrtiti13@hotmail.com','',NULL,'2013-01-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Cmarbelis','Cmarbelis','geromarbe27@hotmail.es','',NULL,'2012-10-26',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Concienciadelcamino','Concienciadelcamino','concienciadelcamino@yahoo.com.ar','',NULL,'2012-12-10',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Coronel','Coronel','losluisesdecadiz@hotmail.com','',NULL,'2012-08-26',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Criescace','Criescace','restaurantelaspiletas@hotmail.es','',NULL,'2012-08-09',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Cristian-Rada','Cristian Rada','cristiandavidradaher@hotmail.com','',NULL,'2012-09-29',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Cristina-Leiva','Cristina Leiva','cristina.leiva2012@yahoo.es','',NULL,'2012-12-01',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Cristina-Zeta','Cristina Zeta','cristina-zeta@hotmail.com','',NULL,'2012-10-29',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Croqueta','Croqueta','helenatejera@gmail.com','',NULL,'2012-11-02',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Cvredits','Cvredits','lineghilslaine9@yahoo.fr','',NULL,'2012-12-12',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Cybernapya','Cybernapya','cybernapya@gmail.com','',NULL,'2012-08-14',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('DamianAlegria','Damian.Alegria','damian.alegria@gmail.com','',NULL,'2012-11-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Daniel-Hernandez','Daniel Hernàndez','hdaniel121@hotmail.com','',NULL,'2012-12-10',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Daniela-Luna','Daniela Luna','daniela_vega96@hotmail.com','',NULL,'2012-08-08',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Daniela_Micaela','Daniela_Micaela','solopersonal@hotmail.com.ar','',NULL,'2012-11-13',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Danielle-Cuppone','Danielle Cuppone','danni_poeta@hotmail.com','',NULL,'2012-08-19',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Danilo-Umana-Sacasa','Danilo Umaña Sacasa','dusacasa@gmail.com','',NULL,'2012-12-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('David-Sanchez','David Sanchez','davidsanz68@gmail.com','',NULL,'2012-11-15',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Denissecienfuegos','Denissecienfuegos','denisseprobable@gmail.com','',NULL,'2012-08-31',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Dffigueroa','Dffigueroa','diegoffigueroa@gmail.com','',NULL,'2013-01-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Dhioyi','Dhioyi','dhivi@hotmail.es','',NULL,'2012-11-15',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Dicc-Agro-Ganadero','Dicc. Agro Ganadero','hernia88@hotmail.com','',NULL,'2012-12-10',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Divaldy','Divaldy','divaldy@msn.com','',NULL,'2012-12-11',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Djovenes','Djovenes','papirico26@hotmail.com','',NULL,'2012-10-15',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Dominic','Dominic','guadafues@hotmail.com','',NULL,'2012-11-08',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Domusaurea','Domusaurea','santiago.maspoch@yahoo.es','',NULL,'2012-08-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Dorian-Villa-S','Dorian Villa S.','dorianvilla1@gmail.com','',NULL,'2012-12-29',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Douglas','Douglas','dk3chat@gmail.com','',NULL,'2012-09-15',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Dowglasz','Dowglasz','dowglasz@gmail.com','',NULL,'2012-09-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Dracox555','Dracox555','farben-5@hotmail.com','',NULL,'2012-10-12',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Dragon_Literario','Dragon_Literario','dragonne71@yahoo.com','',NULL,'2012-12-14',1,0,0,1,'',1,'0.00','0.00','0.00','http://tintero.weebly.com/index.html','','','...\"Mi meta en la vida es ser tan buena persona como mi perro cree que soy\"','...Soy de los que todavía disfruta del casi extinto arte de la conversación.','img/users/Dragon_Literario.jpg',''),
 ('E065701','E065701','che_emily@hotmail.com','',NULL,'2012-12-09',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Ecaf','Ecaf','edcastillof@hotmail.com','',NULL,'2012-12-01',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Eddy-Leon','Eddy Leon','eddyleonb@gmail.com','',NULL,'2012-08-01',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Eddy-Leon.jpg',''),
 ('Ediciones-Casas','Ediciones Casas','edicionescasas@yahoo.es','',NULL,'2012-10-29',1,0,0,1,'',1,'0.00','0.00','0.00','http://www.edicionescasas.com/','','https://twitter.com/edicionescasas','','','img/users/Ediciones-Casas.jpg',''),
 ('Edmirandao','Edmirandao','edmirandao@gmail.com','',NULL,'2012-08-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Eduardo-Denunzio','Eduardo Denunzio','eduardo_denunzio@hotmail.com.ar','',NULL,'2012-10-18',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Eduardogzava','Eduardogzava','carloseduardo5_11@hotmail.com','',NULL,'2012-11-02',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Elena-Saldana-Ruiz','Elena Saldaña Ruiz','mgatogonz@gmail.com','',NULL,'2012-12-12',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Elenabr','Elenabr','elenabuilrobres@hotmail.es','',NULL,'2012-10-24',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Elmer-Rojas','Elmer Rojas','ehra1@yahoo.es','',NULL,'2012-11-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Emanuel','Emanuel','emanuel_delgado@hotmail.es','',NULL,'2012-11-28',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Emilio-Tejera','Emilio Tejera','emilio.tejera@gmail.com','',NULL,'2012-07-30',1,6,1,1,'',1,'3.00','3.00','0.00','http://emilio-tejera.blogspot.com.es/','http://www.facebook.com/emilio.tejera','https://twitter.com/EmilioTejera1','Un mito es una historia que una noche sueña alguien dormido, y al día siguiente, cuando la cuenta, la sueñan todos despiertos.','Al curioso que se haya adentrado en esta página: bienvenido. Yo he sido y soy igual que tú, de los que dan vida a las palabras al posar mis ojos sobre ellas, pero también he cruzado al otro lado. Y ahora, navegante, te propongo un reto: lee el diario de a bordo que voy a empezar a escribir aquí, y cuéntame qué te parece. Y por si te interesa saber algo más de mí, soy un andaluz afincado en Madrid desde hace ya más de una década, adonde llegué para estudiar ciencias y donde acabé por convertirme en bioquímico; me gusta por supuesto la literatura, incluyendo toda clase de géneros, pero también soy un entusiasta del cine, de la realidad social de nuestro mundo, y de los sucesos curiosos, sorprendentes e inesperados que tienen lugar a diario. Y todas esas cosas tienen un reflejo en  las páginas que escribo, y en las historias que narro. Supongo que, quizás como muchos de vosotros, aspiro a provocar las mismas sensaciones que generaban aquellos cuentacuentos que, a la luz de la luna, y desde tiempos inmemoriales, daban vida a personajes hasta entonces desconocidos, o mostraban bajo otra luz escenas aparentemente cotidianas. Pero siempre, buscando algo diferente, algo que a ti, que estás al otro lado, te haga meditar, sonreír, recordar, soñar, cuestionarte el mundo, te despierte y saque de lo normal. Que no te deje indiferente. Es tu turno, te toca decidir si lo he conseguido. Y a partir de ahora, juntos, en peopleEbooks.com, lo vamos a averiguar.','img/users/Emilio-Tejera.jpg','vid/users/Emilio-Tejera.webm'),
 ('Emma-Campbell','Emma Campbell','emmacalderin1975@hotmail.com','',NULL,'2012-09-19',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Eric_Kirby','Eric_Kirby','eric_kirby@hotmail.es','',NULL,'2012-08-12',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Erick','Erick','erick_seifert@hotmail.com','',NULL,'2012-08-10',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Ernesto-Ramirez','Ernesto Ramírez','ernestrv@yahoo.es','',NULL,'2012-07-30',1,3,0,1,'',1,'1.50','1.50','0.00','','','','...Cuando leemos, nuestro conocimiento del mundo y nuestra mente siempre se expanden, como las estrellas en el universo','','img/users/Ernesto-Ramirez.jpg',''),
 ('Esaud','Esaud','abravo0005@gmail.com','',NULL,'2012-11-04',0,0,0,1,'',1,'0.00','0.00','0.00','','https://www.facebook.com/alexander.b.garcia','','','','img/users/Esaud.jpg',''),
 ('Escribidor-Humberto','Escribidor Humberto','vivebienysefeliz@hotmail.es','',NULL,'2012-11-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Espepe','Espepe','espepe@hotmail.com','',NULL,'2012-09-23',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Evaristo-A-Ujueta-A','Evaristo A Ujueta A','evaristoujueta@hotmail.com','',NULL,'2012-11-16',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Evodio-Perez-Castro','Evodio Pérez Castro','evodioperezcastro@hotmail.com','',NULL,'2012-10-27',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('FGMota','F.G.Mota','francisco.g.mota@gmail.com','',NULL,'2013-01-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Facu-Russi','Facu Russi','larba103@gmail.com','',NULL,'2012-11-10',0,0,0,1,'',1,'0.00','0.00','0.00','','http://larba103@gmail.com','','','','img/users/Facu-Russi.jpg',''),
 ('Fajardo','Fajardo','leoterraneo@gmail.com','',NULL,'2012-11-30',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Fdragone','Fdragone','fdragonne@yahoo.com','',NULL,'2013-01-02',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Felicidad','Felicidad','antonieta_mm@yahoo.com','',NULL,'2012-11-29',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Fernad123','Fernad123','creditrapidos@gmail.com','',NULL,'2012-12-14',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Fernando','Fernando','ferferpal@hotmail.com','',NULL,'2012-11-09',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Fernando-Marttell-C','Fernando Marttell C.','poescrit@live.cl','',NULL,'2012-12-15',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Fernando-Romero-S','Fernando Romero S','carlos.15401@hotmail.com','',NULL,'2013-01-08',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Fernando-Roque','Fernando Roque','arcanus.viritis@gmail.com','',NULL,'2012-08-26',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Fernando-Salvador','Fernando Salvador','vueloaltoazul@gmail.com','',NULL,'2012-12-03',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Fernando-Salvador.jpg',''),
 ('Feysalud','Feysalud','feysalud@gmail.com','',NULL,'2013-01-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('FinanciaPrestito','Financia.Prestito','financia.prestito28@gmail.com','',NULL,'2012-12-11',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Fjpago','Fjpago','fjpago@hotmail.com','',NULL,'2012-12-10',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Foreverjamas','Foreverjamas','forever.jamas@outlook.com','',NULL,'2012-09-06',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Francis','Francis','fsolis@sima88.com.pa','',NULL,'2012-12-12',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Francisco-Benitez','Francisco Benitez','salem8250@hotmail.com','',NULL,'2012-12-30',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Frank-Losa-Aguila','Frank Losa Águila','editor1@spicm.cfg.sld.cu','',NULL,'2012-07-31',1,1,0,1,'',1,'0.50','0.50','0.00','','','','...No necesitas mentir. La magia fluye de ti.','...Hola amigos de peopleebooks. Sean bienvenidos a vivir una aventura con \"El heredero de Merlín\" el primero de una zaga, que continuará con \"La era de los colmillos\", aún en preparación. Qué lo disfruten.','img/users/Frank-Losa-Aguila.jpg',''),
 ('Franklinlizano','Franklinlizano','franklinlizano7@hotmail.com','',NULL,'2012-12-06',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Fray-J-Granado-M','Fray J. Granado M.','fray.granado@gmail.com','',NULL,'2012-12-06',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Freddy-Marcial','Freddy Marcial','freddymarcial@gmail.com','',NULL,'2013-01-08',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Fredymido','Fredymido','fredymido@gmail.com','',NULL,'2012-12-13',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Fvacasg','Fvacasg','fvacas_gonzales@hotmail.com','',NULL,'2012-11-19',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Gabriel','Gabriel','gabriel_gallardo_lozano@yahoo.es','',NULL,'2012-08-20',0,0,2,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Gabriel-Faet','Gabriel Faet','gfalcober@gmail.com','',NULL,'2012-07-31',0,0,6,1,'',1,'0.00','0.00','0.00','','','','El mundo entero se aparta cuando ve pasar a un hombre que sabe hacia dónde va. (Antoine de Saint-Exupéry)','Al fin, desconectado del futuro. A paso brillante me dirijo al próximo capítulo sabiendo esta vez que, cuando se oculte la luna y el sol nos embelese con su perversa luz, todo habrá acabado. Hasta entonces, exploraré mi propio hogar...','img/users/Gabriel-Faet.jpg',''),
 ('Gabrieldavalos','Gabrieldavalos','gabriel.td@hotmail.com','',NULL,'2012-08-06',0,0,0,1,'',1,'0.00','0.00','0.00','http://gabdava.wordpress.com','http://www.facebook.com/gabriel.davalos2','','','Hola, me presento soy Gabriel Dávalos, espero que esto que escribo sea de su agrado, se aceptan abucheos y aplausos. ','img/users/Gabrieldavalos.jpg',''),
 ('Galicamilo','Galicamilo','evildark1939@gmail.com','',NULL,'2012-08-13',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Gammaleuka','Gammaleuka','gammaleuka@gmail.com','',NULL,'2012-08-19',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Gaviota-Viajera','Gaviota Viajera','anitaperu21@gmail.com','',NULL,'2012-11-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Gelito','Gelito','bonikun@gmail.com','',NULL,'2012-07-31',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Gelito.jpg',''),
 ('Gema-Heras','Gema Heras','sylviacr@hotmail.es','',NULL,'2012-08-02',1,2,0,1,'',1,'1.00','1.00','0.00','http://lapiramidedelasletras.blogspot.com.es/','','https://twitter.com/gemaherascruz','','Escritora de vocación. Nació en Madrid en 1975 y ha publicado recientemente su primera novela \"La pirámide de luz\"\n\nEstudió Máster en Dirección y Gestión de Recursos Humanos y Protocolo Institucional, aunque su gran pasión siempre ha sido la escritura.\n\nEnamorada del antiguo Egipto desde su más tierna infancia, descubrió todo un mundo nuevo de la mano de su escritor favorito, el egiptólogo Christian Jacqs, cuyos libros han sido la fuente de inspiración para decidir escribir su primera novela','img/users/Gema-Heras.jpg',''),
 ('Gemam38','Gemam38','gemam38@hotmail.com','',NULL,'2012-08-03',0,0,2,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('German-Albo-Lastra','German Albo Lastra','german.lastra@hotmail.com','',NULL,'2012-11-24',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/German-Albo-Lastra.jpg',''),
 ('German-Sanchez','Germán Sánchez','ballack_dr.house@hotmail.com','',NULL,'2012-10-30',1,0,0,1,'',1,'0.00','0.00','0.00','','','','\"Un buen libro es aquel que se abre con expectación y se cierra con provecho\"\n                                                                                          ','Germán Sánchez (es decir, yo) es al igual que la mayoría de ustedes, un amante de las letras. Ve en ellas el unico cofre en el que se pueden guardar, crear y conservar los mas lindos y preciados tesoros. Eso es la literatura. Es un escritor joven, entusiasta, vanguardista...que trata y seguirá tratando, de llevar la literatura a un siguiente paso. Rompiendo esquemas, creando nuevos textos, nuevas tipografías, nuevos contextos...en si...creando nueva literatura. Si les llama la atención de por qué hablo de mi en Tercera persona...bueno, me encantaría que si alguna vez muero (que de seguro ocurrira, no aún, pero ocurrirá) alguien diga eso de mi.','',''),
 ('Germanfueyo','Germanfueyo','german@fueyo.com','',NULL,'2012-08-03',0,0,0,1,'',1,'0.00','0.00','0.00','http://fue.io/GFu3y0','','','Lupus est homo homini, non homo, quom qualis sit non novit.','A veces me siento perro, a veces me siento león, a veces soy buena gente, a veces soy un cabrón.','img/users/Germanfueyo.jpg',''),
 ('Giselacarolina','Giselacarolina','parapsicologacarol@hotmail.com','',NULL,'2012-12-12',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Giselacarolinatv','Giselacarolinatv','tarotgiselatv@gmail.com','',NULL,'2012-12-23',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Giselita','Giselita','gisselita_nyuu.19@hotmail.com','',NULL,'2012-12-22',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Gmedrano','Gmedrano','medrano.digital@gmail.com','',NULL,'2012-10-27',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Gmedrano.jpg',''),
 ('Grimorio-Cronequiur','Grimorio Cronequiur','business.pereyra@gmail.com','',NULL,'2012-10-13',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Guereyda','Guereyda','guereyda@hotmail.com','',NULL,'2012-08-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Gustavo-Cano','Gustavo Cano','rolandogustavocano@gmail.com','',NULL,'2012-12-18',0,0,0,1,'',1,'0.00','0.00','0.00','http://www.guscano.blogspot.com','','','','','',''),
 ('Gustavo-Piquero','Gustavo Piquero','gustavopiquero@hotmail.com','',NULL,'2012-12-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Hector-Awazacko','Hector Awazacko','hectorawazackoreyes@hotmail.com','',NULL,'2012-08-02',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Helena','Helena','haillons@gmail.com','',NULL,'2012-11-19',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Heriberto','Heriberto','heribertotellez430@hotmail.com','',NULL,'2012-11-26',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Hojadeamor','Hojadeamor','hurtado@hojadeamor.com','',NULL,'2012-08-14',0,0,0,1,'',1,'0.00','0.00','0.00','http://www.hojadeamor.com','','','','','img/users/Hojadeamor.jpg',''),
 ('Hospa','Hospa','hostiliano24@hotmail.com','',NULL,'2012-10-15',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Hrgeesemount','Hrgeesemount','hugo.roig@yahoo.com','',NULL,'2012-09-21',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Igor-Rodtem','Igor Rodtem','rodtem@gmail.com','',NULL,'2012-08-15',0,0,0,1,'',1,'0.00','0.00','0.00','http://rodtem.blogspot.com.es/','http://www.facebook.com/Rodtem','http://twitter.com/IgorRodtem','\"Que no está muerto lo que yace eternamente, y con el paso de los eones, incluso la Muerte puede morir\". (H.P. Lovecraft)','Escribo sobre lo que me da miedo, me fascina o me emociona. Cuento mentiras que se vuelven realidad al empaparse con tinta. A veces, las ideas me obsesionan. Otras veces, las obsesiones me dan ideas. Mi nombre es Igor Rodtem y, cada 11 de junio, renazco de mis cenizas en Bilbao.','img/users/Igor-Rodtem.jpg',''),
 ('Ihelped12','Ihelped12','globalcapitals1@aol.com','',NULL,'2012-11-20',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Ilusion4','Ilusion4','una_ilusion@hotmail.es','',NULL,'2012-10-02',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Imagina-Yo','Imagina-Yo','amidamaru_13@hotmail.com','',NULL,'2012-09-24',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Imawolfe','Imawolfe','amatxu@hotmail.es','',NULL,'2012-11-04',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','Desde pequeña he soñado con mundos que no existían y siempre terminaba en plasmarlos en algún relato.\nDe esa pasión de niñez nació mi pasión por la literatura. Soy devoradora de libros de manera casi compulsiva y, entre libro y libro, dejó volar mi imaginación creando mis propias fantasías.\nEspero que mis libros os hagan volar a mundos y vidas diferentes donde todo es posible...','img/users/Imawolfe.jpg',''),
 ('Inma-Perez','Inma Pérez','inmaperezortola@ymail.com','',NULL,'2012-12-10',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Inma-Perez.jpg',''),
 ('Ionzaga','Ionzaga','ionzaga@yahoo.es','',NULL,'2012-08-16',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Isabo','Isabo','idelaflor06@gmail.com','',NULL,'2012-08-09',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Iseth','Iseth','isethbiel@gmail.com','',NULL,'2012-09-23',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Iseth1','Iseth1','vicortalv@msn.com','',NULL,'2012-12-23',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Ishtar89','Ishtar89','sarag1989@gmail.com','',NULL,'2012-11-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Isidora','Isidora','isianriquez_22@hotmail.com','',NULL,'2012-09-28',0,0,0,1,'',1,'0.00','0.00','0.00','','','','\"nunca te arrepientas de lo que alguna vez te hizo sonreír\" \n\"vivimos una sociedad que en vez de trabajar el espíritu, trabaja las manos\"','','',''),
 ('Jahirob','Jahirob','jahirob@gmail.com','',NULL,'2012-12-01',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jaime-E-Villa-Mejia','Jaime E, Villa Mejia','jvillamejia94@gmail.com','',NULL,'2012-11-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Janmarco','Janmarco','ingeniero__@live.com','',NULL,'2012-10-28',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jantog','Jantog','aries14_93@hotmail.com','',NULL,'2012-09-14',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jason','Jason','bjason128@gmail.com','',NULL,'2012-10-06',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Javier-Beirute','Javier Beirute','javier.beirute@gmail.com','',NULL,'2012-08-07',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Javier-Garcia-Wk','Javier García Wk','javier.garcia.wk@gmail.com','',NULL,'2012-08-06',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Javiernavarro','Javiernavarro','j_r_s_212216@hotmail.com','',NULL,'2012-12-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jbarriga','Jbarriga','josebarriga@entelnet.bo','',NULL,'2012-10-29',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jcmaldonado','Jcmaldonado','jcmr72@yahoo.es','',NULL,'2012-08-02',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','Médico y Máster en Farmacoepidemiología. Desde 1998 trabajando en investigaciones biomédicas, con particular énfasis en los campos de la Farmacología Clínica, Farmacoepidemiología y Uso adecuado de medicamentos. Docente universitario para pregrado y postgrado en la Universidad Central del Ecuador, con actividades en las asignaturas de Farmacología e Investigación Científica. Investigador asociado del Centro de Biomedicina - UCE. Consultor científico-técnico en medicamentos e investigaciones clínicas. Más de 40 publicaciones científicas (artículos y capítulos de libros), varias ponencias de investigaciones en eventos científicos e informes por consultoría. Desde 1998 miembro activo del Drug Utilization Research Group ? Latin America y desde el 2008 colaborador de la Red Iberoamericana de Farmacogenética.\n\n\nAlgunas obras, principalmente no biomédicas, se prevé publicarlas próximamente por este medio.','img/users/Jcmaldonado.jpg',''),
 ('Jeav10','Jeav10','jorarismendi@gmail.com','',NULL,'2012-08-04',1,0,0,1,'',1,'0.00','0.00','0.00','http://climaxlit@blogspot.com','','','\"...Poesía eres tú...\" Rubén Darío','Hace tiempo que me refugio en las escrituras, intentando quizá ser un escritor mientras hablo con las páginas','',''),
 ('Jefferson-Moreno','Jefferson Moreno','boris_pelotero.100@hotmail.com','',NULL,'2012-12-01',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jessy_Arenas','Jessy_Arenas','jessyrock_30@hotmail.com','',NULL,'2012-11-24',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jesus-De-Castro','Jesús De Castro','sthendal-1966@hotmail.com','',NULL,'2012-09-13',0,0,0,1,'',1,'0.00','0.00','0.00','','','','...La soledad es la virtud que acompaña la excelencia del alma','...Me gusta escribir desde siempre, me atrevo a escribir desde hace algunos años, he publicado algunas obras y creo que soy adicto a la vida. En cuanto a mi talento como escritor mejor juzga por ti mismo despues de leerme.','img/users/Jesus-De-Castro.jpg',''),
 ('Jesus-Fernandez','Jesús Fernández','jesusfernandezayas@gmail.com','',NULL,'2012-07-30',1,3,0,1,'',1,'1.50','1.50','0.00','http://www.youtube.com/user/archimaldito','','','Muchas personas han sido educadas para no hablar con la boca llena, pero pocas para no hacerlo con la cabeza vacía. (ORSON WELLES)','No voy a decir que escribo desde siempre, porque no es verdad. \nSí puedo decir que leo desde siempre, y este amor por la lectura, y por extensión, por la Literatura, me llevó, un buen día, a atreverme a dejar mis pensamientos, primero, y mis sueños, después, sobre innumerables cuadernos y hojas de papel en blanco.\nCrecí leyendo ciencia ficción y, aunque ahora mis gustos son más variados, empecé escribiendo microrrelatos, cuentos y novelas cortas de este género narrativo. \nMe complace presentarme ante ustedes con dos novelas cortas, la primera que escribí hace ya algún tiempo, y la última, ambas de una ciencia ficción poco ortodoxa.\nEspero que disfruten con su lectura tanto como yo disfruté escribiéndolas.\n','img/users/Jesus-Fernandez.jpg',''),
 ('Jesusmanuel','Jesusmanuel','jm251995@hotmail.com','',NULL,'2012-11-09',1,0,0,1,'',1,'0.00','0.00','0.00','','','','Haz el bien sin mirar a quien.','Yo soy tu amigo escritor Jesús Manuel Hernández Sastré.','img/users/Jesusmanuel.jpg',''),
 ('Jesusmrr','Jesusmrr','jesusmrr@hotmail.com','',NULL,'2012-12-21',0,0,0,1,'',1,'0.00','0.00','0.00','','','','\"Las cosas más importantes son siempre las mas difíciles de contar...\" Stephen King.','Espero y aspiro poder formar parte de esta web tan dinámica, si te entretiene el genero del Horror soy la persona para darte ese gusto. Ojala pueda llegar a sus corazones y entrar en sus mentes para poder llenarlas de placer y entretenimiento hasta el punto en que revienten y cubran con una sangre espesa la pared mas lejana de la habitación.','',''),
 ('Jesusvr','Jesusvr','jesus_villanueva_ramirez@hotmail.com','',NULL,'2012-11-14',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jleiva','Jleiva','juanleivamarquez@gmail.com','',NULL,'2012-07-31',0,0,0,1,'',1,'0.00','0.00','0.00','http://www.12code.es','','','','','',''),
 ('Jmdeben','Jmdeben','josemanuel.deben@gmail.com','',NULL,'2012-08-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jmelcue','Jmelcue','jmelcue56@hotmail.com','',NULL,'2012-11-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Joan-Abello','Joan Abello','joan.abello@gmail.com','',NULL,'2012-09-09',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Joan-Abello.jpg',''),
 ('Joangelitoxicomano','Joangelitoxicomano','joangelitoxicomano@hotmail.es','',NULL,'2012-11-27',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Joao','Joao','tayson-xsiempre@hotmail.com','',NULL,'2012-12-25',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Joaquin-Dholdan','Joaquín Dholdan','joadoldan@gmail.com','',NULL,'2012-10-25',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Joaquin-Dholdan.jpg',''),
 ('Joaquin_Pereira','Joaquin_Pereira','tallerdejoaquinpereira@gmail.com','',NULL,'2012-08-21',1,0,1,1,'',1,'0.00','0.00','0.00','http://tallerdejoaquinpereira.ning.com/','https://www.facebook.com/MiPrimerOvni','https://twitter.com/Joaquin_Pereira','','Escritor - Periodista - Fotógrafo','img/users/Joaquin_Pereira.jpg',''),
 ('Joaquincaniete','Joaquincaniete','joaquincaniete@gmail.com','',NULL,'2012-12-17',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Joelfortunato','Joelfortunato','joelfortunato@yahoo.com.mx','',NULL,'2012-12-12',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Johandiaz95','Johandiaz95','johandiaz625@gmail.com','',NULL,'2012-09-29',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Johnny-Rengifo','Johnny Rengifo','elultimoagujero@gmail.com','',NULL,'2012-11-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('JokiRomero','Joki.Romero','joki.romero@gmail.com','',NULL,'2012-10-01',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jonathan','Jonathan','jonathanestebansanchez@gmail.com','',NULL,'2012-08-24',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jorge-Bericat','Jorge Bericat','jorgebericat@hotmail.com','',NULL,'2012-08-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jorge-Garibaldi','Jorge Garibaldi','jorgegaribaldig@hotmail.com','',NULL,'2012-08-30',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jorge-Sotto-Mayor','Jorge Sotto Mayor','jorgesottomayor36@hotmail.com','',NULL,'2012-11-29',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jorgescritor','Jorgescritor','jorgekiss.inventarios@gmail.com','',NULL,'2012-08-20',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Jorgescritor.jpg',''),
 ('Jose','José','miguel2103_5@hotmail.com','',NULL,'2012-12-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jose-Antonio','Jose Antonio','profetasyapostoles@hotmail.com','',NULL,'2012-12-18',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jose-Carrera','José Carrera','joerace@gmail.com','',NULL,'2012-09-12',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jose-Fuenmayor','Jose Fuenmayor','jocefuenmayor@hotmail.com','',NULL,'2012-10-25',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jose-J-Bejaranis','Jose J. Bejaranis','jotabejaqu@yahoo.es','',NULL,'2012-07-30',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Jose-J-Bejaranis.jpg',''),
 ('Jose-Luis-Urbano','Jose Luis Urbano','j.luis.urbano@gmail.com','',NULL,'2012-12-25',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Josemaria','Josemaria','josemaria@morales-vazquez.com','',NULL,'2012-08-19',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Joshua-Mdq','Joshua Mdq','laion277@hotmail.com','',NULL,'2012-10-27',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jr-Marklin','Jr Marklin','jrmarklin@gmail.com','',NULL,'2013-01-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Juan','Juan','juannorte123@gmail.com','',NULL,'2012-12-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Juan-C-Munoz-E','Juan C. Muñoz E.','jjccmunoz@gmail.com','',NULL,'2012-11-09',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Juan-Deschamps','Juan Deschamps','juandediosdeschamps@hotmail.com','',NULL,'2012-09-10',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Juan-Freiz','Juan Freiz','jlagos@colegioingles.org','',NULL,'2012-10-02',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Juan-Munoz','Juan Muñoz','judas652000@yahoo.com','',NULL,'2013-01-17',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Juan-Munoz.jpg',''),
 ('Juan_Gonzalez','Juan_Gonzalez','juan2005gg@hotmail.com','',NULL,'2012-11-30',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Juana-Rivera-Bolados','Juana Rivera Bolados','jrivera.bolados@gmail.com','',NULL,'2012-11-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Juanadm','Juanadm','juanadm@yahoo.es','',NULL,'2012-08-08',0,0,2,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Juanan-Zea','Juanan Zea','juan.zea.herranz@gmail.com','',NULL,'2012-08-01',0,0,0,1,'',1,'0.00','0.00','0.00','','','','Convencerte de que es imposible que tú cambies el mundo es el mayor triunfo de aquellos que no quieren que cambie.','','img/users/Juanan-Zea.jpg',''),
 ('Juankarloscf','Juankarloscf','juankarloscf@hotmail.com','',NULL,'2012-08-19',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Juantricell','Juantricell','umbrellacorp_marty@hotmail.com','',NULL,'2012-08-01',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Jukka','Jukka','jukkapalacios@hotmail.com','',NULL,'2012-12-12',0,0,0,1,'',1,'0.00','0.00','0.00','','http://www.facebook.com/jukka.palacios?ref=tn_tnmn','','El primer paso es siempre el mas difícil...','Bueno soy escritor amateur y estaría honrado de recibir sus criticas...','img/users/Jukka.jpg',''),
 ('Juli-Moreira','Juli Moreira','julimoreira_7@hotmail.com','',NULL,'2012-09-24',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Juliana001','Juliana001','julianakolb@aol.fr','',NULL,'2012-12-16',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Juliette','Juliette','julias02@hotmail.com','',NULL,'2012-11-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Junior','Junior','junior238335@gmail.com','',NULL,'2012-11-30',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Junji','Junji','macolloca@gmail.com','',NULL,'2012-08-19',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Kami','Kami','kamichurasan@hotmail.com','',NULL,'2012-12-09',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Karina-Quinones','Karina Quinones','kquinonescsesur@gmail.com','',NULL,'2012-11-08',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Kelevra','Kelevra','keyo_007@hotmail.com','',NULL,'2012-12-11',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Kenna','Kenna','c.quesadamorales@gmail.com','',NULL,'2012-08-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Kenna.jpg',''),
 ('Kenni-Bolanos','Kenni Bolaños','clubdelacultura@hotmail.com','',NULL,'2012-07-30',1,1,0,1,'',1,'0.50','0.50','0.00','','','','','','img/users/Kenni-Bolanos.jpg',''),
 ('Kevin','Kevin','krivas-1405@hotmail.com','',NULL,'2012-11-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Kevin60082829','Kevin60082829','kevin_fuentes98@hotmail.com','',NULL,'2012-12-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Kevinsanlukas','Kevinsanlukas','kag-2014@hotmail.com','',NULL,'2013-01-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Ksanchez','Ksanchez','karinasancz@gmail.com','',NULL,'2012-10-31',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('La-Clave-Espiritual','La Clave Espiritual','contacto@dioslaclaveespiritual.com','',NULL,'2012-08-03',0,0,0,1,'',1,'0.00','0.00','0.00','http://www.Dioslaclaveespiritual.com','https://www.facebook.com/Fundacioncuatrosoles','','Fe en Dios','No hay camino más alto que la directa relación con Dios','img/users/La-Clave-Espiritual.jpg',''),
 ('Laramaceda','Laramaceda','lara_maceda@hotmail.com','',NULL,'2012-08-19',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Larxs','Larxs','5tornado@myway.com','',NULL,'2012-10-28',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Laura-Peralta','Laura Peralta','laskulisueltas_@hotmail.com','',NULL,'2012-08-22',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','...bueno ...hola me presento soy laura de Rio Negro Argentina tengo 17 voy para los 18 y bueno espero que les guste mis novelas por que ¡amo escribir ! es una de mis pasiones bueno besitos y saludos\n','img/users/Laura-Peralta.jpg',''),
 ('Laureanosecundo','Laureanosecundo','laureanosecundo@ibest.com.br','',NULL,'2012-10-19',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Leo-Antonelli','Leo Antonelli','daxlife@hotmail.com','',NULL,'2012-11-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Leo-De-Quevedo','Leo De Quevedo','prometoe007@hotmail.com','',NULL,'2012-08-12',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Leonardo-Avello','Leonardo Avello','recuerdosyalgomas@gmail.com','',NULL,'2012-09-26',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','...Hay en mi una necesidad imperiosa de escribir. Y quiero que tu lo entiendas.\n','img/users/Leonardo-Avello.jpg',''),
 ('Leonor-Sampelayoruiz','Leonor Sampelayoruiz','sampelayoruiz@hotmail.com','',NULL,'2012-12-26',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Ley-Del-Artista','Ley Del Artista','leydelartista@bolivia.com','',NULL,'2012-12-19',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Lilithazure','Lilithazure','apocalipsys8_1@hotmail.com','',NULL,'2012-11-14',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Literalyaritza','Literalyaritza','irreal_1992@hotmail.com','',NULL,'2012-12-26',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Lluis-Carrera','Lluís Carrera','lluiskr@hotmail.com','',NULL,'2012-12-04',1,0,0,1,'',1,'0.00','0.00','0.00','https://www.facebook.com/MiracleALInfern?ref=hl','https://www.facebook.com/lluiskr','','','','img/users/Lluis-Carrera.jpg',''),
 ('Lmedinac','Lmedinac','oriana1979@hotmail.com','',NULL,'2012-10-16',0,0,0,1,'',1,'0.00','0.00','0.00','','','','Vive cada día como si fuera el último...algún día acertarás!','Quiero compartir todas estas historias con alguien más que mis neuronas!','',''),
 ('Locueloman','Locueloman','locueloman@hotmail.com','',NULL,'2012-08-01',0,0,2,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Loli','Loli','lolasantamaria8@yahoo.com.ar','',NULL,'2012-09-11',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Lolisan','Lolisan','lolimarsan@gmail.com','',NULL,'2012-11-30',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Lolitasi','Lolitasi','lolasantamaria08@yahoo.com.ar','',NULL,'2012-09-11',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Lorenochka','Lorenochka','lomepa@gmail.com','',NULL,'2012-08-06',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Lsoto1953','Lsoto1953','lsoto1953@yahoo.com','',NULL,'2012-12-20',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Ltm-Decoraciones','Ltm. Decoraciones','ltm_servicios@hotmail.com','',NULL,'2012-11-15',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Lualewis','Lualewis','luanidia@hotmail.com','',NULL,'2012-09-30',0,0,0,1,'',1,'0.00','0.00','0.00','','','https://twitter.com/lua_lewis','Beeing brave is like a rose its bigger and red when you grow','Dancer, Prancer, Loon','img/users/Lualewis.jpg',''),
 ('Lucas-Julian','Lucas Julian','davidmallyguerra@gmail.com','',NULL,'2012-11-14',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Lucia','Lucia','rataflaca9@hotmail.com','',NULL,'2012-08-01',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Lucilia-Ribeiro','Lucília Ribeiro','luciliabrribeiro@gmail.com','',NULL,'2012-08-18',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Luis-Arroyo-Perez','Luis Arroyo Perez','luisatilioarroyo@yahoo.com.ar','',NULL,'2012-11-18',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Luis-Benavides','Luis Benavides','luismkt@latinmail.com','',NULL,'2012-11-24',0,0,0,1,'',1,'0.00','0.00','0.00','','','','Hasta esos momentos tan especiales se vuelven nada, porque no compartimos nuestros almas','Hola soy Luis','',''),
 ('LuisRoman','Luis.Roman','eyder_xipe_totec@hotmail.com','',NULL,'2012-12-20',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Luis_Sahe','Luis_Sahe','luis_sahe@hotmail.com','',NULL,'2012-08-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Luiscabezos','Luiscabezos','luis_cabezos@hotmail.com','',NULL,'2012-08-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Luiscabezos.jpg',''),
 ('Luisricardo','Luisricardo','arevalo777@gmail.com','',NULL,'2012-09-22',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Luli','Luli','espinos@adinet.com.uy','',NULL,'2012-09-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Luzdalila1974','Luzdalila1974','luz_entusojos@hotmail.com','',NULL,'2012-11-10',1,0,0,1,'',1,'0.00','0.00','0.00','http://www.especialestodos.jimbo.com','','','...cuando ya creía que nada tenia sentido, una luz me mostró el camino....','...la vida me enseño que cada día es el más importante de tu vida, ríe y se feliz esa es la clave...','img/users/Luzdalila1974.jpg',''),
 ('Luzma','Luzma','luznaveda@gmail.com','',NULL,'2012-12-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Mafe-Sabatini','Mafe Sabatini','mafesabatini@hotmail.com','',NULL,'2012-12-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Maheluz','Maheluz','maheluz@hotmail.com','',NULL,'2012-10-12',0,0,2,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Malu','Malu','mispoemasehistorias@gmail.com','',NULL,'2012-09-08',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Manuel-Gomez','Manuel Gómez','manuel.goberman@gmail.com','',NULL,'2012-10-25',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Manuel-Ibarra','Manuel Ibarra','manibarra@hotmail.com','',NULL,'2012-12-09',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Manuel-Montenegro-R','Manuel Montenegro R.','periodistamontenegro@gmail.com','',NULL,'2012-11-16',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','','');
INSERT INTO `db`.`users` VALUES  ('Manuel-Silva','Manuel Silva','alegresilva@sapo.pt','',NULL,'2012-09-09',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Manuel3','Manuel3','dasilvamanuel3@hotmail.fr','',NULL,'2012-12-06',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Manuelalbaprieto','Manuelalbaprieto','manuel.alba.prieto@hotmail.com','',NULL,'2012-11-09',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Manuelrebollo','Manuelrebollo','manuelrebollo@yahoo.es','',NULL,'2012-09-18',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Manuelrebollo.jpg',''),
 ('Manuzon','Manuzon','manuzon_666_3@hotmail.com','',NULL,'2012-11-21',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Marco-Ramos','Marco Ramos','marco_ramos1@hotmail.com','',NULL,'2012-11-23',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Marcos-Urbina','Marcos Urbina','marcosescri@hotmail.com','',NULL,'2012-11-06',0,0,0,1,'',1,'0.00','0.00','0.00','http://www.facebook.com/PoestaMarcosUrbina?ref=hl','http://www.facebook.com/PoestaMarcosUrbina?ref=hl','','','Una persona tranquila, amigable, emprendedora, trabajadora','img/users/Marcos-Urbina.jpg',''),
 ('Maria-Fiore','María Fiore','dcvmariadelrosariofiore@gmail.com','',NULL,'2012-11-12',1,0,0,1,'',1,'0.00','0.00','0.00','','https://www.facebook.com/RoFioreTorres','https://twitter.com/rofioretorres','','MARÍA DEL ROSARIO FIORE, es platense. Egresó como Diseñadora en Comunicación Visual en la Facultad de Bellas Artes de la Universidad Nacional de La Plata.\nSe considera una comunicadora visual que intenta una búsqueda de expresión en la palabra. Las metáforas visuales se transformaron en idioma y así encontró en sus inicios con la poesía su mejor lugar creativo.\nEditó en 2007 ?Versos Imaginarios? un libro objeto que combina poesía, arte digital y una partitura de tango.','img/users/Maria-Fiore.jpg',''),
 ('Maria-Jesus','María Jesús.','alcobermj@hotmail.com','',NULL,'2012-07-31',0,0,2,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Maria-Jesus.jpg',''),
 ('Maria-Jose-Morai','Maria Jose Morai','luzvioleta999@hotmail.com','',NULL,'2012-09-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Mariano','Mariano','marianocofre@hotmail.cl','',NULL,'2012-10-21',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Mariela-Andrea','Mariela Andrea','marielaandreagonzalez@hotmail.com','',NULL,'2012-08-20',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Mariluz-Lozano-Gago','Mariluz Lozano Gago','marateneapalas@yahoo.es','',NULL,'2012-08-16',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Mario-Valencia','Mario Valencia','mvalehe@yahoo.com.mx','',NULL,'2012-07-30',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Mario-Valencia.jpg',''),
 ('Marisol-Escribano','Marisol Escribano','marisol_princesa@hotmail.com','',NULL,'2012-07-30',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Marisol-Escribano.jpg',''),
 ('Martillo','Martillo','martrix24@hotmail.com','',NULL,'2012-08-06',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Martin','Martin','martinsoriapintor@gmail.com','',NULL,'2012-09-21',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Martin-Rae','Martin Rae','nvgmlla@yahoo.es','',NULL,'2012-10-20',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Mary-Carmen','Mary Carmen','arfasay@gmail.com','',NULL,'2012-11-04',1,1,0,1,'',1,'0.50','0.50','0.00','http://marycarmenredondo.jimbo.com','','','','','',''),
 ('Matilde-Pelaez','Matilde Pelaez','fundacrister@hotmail.com','',NULL,'2012-10-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Max-Power','Max Power','marmen1971@hotmail.com','',NULL,'2012-11-13',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Maximiliano-Martin','Maximiliano Martín','maximartin2011@hotmail.com.ar','',NULL,'2012-08-25',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Maxus800','Maxus800','phamtomganon@hotmail.com','',NULL,'2012-12-16',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Maxus800.jpg',''),
 ('Mdorao','Mdorao','martadorao@hotmail.com','',NULL,'2012-11-15',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Melanie','Melanie','estergutierrez591@gmail.com','',NULL,'2012-11-16',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Mendezz','Mendezz','mendezzcarlos@gmail.com','',NULL,'2012-08-13',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Mensnudum','Mensnudum','mensnudum@gmail.com','',NULL,'2012-08-09',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Mensnudum.jpg',''),
 ('Mich77','Mich77','danitojessita@hotmail.com','',NULL,'2012-11-23',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Miguel-Alvarez','Miguel Alvarez','ajedrecistanovato@gmail.com','',NULL,'2012-08-03',0,0,0,1,'',1,'0.00','0.00','0.00','http://laalacenademiguel.blogspot.com.es/','https://www.facebook.com/miguel.alvarezmorales','https://twitter.com/MiguelAl1982','','Hola, me llamo Miguel, hace poco he publicado mediante la editorial Irish Press Internacional un relato corto juvenil y nada, quiero ver más opciones posibles de publicación...','',''),
 ('Miguel-Castro','Miguel Castro','miguel.castro@avina.net','',NULL,'2012-10-28',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Miguel-Gutierrez','Miguel Gutiérrez','nocter@live.com','',NULL,'2012-08-14',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Miguel-Marroquin','Miguel Marroquin','miguel.marroquin.mm@gmail.com','',NULL,'2012-10-08',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Miguel-Pinto','Miguel Pinto','miguelpintosalvatierra@hotmail.com','',NULL,'2012-12-31',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Miguel-Torres-E','Miguel Torres E.','lauromigueltorresencalada@gmail.com','',NULL,'2012-08-04',1,0,0,1,'',1,'0.00','0.00','0.00','','http://lauromiguel.torresencalada','','','','img/users/Miguel-Torres-E.jpg',''),
 ('Miguell-E-Valdivia','Miguell E. Valdivia','micky0018@hotmail.com','',NULL,'2012-07-30',1,0,0,1,'',1,'0.00','0.00','0.00','','http://www.facebook.com/migueleduardovc','http://twitter.com/migUe_pe/','','','img/users/Miguell-E-Valdivia.jpg',''),
 ('Miguelramirez','Miguelramirez','comunicaciones@worldlegalcorp.com','',NULL,'2012-12-02',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Mikel','Mikel','mikelestefania@yahoo.es','',NULL,'2013-01-17',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Milad-Jilo','Milad Jilo','milad.jilo@hotmail.com','',NULL,'2012-12-19',0,0,0,1,'',1,'0.00','0.00','0.00','','http://www.facebook.com/milad.jilo','','...','Emprendedor de grandes aspiraciones, virtuoso desde muchos puntos de vista, libre-pensador, racionalista y objetivista, amante de la redacción, con ganas de iniciarse en el mundo de la literatura, soy sin dudas el claro ejemplo de la perseverancia, condeno las perezas humanas, aspirante a estudiar medicina y filosofía, respetuoso, critico y analítico, en resumen y a grandes rasgos ese soy yo, Milad Jilou.','img/users/Milad-Jilo.jpg',''),
 ('Millylennon','Millylennon','milly.yeey@yahoo.com','',NULL,'2012-10-02',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Mio-Bresni','Mio Bresni','berolouniletras@aveviajera.org','',NULL,'2012-11-12',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Miriamse','Miriamse','doll.dllmrdr@gmail.com','',NULL,'2013-01-30',0,0,0,1,'',1,'0.00','0.00','0.00','http://miriamsiescribe.blogspot.com.es/','','','\"No puedes hacer frente al mundo si no eres quien realmente eres.\"','...Nacida en Madrid, España el 17 de Octubre de 1991. Si enumerara sus sueños desde pequeña todos están relacionados con libros, quería tener una librería, luego una editorial y finalmente escribir sus propios libros. Desde que tiene memoria le gusta mucho las culturas Asiáticas, el manga, el anime y todo lo relacionado a ellas, tradiciones, lugares, etc. A trabajado como blogger y redactora freelance. También le gusta dibujar, escuchar música y  leer. Los autores que más la han influenciado son Edgar  Allan Poe, Anthony Burgges, George Orwell, Andrzej Sapkowski y Kafka, entre otros. Tiene varios escritos gratuitos publicados en Internet y un blog donde publica criticas de libros y manga, opiniones sobre concursos y habla sobre como publicar un libro. ','img/users/Miriamse.jpg',''),
 ('Moka','Moka','unhdl.t@gmail.com','',NULL,'2012-12-08',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Mometolo-Alejandro','Mometolo Alejandro','almotre-20@hotmail.com','',NULL,'2012-11-30',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Monicgordon','Monicgordon','monicga@gmail.com','',NULL,'2012-09-24',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Morislandro','Morislandro','morislandro@gmail.com','',NULL,'2012-12-14',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Mprin12','Mprin12','marin.pierre12@yahoo.com','',NULL,'2012-11-13',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('MtcaInfo','Mtca.Info','mtca.info@gmail.com','',NULL,'2012-12-21',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Mvcrendija','Mvcrendija','mvasquez.carmona@gmail.com','',NULL,'2012-10-31',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Natalia','Natalia','dorisbel@ryaguajay.icrt.cu','',NULL,'2012-11-13',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Nefi-Reyes','Nefi Reyes','nefireyes96@hotmail.com','',NULL,'2012-12-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Nelcolosho','Nelcolosho','renderos.nelson@outlook.com','',NULL,'2012-08-02',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Neo3504','Neo3504','neo3504@yahoo.com','',NULL,'2012-12-01',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Neptuno','Neptuno','otzi777@hotmail.com','',NULL,'2012-11-25',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Neriant-Ramos','Neriant Ramos','dexter416@gmail.com','',NULL,'2012-12-22',0,0,0,1,'',1,'0.00','0.00','0.00','','http://facebook.com/neriant.ramos','','...\"No podemos ser nada sin jugar a serlo.\" Jean Paul Sartre','','img/users/Neriant-Ramos.jpg',''),
 ('Nestor-R-Ruiz-M','Néstor R. Ruiz. M','ruizmatiauda@gmail.com','',NULL,'2012-11-15',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Ninoska-Davila','Ninoska Davila','ndavila14@gmail.com','',NULL,'2012-11-30',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Noctis-Caelum','Noctis Caelum','nocturno_cielo@hotmail.com','',NULL,'2013-01-08',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Noe-Puigdengolas','Noe Puigdengolas','noe.puig3@gmail.com','',NULL,'2013-01-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Noelp','Noelp','noelp@ccb.uclv.edu.cu','',NULL,'2012-12-01',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Nono-Espinar','Nono Espinar','nonoech@gmail.com','',NULL,'2012-07-31',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Nono-Espinar.jpg',''),
 ('Ny1109','Ny1109','hf@antevasin.com','',NULL,'2012-09-10',0,0,2,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Olga157','Olga157','kitsia2000-@hotmail.com','',NULL,'2012-11-25',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Omar-Gomez','Omar Gomez','gomezomar13@hotmail.com','',NULL,'2012-08-20',1,0,0,1,'',1,'0.00','0.00','0.00','','https://www.facebook.com/omarguitarracomp','','Milord, me apenaría haberlos solamente divertido, yo quisiera hacerlos mejores...\nSir George Frederick Haendel','...Músico, Compositor, Escritor, Pedagogo, Padre de familia, Esposo, y otros oficios','img/users/Omar-Gomez.jpg',''),
 ('Omar-Omara','Omar Omara','lex145@hotmail.com','',NULL,'2012-09-21',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Omar1980','Omar1980','scornedang@hotmail.com','',NULL,'2012-08-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Omargom13','Omargom13','gomezomar13@operamail.com','',NULL,'2012-08-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Onel-Jose-Gonzalez','Onel Jose Gonzalez','onelcaballerog@gmail.com','',NULL,'2012-12-18',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Osavan','Osavan','osavan39@hotmail.com','',NULL,'2012-11-13',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Oscar','Oscar','teorios09@gmail.com','',NULL,'2012-10-26',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Oscar-Juarez','Oscar Juárez','oscar_hjh@hotmail.com','',NULL,'2012-12-29',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Oscar-Peraza','Oscar Peraza','oscarperaza@hotmail.com','',NULL,'2012-10-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Osvaldo','Osvaldo','osvaldowsp@live.cl','',NULL,'2012-11-30',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Otaku-Juan','Otaku Juan','juan93pablodm@hotmail.com','',NULL,'2012-11-02',0,0,0,1,'',1,'0.00','0.00','0.00','','http://www.facebook.com/jpduran3?ref=tn_tnmn','','...la vida es como la arcilla, que la puedes moldear como quieras, el exito solo depende de que seas un buen artista.','...','img/users/Otaku-Juan.jpg',''),
 ('Otto-Ortega','Otto Ortega','ottomvs@hotmail.com','',NULL,'2012-08-01',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Pablodelai','Pablodelai','poreldespertar@gmail.com','',NULL,'2012-08-21',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Pakinen','Pakinen','muuu_85@hotmail.com','',NULL,'2012-08-02',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Panzertruppen','Panzertruppen','okh1939@gmail.com','',NULL,'2012-11-17',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Paolacardenasmercado','Paolacardenasmercado','paolacardenasmercado@gmail.com','',NULL,'2012-08-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Paolomaquerni','Paolomaquerni','nico.vs@hotmail.com.ar','',NULL,'2012-09-29',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Patricia-Escarbais','Patricia Escarbais','pattescarbais@hotmail.com','',NULL,'2012-10-26',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Patricio63Rojas','Patricio63Rojas','patricio63rojas@gmail.com','',NULL,'2012-12-20',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Pattychiba','Pattychiba','silvita1986@hotmail.com','',NULL,'2012-10-30',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Pattychiba.jpg',''),
 ('Paumaneiro','Paumaneiro','paumaneiro@gmail.com','',NULL,'2012-10-08',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Pcarballosa','Pcarballosa','pedrocarballosam@gmail.com','',NULL,'2012-12-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Pepecomunidades','Pepecomunidades','pepemanager@gmail.com','',NULL,'2012-09-16',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Perezj','Perezj','perezj@lipesa.com','',NULL,'2012-11-30',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Poetayanes','Poetayanes','poetayanes@hotmail.com','',NULL,'2012-11-22',1,0,0,1,'',1,'0.00','0.00','0.00','http://www.poesiagt.com','https://www.facebook.com/poetayanes','https://twitter.com/poetayanes','','LIBERTAD\n¿Dime dónde estás?\n¿Te quedaste en la imaginación?\n¿Eres tan sólo una ilusión?\nO quizás un disfraz\n\nMucho se habla de ti,\nLos filósofos te cuestionan,\nLos juristas te traicionan\nY yo quiero creer en ti.\n\n¿Qué eres en verdad?\nDe pronto una fantasía,\nAlgo más que utopía\nO simple vanidad.\n\nHacer uso de ti quiero,\nQue importa la cordura,\nSi el alma no tiene cura,\nEn un ser prisionero.\n\nA quienes han bebido de tu fuente,\nLa sociedad ha querido postergar,\nCondenando todo de pecado,\nSin embargo han sabido triunfar.','img/users/Poetayanes.jpg',''),
 ('Polloman','Polloman','criescace@hotmail.com','',NULL,'2012-08-09',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Pomelapocha','Pomelapocha','pomelapocha1@gmail.com','',NULL,'2012-11-14',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Pookie','Pookie','johanna_dominguez@hotmail.es','',NULL,'2012-11-20',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Profe_Creador','Profe_Creador','deleyade.creador@gmail.com','',NULL,'2012-12-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Radhames-Ramos-Pena','Radhames Ramos Peña','radhamesramos23@hotmail.com','',NULL,'2012-11-27',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Rafael','Rafael','rgb@cubarte.cult.cu','',NULL,'2012-12-27',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Rafael-Casanova','Rafael Casanova','rafael.casanova.fuertes@gmail.com','',NULL,'2012-12-06',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Rafael-Josue-Baez','Rafael Josue Baez','rafaeljosue_basilisco@hotmail.com','',NULL,'2012-11-25',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Rafael-Lindem','Rafael Lindem','lindem75@hotmail.com','',NULL,'2012-08-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Rafael-Mendez','Rafael Mendez','erbealles@gmail.com','',NULL,'2012-11-19',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Ramon-Chavez','Ramon Chavez','ramon.chavez@prodigy.net.mx','',NULL,'2012-09-16',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Ransett','Ransett','ransett.tv@hotmail.com','',NULL,'2012-10-01',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Raul','Raul','raulbonachia@hotmail.com','',NULL,'2012-11-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Raul-Nurena','Raul Nureña','raul_ndc@hotmail.com','',NULL,'2012-11-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Raulcardillo','Raulcardillo','raul.cardillo@gmail.com','',NULL,'2012-09-02',0,0,0,1,'',1,'0.00','0.00','0.00','http://giallotrip.blogspot.com','http://www.facebook.com/raulcardillo','','Toda vida es un proceso de demolición.','Quiero que conozcan mi obra.','img/users/Raulcardillo.jpg',''),
 ('Rendan','Rendan','fernandousal@hotmail.com','',NULL,'2012-12-31',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Ricardo','Ricardo','ricardo.valdiviezo@hotmail.com','',NULL,'2012-12-17',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Ricardo-SA','Ricardo S.A.','ricardosalgadoarias@yahoo.es','',NULL,'2012-10-14',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Rickycorti','Rickycorti','clubpuertochic@gmail.com','',NULL,'2012-09-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Rigo-Mozart','Rigo Mozart','mozartrigo@gmail.com','',NULL,'2012-11-15',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Rigores','Rigores','rigores1507@hotmail.com','',NULL,'2012-11-22',1,2,0,1,'',1,'1.00','1.00','0.00','http://www.loscuentos.com','','','\"Quisiera conocer lo que guarda el mundo en sus entrañas, conocer el desarrollo de las fuerzas vivas y dejar para siempre el juego de las palabras vacías\" \n\nJ.W.Goethe','...Bienvenidos a mi página, que como ven todavía esta en construcción, mientras aprendo a llenar todos los recursos que amablemente me ofrece peopleebooks.\n\nCualquier detalle, sugerencia, crítica y hasta alabanza, jejeje, me la pueden dejar acá o en mi mail: rigores1507@hotmail.com estaré muy atento a su opinión.\n\nUn abrazo\n\nRigores','img/users/Rigores.jpg',''),
 ('Rigores1507','Rigores1507','santaellar@lipesa.com','',NULL,'2012-11-22',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Rmilanes','Rmilanes','milanesbalsalobre@hotmail.es','',NULL,'2012-08-11',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Robe-Ferrer','Robe Ferrer','robe_spv@wanadoo.es','',NULL,'2012-08-17',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Robe-Ferrer.jpg',''),
 ('Roberto-Rios-Reyes','Roberto Rios Reyes','rhrr08@hotmail.com','',NULL,'2012-12-15',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Roberto01','Roberto01','robertodubois002@gmail.com','',NULL,'2012-11-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Robertorafael','Robertorafael','rramirezm@estudiantes.uci.cu','',NULL,'2012-10-08',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('RodolfoBlanquillo','Rodolfo.Blanquillo','r.blanquillo@hotmail.com','',NULL,'2012-12-06',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Rodrigo222','Rodrigo222','rodrigo_lujan99@hotmail.com','',NULL,'2012-12-24',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Roger','Roger','r9lio99@gmail.com','',NULL,'2013-01-02',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Ronald-Harrison-Qp','Ronald Harrison Qp','ronaldh.qp@gmail.com','',NULL,'2013-01-08',0,0,0,1,'',1,'0.00','0.00','0.00','','','','...','','img/users/Ronald-Harrison-Qp.jpg',''),
 ('Ronald-Urquizo','Ronald Urquizo','raurquizo@gmail.com','',NULL,'2012-08-19',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Roy-Lima','Roy Lima','rlcrodrigo0@hotmail.com','',NULL,'2012-10-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Rpalacio','Rpalacio','ruben_p@hotmail.com','',NULL,'2012-08-19',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Rrobertdux','Rrobertdux','roberttauro@latinmail.com','',NULL,'2012-11-10',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Rrrr','Rrrr','rrrodriguez05@yahoo.com.mx','',NULL,'2012-10-27',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Rubenoide','Rubenoide','rubenalistevidal@gmail.com','',NULL,'2012-09-11',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Rubizul','Rubizul','el_rincon_de_eva@hotmail.com','',NULL,'2012-08-04',1,0,0,1,'',1,'0.00','0.00','0.00','http://escribiresmiformadevivir.blogspot.com.es','https://www.facebook.com/RUBIZUL76','','Carpe Diem','Soy Eva María Maisanava Trobo, vivo en Pozuelo de Alarcón, tengo 36 años y desde hace algún tiempo tengo la necesidad de escribir, de plasmar el día a día de mi vida y de la gente que me rodea. \n\nSí con ello consigo que la gente sea feliz leyendo lo que escribo mucho mejor. \n\nQuizás éste sea el principio de muchos más poemas, más relatos, más sueños que plasmar y más sentimientos que compartir con toda la gente que quiera seguir leyéndome. \n\nGracias por apreciar la lectura y así mantener vuestra alma viva. \n\nUn saludo \n\nEva María \n','img/users/Rubizul.jpg',''),
 ('S0Met','S0Met','carlos.somet@yahoo.com','',NULL,'2012-11-01',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Saloduenas','Saloduenas','salo.duenas@hotmail.com','',NULL,'2012-08-23',0,0,0,1,'',1,'0.00','0.00','0.00','','https://www.facebook.com/SaloDuenaz?ref=tn_tnmn','https://twitter.com/Salo_Duenas','Cuando dejes de hablar, y comiences a leer, entenderás la sabiduría del silencio. ?','Soy Salomé Dueñas, me encanta leer, y estoy iniciándome en el campo de la escritura literaria. ','img/users/Saloduenas.jpg',''),
 ('Sammy01','Sammy01','incltd4@yahoo.com','',NULL,'2012-11-14',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Samrivas','Samrivas','samrivas14@gmail.com','',NULL,'2012-11-22',1,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Samrivas.jpg',''),
 ('Samuel-David','Samuel David','samif64@hotmail.com','',NULL,'2012-10-30',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Samuel-Elias-B','Samuel Elias B.','tu.friend-forever@hotmail.com','',NULL,'2012-09-22',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Samuel-Elias-B.jpg',''),
 ('Sandra','Sandra','sandrawandemberg@hotmail.com','',NULL,'2012-11-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Sandra-Rodriguez','Sandra Rodriguez','srrnlove@hotmail.com','',NULL,'2012-12-09',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Sandra74','Sandra74','sdrasmne@yahoo.com.ar','',NULL,'2012-12-01',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Santaella','Santaella','rigobertos1@cantv.net','',NULL,'2012-11-26',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Santiago-E-Torres','Santiago E. Torres','santiago_tr_sucks@hotmail.com','',NULL,'2012-10-29',0,0,0,1,'',1,'0.00','0.00','0.00','','http://www.facebook.com/santiago.t.ruiz16','https://twitter.com/SantiagooSucks','Si quieres ser feliz, como me dices, no analices, muchacho, no analices. \nJoaquín Bartrina (1850-1880) Poeta español. ','','img/users/Santiago-E-Torres.jpg',''),
 ('Sara-Duval','Sara Duval','spadilla2908@hotmail.com','',NULL,'2012-12-12',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Sarai','Sarai','miris_yo@hotmail.com','',NULL,'2012-12-21',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Scarlett','Scarlett','criz.nwh_9208@hotmail.com','',NULL,'2012-08-23',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Seba_Anuch5','Seba_Anuch5','seba_anuch5@hotmail.com','',NULL,'2012-08-19',0,0,0,1,'',1,'0.00','0.00','0.00','','https://www.facebook.com/seba.anuch','https://twitter.com/seba_anuch','','','img/users/Seba_Anuch5.jpg',''),
 ('Seka21','Seka21','am21@podmod.eu','',NULL,'2012-12-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Serafin-Gimeno','Serafín Gimeno','serafi_gimeno@yahoo.es','',NULL,'2012-07-29',1,2,0,1,'',1,'1.00','1.00','0.00','','','','','','img/users/Serafin-Gimeno.jpg',''),
 ('Sergio-Dasilva','Sergio Dasilva','sergiodasilva263@yahoo.es','',NULL,'2012-11-12',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Silescribe','Silescribe','silje07@gmail.com','',NULL,'2012-11-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Sofia-Del-Peru','Sofía Del Perú','corinarimondi@hotmail.com','',NULL,'2012-11-02',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Sofisilan','Sofisilan','sofisilan@gmail.com','',NULL,'2012-08-13',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Solmarialuz','Solmarialuz','mergongross1@yahoo.com','',NULL,'2012-11-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Solselene','Solselene','selene2sol@hotmail.com','',NULL,'2012-12-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Sonia28','Sonia28','taki_sonia@hotmail.com','',NULL,'2012-11-09',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','...hola soy sony una chica muy callada en algunas ocasiones,muy confiada en todo lo q me dicen en entrar en confianza muy seguido pero asi me va tambien con un marido y 3 hermosos hijos','img/users/Sonia28.jpg',''),
 ('Stormtrooper71126','Stormtrooper71126','stormtrooper71126@hotmail.com','',NULL,'2012-11-26',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Stormtrooper71126.jpg',''),
 ('Tamil','Tamil','tamilmorgan@hotmail.com','',NULL,'2012-12-15',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Tania-Puertas','Tania Puertas','universopresente@gmail.com','',NULL,'2012-08-26',1,1,0,1,'',1,'0.50','0.50','0.00','http://taniapuertasgonzalez.wordpress.com','http://www.facebook.com/taniapuertas','https://twitter.com/luzpresente','...En tiempos de crisis es mas importante la imaginación que el saber...','','img/users/Tania-Puertas.jpg',''),
 ('Tanitaek','Tanitaek','tanitaek@hotmail.com','',NULL,'2012-10-29',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Tayiel','Tayiel','jchristian318@gmail.com','',NULL,'2012-11-29',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Tayiel.jpg',''),
 ('Templar-Puerto-Rico','Templar Puerto Rico','cwodjs@gmail.com','',NULL,'2012-08-18',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Tentets-Alex','Tentets Alex','tentetsalex1993@yahoo.es','',NULL,'2012-11-08',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Tonnydau','Tonnydau','tonnydau@yahoo.com','',NULL,'2012-10-26',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Tremel','Tremel','tremel.marjorie@gmail.com','',NULL,'2012-12-23',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Ueliton-RPereira','Ueliton R.Pereira','tom.pereira@hotmail.com','',NULL,'2012-11-28',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Umar-Faruk-Al-Sadik','Umar Faruk Al Sadik','archivodelsol@yahoo.es','',NULL,'2012-10-24',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Valerymr','Valerymr','vale_mar_r@hotmail.es','',NULL,'2012-11-07',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Vanessa','Vanessa','vanessalumo@hotmail.com','',NULL,'2012-08-03',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Vanessajoses','Vanessajoses','vanessajoses@yahoo.com','',NULL,'2012-11-03',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Verano-Brisas','Verano Brisas','veranobrisas@gmail.com','',NULL,'2012-12-01',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Veronica','Veronica','verito.quezada.h@gmail.com','',NULL,'2012-12-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Viajeschina','Viajeschina','vacacionchina@hotmail.com','',NULL,'2012-12-23',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Viasoluciones','Viasoluciones','viasoluciones@outlook.com','',NULL,'2012-10-31',0,0,0,1,'',1,'0.00','0.00','0.00','','http://www.facebook.com/viasoluciones','https://twitter.com/viasoluciones','Si se puede imaginar, se puede crear\nAdelante ... !!!','Profesional en Computacion e Informatica de la San Ignacio de Loyola\n\nEmprendedor de día, desarrollador de noche, dedicado a gestionar y vincular proyectos por Internet. Apasionado de la ciencia, tecnología, arte, mecatronica, electrónica, programación, actualmente investigando sobre Diseño de Experiencias del Usuario, es firme creyente de la filosofía Jobs y de la importancia de la diminución de la brecha digital en el País, trabaja con tecnología en Plataformas Web, Desktop y móviles IOS/Android/Windows Phone/Firefox OS. Participa en iniciativas para fomentar la invención e innovación.','img/users/Viasoluciones.jpg',''),
 ('Victor-De-Domingo','Víctor De Domingo','leonitas_2505@hotmail.com','',NULL,'2012-08-27',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Vroc','Vroc','veraemlille@hotmail.com','',NULL,'2013-01-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Wachter002','Wachter002','wachterbruno6@gmail.com','',NULL,'2012-12-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Wallp','Wallp','walterpascual@gmail.com','',NULL,'2012-08-23',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Walter-Delgado','Walter Delgado','instrenacer@yahoo.com','',NULL,'2012-11-14',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Walter-Quispe-Castro','Walter Quispe Castro','walqcas@yahoo.es','',NULL,'2012-10-04',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Workimedes','Workimedes','geral@workimedes.pt','',NULL,'2012-08-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Xabitxo','Xabitxo','romera@euskaltel.net','',NULL,'2012-09-09',0,0,2,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Xavie','Xavie','beaumonxavier@gmail.com','',NULL,'2012-12-22',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Xlvx','Xlvx','alkbus16@hotmail.com','',NULL,'2012-10-31',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Yader','Yader','rayj_2@hotmail.com','',NULL,'2012-11-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Yanmarcosjbc','Yanmarcosjbc','eltriunfador05@hotmail.com','',NULL,'2012-09-25',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Yazbek-Heredia-Ojeda','Yazbek Heredia Ojeda','yazbek.14@hotmail.com','',NULL,'2012-11-23',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Yolanda-Palomares','Yolanda Palomares','ypmarcos@gmail.com','',NULL,'2012-07-31',0,0,1,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Yolanda-Palomares.jpg',''),
 ('Yulicame','Yulicame','yuleidyscaraballo@hotmail.com','',NULL,'2012-11-02',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Zarquis','Zarquis','zarquisb22@gmail.com','',NULL,'2012-11-05',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Zenonita','Zenonita','lujanenroma@gmail.com','',NULL,'2012-11-16',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','',''),
 ('Zeronightmares','Zeronightmares','zeronightmares@gmail.com','',NULL,'2012-07-29',0,0,0,1,'',1,'0.00','0.00','0.00','','','','','','img/users/Zeronightmares.jpg','');
UNLOCK TABLES;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


--
-- Definition of table `db`.`withdrawals`
--

DROP TABLE IF EXISTS `db`.`withdrawals`;
CREATE TABLE  `db`.`withdrawals` (
  `operation_id` int(11) NOT NULL AUTO_INCREMENT,
  `operation_date` date NOT NULL,
  `user_id` varchar(55) NOT NULL,
  `amount` decimal(64,2) NOT NULL,
  `payment_account` varchar(75) NOT NULL,
  `hashes` varchar(320) NOT NULL,
  PRIMARY KEY (`operation_id`),
  KEY `withdrawals_fbfc09f1` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db`.`withdrawals`
--

/*!40000 ALTER TABLE `withdrawals` DISABLE KEYS */;
LOCK TABLES `withdrawals` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `withdrawals` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
