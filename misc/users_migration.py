# coding: utf-8

import pymssql, unicodedata, string, MySQLdb
from MySQLdb import IntegrityError


# Turn usernames into 'urlizable' strings (for usernames)
def urlizer(string):
    remove = ['.', ',', ':', ';', '?', '!', ' ', '"']
    ascii_string = unicodedata.normalize('NFKD', string.replace(' ', '-')).encode('ascii', 'ignore')
    for i in remove:
        if i in ascii_string:
            ascii_string = ascii_string.replace(i, '')
    return ascii_string


    
#-------------------------------------------------------------------------------

conn = pymssql.connect(host='188.93.75.80', user='sa', password='p0rn0ll4m4rt3r0dr1g0.', database='Web', as_dict=True)
cur = conn.cursor()

cur.execute('SELECT * FROM dbo.Usuarios WHERE Activo=1 ORDER BY Usuario')
#-------------------------------------------------------------------------------
# Get current dB server users' data and process to match new requirements:
#-------------------------------------------------------------------------------
users = [{}]
for item in cur:
    new_row = {}
    new_row[u'user'] = urlizer(unicode(item[9], 'iso-8859-1').strip().title())
    new_row[u'full_name'] = unicode(item[9], 'iso-8859-1').strip().title()
    new_row[u'passwd'] = unicode(item[10], 'iso-8859-1')
    new_row[u'signup_date'] = unicode(item[11], 'iso-8859-1')
    if (item[23] > '0001-01-01'): new_row[u'is_author'] = 1
    else: new_row[u'is_author'] = 0
    new_row[u'sales'] = 0
    new_row[u'purchases'] = 0
    new_row[u'total_profits'] = 0
    new_row[u'money_withdrawn'] = 0
    #new_row[u'address'] = 
    #new_row[u'city'] = 
    new_row[u'current_balance'] = 0
    new_row[u'first_name'] = ''
    new_row[u'last_name'] = ''
    new_row[u'last_login'] = '0001-01-01'
    if item[8] == '6ea0bbee': new_row[u'video'] = ''
    else: new_row[u'video'] = 'vid/users/' + new_row['user'] + '.webm'
    if item[7] != None: new_row[u'photo'] = 'img/users/' + new_row['user'] + '.jpg'
    else: new_row[u'photo'] = ''
    new_row[u'wants_mail_notifications'] = 1 
    new_row[u'hashes'] = ''
    new_row[u'account_active'] = 1
    new_row[u'email'] = item[4].lower()
    if item[5] != None and item[5] != 'NULLVal' and item[5] != '':
        if item[5].startswith('http'):
            new_row[u'website'] = unicode(item[5], 'iso-8859-1')
        else: new_row[u'website'] = 'http://' + unicode(item[5], 'iso-8859-1')
    else: new_row[u'website'] = u''
    
    if item[6] != None and item[6] != 'NULLVal' and item[6] != '': 
        if item[6].startswith('http'):
            new_row[u'facebook'] = unicode(item[6], 'iso-8859-1')
        else: new_row[u'facebook'] = 'http://' + unicode(item[6], 'iso-8859-1')
    else: new_row[u'facebook'] = u''
        
    if item[20] != None and item[20] != 'NULLVal' and item[20] != '': 
        if item[20].startswith('http'):
            new_row[u'twitter'] = unicode(item[20], 'iso-8859-1')
        else: new_row[u'twitter'] = 'http://' + unicode(item[20], 'iso-8859-1')
    else: new_row[u'twitter'] = u''    
    
    if item[21] != 'NULLVal': 
        if item[21].isupper():
            new_row[u'quote'] = unicode(item[21].capitalize(), 'iso-8859-1')
        else:
            new_row[u'quote'] = unicode(item[21], 'iso-8859-1')
    else: new_row[u'quote'] = u''
    if item[22] != 'NULLVal':
        if item[22].isupper():
            new_row[u'presentation'] = unicode(item[22].capitalize(), 'iso-8859-1')
        else:
            new_row[u'presentation'] = unicode(item[22], 'iso-8859-1')
    else: new_row[u'presentation'] = u''
    
    users.append(new_row)
conn.close()

#-------------------------------------------------------------------------------
# Connect to new dB server to write users' data to:
#-------------------------------------------------------------------------------
conn = MySQLdb.connect(host='127.0.0.1', user='root', passwd='p0rn0ll4m4rt3r0dr1g0.', db='prepro')
cur = conn.cursor()

written = 0
errors = []
for item in users[1:]:
    try:
        # Table 'users':
        query_users = "INSERT INTO users (user, full_name, email, website, facebook, twitter, quote, presentation, passwd, signup_date, is_author, wants_mail_notifications, hashes, account_active, photo, video, current_balance, money_withdrawn, total_profits, purchases, sales) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (item['user'], item['full_name'], item['email'], item['website'], item['facebook'], item['twitter'], item['quote'], item['presentation'], '', item['signup_date'], item['is_author'], item['wants_mail_notifications'], '', item['account_active'], item['photo'], item['video'], item['current_balance'], item['money_withdrawn'], item['total_profits'], item['purchases'], item['sales'])
        cur.execute(query_users)
        # Table 'auth_user' (Django auth framework):
        query_auth_users = "INSERT INTO auth_user (username, email, is_staff, is_active, is_superuser, date_joined, password, last_login, last_name, first_name) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (item['user'], item['email'], 0, 1, 0, item['signup_date'], item['passwd'], item['last_login'], item['last_name'], item['first_name'])
        cur.execute(query_auth_users)
                
        written += 1
    except IntegrityError:
        errors.append(item)
        

conn.commit()
conn.close()

#-------------------------------------------------------------------------------
# Output info:
#-------------------------------------------------------------------------------
print('\n' + unicode(len(users) - 1) + ' registros de entrada')
print(unicode(written) + ' registros de salida\n')
if len(errors): print 'REGISTROS OMITIDOS:'
for item in errors:
    print item, '\n'
print '\n'  

