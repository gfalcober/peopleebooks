# coding: utf-8

import pymssql, unicodedata, string, MySQLdb
from MySQLdb import IntegrityError
from decimal import Decimal


# Turn usernames into 'urlizable' strings (for usernames)
def urlizer(string):
    remove = ['.', ',', ':', ';', '?', '!', ' ', '"']
    ascii_string = unicodedata.normalize('NFKD', string.replace(' ', '-')).encode('ascii', 'ignore')
    for i in remove:
        if i in ascii_string:
            ascii_string = ascii_string.replace(i, '')
    return ascii_string


    
#-------------------------------------------------------------------------------

conn = pymssql.connect(host='188.93.75.80', user='sa', password='p0rn0ll4m4rt3r0dr1g0.', database='Web', as_dict=True)
cur = conn.cursor()

cur.execute('SELECT * FROM dbo.Libros WHERE ACTIVO=1 ORDER BY Titulo')
#-------------------------------------------------------------------------------
# Get current dB server books' data and process to match new requirements:
#-------------------------------------------------------------------------------
books = [{}]
for item in cur:
    new_row = {}
    new_row[u'book'] = urlizer(unicode(item[1], 'iso-8859-1').strip().title())
    new_row[u'title'] = unicode(item[1], 'iso-8859-1').strip().title()
    new_row[u'synopsis'] = unicode(item[2], 'iso-8859-1')
    new_row[u'publication_date'] = unicode(item[8], 'iso-8859-1')
    new_row[u'price'] = Decimal('0.99')
    new_row[u'sales'] = 0
    new_row[u'book_active'] = 1
    if item[14] != None:
        if item[14].startswith('http'):
            new_row[u'website'] = item[14]
        else: new_row[u'website'] = 'http://' + item[14]
    else: new_row[u'website'] = ''   
    new_row[u'user_id'] = ''
    
    books.append(new_row)
conn.close()

#-------------------------------------------------------------------------------
# Connect to new dB server to write users' data to:
#-------------------------------------------------------------------------------
conn = MySQLdb.connect(host='127.0.0.1', user='root', passwd='p0rn0ll4m4rt3r0dr1g0.', db='prepro')
cur = conn.cursor()

written = 0
errors = []
for item in books[1:]:
    try:
        # Table 'users':
        query_books = "INSERT INTO books (book, title, synopsis, website, publication_date, price, sales, book_active, user_id, cover, video, book_file_epub, book_file_pdf) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (item['book'], item['title'], item['synopsis'], item['website'], item['publication_date'], item['price'], item['sales'], item['book_active'], item['user_id'], '', '', '', '')
        cur.execute(query_books)
                
        written += 1
    except IntegrityError:
        errors.append(item)
        

conn.commit()
conn.close()

#-------------------------------------------------------------------------------
# Output info:
#-------------------------------------------------------------------------------
print('\n' + unicode(len(books) - 1) + ' registros de entrada')
print(unicode(written) + ' registros de salida\n')
if len(errors): print 'REGISTROS OMITIDOS:'
for item in errors:
    print item, '\n'
print '\n'  

