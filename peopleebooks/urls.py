# coding: utf-8

from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.contrib.sitemaps import Sitemap
from Main.sitemap import BooksSitemap, UsersSitemap, BlogSitemap
admin.autodiscover()

handler500 = 'Main.views.error500'

sitemaps = {
  'Books': BooksSitemap,
  'Users': UsersSitemap,
  'Blog': BlogSitemap
}

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'peopleebooks.views.home', name='home'),
    # url(r'^peopleebooks/', include('peopleebooks.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'Main.views.main'),                                             # Main page
    url(r'^books/(?P<title>[\w\-]+)/$', 'Main.views.book'),                    # Book page
    url(r'^people/(?P<username>[\w\-]+)/$', 'Main.views.user'),                # User page
    url(r'^publish$', 'Main.views.publish'),                                   # Publishing form
    url(r'^signup$', 'Main.views.signup'),                                     # Sign up page
    url(r'^bookstore$', 'Main.views.bookstore'),                               # Bookstore
    url(r'^search$', 'Main.views.search_results'),                             # Search results page
    url(r'^logout$', 'Main.views.logout_view'),                                # Logout redirect page
    url(r'^inactive-account$', 'Main.views.inactive_account'),                 # Account inactive info page     
    url(r'^activation/(?P<hashes>[\w]+)/$', 'Main.views.activate_account'),    # User activation page
    url(r'^pending/(?P<user>[\w\-]+)/$', 'Main.views.pending'),                # Activation email sent
    url(r'^faq$', 'Main.views.faq'),                                           # FAQs  (Static)
    url(r'^ceo$', 'Main.views.ceo'),                                           # About us  (Static)
    url(r'^about$', 'Main.views.about'),                                       # Our vision  (Static)
    url(r'^legal-terms$', 'Main.views.legal_terms'),                           # Legal terms  (Static)   
    url(r'^contact$', 'Main.views.contact'),                                   # Contact  (Static)
    url(r'^my-password$', 'Main.views.password_change_request'),               # Password change request
    url(r'^password-pending$', 'Main.views.password_pending'),                 # Password change link sent / nonexistent account
    url(r'^password-change/(?P<hashes>[\w]+)/$', 'Main.views.password_change'),# Password change form
    url(r'^password-change-confirmed/(?P<hashes>[\w]+)/$', 'Main.views.password_change_confirmed'),  # Password changed confirmation
    url(r'^home/(?P<username>[\w\-]+)/$', 'Main.views.user_home'),             # User's home                               
    url(r'^signup-error/(?P<error>[\w\.\-]+)$', 'Main.views.signup_error'),      
    url(r'^cart$', 'Main.views.shopping_cart'),                                # Shopping cart
    url(r'^checkout-complete$', 'Main.views.checkout_complete'),
    url(r'^checkout$', 'Main.views.checkout'),                                 # Pre-PayPal page
    #url(r'^download/(?P<link>[\w]+)/$', 'download'),
    url(r'^email-change/(?P<hashes>[\w\-@\.]+)/$', 'Main.views.email_change'),
    url(r'^edit/(?P<book>[\w\-]+)/$', 'Main.views.edit_book'),                     # Edit book
    url(r'^withdrawal/(?P<hashes>[\w]+)/$', 'Main.views.withdrawal'),              # Money withdrawal confirmation page
    url(r'^sales_report/(?P<book_title>[\w\.\-]+)/$', 'Main.views.sales_report'),  # Book sales report
    url(r'^paypal-ipn$', 'Main.views.paypal_ipn'),                                 # PayPal Instant Payment Notification processing.
    url(r'^awaiting-payment$', 'Main.views.awaiting_payment'),                     # Pending payment
    url(r'^news$', 'Main.views.news'),
    url(r'^publishing-services$', 'Main.views.publishing_services'),
    # Blog application URLs:
    url(r'^blog/(?P<article>[\w\.\-]+)/$', 'Blog.views.blog'),
    url(r'^blog/$', 'Blog.views.blog'),
    url(r'^blogger$', 'Blog.views.blogger'),
    url(r'^blog-preview$', 'Blog.views.blog_preview'),
    # ------------------------------
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps})
)

urlpatterns += patterns('',        # These files must be accessible only from the local machine (because of the books) ENFORCED IN APACHE'S 'httpd.conf'
     url(r'^uploaded/(?P<path>.*)$', 'django.views.static.serve', {
     'document_root': settings.MEDIA_ROOT,
     }),
)



